﻿function closeSession() {
    window.location.href = "/Login";
}

$(document).ready(function () {
    $.ajax({
        url: "../Home/GetVisual",
        type: 'POST',
        async: false
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            $('#bannerGral').css('background', msg.visual.Visual_BannerColor);
        }
        else {
            alert(msg.Mensajes);
        }

    });


    
});

function showChat() {
    if ($('.frameChat').hasClass('mostrar')) {
        $('.frameChat').removeClass('mostrar');
    }
    else {
        $('.frameChat').addClass('mostrar');
    }
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})