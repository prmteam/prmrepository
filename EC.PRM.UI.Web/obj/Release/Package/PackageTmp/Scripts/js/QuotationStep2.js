﻿/*
$(window).on('unload', function (event) {
    var origen = event.target.URL;

    if (origen.toString().includes("Quotation/Step3")) {
        $('#cboProducto').val('0');
        $("#tblEquipos tbody tr").remove();
    }
    
});
*/
$(document).ready(function () {

    $('#txtPrecio').mask('$000,000,000,000,000.00', { reverse: true });

    $('#cboFamilia').change(function () {
        var programaSel = $('#cboPrograma').val();

        if (!programaSel || programaSel == 0) {
            bootstrap_alert.warning('Favor de elegir un programa para mostrar los activos disponibles.');
            $(this).prop('selectedIndex', 0);
            $('#cboPrograma').focus();
            return;
        }

        $("#cboFamilia option:selected").each(function () {
            $('#cboProducto').empty();
            $('#cboProducto').append('<option>--- Elige un activo ---');
            $('#txtPrecio').val('');
            $('#txtCantidad').val('');

            var familiaSel = $(this).val();

            $.ajax({
                url: "/Quotation/GetProductByFamily",
                type: 'POST',
                data: { familiaId: familiaSel, programaId: programaSel }
            }).success(function (msg) {
                if (msg.CargadoCorrecto) {

                    $.each(msg.lstProducto, function (id, value) {
                        $('#cboProducto').append('<option value="' + msg.lstProducto[id].AssetId + '">' + msg.lstProducto[id].Asset_Description + '</option>');
                    });
                }
                else {
                    alert(msg.Mensajes);
                }

            });
        })
    });

    $('#cboProducto').change(function () {
        var strProductoId = $(this).val();

        $.ajax({
            url: "/Quotation/GetProductData?productoId=" + strProductoId,
            type: 'POST',
            data: { productoId: strProductoId }
        }).success(function (msg) {
            if (msg.CargadoCorrecto) {
                var precio = msg.productoSelected.Asset_OEC;
                var moneda = msg.productoSelected.Asset_CurrencyId == 1 ? "USD" : "MXN";
                moneda = '(' + moneda + ') $';

                $('#txtPrecio').val( Intl.NumberFormat('es-MX').format(precio));
                $('#txtCantidad').val('');
                $('#lblMoneda').text(moneda);
            }
            else {
                alert(msg.Mensajes);
            }

        });

    });
    
    $('#tblEquipos').on('click', '.delete-row', function () {
        $(this).parent().remove();
    });

    $('#txtCantidad').focusout(function () {
        $(".alert").alert('close');
    });

    $('#cboPrograma').change(function () {
        $(".alert").alert('close');
    });
    
});

function addRow() {
    var sf = document.getElementById("cboFamilia");
    var strFamiliaId = sf.options[sf.selectedIndex].value;
    var sp = document.getElementById("cboProducto");
    var strProductoId = sp.options[sp.selectedIndex].value;
    var strProducto = sp.options[sp.selectedIndex].text;
    var strPrecio = $('#txtPrecio').val();
    var strCantidad = $('#txtCantidad').val();

    if (strCantidad.length < 1 || $('#txtPrecio').val().length < 1 || parseFloat(strPrecio) <= 0 || sp.selectedIndex < 1) {
        bootstrap_alert.warning('Favor de elegir un producto e ingresar costo y cantidad.');
        $('#txtCantidad').focus();
        return;
    }

    $("#tblEquipos tbody tr").each(function (index) {
        var tbCantidad, tbProd, tbPrecio, tbEliminar, tbIdProd;

        $(this).children("td").each(function (index2) {
            switch (index2) {
                case 0:
                    tbCantidad = $(this).text().replace('(', '').replace(')', '').replace(/\s/g, '');
                    break;
                case 1:
                    tbProd = $(this).text();
                    break;
                case 2:
                    tbPrecio = $(this).text().replace(/\s/g, '');
                    break;
                case 3:
                    tbEliminar = $(this).text().replace(/\s/g, '');
                    break;
                case 4:
                    tbIdProd = $(this).text().replace(/\s/g, '');
                    if (tbIdProd) {
                        if (tbIdProd == strProductoId) {
                            $(this).parent().remove();
                            strCantidad = parseInt(strCantidad) + parseInt(tbCantidad);
                        }
                    }
                    break;
            }
        });

    });

    strPrecio = parseInt(strCantidad) * parseFloat(strPrecio.replace(/\,/g, ''));
    strPrecio = strPrecio.toString();

    var markup = "<tr><td width='5%'>(" + strCantidad + ")</td><td width='55%'>" +
        strProducto + "<td width='20%'>" + Intl.NumberFormat('es-MX').format(parseFloat(strPrecio.replace(/\,/g, ''))) +
        "</td><td width='20%' class='delete-row' style='cursor: pointer; color: #7ad98f'>(Eliminar)</td><td class='td_id' style='display:none;'>" +
        strProductoId + "</td></tr>";
    $("#tblEquipos tbody").append(markup);

    $('#td_id').hide();

}

function keepDataStep2(url) {
    //event.preventDefault();
    var numequipos = $("#tblEquipos tbody tr").length - 1;

    if (numequipos < 1) {
        bootstrap_alert.warning('Favor de agregar activos para cotizar.');
        $('#txtCantidad').focus();
        return;
    }

    //var numReg = 0;

    $("#tblEquipos tbody tr").each(function (index) {
        var tbCantidad, tbProd, tbPrecio, tbEliminar, tbIdProd;

        $(this).children("td").each(function (index2) {
            switch (index2) {
                case 0:
                    tbCantidad = $(this).text().replace(/\s/g, '');
                    break;
                case 1:
                    tbProd = $(this).text();
                    break;
                case 2:
                    tbPrecio = $(this).text().replace(/\s/g, '');
                    break;
                case 3:
                    tbEliminar = $(this).text().replace(/\s/g, '');
                    break;
                case 4:
                    tbIdProd = $(this).text().replace(/\s/g, '');
                    break;
            }
            
            //$(this).css("background-color", "#ECF8E0");
            
        });

        if (tbCantidad != 'Equipos') {   
            var strcantidad = tbCantidad.replace('(', '').replace(')', '');
            var strmonto = parseFloat(tbPrecio.replace(/\,/g, ''));
            var strProgramaId = $("#cboPrograma").val();
            //var limpiar = totalReg == numReg ? true : false;

            $.ajax({
                url: url,
                type: 'POST',
                async: false,
                data: { cantidad: strcantidad, productoId: tbIdProd, monto: strmonto, programaId: strProgramaId, numReg: index }
            }).success(function (msg) {
                if (msg.CargadoCorrecto) {
                    //numReg += 1;
                    if (numequipos == index) {
                        window.location.href = "/Quotation/Step3";
                    }
                }
                else {
                    alert(msg.Mensajes);
                }
            });

            
        }
    });
}

function previousStep() {
    window.location = "/Quotation/Step1";
}