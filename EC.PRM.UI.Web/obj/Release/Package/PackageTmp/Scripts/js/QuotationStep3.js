﻿function keepDataStep3(url) {
    var strFinTipoId = $("input[name='cbxFinTipo']:checked").val();
    var strMonedaId = $("input[name='cbxMoneda']:checked").val();
    var strPlazoId = $("input[name='cbxFinPlazo']:checked").val();

    if (!(strFinTipoId) || !(strMonedaId) || !(strPlazoId)) {
        bootstrap_alert.warning('Favor de seleccionar todos los valores para el financiamiento.');
        $('#cbxFinTipo').focus();
        return;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: { finTipoId: strFinTipoId, monedaId: strMonedaId, plazosId: strPlazoId }
    }).success(function (msg) {

        if (msg.CargadoCorrecto) {
            window.location.href = "/Quotation/Step4";
        }
        else {
            alert(msg.Mensajes);
        }
    });
}

function previousStep() {
    window.location = "/Quotation/Step2";
    /*
    $.ajax({
        url: "/Quotation/ReleaseListAsset",
        type: 'POST'
    }).success(function (msg) {
        if (!msg.CargadoCorrecto) {
            alert(msg.Mensajes);
        }
        else {
            window.location = "/Quotation/Step2";
        }
    });
    */
}

function calculatePayment(url) {
    var strFinTipoId = $("input[name='cbxFinTipo']:checked").val();
    var strMonedaId = $("input[name='cbxMoneda']:checked").val();
    var strPlazoId = $("input[name='cbxFinPlazo']:checked").val();
    var segFinanciado = $("input[name='cbxSecFinanciado']:checked").val();
    //var opCompra = $("input[name='cbxPOption']:checked").val();

    if (!(strFinTipoId) || !(strMonedaId) || !(strPlazoId)) {
        bootstrap_alert.warning('Favor de seleccionar todos los valores para el financiamiento.');
        $('#cbxFinTipo').focus();
        return;
    }

    // colectar datos de inputs
    var arrInputs = new Array();

    $("#contentInputs input[type=text]").each(function () {
        var idElement = $(this).attr('id');
        arrInputs.push({ "fieldName": idElement, "fieldValue": $(this).val() });
    });

    var jsonObj = {
        "productoId": strFinTipoId,
        "monedaId": strMonedaId,
        "plazoId": strPlazoId,
        "lstInputs": arrInputs,
        "insFinancied": segFinanciado
        //"purOption": opCompra
    };

    $('#txtFinanciamiento').text('Calculando...');

    $.ajax({
        url: url,
        type: 'POST',
        data: JSON.stringify(jsonObj),
        dataType: 'json',
        contentType: 'application/json'
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            $('#txtFinanciamiento').text('Producto: ' + msg.product + '\nPago: ' + Intl.NumberFormat('es-MX', { style: 'currency', currency: 'MXN' }).format(msg.pagoMensual) + '\nTasa: ' + parseFloat(msg.irr).toFixed(2) + "%\n" + msg.Mensajes);
            if (segFinanciado == "true") {
                //var textCalculos = ' Pago: ' + Intl.NumberFormat('es-MX', { style: 'currency', currency: 'MXN' }).format(msg.pagoMensual) + '\n Interest Rate: ' + parseFloat(msg.irr).toFixed(2) +
                var textCalculos = $('#txtFinanciamiento').text() +
                    '\nPago de seguro: ' + Intl.NumberFormat('es-MX', { style: 'currency', currency: 'MXN' }).format(msg.arrayInsurance[10]);// + ' IRR: ' + msg.irrInsurance;
                $('#txtFinanciamiento').text(textCalculos);// + "\n" + msg.Mensajes);
            }
        }
        else {
            alert(msg.Mensajes);
            $('#txtFinanciamiento').text('Haz click en Calcular');
        }
    });

    if (url.indexOf('TablaAmortizacion') > 0) {
        window.location.href = url;
    }
}

//window.addEventListener("beforeunload", function (event) {
//    event.preventDefault();
//    event.returnValue = 'Se perderan los datos de la cotización, ¿Deseas continuar?';
    //var origen = event.target.URL;

    //if (origen.toString().includes("Quotation/Step3")) {

    //    $.ajax({
    //        url: "/Quotation/ReleaseListAsset",
    //        type: 'POST'
    //    }).success(function (msg) {
    //        if (!msg.CargadoCorrecto) {
    //            alert(msg.Mensajes);
    //            //window.location = "/Quotation/Step1";
    //        }
    //    });
    //}
//});

$(document).ready(function () {

    $('input[type=number]').bind('keyup mouseup', function (e) {
        var min = parseFloat($(this).attr('min'));
        var max = parseFloat($(this).attr('max'));
        var curr = parseFloat($(this).val());
        if (curr > max) { $(this).val(max); var changed = true; }
        if (curr < min) { $(this).val(min); var changed = true; }
        if (changed) {
            return false;
        }
    });
    
    var sldBalPay = document.getElementById("sldBalloonPayment");
    $('#txtBalloonPayment').val(sldBalPay.value);
    sldBalPay.oninput = function () {
        $('#txtBalloonPayment').val(sldBalPay.value);
    }

    var sldPurOpt = document.getElementById("sldPurchaseOption");
    $('#txtPurchaseOption').val(sldPurOpt.value);
    sldPurOpt.oninput = function () {
        $('#txtPurchaseOption').val(sldPurOpt.value);
    }

    var sldOpFee = document.getElementById("sldOpFee");
    $('#txtOpFee').val(sldOpFee.value);
    sldOpFee.oninput = function () {
        $('#txtOpFee').val(sldOpFee.value);
    }

    var sldSecDep = document.getElementById("sldSecurityDeposit");
    $('#txtSecurityDeposit').val(sldSecDep.value);
    sldSecDep.oninput = function () {
        $('#txtSecurityDeposit').val(sldSecDep.value);
    }

    var sldBliDis = document.getElementById("sldBlindDiscount");
    $('#txtBlindDiscount').val(sldBliDis.value);
    sldBliDis.oninput = function () {
        $('#txtBlindDiscount').val(sldBliDis.value);
    }

    var sldRefFee = document.getElementById("sldReferalFee");
    $('#txtReferalFee').val(sldRefFee.value);
    sldRefFee.oninput = function () {
        $('#txtReferalFee').val(sldRefFee.value);
    }

    var sldDelay = document.getElementById("sldDelay");
    $('#txtDelay').val(sldDelay.value);
    sldDelay.oninput = function () {
        $('#txtDelay').val(sldDelay.value);
    }
    var sldGraMon = document.getElementById("sldGraceMonths");
    $('#txtGraceMonths').val(sldGraMon.value);
    sldGraMon.oninput = function () {
        $('#txtGraceMonths').val(sldGraMon.value);
    }
    var sldAdiCos = document.getElementById("sldAdditionalCosts");
    $('#txtAdditionalCosts').val(sldAdiCos.value);
    sldAdiCos.oninput = function () {
        $('#txtAdditionalCosts').val(sldAdiCos.value);
    }
    //$("#txtAdditionalCosts").mask('$000,000,000,000,000.00', { reverse: true });
    var sldAdiCosMXN = document.getElementById("sldAdditionalCostsMXN");
    $('#txtAdditionalCostsMXN').val(sldAdiCosMXN.value);
    sldAdiCosMXN.oninput = function () {
        $('#txtAdditionalCostsMXN').val(sldAdiCosMXN.value);
    }
    //$("#txtAdditionalCostsMXN").mask('$000,000,000,000,000.00', { reverse: true });

    var sldLegFee = document.getElementById("sldLegalFees");
    $('#txtLegalFees').val(sldLegFee.value);
    sldLegFee.oninput = function () {
        $('#txtLegalFees').val(sldLegFee.value);
    }
    //$("#txtLegalFees").mask('$000,000,000,000,000.00', { reverse: true });
    var sldLegFeeMXN = document.getElementById("sldLegalFeesMXN");
    $('#txtLegalFeesMXN').val(sldLegFeeMXN.value);
    sldLegFeeMXN.oninput = function () {
        $('#txtLegalFeesMXN').val(sldLegFeeMXN.value);
    }
    //$("#txtLegalFeesMXN").mask('$000,000,000,000,000.00', { reverse: true });

    var sldDown = document.getElementById("sldDownpayment");
    $('#txtDownpayment').val(sldDown.value);
    sldDown.oninput = function () {
        $('#txtDownpayment').val(sldDown.value);
    }

    var strFinTipoId = $("input[name='cbxFinTipo']:checked").val();
    switch (strFinTipoId) {
        case "1":
            $('#sldBalloonPayment').hide();
            $('#lblBalloonPayment').hide();
            $('#lblsBalloonPayment').hide();
            $('#txtBalloonPayment').hide();

            $('#sldPurchaseOption').hide();
            $('#lblPurchaseOption').hide();
            $('#lblsPurchaseOption').hide();
            $('#txtPurchaseOption').hide();
            break;
        case "2":
            $('#sldBalloonPayment').hide();
            $('#lblBalloonPayment').hide();
            $('#lblsBalloonPayment').hide();
            $('#txtBalloonPayment').hide();

            $('#sldPurchaseOption').show();
            $('#lblPurchaseOption').show();
            $('#lblsPurchaseOption').show();
            $('#txtPurchaseOption').show();
            break;
        case "4":
            $('#sldBalloonPayment').show();
            $('#lblBalloonPayment').show();
            $('#lblsBalloonPayment').show();
            $('#txtBalloonPayment').show();

            $('#sldPurchaseOption').hide();
            $('#lblPurchaseOption').hide();
            $('#lblsPurchaseOption').hide();
            $('#txtPurchaseOption').hide();
            break;
    }
 
    $('input[type=radio][name=cbxFinTipo]').change(function () {

        if (this.value == 1) {
            $('#sldBalloonPayment').hide();
            $('#lblBalloonPayment').hide();
            $('#lblsBalloonPayment').hide();
            $('#txtBalloonPayment').hide();

            $('#sldPurchaseOption').hide();
            $('#lblPurchaseOption').hide();
            $('#lblsPurchaseOption').hide();
            $('#txtPurchaseOption').hide();
        }
        else if (this.value == 2) {
            $('#sldBalloonPayment').hide();
            $('#lblBalloonPayment').hide();
            $('#lblsBalloonPayment').hide();
            $('#txtBalloonPayment').hide();

            $('#sldPurchaseOption').show();
            $('#lblPurchaseOption').show();
            $('#lblsPurchaseOption').show();
            $('#txtPurchaseOption').show();
        }
        else if (this.value == 4) {
            $('#sldBalloonPayment').show();
            $('#lblBalloonPayment').show();
            $('#lblsBalloonPayment').show();
            $('#txtBalloonPayment').show();

            $('#sldPurchaseOption').hide();
            $('#lblPurchaseOption').hide();
            $('#lblsPurchaseOption').hide();
            $('#txtPurchaseOption').hide();
        }
    });

    $('input[type=radio][name=cbxPOption]').change(function () {
        if (this.value == 'false') {
            $('#txtPurchaseOption').val('0');
            $('#sldPurchaseOption').hide();
            $('#lblPurchaseOption').hide();
            $('#lblsPurchaseOption').hide();
            $('#txtPurchaseOption').hide();
        }
        else {
            $('#txtPurchaseOption').val('1');
            $('#sldPurchaseOption').val('1');
            $('#sldPurchaseOption').show();
            $('#lblPurchaseOption').show();
            $('#lblsPurchaseOption').show();
            $('#txtPurchaseOption').show();
        }
    });

    $('input[type=radio][name=cbxMoneda]').change(function () {
        /*
        switch (this.value) {
            case "1":
                $('#rowTxtLegalFeeMXN').hide();
                $('#rowSldLegalFeeMXN').hide();
                $('#rowTxtLegalFee').show();
                $('#rowSldLegalFee').show();

                $('#rowTxtAdditionalCostsMXN').hide();
                $('#rowSldAdditionalCostsMXN').hide();
                $('#rowTxtAdditionalCosts').show();
                $('#rowSldAdditionalCosts').show();
                break;
            case "2":
                $('#rowTxtLegalFeeMXN').show();
                $('#rowSldLegalFeeMXN').show();
                $('#rowTxtLegalFee').hide();
                $('#rowSldLegalFee').hide();

                $('#rowTxtAdditionalCostsMXN').show();
                $('#rowSldAdditionalCostsMXN').show();
                $('#rowTxtAdditionalCosts').hide();
                $('#rowSldAdditionalCosts').hide();
                break;
        }
        */
    });

    var strMonedaId = $("input[name='cbxMoneda']:checked").val();
    switch (strMonedaId) {
        case "1":
            $('#rowTxtAdditionalCostsMXN').hide();
            $('#rowSldAdditionalCostsMXN').hide();
            $('#rowTxtLegalFeeMXN').hide();
            $('#rowSldLegalFeeMXN').hide();

            $('#rowTxtAdditionalCosts').show();
            $('#rowSldAdditionalCosts').show();
            $('#rowTxtLegalFee').show();
            $('#rowSldLegalFee').show();
            break;
        case "2":
            $('#rowTxtAdditionalCosts').hide();
            $('#rowSldAdditionalCosts').hide();
            $('#rowTxtLegalFee').hide();
            $('#rowSldLegalFee').hide();

            $('#rowTxtAdditionalCostsMXN').show();
            $('#rowSldAdditionalCostsMXN').show();
            $('#rowTxtLegalFeeMXN').show();
            $('#rowSldLegalFeeMXN').show();
            break;
    }

    var hideLegalFees = $("#hdLegalFeesHide").val();
    if (hideLegalFees == 1) {
        $('#rowTxtLegalFee').hide();
        $('#rowSldLegalFee').hide();
        $('#rowTxtLegalFeeMXN').hide();
        $('#rowSldLegalFeeMXN').hide();
    }

    var hideAdditionalCosts = $("#hdAdditionalCostsHide").val();
    if (hideAdditionalCosts == 1) {
        $('#rowTxtAdditionalCosts').hide();
        $('#rowSldAdditionalCosts').hide();
        $('#rowTxtAdditionalCostsMXN').hide();
        $('#rowSldAdditionalCostsMXN').hide();
    }
});