﻿$(document).ready(function () {
    $('#tblUser thead tr th:nth-child(10)').hide();
    $('#tblUser tbody tr td:nth-child(10)').hide();
    $('#tblUser thead tr th:nth-child(3)').hide();
    $('#tblUser tbody tr td:nth-child(3)').hide();
    $('#tblUser thead tr th:nth-child(2)').hide();
    $('#tblUser tbody tr td:nth-child(2)').hide();
    $('#tblUser thead tr th:nth-child(1)').hide();
    $('#tblUser tbody tr td:nth-child(1)').hide();

    $('#tblUser tbody tr').click(function () {

        var selected = $(this).hasClass("highlight");
        $("#tblUser tbody tr").removeClass("highlight");
        if (!selected)
            $(this).addClass("highlight");

        var id = $(this).children("td:eq(0)").html();
        var user = $(this).children("td:eq(3)").html();
        var profId = $(this).children("td:eq(1)").html();
        var vendId = $(this).children("td:eq(2)").html();
        var pass = $(this).children("td:eq(9)").children("input:eq(0)").val();
        var nombre = $(this).children("td:eq(6)").html();
        var apellidos = $(this).children("td:eq(7)").html();
        var email = $(this).children("td:eq(8)").html();

        $('#txtUsuarioId').val(id.replace(/\s/g, ''));
        $('#idUser').val(id.replace(/\s/g, ''));
        $('#txtUsuario').val(user.replace(/\s/g, ''));
        $('#cboPerfil').val(profId.replace(/\s/g, ''));
        $('#cboVendor').val(vendId.replace(/\s/g, ''));
        $('#txtPassword').val(pass.replace(/\s/g, ''));
        $('#txtNombreUsuario').val(nombre.replace(/\s/g, ''));
        $('#txtApellidos').val(apellidos.replace(/\s/g, ''));
        $('#txtEmailUsuario').val(email.replace(/\s/g, ''));

        $('#btnActualizar').show();
        $('#btnGuardar').hide();
        $('#divPhoto').show();
    });

    $('#btnActualizar').hide();
    $('#divPhoto').hide();
});

function saveRow(url) {
    var usuario = $('#txtUsuario').val();
    var pass = $('#txtPassword').val();
    var vendorId = $('#cboVendor').val();
    var perfilId = $('#cboPerfil').val();

    var nombre = $('#txtNombreUsuario').val();
    var apellidos = $('#txtApellidos').val();
    var email = $('#txtEmailUsuario').val();

    if (usuario.length <= 0 || pass.length <= 0 || vendorId <= 0 || perfilId <= 0) {
        alert('Se deben ingresar todos los datos del usuario.');
        $('#txtUsuario').focus();
        return;
    }

    var objJson = {
        "User_FirstName": nombre,
        "User_LastName": apellidos,
        "User_User": usuario,
        "User_Password": pass,
        //"[User_ImageUrl]": pathFoto,
        "User_Email": email,
        "User_VendorId": vendorId,
        "User_UserProfileId": perfilId
    };

    $.ajax({
        url: url,
        type: 'POST',
        //data: { user: usuario, pass: pass, vendorId: vendorId, profileId: perfilId }
        data: { userInfo: objJson }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location.href = "/UserProfile/Index";
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function updateRow(url, active) {
    var id = $('#txtUsuarioId').val();
    var usuario = $('#txtUsuario').val();
    var pass = $('#txtPassword').val();
    var vendorId = $('#cboVendor').val();
    var perfilId = $('#cboPerfil').val();

    var nombre = $('#txtNombreUsuario').val();
    var apellidos = $('#txtApellidos').val();
    var email = $('#txtEmailUsuario').val();

    if (usuario.length <= 0 || pass.length <= 0 || vendorId <= 0 || perfilId <= 0) {
        alert('Se deben ingresar todos los datos.');
        $('#txtUsuario').focus();
        return;
    }

    var objJson = {
        "UserId": id,
        "User_FirstName": nombre,
        "User_LastName": apellidos,
        "User_User": usuario,
        "User_Password": pass,
        //"[User_ImageUrl]": pathFoto,
        "User_Email": email,
        "User_VendorId": vendorId,
        "User_UserProfileId": perfilId
    };

    $.ajax({
        url: url,
        type: 'POST',
        //data: {userId: id, user: usuario, pass: pass, vendorId: vendorId, profileId: perfilId, activo: active }
        data: { userInfo: objJson, activo: active }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location.href = "/UserProfile/Index";
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function filterTable(url) {
    var dataSearch = $('#txtBuscar').val();

    window.location.href = url + "?&dsearch=" + dataSearch;
}

function newRow() {
    $('#txtUsuario').val('');
    $('#txtPassword').val('');
    $('#cboPerfil').val('0');
    $('#cboVendor').val('0');
    //$('#cboPrograma').val('0');

    $('#btnActualizar').hide();
    $('#btnGuardar').show();

    $("#tblUser tbody tr").removeClass("highlight");
    $('#txtUsuario').focus();
}

function uploadPhoto(element, url) {
    if ($(element).val() != "") {
        var formElement = new FormData($('#form_upload_documents')[0]);
        formElement.append('idUser', 5);

        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                // progress file upload
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var porcentaje = parseInt((evt.loaded / evt.total) * 100);
                        if (porcentaje == 100) {
                            //$('.status-form').html('Por favor espere, procesando...').show();	
                        } else {
                            //$('.status-form').html('Cargando alumnos: <b>'+porcentaje+'%</b>').show();	
                        }
                        //$load_progress = '<div class="progress progress-striped"> <div class="progress-bar progress-bar-info" style="width: '+porcentaje+'%"></div> </div>';
                        //smart_m('danger',$load_progress,'',i,'danger');
                    }
                }, false);

                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var porcentaje = parseInt((evt.loaded / evt.total) * 100);
                        if (porcentaje == 100) {
                            //$('.status-form').html('Por favor espere, procesando...').show();	
                        } else {
                            //$('.status-form').html('Cargando alumnos: <b>'+porcentaje+'%</b>').show();	
                        }

                        //$load_progress = '<div class="progress progress-striped"> <div class="progress-bar progress-bar-success" style="width: '+porcentaje+'%"></div> </div>';
                        //smart_m('danger',$load_progress,'',i,'danger');
                    }
                }, false);
                return xhr;
            },
            url: url,
            type: 'POST',
            data: formElement,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {

            },
            success: function (dataResponse) {
                dataContent = dataResponse;
            },
            complete: function () {
                console.log(dataContent);

                //$('#form_upload_documents')[0].reset();
                $('#imgUser').attr("src", dataContent.Mensajes);

                //alert('Se importaron correctamente los grados ');
                $('#form_upload_documents')[0].reset();
            }
        });
    }
}