﻿$(document).ready(function () {
    pieChart('../Dashboard/GetConvertionData',
        '#chartConversion',
        'Índice de conversión',
        //'Porcentaje de conversión de cotizaciones.');
        '');
    pieChart('../Dashboard/GetSystemUserData',
        '#chartUso',
        'Uso del sistema',
        //'Porcentaje de uso del sistema.');
        '');
    barChart('../Dashboard/GetEstausData',
        '#chartLead',
        '',
        '');

    // Create jqxTabs.
    $('#tabs').jqxTabs({ width: 730, height: 450, position: 'top', theme: 'fresh' });
    $('#tabs .jqx-tabs-content-element').css("background-color", "#f6f9fc");
    $('#tabs .jqx-tabs-title-container').css("background-color", "#f6f9fc");
    $('#tabs .jqx-tabs-header').css("padding-top", "0px");
    $('#tabs').css("border", "none");
    
    $("#txtFFechaIni").datepicker({
        defaultDate: "0d",
        dateFormat: 'yy/mm/dd',
        showAnim: "slide",
        changeYear: true,
    });
    $("#txtFFechaFin").datepicker({
        defaultDate: "0d",
        dateFormat: 'yy/mm/dd',
        showAnim: "slide",
        changeYear: true,
    });

});

function barChart(url, elemento, titulo, descripcion) {
    var source =
    {
        datatype: "json",
        datafields: [
            { name: 'Texto' },
            { name: 'Valor' }
        ],
        url: url
    };

    var dataAdapter = new $.jqx.dataAdapter(source, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });
    // prepare jqxChart settings
    var settings = {
        title: titulo,
        description: descripcion,
        enableAnimations: true,
        animationDuration: 1500,
        showLegend: false,
        showBorderLine: false,
        padding: { left: 0, top: 10, right: 0, bottom: 0 },
        titlePadding: { left: 0, top: 10, right: 0, bottom: 0 },
        source: dataAdapter,
        colorScheme: 'scheme20',
        backgroundColor: 'rgba(0,0,0,0)',
        
        categoryAxis: {
            dataField: 'Texto',
            displayText:'Estatus',
            showGridLines: false
        },
        seriesGroups:
            [
                {
                    type: 'column',
                    showLabels: true,
                    series:
                        [
                            {
                                verticalAlignment: 'top',
                                dataField: 'Valor',
                                displayText: 'Cotizaciones',
                                formatSettings: { decimalPlaces: 0 }
                            }
                        ]
                }
            ]
    };

    // setup the chart
    $(elemento).jqxChart(settings);

    setTimeout(function () {
        var labels = $("#chartLead .jqx-chart-label-text");
        for (var i = 0; i < dataAdapter.records.length; i++) {
            $("#chartLead .jqx-chart-label-text:eq(" + i + ")").css("font-weight", "bold");
        };
    }, 1600);
}

function pieChart(url, elemento, titulo, descripcion) {
    var source =
    {
        datatype: "json",
        datafields: [
            { name: 'Texto' },
            { name: 'Valor' }
        ],
        url: url
    };

    var dataAdapter = new $.jqx.dataAdapter(source, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });
    // prepare jqxChart settings
    var settings = {
        title: titulo,
        description: descripcion,
        enableAnimations: true,
        animationDuration: 1500,
        showLegend: false,
        showBorderLine: false,
        padding: { left: 0, top: 0, right: 0, bottom: 0 },
        titlePadding: { left: 0, top: 10, right: 0, bottom: 0 },
        source: dataAdapter,
        colorScheme: 'scheme20',
        backgroundColor: 'rgba(0,0,0,0)',//'#f6f9fc',
        toolTipHideDelay: 1000,

        seriesGroups:
            [
                {
                    type: 'donut',
                    showLabels: true,
                    series:
                        [
                            {
                                dataField: 'Valor',
                                displayText: 'Texto',
                                labelRadius: 100,
                                initialAngle: 15,
                                radius: 140,
                                innerRadius: 60,
                                centerOffset: 0,
                                formatSettings: { sufix: '%', decimalPlaces: 0 }
                            }
                        ]
                }
            ]
    };

    // setup the chart
    $(elemento).jqxChart(settings);
}

function filterDetail(url) {
    var vendor = $('#txtFVendor').val();
    var programa = $('#txtFPrograma').val();
    var fechaIni = $('#txtFFechaIni').val();
    var fechaFin = $('#txtFFechaFin').val();
    var idEstatus = $('#cboEstatus').val();
    var ejecutivo = $('#txtFEjecutivo').val();

    window.location.href = url + "?vendor=" + vendor + "&fechaIni=" + fechaIni + "&fechaFin=" + fechaFin + "&programa=" + programa + "&idEstatus=" + idEstatus + "&ejecutivo=" + ejecutivo;
}

function filterClear() {
    $('#txtFVendor').val('');
    $('#txtFPrograma').val('');
    $('#txtFFechaIni').val('');
    $('#txtFFechaFin').val('');
    $('#cboEstatus').val('0');
    $('#txtFEjecutivo').val('');
    $('#txtFPalabra').val('');
}