﻿$(document).ready(function () {
    $('#tblProgram thead tr th:nth-child(1)').hide();
    $('#tblProgram tbody tr td:nth-child(1)').hide();
    $('#tblProgram thead tr th:nth-child(2)').hide();
    $('#tblProgram tbody tr td:nth-child(2)').hide();

    $('#tblProgram tbody tr').click(function () {

        var selected = $(this).hasClass("highlight");
        $("#tblProgram tbody tr").removeClass("highlight");
        if (!selected)
            $(this).addClass("highlight");

        var id = $(this).children("td:eq(0)").html();
        var vendId = $(this).children("td:eq(1)").html();
        var programa = $(this).children("td:eq(2)").html();
        var segmento = $(this).children("td:eq(3)").html();
        var timing = $(this).children("td:eq(4)").html();

        $('#txtPrograma').val(programa);
        $('#cboVendor').val(vendId.replace(/\s/g, ''));
        $('#cboTiming').val(timing.replace(/\s/g, ''));
        $('#cboSegmento').val(segmento.replace(/\s/g, ''));
        $('#txtProgramId').val(id.replace(/\s/g, ''));

    });
});

function saveRow(url, active) {
    var program = $('#txtPrograma').val();
    var timing = $('#cboTiming').val();
    var segment = $('#cboSegmento').val();
    var vendorId = $('#cboVendor').val();
    var id = $('#txtProgramId').val();

    if (program.length <= 0 || vendorId <= 0 || timing == "0" || segment == "0") {
        alert('Se deben ingresar todos los datos.');
        $('#txtPrograma').focus();
        return;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: {programId: id, program: program, vendorId: vendorId, segment: segment, timing: timing, active: active }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location.href = "/ProgramCatalog/Index";
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function filterTable(url) {
    var dataSearch = $('#txtBuscar').val();

    window.location.href = url + "?&dsearch=" + dataSearch;
}

function showProgramDetail() {
    var programId = $('#txtProgramId').val();

    if (programId == 0) {
        alert('Selecciona un programa para visualizar el detalle.');
        return;
    }

    window.location.href = "/ProgramSetting/Index?programId=" + programId;
}