﻿$(document).ready(function () {
    $('#tblVendor thead tr th:nth-child(2)').hide();
    $('#tblVendor tbody tr td:nth-child(2)').hide();

    $('#tblVendor tbody tr').click(function () {

        var selected = $(this).hasClass("highlight");
        $("#tblVendor tbody tr").removeClass("highlight");
        if (!selected)
            $(this).addClass("highlight");

        var id = $(this).children("td:eq(0)").html();
        var locId = $(this).children("td:eq(1)").html();
        var name = $(this).children("td:eq(2)").html();
        var sic = $(this).children("td:eq(3)").html();
        var channel = $(this).children("td:eq(4)").html();
        var phone = $(this).children("td:eq(5)").html();
        var email = $(this).children("td:eq(6)").html();
        var location = $(this).children("td:eq(7)").html();

        $('#txtVendorId').val(id.replace(/\s/g, ''));
        $('#txtVendor').val(name);
        $('#txtSIC').val(sic.replace(/\s/g, ''));
        $('#txtCanal').val(channel.replace(/\s/g, ''));
        $('#txtTelefono').val(phone.replace(/\s/g, ''));
        $('#txtEmail').val(email.replace(/\s/g, ''));
        $('#cboUbicacion').val(locId.replace(/\s/g, ''));

        $('#btnActualizar').show();
        $('#btnGuardar').hide();
    });

    $('#btnActualizar').hide();
});

function saveRow(url) {
    var name = $('#txtVendor').val();
    var sic = $('#txtSIC').val();
    var channel = $('#txtCanal').val();
    var phone = $('#txtTelefono').val();
    var email = $('#txtEmail').val();
    var locId = $('#cboUbicacion').val();

    if (name.length <= 0 || sic.length <= 0 || channel.length <= 0 ||
    phone.length <= 0 || email.length <= 0 || locId <= 0) {
        alert('Se deben ingresar todos los datos.');
        $('#txtVendor').focus();
        return;
    }

    var objJson = {
        "Vendor_Name": name,
        "Vendor_SICCode": sic,
        "Vendor_ChannelManager": channel,
        "Vendor_PhoneNumber": phone,
        "Vendor_Email": email,
        "Vendor_LocationId": locId
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: { vendorInfo: objJson }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location.href = "/VendorCatalog/Index";
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function updateRow(url, active) {
    var id = $('#txtVendorId').val();
    var name = $('#txtVendor').val();
    var sic = $('#txtSIC').val();
    var channel = $('#txtCanal').val();
    var phone = $('#txtTelefono').val();
    var email = $('#txtEmail').val();
    var locId = $('#cboUbicacion').val();

    if (name.length <= 0 || sic.length <= 0 || channel.length <= 0 ||
        phone.length <= 0 || email.length <= 0 || locId <= 0) {
        alert('Se deben ingresar todos los datos.');
        $('#txtVendor').focus();
        return;
    }

    var objJson = {
        "VendorId": id,
        "Vendor_Name": name,
        "Vendor_SICCode": sic,
        "Vendor_ChannelManager": channel,
        "Vendor_PhoneNumber": phone,
        "Vendor_Email": email,
        "Vendor_LocationId": locId
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: { vendorInfo: objJson, activo: active }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location.href = "/VendorCatalog/Index";
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function filterTable(url) {
    var dataSearch = $('#txtBuscar').val();

    window.location.href = url + "?&dsearch=" + dataSearch;
}

function newRow() {
    $('#txtVendor').val('');
    $('#txtSIC').val('');
    $('#txtCanal').val('');
    $('#txtTelefono').val('');
    $('#txtEmail').val('');
    $('#cboUbicacion').val('0');

    $('#btnActualizar').hide();
    $('#btnGuardar').show();

    $("#tblVendor tbody tr").removeClass("highlight");
    $('#txtVendor').focus();
}