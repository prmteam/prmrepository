﻿$(document).ready(function () {

    $('#colorSelector').ColorPicker({
        color: '#000000',
        onShow: function (colpkr) {
            $(colpkr).fadeIn(500);
            return false;
        },
        onHide: function (colpkr) {
            $(colpkr).fadeOut(500);
            return false;
        },
        onChange: function (hsb, hex, rgb) {
            $('#colorSelector div').css('backgroundColor', '#' + hex);
            $('#txtColor').val(rgb.r+ ',' + rgb.g + ','+rgb.b);
        }
    });

    $('#tblActivos').on('click', '.delete-row', function () {
        var assetId = $(this).parent().children('td:eq(3)').text().replace(/\s/g, '');
        var programId = $('#hdProgramId').val();
        var row = $(this).parent();

        $.ajax({
            url: "/ProgramSetting/DeleteAsset",
            type: 'POST',
            data: { programId: programId, assetId: assetId }
        }).success(function (msg) {
            if (msg.CargadoCorrecto) {
                row.remove();
            }
            else {
                alert(msg.Mensajes);
            }
        });

    });

    $('#tblActivos').on('click', '.add-service', function () {
        //alert($(this).parent().find("td:eq(6)").html());
        
        var contentServicio = $('#content-Servicio').html();

        $("a#linkServicio").fancybox({
            href: '#content-Servicio',
            transitionIn: 'elastic',
            transitionOut: 'elastic',
            speedIn: 600,
            speedOut: 200,
            touch: false,
            
            afterClose: function () {
                $('#content-Servicio').html(contentServicio);
            }
        });

        
    });

    var contentVendor = $('#content-Vendor').html();

    $("a#linkVendor").fancybox({
        href: '#content-Vendor',
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        speedIn: 600,
        speedOut: 200,
        overlayShow: true,
        afterClose: function () {
            $('#content-Vendor').html(contentVendor);
        }
    });

    /*
    var contentProfit = $('#content-Profit').html();

    $("a#linkProfit").fancybox({
        href: '#content-Profit',
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        speedIn: 600,
        speedOut: 200,
        overlayShow: true,
        afterClose: function () {
            //$('#content-Profit').html(contentProfit);
        }
    });
    */
    /*
    var contentPretend = $('#content-Pretend').html();

    $("a#linkPretend").fancybox({
        href: '#content-Pretend',
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        speedIn: 600,
        speedOut: 200,
        overlayShow: true,
        afterClose: function () {
            //$('#content-Pretend').html(contentPretend);
        }
    });
    */
    $('#txtPretMonto').mask('$000,000,000,000,000.00', { reverse: true });

    $("input[type=number]").bind('keyup mouseup', function () {
        var idElement = $(this).attr('id');
        var nameEnd = idElement.substr(idElement.length - 3);
        var nameElement = idElement.substr(0, idElement.length - 3);

        if (nameEnd == 'Min') {
            var newName = '#' + nameElement + 'Max';
            $(newName).attr('min', $(this).val());
            $(this).attr('max', $(newName).val());

            if (parseInt($(this).val()) > parseInt($(newName).val())) {
                $(newName).val($(this).val());
            }
        }
        else if(nameEnd == 'Max') {
            var newName = '#' + nameElement + 'Min';
            $(newName).attr('max', $(this).val());
            $(this).attr('min', $(newName).val());

            if (parseInt($(this).val()) < parseInt($(newName).val())) {
                //$(newName).val($(this).val());
                //$(this).val($(newName).val());
            }
        }

    });

    $("input[type=number]").blur(function () {
        var idElement = $(this).attr('id');
        var nameEnd = idElement.substr(idElement.length - 3);
        var nameElement = idElement.substr(0, idElement.length - 3);

        if (nameEnd == 'Max') {
            var newName = '#' + nameElement + 'Min';

            if (parseInt($(this).val()) < parseInt($(newName).val())) {
                $(this).val($(newName).val());
            }
        }
    });

});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

function addAsset(url, element) {
    if (element == undefined) {
        window.location.href = url + "?assetId=0&programId=" + $("#hdProgramId").val();
    }
    else {
        var id = element.parentElement.parentElement.children[3].innerText;
        id = id.replace(/\s/g, '');
        window.location.href = url + "?assetId=" + id + "&programId=" + $("#hdProgramId").val();
    }
}

function saveSetting() {

    var progName = $('#txtProgramaNombre').val();
    var vendorId = $('#cboVendor').val();
    var segmento = $('#cboSegmento').val();
    var timing = $('#cboTiming').val();
    var vigencia = $('#txtVigencia').val();

    if (progName.length <= 0 || vendorId <= 0 || segmento <= 0 || timing <= 0) {
        alert('Se deben capturar los datos solicitados para el programa.');
        $('#txtProgramaNombre').focus();
        return;
    }

    var productoIds = $("input[name='cbxProducto']:checked").map(function () {
        return $(this).val();
    });
    productoIds = Array.from(productoIds);
    var monedaIds = $("input[name='cbxMoneda']:checked").map(function () {
        return $(this).val();
    });
    monedaIds = Array.from(monedaIds);
    var plazoIds = $("input[name='cbxPlazo']:checked").map(function () {
        return $(this).val();
    });
    plazoIds = Array.from(plazoIds);

    if (productoIds.length <= 0 || monedaIds.length <= 0 || plazoIds.length <= 0) {
        alert('Se debe habilitar la configuración deseada para producto, moneda y plazo.');
        $('#cbxProducto').focus();
        return;
    }

    // colectar datos de inputs
    var arrInputs = new Array();

    $("#contentInputs input[type=number]").each(function () {
        var idElement = $(this).attr('id');
        arrInputs.push({ "fieldName": idElement, "fieldValue": $(this).val() });
    });

    $("#contentInputs input[type=checkbox]").each(function () {
        var idElement = $(this).attr('id');
        if (idElement!=null && idElement.indexOf('Ver') != -1) {
            var isChecked = $('#' + idElement).is(":checked");
            arrInputs.push({ "fieldName": idElement, "fieldValue": isChecked });
        }
        if (idElement != null && idElement.indexOf('Use') != -1) {
            var isChecked = $('#' + idElement).is(":checked");
            arrInputs.push({ "fieldName": idElement, "fieldValue": isChecked });
        }
    });

    var rgbColor = 'rgba(' + $('#txtColor').val() + ')';
    var programId = $('#hdProgramId').val();

    var jsonObj = {
        "programaId": programId,
        "nombre": progName,
        "vendorId": vendorId,
        "segmentoId": segmento,
        "timingId": timing,
        "vigencia": vigencia,
        "lstProductoId": productoIds,
        "lstPlazoId": plazoIds,
        "lstMonedaId": monedaIds,
        "lstInputs": arrInputs,
        "rgbColor": rgbColor
    };

    $.ajax({
        url: "/ProgramSetting/SaveProgram",
        type: 'POST',
        data: JSON.stringify(jsonObj),
        dataType: 'json',
        contentType: 'application/json'
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            alert('datos guardados');
            window.location = "/ProgramSetting/Index?programId=" + msg.programaId;
        }
        else {
            alert(msg.Mensajes);
        }

    });

}

function addRowActivo(url) {

    var exit = false;
    var productoSel = $('#cboProductoPS').val();
    var plazoSel = $('#cboPlazoPS').val();
    var coverId = $('#cboCoberturaActivo').val();

    if (isNaN(productoSel) || productoSel == 0) {
        alert('Selecciona un activo para agregar al programa.');
        return;
    }

    if (isNaN(plazoSel) || plazoSel == 0) {
        alert('Selecciona un plazo para configurar los valores del activo.');
        return;
    }

    if (isNaN(coverId) || coverId == 0) {
        alert('Selecciona un cobertura de seguro.');
        return;
    }

    $("#tblActivos tbody tr").each(function (index) {
        var tbModelo, tbPlazo;

        $(this).children("td").each(function (index2) {
            switch (index2) {
                case 5:
                    tbModelo = $(this).text();
                    break;
                case 6:
                    tbPlazo = $(this).text();
                    break;
            }
        });

        if(productoSel == tbModelo.replace(/\s/g, '') && plazoSel == tbPlazo.replace(/\s/g, '')) {
            alert('La configuración con el activo y plazo seleccionados ya existe.');
            $('#cboProductoPS').focus();
            exit = true;
        }
    });

    if (exit)
        return;

    var objJson = {
        "AssetFinancialTerm_AssetId": productoSel,
        "AssetFinancialTerm_FinancialTermId": plazoSel
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: { relRV: objJson, coverageId: coverId }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {

            var markup = "<tr><td>" + msg.activoSelected.Asset_Model + "</td><td>" +
                msg.plazo.FinancialTerm_Description + "</td><td>" +
                msg.rvInfo.AssetFinancialTerm_ResidualValue + "</td><td>" +
                msg.cobertura.Coverage_Cost + "</td>" +

                "<td class='add-service'><a id='linkServicio' href='#content-Servicio'><span class='glyphicon glyphicon-plus-sign' aria-hidden='true'></span></a></td>" +
                "<td class='delete-row' style='cursor: pointer;'><span class='glyphicon glyphicon-minus-sign' aria-hidden='true'></span></td>" +

                "<td class='td_id' style='display:none;'>" +
                productoSel + "</td>" +
                "<td class='td_id' style='display:none;'>" +
                plazoSel + "</td></tr>";

            $("#tblActivos tbody").append(markup);
            $("#tblActivos tfoot").remove();

            $.fancybox.close();

        }
        else {
            alert(msg.Mensajes);
        }

    });

}

function addProfit() {
    $.fancybox.close();
}

function addPretend(){
    var monto = parseFloat($('#txtPretMonto').val().replace(/,/g, ""));
    var moneda = $('#cboPretMoneda').val();
    var plazo = $('#cboPretPlazo').val();
    var ftipo = $('#cboPretProducto').val();

    var rv = $('#cboRValue').val();
    var secdis = $('#cboSDeposit').val();
    var opfee = $('#cboOFee').val();
    var bd = $('#cboBDiscount').val();
    var reffee = $('#cboRFee').val();
    var legfee = $('#txtLFees').val();
    var gadicional = $('#txtGastos').val();

    var rv_mon = (rv / 100) * monto;
    var sd_mon = (secdis / 100) * monto;
    var of_mon = (opfee / 100) * monto;
    var bd_mon = (bd / 100) * monto;
    var rf_mon = (reffee / 100) * monto;

    var mescero = (-monto) + sd_mon + of_mon + bd_mon + (-rf_mon) + parseFloat(-legfee) + parseFloat(-gadicional);
    var mesultimo = rv_mon + (-sd_mon);

    $('#lblProfit').text('Calculando IRR...');
    $.fancybox.close();
    //alert(mescero);
    
    $.ajax({
        url: "/ProgramSetting/GetIRR",
        type: 'POST',
        data: { monto: monto, mesCero: mescero, mesUltimo: mesultimo, monedaId: moneda, plazoId: plazo, productoId: ftipo, residualValue: (rv / 100) }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            $('#lblProfit').text(msg.profitPercentage + '% Pago mensual: $' + Intl.NumberFormat('es-MX').format(parseFloat(msg.paymentAmount)));
        }
        else {
            $('#lblProfit').text(msg.Mensajes);
        }
    });
    
}

function addVendor(url) {
 
    var exit = false;
    $("#content-Vendor input").each(function () {
        if ($(this).val() == "") {

            $(this).focus();
            exit = true;
            return;
        }
    });

    if (exit || $('#cboVendorUbicacion').val() == 0) {
        alert('Todos los campos son requeridos.');
        return;
    }

    var jsonObj = {
        "Vendor_Name": $('#txtVendorName').val(),
        "Vendor_Sector": $('#txtVendorSector').val(),
        "Vendor_RFC": $('#txtVendorRFC').val(),
        "Vendor_PhoneNumber": $('#txtVendorPhone').val(),
        "Vendor_Email": $('#txtVendorEmail').val(),
        "Vendor_LocationId": $('#cboVendorUbicacion').val()
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: JSON.stringify(jsonObj),
        dataType: 'json',
        contentType: 'application/json'
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            $('#cboVendor').empty();
            $('#cboVendor').append('<option>--- Elige una opción ---');

            $.each(msg.lstVendor, function (id, value) {
                $('#cboVendor').append('<option value="' + msg.lstVendor[id].VendorId + '">' + msg.lstVendor[id].Vendor_Name + '</option>');
            });

            $.fancybox.close();
        }
        else {
            alert(msg.Mensajes);
        }

    });
}