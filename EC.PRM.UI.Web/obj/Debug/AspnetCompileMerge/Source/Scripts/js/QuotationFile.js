﻿function downQuote(id) {

    var objQuote = new CreateQuoteFile({
        "ItemId": id
    });

    objQuote.Create();
}

function CreateQuoteFile(opciones) {
    this.UrlBase = "../";
    this.ItemId = null;

    if (opciones !== undefined && opciones != null) {
        if (opciones.UrlBase !== undefined && opciones.UrlBase != null) {
            this.UrlBase = opciones.UrlBase;
        }
        if (opciones.ItemId !== undefined && opciones.ItemId != null && opciones.ItemId != '') {
            this.ItemId = opciones.ItemId;
        }
    }
}

CreateQuoteFile.prototype.Create = function () {
    AbrirModal(
    {
        Mensaje: "<p><h3>Generando cotización...</h3></p>",
        AlineacionMensaje: "center"
    });

    var url = this.UrlBase + "api/QuotationApi/CreateQuoteFile";

    $.ajax({
        url: url,
        type: "GET",
        dataType: "JSON",
        data: { "itemId": this.ItemId }
    }).done(function (data) {
        if (data.CargadoCorrecto) {
            window.open(data.Mensajes, "_blank");
        }
        else {
            alert(data.Mensajes);
        }
        CerrarModal();
    })
    .fail(function (data, status, headers) {
        if (data != null && data.responseText != null && data.responseText != 'undefined' && data.responseText != '') {
            alert(data.Mensajes);
        }
        else {
            alert("Error desconocido.");
        }
        CerrarModal();
    });
}