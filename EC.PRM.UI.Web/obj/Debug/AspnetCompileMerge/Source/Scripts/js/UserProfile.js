﻿$(document).ready(function () {
    $('#tblUser thead tr th:nth-child(7)').hide();
    $('#tblUser tbody tr td:nth-child(7)').hide();
    $('#tblUser thead tr th:nth-child(8)').hide();
    $('#tblUser tbody tr td:nth-child(8)').hide();
    $('#tblUser thead tr th:nth-child(3)').hide();
    $('#tblUser tbody tr td:nth-child(3)').hide();
    $('#tblUser thead tr th:nth-child(2)').hide();
    $('#tblUser tbody tr td:nth-child(2)').hide();


    $('#tblUser tbody tr').click(function () {

        var selected = $(this).hasClass("highlight");
        $("#tblUser tbody tr").removeClass("highlight");
        if (!selected)
            $(this).addClass("highlight");

        var id = $(this).children("td:eq(0)").html();
        var user = $(this).children("td:eq(3)").html();
        var profId = $(this).children("td:eq(1)").html();
        var vendId = $(this).children("td:eq(2)").html();
        var pass = $(this).children("td:eq(6)").children("input:eq(0)").val();
        var progId = $(this).children("td:eq(7)").html();

        $('#txtUsuarioId').val(id.replace(/\s/g, ''));
        $('#txtUsuario').val(user.replace(/\s/g, ''));
        $('#cboPerfil').val(profId.replace(/\s/g, ''));
        $('#cboVendor').val(vendId.replace(/\s/g, ''));
        $('#txtPassword').val(pass.replace(/\s/g, ''));
        $('#cboPrograma').val(progId.replace(/\s/g, ''));

        $('#btnActualizar').show();
        $('#btnGuardar').hide();
    });

    $('#btnActualizar').hide();
});

function saveRow(url) {
    var usuario = $('#txtUsuario').val();
    var pass = $('#txtPassword').val();
    var vendorId = $('#cboVendor').val();
    var perfilId = $('#cboPerfil').val();
    var progId = $('#cboPrograma').val();

    if (usuario.length <= 0 || pass.length <= 0 || vendorId <= 0 || perfilId <= 0) {
        alert('Se deben ingresar todos los datos.');
        $('#txtUsuario').focus();
        return;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: { user: usuario, pass: pass, vendorId: vendorId, profileId: perfilId, programId: progId }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location.href = "/UserProfile/Index";
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function updateRow(url, active) {
    var id = $('#txtUsuarioId').val();
    var usuario = $('#txtUsuario').val();
    var pass = $('#txtPassword').val();
    var vendorId = $('#cboVendor').val();
    var perfilId = $('#cboPerfil').val();
    var progId = $('#cboPrograma').val();

    if (usuario.length <= 0 || pass.length <= 0 || vendorId <= 0 || perfilId <= 0) {
        alert('Se deben ingresar todos los datos.');
        $('#txtUsuario').focus();
        return;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: {userId: id, user: usuario, pass: pass, vendorId: vendorId, profileId: perfilId, programId: progId, activo: active }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location.href = "/UserProfile/Index";
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function filterTable(url) {
    var dataSearch = $('#txtBuscar').val();

    window.location.href = url + "?&dsearch=" + dataSearch;
}

function newRow() {
    $('#txtUsuario').val('');
    $('#txtPassword').val('');
    $('#cboPerfil').val('0');
    $('#cboVendor').val('0');
    $('#cboPrograma').val('0');

    $('#btnActualizar').hide();
    $('#btnGuardar').show();

    $("#tblUser tbody tr").removeClass("highlight");
    $('#txtUsuario').focus();
}