﻿$(document).ready(function () {
    var contentCobertura = $('#content-Cobertura').html();

    $("a#linkCobertura").fancybox({
        href: '#content-Cobertura',
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        speedIn: 600,
        speedOut: 200,
        overlayShow: true,
        afterClose: function () {
            $('#content-Cobertura').html(contentCobertura);
        }
    });

    var contentServicio = $('#content-Servicio').html();

    $("a#linkServicio").fancybox({
        href: '#content-Servicio',
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        speedIn: 600,
        speedOut: 200,
        overlayShow: true,
        afterClose: function () {
            $('#content-Servicio').html(contentServicio);
        }
    });
});

function GetAssetByGroup() {
    $("#cboGrupo option:selected").each(function () {
        $('#cboActivo').empty();
        $('#cboActivo').append('<option>Obteniendo registros...');

        var familiaSel = $(this).val();

        $.ajax({
            url: "/ProgramSetting/GetProductByFamily",
            type: 'POST',
            data: { familiaId: familiaSel }
        }).success(function (msg) {
            if (msg.CargadoCorrecto) {
                $('#cboActivo').empty();
                $('#cboActivo').append('<option value=0>--- Selecciona una opción ---');


                $.each(msg.lstActivos, function (id, value) {
                    $('#cboActivo').append('<option value="' + msg.lstActivos[id].AssetId + '">' + msg.lstActivos[id].Asset_Description + '</option>');
                });
            }
            else {
                alert(msg.Mensajes);
            }

        });
    })
}

//function getAssetServices() {
//    var ids = $('#tblServicio tbody input:checked').map(function () {
//        return $(this).closest('tr').find('td:eq(0)').text().replace(/\s/g, '');
//    });

//    //$("#tblServicio tbody tr").each(function (index) {
//    //    var tbId, tbDesc, tbCosto, tbAplica;

//    //    $(this).children("td").each(function (index2) {
//    //        switch (index2) {
//    //            case 0:
//    //                tbId = $(this).text();
//    //                break;
//    //            case 1:
//    //                tbDesc = $(this).text();
//    //                break;
//    //            case 2:
//    //                tbCosto = $(this).text();
//    //                break;
//    //            case 3:
//    //                tbAplica = $(this).text();
//    //                break;
//    //        }
//    //    });

//    //    alert(tbAplica + ' - ' + tbId);
//    //});
//}

function saveAsset(url) {
    if ($('#cboActivo').val() == '0') {
        alert('Se debe seleccionar un activo para agregar al programa.');
        $('#cboActivo').focus();
        return;
    }

    var arrServ = new Array();
    var ids = $('#tblServicio tbody input:checked').map(function () {
        var elemento = $(this).closest('tr').find('td:eq(0)').text().replace(/\s/g, '');
        arrServ.push(elemento);
    });

    var jsonObj = {
        "programId": $('#hdProgramId').val(),
        "assetId": $('#cboActivo').val(),
        "coverageId": $('#cboCobertura').val(),
        "lstServiceId": arrServ
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: JSON.stringify(jsonObj),
        dataType: 'json',
        contentType: 'application/json'
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            alert('Datos guardados');
            window.location = "/ProgramSetting/ProgramAsset?programId=" + msg.programId + "&assetId=" + msg.assetId;
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function updateAsset(url) {
    if ($('#cboActivo').val() == '0') {
        alert('Se debe seleccionar un activo para agregar al programa.');
        $('#cboActivo').focus();
        return;
    }

    var arrServ = new Array();

    var ids = $('#tblServicio tbody input:checked').map(function () {
        //return $(this).closest('tr').find('td:eq(0)').text().replace(/\s/g, '');
        var elemento = $(this).closest('tr').find('td:eq(0)').text().replace(/\s/g, '');
        arrServ.push(elemento);
    });

    var jsonObj = {
        "programId": $('#hdProgramId').val(),
        "assetId": $('#hdAssetId').val(),
        "coverageId": $('#cboCobertura').val(),
        "lstServiceId": arrServ
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: JSON.stringify(jsonObj),
        dataType: 'json',
        contentType: 'application/json'
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            alert('Datos guardados');
            window.location = "/ProgramSetting/ProgramAsset?programId=" + msg.programId + "&assetId=" + msg.assetId;
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function returnSetting() {
    window.location = "/ProgramSetting/Index?programId=" + $('#hdProgramId').val();
}

function selectAllService(source) {
    checkboxes = document.getElementsByName('cbxServicio');
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }
}

function addService(url) {

    var exit = false;
    $("#content-Servicio input").each(function () {
        if ($(this).val() == "") {

            $(this).focus();
            exit = true;
            return;
        }
    });

    if (exit) {
        alert('Todos los campos son requeridos.');
        return;
    }

    var jsonObj = {
        "AssetService_Description": $('#txtServDesc').val(),
        "AssetService_Cost": $('#txtServCosto').val()
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: JSON.stringify(jsonObj),
        dataType: 'json',
        contentType: 'application/json'
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location = "/ProgramSetting/ProgramAsset?programId=" + $('#hdProgramId').val() + "&assetId=" + $('#hdAssetId').val();
        }
        else {
            alert(msg.Mensajes);
        }

    });
}