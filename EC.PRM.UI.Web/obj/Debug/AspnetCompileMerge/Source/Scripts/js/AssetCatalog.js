﻿$(document).ready(function () {

    $('#tblActivo thead tr th:nth-child(2)').hide();
    $('#tblActivo tbody tr td:nth-child(2)').hide();

    $('#tblActivo tbody tr').click(function () {

        var selected = $(this).hasClass("highlight");
        $("#tblActivo tbody tr").removeClass("highlight");
        if (!selected)
            $(this).addClass("highlight");

        var id = $(this).children("td:eq(0)").html();
        var gId = $(this).children("td:eq(1)").html();
        var marca = $(this).children("td:eq(2)").html();
        var modelo = $(this).children("td:eq(3)").html();
        var desc = $(this).children("td:eq(4)").html();
        var anio = $(this).children("td:eq(5)").html();
        var precio = $(this).children("td:eq(6)").html();
        var grupo = $(this).children("td:eq(7)").html();

        $('#txtActivoId').val(id.replace(/\s/g, ''));
        $('#txtMarca').val(marca.replace(/\s/g, ''));
        $('#txtModelo').val(modelo.trim());
        $('#txtDescripcion').val(desc.trim());
        $('#txtAnio').val(anio.replace(/\s/g, ''));
        $('#txtPrecio').val(precio.replace(/\s/g, ''));
        $('#cboGrupo').val(gId.replace(/\s/g, ''));

        $('#btnActualizar').show();
        $('#btnGuardar').hide();
    });

    $('#btnActualizar').hide();

    var content = $('#content-Collateral').html();

    $("a#linkCollateral").fancybox({
        href: '#content-Collateral',
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        speedIn: 600,
        speedOut: 200,
        overlayShow: true,
        afterClose: function () {
            $('#content-Collateral').html(content);
        }
    });
});

function saveRow(url) {
    var marca = $('#txtMarca').val();
    var modelo = $('#txtModelo').val();
    var desc = $('#txtDescripcion').val();
    var anio = $('#txtAnio').val();
    var precio = $('#txtPrecio').val();
    var gId = $('#cboGrupo').val();

    if (modelo.length <= 0 || marca.length <= 0 || desc.length <= 0 ||
    anio.length <= 0 || precio.length <= 0 || gId <= 0) {
        alert('Se deben ingresar todos los datos.');
        $('#txtMarca').focus();
        return;
    }

    var objJson = {
        "Asset_Brand": marca,
        "Asset_Model": modelo,
        "Asset_Description": desc,
        "Asset_Year": anio,
        "Asset_OEC": parseFloat(precio.replace(/\,/g, '')),
        "Asset_AssetCollateralId": gId
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: { assetInfo: objJson }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location.href = "/AssetCatalog/Index";
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function updateRow(url, active) {
    var id = $('#txtActivoId').val();
    var marca = $('#txtMarca').val();
    var modelo = $('#txtModelo').val();
    var desc = $('#txtDescripcion').val();
    var anio = $('#txtAnio').val();
    var precio = $('#txtPrecio').val();
    var gId = $('#cboGrupo').val();

    if (modelo.length <= 0 || marca.length <= 0 || desc.length <= 0 ||
    anio.length <= 0 || precio.length <= 0 || gId <= 0) {
        alert('Se deben ingresar todos los datos.');
        $('#txtMarca').focus();
        return;
    }

    var objJson = {
        "AssetId": id,
        "Asset_Brand": marca,
        "Asset_Model": modelo,
        "Asset_Description": desc,
        "Asset_Year": anio,
        "Asset_OEC": parseFloat(precio.replace(/\,/g, '')),
        "Asset_AssetCollateralId": gId
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: { assetInfo: objJson, activo: active }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location.href = "/AssetCatalog/Index";
        }
        else {
            alert(msg.Mensajes);
        }

    });
}

function filterTable(url) {
    var dataSearch = $('#txtBuscar').val();

    window.location.href = url + "?&dsearch=" + dataSearch;
}

function newRow() {

    $('#txtMarca').val('');
    $('#txtModelo').val('');
    $('#txtDescripcion').val('');
    $('#txtAnio').val('');
    $('#txtPrecio').val('');
    $('#cboGrupo').val('0');

    $('#btnActualizar').hide();
    $('#btnGuardar').show();

    $("#tblActivo tbody tr").removeClass("highlight");
    $('#txtActivo').focus();
}

function addCollateral(url) {

    var exit = false;
    $("#content-Collateral input").each(function () {
        if ($(this).val() == "") {

            $(this).focus();
            exit = true;
            return;
        }
    });

    if (exit) {
        alert('Todos los campos son requeridos.');
        return;
    }

    var jsonObj = {
        "AssetCollateral_Description": $('#txtGrupoDesc').val()
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: JSON.stringify(jsonObj),
        dataType: 'json',
        contentType: 'application/json'
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            $('#cboGrupo').empty();
            $('#cboGrupo').append('<option>--- Selecciona una opción ---');

            $.each(msg.lstCollateral, function (id, value) {
                $('#cboGrupo').append('<option value="' + msg.lstCollateral[id].AssetCollateralId + '">' + msg.lstCollateral[id].AssetCollateral_Description + '</option>');
            });

            $.fancybox.close();
        }
        else {
            alert(msg.Mensajes);
        }

    });
}