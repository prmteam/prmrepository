﻿$(document).ready(function () {
    bootstrap_alert.success = function (message) {
        $('#alert_placeholder').html('<div class="alert alert-success fade in"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')
    }

    $("input[name='cbxDelay']").change(function () {
        $(".alert").alert('close');
    });
    $("input[name='cbxDiscount']").change(function () {
        $(".alert").alert('close');
    });
});

function setDiscount(url) {
    var strDelay = $("input[name='cbxDelay']:checked").val();
    var strDiscount = $("input[name='cbxDiscount']:checked").val();

    $.ajax({
        url: url,
        type: 'POST',
        data: { delay: strDelay, discount: strDiscount }
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            bootstrap_alert.success('Apoyos aplicados.');
        }
        else {
            alert(msg.Mensajes);
        }
    });
}

function mostrarAmortizacion(url) {
    window.location.href = url;
}