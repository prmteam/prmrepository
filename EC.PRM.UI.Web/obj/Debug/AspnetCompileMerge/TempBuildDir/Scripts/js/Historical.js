﻿$(document).ready(function () {
    bootstrap_alert = function () { }
    bootstrap_alert.warning = function (message) {
        $('#alert_placeholder').html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')
    }

    var contentT1 = $('#content-T1').html();

    $("a#linkT1").fancybox({
        href: '#content-T1',
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        speedIn: 600,
        speedOut: 200,
        overlayShow: true,

        afterClose: function () {
            $('#content-T1').html(contentT1);
        }

    });
    
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

function filterTable(url) {
    var strId = document.getElementById("txtFId").value;
    var strCliente = document.getElementById("txtFCliente").value;
    var sf = document.getElementById("cboFFecha");
    var strFecha = sf.options[sf.selectedIndex].text;
    var sp = document.getElementById("cboFPProducto");
    var strProductoId = sp.options[sp.selectedIndex].value;

    window.location.href = url + "?id=" + strId + "&cliente=" + strCliente + "&fechaVal=" + strFecha + "&productoVal=" + strProductoId;

}

function cloneQuote(id) {
    window.location.href = "/Quotation/Step1?quoteId=" + id + "&act=clone";
}

function editQuote(id) {
    window.location.href = "/Quotation/Step2?quoteId=" + id + "&act=edit";
}

function enviarEmail() {
    AbrirModal(
    {
        Mensaje: "<p><h3>Procesando envío de Email...</h3></p>",
        AlineacionMensaje: "center"
    });

    var quoteId = $('#txtQuoteEmailId').val();
    var strEmail = $('#txtQuoteEmail').val();
    this.UrlBase = "../";

    emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (!emailRegex.test(strEmail)) {
        alert('Favor de capturar una cuenta de correo válida.');
        $('#txtEmail').focus();
        return;
    }

    var url = this.UrlBase + "api/QuotationApi/SendQuotationEmail";

    $.ajax({
        url: url,
        type: "GET",
        dataType: "JSON",
        data: { "itemId": quoteId, "address": strEmail }
    }).done(function (data) {
        if (data.CargadoCorrecto) {
            alert('Correo enviado.');
        }
        else {
            alert(data.Mensajes);
        }
        CerrarModal();
    })
    .fail(function (data, status, headers) {
        if (data != null && data.responseText != null && data.responseText != 'undefined' && data.responseText != '') {
            alert(data.Mensajes);
        }
        else {
            alert("Error desconocido.");
        }
        CerrarModal();
    });

    
    $.fancybox.close();
}

function setQuoteId(id) {
    $('#txtQuoteEmailId').val(id);
}
