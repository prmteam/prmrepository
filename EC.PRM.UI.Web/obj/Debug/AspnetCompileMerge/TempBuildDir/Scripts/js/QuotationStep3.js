﻿function keepDataStep3(url) {
    var strFinTipoId = $("input[name='cbxFinTipo']:checked").val();
    var strMonedaId = $("input[name='cbxMoneda']:checked").val();
    var strPlazoId = $("input[name='cbxFinPlazo']:checked").val();

    if (!(strFinTipoId) || !(strMonedaId) || !(strPlazoId)) {
        bootstrap_alert.warning('Favor de seleccionar todos los valores para el financiamiento.');
        $('#cbxFinTipo').focus();
        return;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: { finTipoId: strFinTipoId, monedaId: strMonedaId, plazosId: strPlazoId }
    }).success(function (msg) {

        if (msg.CargadoCorrecto) {
            window.location.href = "/Quotation/Step4";
        }
        else {
            alert(msg.Mensajes);
        }
    });
}

function calculatePayment(url) {
    var strFinTipoId = $("input[name='cbxFinTipo']:checked").val();
    var strMonedaId = $("input[name='cbxMoneda']:checked").val();
    var strPlazoId = $("input[name='cbxFinPlazo']:checked").val();
    var segFinanciado = $("input[name='cbxSecFinanciado']:checked").val();

    if (!(strFinTipoId) || !(strMonedaId) || !(strPlazoId)) {
        bootstrap_alert.warning('Favor de seleccionar todos los valores para el financiamiento.');
        $('#cbxFinTipo').focus();
        return;
    }

    // colectar datos de inputs
    var arrInputs = new Array();

    $("#contentInputs input[type=text]").each(function () {
        var idElement = $(this).attr('id');
        arrInputs.push({ "fieldName": idElement, "fieldValue": $(this).val() });
    });

    var jsonObj = {
        "productoId": strFinTipoId,
        "monedaId": strMonedaId,
        "plazoId": strPlazoId,
        "lstInputs": arrInputs,
        "insFinancied": segFinanciado
    };

    $('#txtFinanciamiento').text('Calculando...');

    $.ajax({
        url: url,
        type: 'POST',
        data: JSON.stringify(jsonObj),
        dataType: 'json',
        contentType: 'application/json'
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            $('#txtFinanciamiento').text('Payment:' + Intl.NumberFormat('es-MX').format(msg.pagoMensual) + ' IRR:' + msg.irr);
            if (segFinanciado == "true") {
                var textCalculos = 'Payment:' + Intl.NumberFormat('es-MX').format(msg.pagoMensual) + ' IRR:' + msg.irr + ' Insurance:' + msg.arrayInsurance[10] + ' IRR:' + msg.irrInsurance;
                $('#txtFinanciamiento').text(textCalculos);
            }
        }
        else {
            $('#txtFinanciamiento').text('Haz click en Calcular');
            alert(msg.Mensajes);
        }
    });

    if (url.indexOf('TablaAmortizacion') > 0) {
        window.location.href = url;
    }
}

$(document).ready(function () {
    $('input[type=number]').bind('keyup mouseup', function (e) {
        var min = parseFloat($(this).attr('min'));
        var max = parseFloat($(this).attr('max'));
        var curr = parseFloat($(this).val());
        if (curr > max) { $(this).val(max); var changed = true; }
        if (curr < min) { $(this).val(min); var changed = true; }
        if (changed) {
            return false;
        }
    });
    
    var sldBalPay = document.getElementById("sldBalloonPayment");
    $('#txtBalloonPayment').val(sldBalPay.value);
    sldBalPay.oninput = function () {
        $('#txtBalloonPayment').val(sldBalPay.value);
    }

    var sldPurOpt = document.getElementById("sldPurchaseOption");
    $('#txtPurchaseOption').val(sldPurOpt.value);
    sldPurOpt.oninput = function () {
        $('#txtPurchaseOption').val(sldPurOpt.value);
    }

    var sldOpFee = document.getElementById("sldOpFee");
    $('#txtOpFee').val(sldOpFee.value);
    sldOpFee.oninput = function () {
        $('#txtOpFee').val(sldOpFee.value);
    }

    var sldSecDep = document.getElementById("sldSecurityDeposit");
    $('#txtSecurityDeposit').val(sldSecDep.value);
    sldSecDep.oninput = function () {
        $('#txtSecurityDeposit').val(sldSecDep.value);
    }

    var sldBliDis = document.getElementById("sldBlindDiscount");
    $('#txtBlindDiscount').val(sldBliDis.value);
    sldBliDis.oninput = function () {
        $('#txtBlindDiscount').val(sldBliDis.value);
    }

    var sldRefFee = document.getElementById("sldReferalFee");
    $('#txtReferalFee').val(sldRefFee.value);
    sldRefFee.oninput = function () {
        $('#txtReferalFee').val(sldRefFee.value);
    }

    var sldDelay = document.getElementById("sldDelay");
    $('#txtDelay').val(sldDelay.value);
    sldDelay.oninput = function () {
        $('#txtDelay').val(sldDelay.value);
    }
    var sldGraMon = document.getElementById("sldGraceMonths");
    $('#txtGraceMonths').val(sldGraMon.value);
    sldGraMon.oninput = function () {
        $('#txtGraceMonths').val(sldGraMon.value);
    }
    var sldAdiCos = document.getElementById("sldAdditionalCosts");
    $('#txtAdditionalCosts').val(sldAdiCos.value);
    sldAdiCos.oninput = function () {
        $('#txtAdditionalCosts').val(sldAdiCos.value);
    }
    var sldLegFee = document.getElementById("sldLegalFees");
    $('#txtLegalFees').val(sldLegFee.value);
    sldLegFee.oninput = function () {
        $('#txtLegalFees').val(sldLegFee.value);
    }

    var strFinTipoId = $("input[name='cbxFinTipo']:checked").val();
    switch (strFinTipoId) {
        case "1":
            $('#sldBalloonPayment').hide();
            $('#lblBalloonPayment').hide();
            $('#lblsBalloonPayment').hide();
            $('#txtBalloonPayment').hide();

            $('#sldPurchaseOption').hide();
            $('#lblPurchaseOption').hide();
            $('#lblsPurchaseOption').hide();
            $('#txtPurchaseOption').hide();
            break;
        case "2":
            $('#sldBalloonPayment').hide();
            $('#lblBalloonPayment').hide();
            $('#lblsBalloonPayment').hide();
            $('#txtBalloonPayment').hide();

            $('#sldPurchaseOption').show();
            $('#lblPurchaseOption').show();
            $('#lblsPurchaseOption').show();
            $('#txtPurchaseOption').show();
            break;
        case 4:
            $('#sldBalloonPayment').show();
            $('#lblBalloonPayment').show();
            $('#lblsBalloonPayment').show();
            $('#txtBalloonPayment').show();

            $('#sldPurchaseOption').hide();
            $('#lblPurchaseOption').hide();
            $('#lblsPurchaseOption').hide();
            $('#txtPurchaseOption').hide();
            break;
    }
 
    $('input[type=radio][name=cbxFinTipo]').change(function () {

        if (this.value == 1) {
            $('#sldBalloonPayment').hide();
            $('#lblBalloonPayment').hide();
            $('#lblsBalloonPayment').hide();
            $('#txtBalloonPayment').hide();

            $('#sldPurchaseOption').hide();
            $('#lblPurchaseOption').hide();
            $('#lblsPurchaseOption').hide();
            $('#txtPurchaseOption').hide();
        }
        else if (this.value == 2) {
            $('#sldBalloonPayment').hide();
            $('#lblBalloonPayment').hide();
            $('#lblsBalloonPayment').hide();
            $('#txtBalloonPayment').hide();

            $('#sldPurchaseOption').show();
            $('#lblPurchaseOption').show();
            $('#lblsPurchaseOption').show();
            $('#txtPurchaseOption').show();
        }
        else if (this.value == 4) {
            $('#sldBalloonPayment').show();
            $('#lblBalloonPayment').show();
            $('#lblsBalloonPayment').show();
            $('#txtBalloonPayment').show();

            $('#sldPurchaseOption').hide();
            $('#lblPurchaseOption').hide();
            $('#lblsPurchaseOption').hide();
            $('#txtPurchaseOption').hide();
        }
    });
});