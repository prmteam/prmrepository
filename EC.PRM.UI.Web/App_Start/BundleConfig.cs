﻿using System.Web;
using System.Web.Optimization;

namespace EC.PRM.UI.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.js",
                        "~/Scripts/jspdf.js",
                        "~/Scripts/jspdf.min.js",
                        "~/Scripts/jspdf.plugin.autotable.js",
                        "~/Scripts/js/Modal.js",
                        "~/Scripts/js/QuotationStep1.js",
                        "~/Scripts/js/QuotationStep2.js",
                        //"~/Scripts/js/QuotationStep3.js",
                        "~/Scripts/js/QuotationStep4.js",
                        "~/Scripts/js/TablaAmortizacion.js"));
                        //"~/Scripts/js/ProgramSetting.js"));
                        //"~/Scripts/js/UserProfile.js",
                        //"~/Scripts/js/Historical.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/jquery.mask.js",
                      "~/Scripts/jquery.mask.min.js",
                      "~/Scripts/jquery.easing.1.3.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/jquery.waypoints.min.js",
                      "~/Scripts/sticky.js",
                      "~/Scripts/owl.carousel.min.js",
                      "~/Scripts/jquery.fancybox.min.js",
                      "~/Scripts/jquery.countTo.js",
                      "~/Scripts/jquery.stellar.min.js",
                      "~/Scripts/jquery.magnific-popup.min.js",
                      "~/Scripts/magnific-popup-options.js",
                      "~/Scripts/main.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/animate.css",
                      "~/Content/bootstrap-datepicker.min.css",
                      "~/Content/flexslider.css",
                      "~/Content/icomoon.css",
                      "~/Content/magnific-popu.css",
                      "~/Content/owl.carouser.min.css",
                      "~/Content/owl.theme.default.min.css",
                      "~/Content/style.css",
                      "~/Content/styles.css",
                      "~/Content/assets.min.css",
                      "~/Content/theme.css",
                      "~/Content/jquery.fancybox.min.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/themify-icons.css",
                      "~/Content/Quotation.css"));
        }
    }
}
