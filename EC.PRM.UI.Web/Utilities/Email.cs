﻿using EC.PRM.UI.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;

namespace EC.PRM.UI.Web.Utilities
{
    public class Email
    {
        /// <summary>
        /// Enviar email
        /// </summary>
        public static bool Send(EmailModel mailData)
        {
            try
            {
                if (string.IsNullOrEmpty(mailData.Recipients))
                {
                    throw new Exception("Email de destinatario no puede estar vacío.");
                }

                SmtpClient client = new SmtpClient(mailData.SMTP, mailData.Port);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential(mailData.User, mailData.Password);

                MailMessage message = new MailMessage();
                message.From = new MailAddress(mailData.Sender);

                foreach (string mailDir in mailData.Recipients.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    message.To.Add(mailDir.Trim());
                }
                if (!string.IsNullOrEmpty(mailData.CopyTo))
                {
                    foreach (string mailDir in mailData.CopyTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        message.CC.Add(mailDir.Trim());
                    }
                }
                if (!string.IsNullOrEmpty(mailData.BoldCopy))
                {
                    foreach (string mailDir in mailData.BoldCopy.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        message.Bcc.Add(mailDir.Trim());
                    }
                }

                AlternateView av = AlternateView.CreateAlternateViewFromString(mailData.Message, null, System.Net.Mime.MediaTypeNames.Text.Html);

                if (mailData.Images != null && mailData.Images.Count > 0)
                {
                    foreach (string key in mailData.Images.Keys)
                    {
                        MemoryStream ms = new MemoryStream(mailData.Images[key]);
                        LinkedResource headerImage = null;
                        if (key.EndsWith("jpg") || key.EndsWith("png"))
                        {
                            headerImage = new LinkedResource(ms);
                            headerImage.ContentId = key.Replace(".jpg", "").Replace(".png", "");
                            av.LinkedResources.Add(headerImage);
                        }
                        else if (key.EndsWith("gif"))
                        {
                            headerImage = new LinkedResource(ms);
                            headerImage.ContentId = key.Replace(".gif", "");
                            av.LinkedResources.Add(headerImage);
                        }
                    }
                }

                message.AlternateViews.Add(av);
                //message.Body = mensaje;
                message.BodyEncoding = Encoding.UTF8;
                message.IsBodyHtml = true;
                message.Subject = mailData.Subject;
                message.SubjectEncoding = Encoding.UTF8;

                if (!String.IsNullOrEmpty(mailData.AttachmentFile))
                {
                    Attachment attachment = new Attachment(mailData.AttachmentFile, MediaTypeNames.Application.Octet);
                    ContentDisposition disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(mailData.AttachmentFile);
                    disposition.ModificationDate = File.GetLastWriteTime(mailData.AttachmentFile);
                    disposition.ReadDate = File.GetLastAccessTime(mailData.AttachmentFile);
                    disposition.FileName = Path.GetFileName(mailData.AttachmentFile);
                    disposition.Size = new FileInfo(mailData.AttachmentFile).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    message.Attachments.Add(attachment);
                }

                //client.TargetName = "STARTTLS/smtp.office365.com";
                client.Send(message);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}