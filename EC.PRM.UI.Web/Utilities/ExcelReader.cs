﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using Excel = Microsoft.Office.Interop.Excel;

namespace EC.PRM.UI.Web.Utilities
{
    public class ExcelReader
    {
        /// <summary>
        /// Obtiene datos desde Excel por medio de la libreria Interop
        /// </summary>
        public static void GetDataFromExcel(string path, bool fuente)
        {
            try
            {
                Excel.Application objExcel = new Excel.Application();
                Excel.Workbook objWB = objExcel.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Excel.Worksheet wSheet = (Excel.Worksheet)objWB.Worksheets.get_Item("Lista Precios - Descripciones");
                Excel.Range range = wSheet.UsedRange;

                int n_rows = range.Rows.Count;
                int n_cols = range.Columns.Count;
                dynamic cell_value;

                for (int rCnt = 1; rCnt <= n_rows; rCnt++)
                {
                    for (int cCnt = 1; cCnt <= n_cols; cCnt++)
                    {
                        cell_value = (range.Cells[rCnt, cCnt] as Excel.Range).Value2;

                        if (cell_value != null)
                        {

                            Type value_type = cell_value.GetType();

                            Console.WriteLine(cell_value);
                        }
                    }
                }

                objWB.Close(true, null, null);
                objExcel.Quit();

                Marshal.ReleaseComObject(wSheet);
                Marshal.ReleaseComObject(objWB);
                Marshal.ReleaseComObject(objExcel);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene datos desde Excel por medio de connection string
        /// </summary>
        public static List<DataTable> GetDataFromExcel(string ExcelFileName)
        {
            try
            {
                string connectionString = string.Empty;
                List<DataTable> lstSheet = new List<DataTable>();

                if (Path.GetExtension(ExcelFileName) == ".xlsx")
                {
                    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelFileName +
                        ";Extended Properties=Excel 12.0;";
                }
                else
                {
                    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + ExcelFileName + ";Extended Properties=Excel 8.0;";
                }

                OleDbCommand selectCommand = new OleDbCommand();
                OleDbConnection connection = new OleDbConnection();
                OleDbDataAdapter adapter = new OleDbDataAdapter();
                connection.ConnectionString = connectionString;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                DataTable dtSchema = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                List<SelectListItem> SheetsName = GetSheetNames(dtSchema);

                for (int i = 0; i < SheetsName.Count; i++)
                {
                    selectCommand.CommandText = "SELECT * FROM [" + SheetsName[i].Value + "]";
                    selectCommand.Connection = connection;
                    adapter.SelectCommand = selectCommand;
                    DataTable Sheet = new DataTable();
                    Sheet.TableName = SheetsName[i].Text;
                    adapter.Fill(Sheet);

                    if (Sheet.Rows.Count > 0)
                    {
                        lstSheet.Add(Sheet);
                    }
                }

                adapter.Dispose();
                connection.Close();
                connection.Dispose();

                return lstSheet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetSheetNames(DataTable dtSchema)
        {
            var itemsOfWorksheet = new List<SelectListItem>();

            if (dtSchema != null)
            {
                //validate worksheet name.
                string worksheetName;
                for (int cnt = 0; cnt < dtSchema.Rows.Count; cnt++)
                {
                    worksheetName = dtSchema.Rows[cnt]["TABLE_NAME"].ToString();

                    if (worksheetName.Contains('\''))
                    {
                        worksheetName = worksheetName.Replace('\'', ' ').Trim();
                    }
                    if (worksheetName.Trim().EndsWith("$"))
                        itemsOfWorksheet.Add(new SelectListItem { Text = worksheetName.TrimEnd('$'), Value = worksheetName });
                }
            }

            return itemsOfWorksheet;
        }

    }

    public class SelectListItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}