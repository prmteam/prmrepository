﻿using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
//using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using Word = Microsoft.Office.Interop.Word;

namespace EC.PRM.UI.Web.Utilities
{
    public static class UtilitiesGeneral
    {
        #region Create Quotaion PDF File

        public static MensajesBaseModel CreateQuoteFileNPOI(QuoteFileModel modelo)
        {
            try
            {
                string RutaPlantilla = System.Web.Hosting.HostingEnvironment.MapPath(modelo.rutaPlantillaWord);
                QuoteFileModel q = GetQuotationData(modelo.itemId);

                NPOI.XWPF.UserModel.XWPFDocument doc = new NPOI.XWPF.UserModel.XWPFDocument(NPOI.OpenXml4Net.OPC.OPCPackage.Open(RutaPlantilla));

                foreach (NPOI.XWPF.UserModel.XWPFTable tbl in doc.Tables)
                {
                    foreach (NPOI.XWPF.UserModel.XWPFTableRow row in tbl.Rows)
                    {
                        foreach (NPOI.XWPF.UserModel.XWPFTableCell cell in row.GetTableCells())
                        {
                            foreach (NPOI.XWPF.UserModel.XWPFParagraph p in cell.Paragraphs)
                            {
                                foreach (NPOI.XWPF.UserModel.XWPFRun r in p.Runs)
                                {
                                    string text = r.GetText(0);
                                    Console.WriteLine(text);

                                    if (!String.IsNullOrEmpty(text) && text.Equals("<<Cotización_Número>>"))
                                    {
                                        r.SetText(modelo.itemId.ToString());
                                    }
                                    /*
                                    FindAndReplace(word, "<<Cotización_Número>>", modelo.itemId);
                                    FindAndReplace(word, "<<Fecha_generación>>", q.cotizacionFecha);
                                    FindAndReplace(word, "<<Nombre_empresa>>", q.clienteNombre);
                                    FindAndReplace(word, "<<Nombre_contacto>>", q.clienteContacto);
                                    FindAndReplace(word, "<<Estado_Oficina_Engenium_Capital >>", q.clienteOficina);
                                    FindAndReplace(word, "<<Nombre_Vendor>>", q.vendorNombre);
                                    FindAndReplace(word, "<<Producto>>", q.producto);
                                    */
                                }
                            }
                        }
                    }
                }
                string path = Path.Combine(HttpRuntime.AppDomainAppPath, @"FilesTemp\file.docx");

                using (FileStream output = new FileStream(path, FileMode.Create))
                {
                    doc.Write(output);
                    
                }
                //doc.Write()

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                modelo.CargadoCorrecto = false;
                modelo.Mensajes = ex.Message;
            }

            return modelo;
        }

        public static MensajesBaseModel CreateQuoteFile(QuoteFileModel modelo)
        {
            try
            {
                Word.Application word = new Word.Application();
                string RutaPlantilla = System.Web.Hosting.HostingEnvironment.MapPath(modelo.rutaPlantillaWord);
                //RutaPlantilla = @"C:\Users\503001077\Documents\PRE_Deployed\DocTemplate\PRM_Quotation.docx";

                object oMissing = System.Reflection.Missing.Value;

                word.Visible = false;
                word.ScreenUpdating = false;
                FileInfo wordFile = new FileInfo(RutaPlantilla);

                // Cast as Object for word Open method
                Object filename = (Object)wordFile.FullName;

                // Use the dummy value as a placeholder for optional arguments
                Word.Document doc = word.Documents.Open(ref filename, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                try
                {
                    doc.Activate();

                    Word.Words wds = doc.Sections[1].Range.Words;

                    QuoteFileModel q = GetQuotationData(modelo.itemId);

                    FindAndReplace(word, "<<Cotización_Número>>", modelo.itemId);
                    FindAndReplace(word, "<<Fecha_generación>>", q.cotizacionFecha);
                    FindAndReplace(word, "<<Nombre_empresa>>", q.clienteNombre);
                    FindAndReplace(word, "<<Nombre_contacto>>", q.clienteContacto);
                    FindAndReplace(word, "<<Estado_Oficina_Engenium_Capital >>", q.clienteOficina);
                    FindAndReplace(word, "<<Nombre_Vendor>>", q.vendorNombre);
                    FindAndReplace(word, "<<Producto>>", q.producto);
                    FindAndReplace(word, "<<Costo_del_equipo>>",
                        String.Format("{0:C2} ({1}) {2}",
                        q.activosCosto,
                        CurrencyFormat.NumeroALetras(q.activosCosto.ToString()),
                        q.activosMoneda));

                    FindAndReplace(word, "<<Comisión_por_apertura>>", q.comisionApertura);
                    FindAndReplace(word, "<<Pagos_anticipados>>", q.pagosAnticipados);
                    FindAndReplace(word, "<<Depósito_de_seguridad>>", q.depositoSeguridad);
                    FindAndReplace(word, "<<Opción_de_compra>>", q.opcionCompra);
                    FindAndReplace(word, "<<Tipo_Downpayment>>", q.tipoDownpayment);
                    FindAndReplace(word, "<<Downpayment>>", q.enganche);
                    FindAndReplace(word, "<<Periodo_gracia_capital>>", q.periodoGracia);
                    FindAndReplace(word, "<<Vigencia_cotización>>", q.cotizacionVigencia + " días naturales.");
                    FindAndReplace(word, "<<Rentas_Pagos>>", q.cotizacionPagos);
                    FindAndReplace(word, "<<Cobertura_seguro>>", q.seguroPoliza);
                    FindAndReplace(word, "<<Nombre_Vendedor_Vendor>>", q.vendedorNombre);
                    FindAndReplace(word, "<<Correo_Vendedor_Vendor>>", q.vendedorCorreo);
                    FindAndReplace(word, "<<tasa_interes>>", q.tasaInteres);

                    // Tabla de equipos
                    Rel_QuotationAsset relQA = new Rel_QuotationAsset()
                    {
                        QuotationAsset_QuotationId = modelo.itemId
                    };
                    Rel_QuotationAssetCollection lstRelQA = relQA.GetCollectionByData();

                    Word.Table tbQuote = doc.Tables[1];
                    Word.Cell cQuote = tbQuote.Rows[1].Cells[1];
                    Word.Range ranQuote = cQuote.Range;
                    int numTb = ranQuote.Tables.Count;

                    int colTbQuote = tbQuote.Tables[1].Columns.Count;
                    Word.Table tbActivo = tbQuote.Tables[1];

                    foreach (Rel_QuotationAsset objQA in lstRelQA)
                    {
                        int rowTbQuote = lstRelQA.IndexOf(objQA) + 1;
                        tbQuote.Tables[1].Rows.Add();

                        Cat_Asset entAsset = new Cat_Asset()
                        {
                            AssetId = objQA.QuotationAsset_AssetId
                        };
                        entAsset = entAsset.GetFirtByData();

                        for (int i = 0; i <= colTbQuote; i++)
                        {
                            Word.Cell cell = tbQuote.Tables[1].Cell(rowTbQuote, i);

                            switch (i)
                            {
                                case 1:
                                    cell.Range.Text = objQA.QuotationAsset_Quantity.ToString();
                                    break;
                                case 2:
                                    cell.Range.Text = entAsset.Asset_Model;
                                    break;
                                case 3:
                                    cell.Range.Text = String.Format("{0} {1:C2}", q.activosMoneda, objQA.QuotationAsset_Amount);
                                    break;
                            }
                        }

                    }

                    // Tabla serivicios
                    List<ServicioModel> lstServicio = GetServices(q.programaId);

                    Word.Table tbServ = tbQuote.Tables[2];
                    int colTbServ = tbQuote.Tables[2].Columns.Count;

                    foreach (ServicioModel objServicio in lstServicio)
                    {
                        int rowTbServ = lstServicio.IndexOf(objServicio) + 1;
                        tbQuote.Tables[2].Rows.Add();

                        for (int i = 1; i <= colTbServ; i++)
                        {
                            Word.Cell cell = tbQuote.Tables[2].Cell(rowTbServ, i);

                            switch (i)
                            {
                                case 1:
                                    cell.Range.Text = objServicio.descripcion;
                                    break;
                                case 2:
                                    cell.Range.Text = String.Format("{0:C2}", objServicio.costo);
                                    break;
                            }
                        }
                    }

                    //// Tabla amortización
                    Det_QuotationDetail detQout = new Det_QuotationDetail()
                    {
                        QuotationDetail_QuotationId = modelo.itemId
                    };
                    Det_QuotationDetailCollection lstDetQuot = detQout.GetCollectionByData();

                    Word.Table tbAmortiza = doc.Tables[4];
                    tbAmortiza.Range.Font.Size = 8;
                    int colTb = tbAmortiza.Columns.Count;

                    foreach (Det_QuotationDetail objDQ in lstDetQuot)
                    {
                        if (objDQ.QuotationDetail_Period > 0)
                        {
                            int rowTb = lstDetQuot.IndexOf(objDQ) + 2;
                            tbAmortiza.Rows.Add();

                            for (int i = 0; i <= colTb; i++)
                            {
                                Word.Cell cell = doc.Tables[4].Cell(rowTb, i);

                                switch (i)
                                {
                                    case 1:
                                        cell.Range.Text = objDQ.QuotationDetail_Period.ToString();
                                        break;
                                    case 2:
                                        cell.Range.Text = String.Format("{0:C2}", objDQ.QuotationDetail_Balance);
                                        break;
                                    case 3:
                                        cell.Range.Text = String.Format("{0:C2}", objDQ.QuotationDetail_Capital);
                                        break;
                                    case 4:
                                        cell.Range.Text = String.Format("{0:C2}", objDQ.QuotationDetail_Interes);
                                        break;
                                    case 5:
                                        cell.Range.Text = String.Format("{0:C2}", objDQ.QuotationDetail_Subtotal);
                                        break;
                                    case 6:
                                        cell.Range.Text = String.Format("{0:C2}", objDQ.QuotationDetail_Insurance);
                                        break;
                                    case 7:
                                        cell.Range.Text = String.Format("{0:C2}", objDQ.QuotationDetail_Iva);
                                        break;
                                    case 8:
                                        cell.Range.Text = String.Format("{0:C2}", objDQ.QuotationDetail_PaymentIva);
                                        break;
                                    default:
                                        cell.Range.Text = String.Format("{0:C2}", objDQ.QuotationDetail_PaymentIva);
                                        break;
                                }

                                cell.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                            }
                        }
                    }

                    int filas = doc.Tables[1].Rows.Count;

                    if (q.opcionCompra.Equals("0 %") || q.producto.Equals("Crédito con tasa fija."))
                    {
                        doc.Tables[1].Rows[10].Delete();
                    }
                    if (q.depositoSeguridad.Equals("0 %"))
                    {
                        doc.Tables[1].Rows[9].Delete();
                    }
                    if (q.periodoGracia.Equals("0 mes(es)"))
                    {
                        doc.Tables[1].Rows[8].Delete();
                    }
                    if (q.enganche.Equals("0 % - $0.00"))
                    {
                        doc.Tables[1].Rows[7].Delete();
                    }
                    if (q.comisionApertura.Equals("0 %"))
                    {
                        doc.Tables[1].Rows[6].Delete();
                    }
                    if (lstServicio.Count == 0)
                    {
                        doc.Tables[1].Rows[5].Delete();
                    }

                    doc.Tables[3].Range.InsertBreak(Word.WdBreakType.wdPageBreak);
                    doc.Tables[5].Range.InsertBreak(Word.WdBreakType.wdPageBreak);

                    string nameQuot = "Quot" + modelo.itemId;
                    object outputFileName = wordFile.FullName.Replace("DocTemplate", "FilesTemp").Replace("PRM_Quotation", nameQuot).Replace(".docx", ".pdf");
                    object fileFormat = Word.WdSaveFormat.wdFormatPDF;

                    // Save document into PDF Format
                    doc.SaveAs(ref outputFileName,
                        ref fileFormat, ref oMissing, ref oMissing,
                        ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                        ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                        ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                    //Show file
                    var request = HttpContext.Current.Request;
                    var pathTmp = string.Format("{0}://{1}", request.Url.Scheme, request.Url.Authority);
                    pathTmp += "/FilesTemp/" + nameQuot + ".pdf";

                    modelo.CargadoCorrecto = true;
                    modelo.Mensajes = pathTmp;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    // Close the Word document, but leave the Word application open.
                    // doc has to be cast to type _Document so that it will find the
                    // correct Close method.                
                    object saveChanges = Word.WdSaveOptions.wdDoNotSaveChanges;
                    ((Word._Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);
                    doc = null;

                    // word has to be cast to type _Application so that it will find
                    // the correct Quit method.
                    ((Word._Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
                    word = null;
                }

            }
            catch (Exception ex)
            {
                modelo.CargadoCorrecto = false;
                modelo.Mensajes = ex.Message;
            }
            finally
            {

            }
            return modelo;
        }

        private static List<ServicioModel> GetServices(int programId)
        {
            List<ServicioModel> lstServicio = new List<ServicioModel>();

            try
            {
                Rel_ProgramAssetService relPAS = new Rel_ProgramAssetService()
                {
                    ProgramAssetService_ProgramAssetId = programId
                };
                Rel_ProgramAssetServiceCollection lstPAS = relPAS.GetCollectionByData();

                Rel_ProgramAssetService auxPAS = new Rel_ProgramAssetService();
                Cat_AssetService auxService = new Cat_AssetService();
                ServicioModel auxServModel = new ServicioModel();
                int cantServicio = 0;

                foreach (Rel_ProgramAssetService objPAS in lstPAS.OrderBy(x => x.ProgramAssetService_AssetServiceId))
                {
                    ServicioModel objServicio = new Models.ServicioModel();

                    Cat_AssetService entAS = new Cat_AssetService()
                    {
                        AssetServiceId = objPAS.ProgramAssetService_AssetServiceId
                    };
                    entAS = entAS.GetFirtByData();

                    objServicio.servicioId = entAS.AssetServiceId.Value;
                    objServicio.descripcion = entAS.AssetService_Description;

                    if (objPAS.ProgramAssetService_AssetServiceId != auxPAS.ProgramAssetService_AssetServiceId)
                    {
                        cantServicio = 1;

                        objServicio.cantidad = cantServicio;
                        objServicio.costo = entAS.AssetService_Cost.Value;

                        auxPAS = objPAS;
                        auxService = entAS;
                    }
                    else
                    {
                        cantServicio++;

                        objServicio.cantidad = cantServicio;
                        objServicio.costo = auxService.AssetService_Cost.Value + entAS.AssetService_Cost.Value;

                        auxService.AssetService_Cost = objServicio.costo;
                    }

                    auxServModel = objServicio;

                    if (lstServicio.Where(x => x.servicioId == objServicio.servicioId).ToList().Count > 0)
                    {
                        int iServ = lstServicio.IndexOf(lstServicio.Find(x => x.servicioId == objServicio.servicioId));
                        lstServicio[iServ] = objServicio;
                    }
                    else
                    {
                        lstServicio.Add(objServicio);
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return lstServicio;
        }
        private static QuoteFileModel GetQuotationData(int quoteId)
        {
            QuoteFileModel quoteData = new Models.QuoteFileModel();

            try
            {
                Ope_Quotation entQuot = new Ope_Quotation()
                {
                    QuotationId = quoteId
                };
                entQuot = entQuot.GetFirtByData();

                quoteData.cotizacionFecha = entQuot.Quotation_Date.Value.ToString("dddd, dd MMMM yyyy", CultureInfo.CreateSpecificCulture("es-MX"));
                quoteData.clienteNombre = entQuot.Quotation_CustomerName;
                quoteData.clienteOficina = entQuot.Quotation_CustomerLocation;
                quoteData.clienteContacto = entQuot.Quotation_CustomerContact;
                quoteData.activosCosto = entQuot.Quotation_Amount.Value;
                quoteData.activosMoneda = entQuot.Quotation_Currency;
                quoteData.tasaInteres = Math.Round(entQuot.Quotation_IRR.Value, 2).ToString() + " %";

                Cat_FinancialType entProducto = new Cat_FinancialType()
                {
                    FinancialType_Description = entQuot.Quotation_FinancialType
                };
                entProducto = entProducto.GetFirtByData();

                quoteData.producto = entProducto.FinancialType_Description.Equals("A. Puro") ? entProducto.FinancialType_Text + " con rentas fijas." : entProducto.FinancialType_Text + " con tasa fija.";
                quoteData.tipoDownpayment = entProducto.FinancialType_Description.Equals("A. Puro") ? "Renta anticipada" : "Enganche";
                quoteData.cotizacionPagos = entQuot.Quotation_FinancialTerm;

                Ope_Program entProgram = new Ope_Program()
                {
                    ProgramId = entQuot.Quotation_ProgramId
                };
                entProgram = entProgram.GetFirtByData();

                quoteData.programaId = entProgram.ProgramId.Value;
                quoteData.programaNombre = entProgram.Program_Description;
                quoteData.pagosAnticipados = entProgram.Program_Timing;
                quoteData.cotizacionVigencia = entProgram.Program_QuoteValidity.ToString();

                Cat_Vendor entVendor = new Cat_Vendor()
                {
                    VendorId = entProgram.Program_VendorId
                };
                entVendor = entVendor.GetFirtByData();

                quoteData.vendorNombre = entVendor.Vendor_Name;

                Ope_QuotInput entQInput = new Ope_QuotInput()
                {
                    QuotInput_QuotationId = quoteId
                };
                entQInput = entQInput.GetFirtByData();

                var comAperturaAmount = entQuot.Quotation_Amount * (entQInput.QuotInput_OpFee.Value / 100);
                var opCompraAmount = entQuot.Quotation_Amount * (entQInput.QuotInput_PurchaseOption.Value / 100);

                quoteData.comisionApertura = String.Format("{0} % - {1:C} ({2}) {3} más IVA.", 
                    entQInput.QuotInput_OpFee, 
                    comAperturaAmount,
                    CurrencyFormat.NumeroALetras(comAperturaAmount.ToString()),
                    entQuot.Quotation_Currency);
                quoteData.periodoGracia = entQInput.QuotInput_GraceMonths.ToString() + " mes(es)";
                quoteData.depositoSeguridad = entQInput.QuotInput_SecurityDeposit.ToString() + " %";
                quoteData.opcionCompra = String.Format("{0} % - {1:C} ({2}) {3} más IVA.",
                    entQInput.QuotInput_OpFee, 
                    opCompraAmount,
                    CurrencyFormat.NumeroALetras(opCompraAmount.ToString()),
                    entQuot.Quotation_Currency);

                double montoEnganche = entQuot.Quotation_Amount.Value * (entQInput.QuotInput_Downpayment.Value / 100);
                quoteData.enganche = entQInput.QuotInput_Downpayment.ToString() + " % - " + montoEnganche.ToString("C");

                Rel_ProgramAsset relPA = new Rel_ProgramAsset()
                {
                    ProgramAsset_ProgramId = entProgram.ProgramId
                };
                relPA = relPA.GetFirtByData();

                Cat_Coverage entCover = new Cat_Coverage()
                {
                    CoverageId = relPA.ProgramAsset_CoverageId
                };
                entCover = entCover.GetFirtByData();

                if (entCover != null)
                {
                    quoteData.seguroPoliza = entCover.Coverage_Description;
                }
                else
                {
                    quoteData.seguroPoliza = "No Aplica.";
                }

                Cat_User entVendedor = new Cat_User()
                {
                    UserId = entQuot.Quotation_UserId
                };
                entVendedor = entVendedor.GetFirtByData();

                quoteData.vendedorNombre = entVendedor.User_FirstName + " " + entVendedor.User_LastName;
                quoteData.vendedorCorreo = entVendedor.User_Email;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return quoteData;
        }

        private static void FindAndReplace(Word.Application WordApp, object findText, object replaceWithText)
        {
            object matchCase = true;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object nmatchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue;
            object replaceAll = Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll;
            WordApp.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord, ref matchWildCards, ref matchSoundsLike,
            ref nmatchAllWordForms, ref forward,
            ref wrap, ref format, ref replaceWithText,
            ref replaceAll, ref matchKashida,
            ref matchDiacritics, ref matchAlefHamza,
            ref matchControl);
        }

        #endregion

        public static Ope_Visual GetVisual()
        {
            try
            {
                Ope_Visual entLook = new Ope_Visual();

                if (HttpContext.Current.Session["objUser"] != null)
                {
                    Cat_User objUser = (Cat_User)HttpContext.Current.Session["objUser"];

                    entLook.Visual_ProgramId = objUser.User_ProgramId.Value;
                    entLook = entLook.GetFirtByData();
                }

                return entLook;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        #region GetDataListAvaliable

        public static List<DataList> GetListValues(string settingKey, string sub = "")
        {
            //GetConfiguration();

            List<DataList> resp = new List<DataList>();
            List<String> valores = ConfigurationManager.AppSettings[settingKey].ToString().Split(',').ToList();
            int valId = 1;

            foreach (String val in valores)
            {
                resp.Add(new DataList { Value = val, Text = sub.Equals("") ? val : val + sub, Selected = false });
                valId++;
            }

            return resp;

        }
        #endregion

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source?.IndexOf(toCheck, comp) >= 0;
        }

        public static void GetConfiguration()
        {
            try
            {
                System.Configuration.Configuration configFile = null;
                if (System.Web.HttpContext.Current != null)
                {
                    configFile = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
                }
                else
                {
                    configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                }

                var settingsApp = configFile.AppSettings.Settings;
                string key = string.Empty;

                Set_Setting entSet = new Set_Setting();
                entSet.Setting_Active = true;
                Set_SettingCollection settingsDB = entSet.GetCollectionByData();

                foreach (Set_Setting setting in settingsDB)
                {
                    foreach (KeyValueConfigurationElement keyApp in settingsApp)
                    {
                        if (keyApp.Key.Equals(setting.Setting_Key))
                        {
                            key = setting.Setting_Key;
                            settingsApp[key].Value = setting.Setting_Value;
                        }
                    }
                }

                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Cálculo de seguro basado en matríz de factores de coberturas.
        /// </summary>
        public static double CalculateInsuranceAmount(QuotationModel dataProgram, QuotationDataModel dataPlazo)
        {
            double amountIns = 0.0;

            try
            {    
                EquipoModel equipo = dataProgram.LstEquipos.First();

                List<Cat_Asset> lstAsset = new List<Cat_Asset>();
                foreach (EquipoModel eq in dataProgram.LstEquipos)
                {
                    Cat_Asset asset = new Cat_Asset
                    {
                        AssetId = eq.ProductoId
                    };
                    asset = asset.GetFirtByData();

                    lstAsset.Add(asset);

                    Rel_ProgramAsset relProgAsset = new Rel_ProgramAsset()
                    {
                        ProgramAsset_ProgramId = dataProgram.Programa.ProgramId,
                        ProgramAsset_AssetId = eq.ProductoId
                    };
                    relProgAsset = relProgAsset.GetFirtByData();

                    Cat_Coverage entCover = new Cat_Coverage()
                    {
                        CoverageId = relProgAsset.ProgramAsset_CoverageId
                    };
                    entCover = entCover.GetFirtByData();

                    /*
                     * Se deja de utilizar el factor de plazo, por instrucción de Narain. En su lugar se utiliza el factor de la cobertura.
                    if (entCover != null)
                    {
                        Rel_CoverageFinancialTerm relCoverPlazo = new Rel_CoverageFinancialTerm()
                        {
                            CoverageFinancialTerm_CoverageId = entCover.CoverageId,
                            CoverageFinancialTerm_FinancialTermId = Convert.ToInt32(dataPlazo.plazoId)
                        };
                        relCoverPlazo = relCoverPlazo.GetFirtByData();
                    }
                    */

                    double insFactor = Convert.ToDouble(ConfigurationManager.AppSettings["Insurance_Factor"]);
                    double coverFactor = entCover != null ? entCover.Coverage_Factor.Value : 0.0;

                    // Por excel Montse indica que se multiplica x 2 un monto capturado
                    // El valor más aproximado para lograr 32,000 del ejemplo es justo el valor del factor que proporcina Nara
                    // Se establece la variable de configuración de 2.5 a 2 para lograr el x2 solicitado
                    //amountIns += coverFactor * eq.Precio * insFactor;//* relCoverPlazo.CoverageFinancialTerm_Factor.Value;

                    //amount += entActivo.Asset_CurrencyId == 1 && model.monedaId.Equals("2") ?
                    //    activo.Precio * entTC.ExchangeRate_MXN.Value :
                    //    entActivo.Asset_CurrencyId == 2 && model.monedaId.Equals("1") ?
                    //    activo.Precio / entTC.ExchangeRate_MXN.Value :
                    //    activo.Precio;

                    Ope_ExchangeRate entTC = Ope_ExchangeRate.GetAll().OrderByDescending(x => x.ExchangeRate_Date).FirstOrDefault();

                    var montoEquipo = asset.Asset_CurrencyId == 1 && dataPlazo.monedaId.Equals("2") ?
                        eq.Precio * entTC.ExchangeRate_MXN.Value :
                        asset.Asset_CurrencyId == 2 && dataPlazo.monedaId.Equals("1") ?
                        eq.Precio / entTC.ExchangeRate_MXN.Value :
                        eq.Precio;

                    amountIns += montoEquipo * coverFactor;
                }

                dataProgram.CargadoCorrecto = true;
            }
            catch(Exception ex)
            {
                dataProgram.CargadoCorrecto = false;
                dataProgram.Mensajes = ex.Message;
            }
            return amountIns;
        }

        /// <summary>
        /// Cálculo de seguro basado iteraciones similar a los archivos Excel.
        /// </summary>
        public static QuotationDataModel CalculateInsurance(QuotationDataModel model,
            double amount, double target,
            Ope_Program program)
        {
            try
            {
                Cat_FinancialTerm entPlazo = new Cat_FinancialTerm()
                {
                    FinancialTermId = Convert.ToInt32(model.plazoId)
                };
                entPlazo = entPlazo.GetFirtByData();

                Ope_ProgramInput entInput = new Ope_ProgramInput()
                {
                    ProgramInput_ProgramId = program.ProgramId
                };
                entInput = entInput.GetFirtByData();

                int graceMonths = entInput.ProgramInput_GraceMonthsUse.Value ?
                    Convert.ToInt32(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "GraceMonths", StringComparison.CurrentCultureIgnoreCase)).fieldValue) : 0;

                double[] arraySec = new double[entPlazo.FinancialTerm_Value.Value + 1];
                double guessValue = Convert.ToDouble(ConfigurationManager.AppSettings["Profit_IRRGuess"]);

                double firstMonth = (-amount) + (double)model.arrayPagos.GetValue(0);

                // Se cambia libreria Microsoft.VisualBasic por NPOI
                //double insRent = Financial.Pmt((model.irr / 100) / 12, entPlazo.FinancialTerm_Value.Value - graceMonths, (-amount * 2), 0, 0);

                double insRent = NPOI.SS.Formula.Functions.Finance.PMT((model.irr / 100) / 12, entPlazo.FinancialTerm_Value.Value - graceMonths, (-amount * 2), 0, 0);

                arraySec[0] = firstMonth;
                for (int i = 1; i < model.arrayPagos.Length; i++)
                {
                    arraySec[i] = insRent;
                }

                model.arrayInsurance = arraySec;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;
        }

        public static QuotationDataModel CalculateQuotation(QuotationDataModel model, 
            double amount, string segment, double resVal, double percTarget,
            Ope_Program program, Cat_FinancialType product)//, double insCost, bool insFinancied)
        {
            try
            {
                double paymentInc = 0.0;
                double percentage = 0.01;

                // validate wether the input is used or not
                Ope_ProgramInput entInput = new Ope_ProgramInput()
                {
                    ProgramInput_ProgramId = program.ProgramId
                };
                entInput = entInput.GetFirtByData();

                // Purchase Option solo aplica para A. Financiero
                double purchaseOp = product.FinancialType_Description.Equals("A. Financiero") ? entInput.ProgramInput_PurchaseOptionUse.Value ?
                    (Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "PurchaseOption", StringComparison.CurrentCultureIgnoreCase)).fieldValue) / 100) * amount : 0 : 0;
                // Balloon Payment solo aplica para Crédito
                double balloonPay = product.FinancialType_Description.Equals("Crédito") ? entInput.ProgramInput_BalloonPaymentUse.Value ?
                    (Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "BalloonPayment", StringComparison.CurrentCultureIgnoreCase)).fieldValue) / 100) * amount : 0 : 0;
                double downPay = entInput.ProgramInput_DownpaymentUse.Value ?
                    (Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "Downpayment", StringComparison.CurrentCultureIgnoreCase)).fieldValue) / 100) * amount : 0;
                double secDep = entInput.ProgramInput_SecurityDepositUse.Value ?
                    (Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "SecurityDeposit", StringComparison.CurrentCultureIgnoreCase)).fieldValue) / 100) * amount : 0;
                double opFee = entInput.ProgramInput_OpFeeUse.Value ?
                    (Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "OpFee", StringComparison.CurrentCultureIgnoreCase)).fieldValue) / 100) * amount : 0;
                double blindDisc = entInput.ProgramInput_BlindDiscountUse.Value ?
                    (Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "BlindDiscount", StringComparison.CurrentCultureIgnoreCase)).fieldValue) / 100) * amount : 0;
                double refFee = entInput.ProgramInput_ReferalFeeUse.Value ?
                    (Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "ReferalFee", StringComparison.CurrentCultureIgnoreCase)).fieldValue) / 100) * amount : 0;
                double legalFees = entInput.ProgramInput_LegalFeesUse.Value ?
                    model.monedaId == "1" ?
                    Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "LegalFees", StringComparison.CurrentCultureIgnoreCase)).fieldValue) :
                    Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "LegalFees", StringComparison.CurrentCultureIgnoreCase)).fieldValue) : 0;
                double addCosts = entInput.ProgramInput_AdditionalCostsUse.Value ?
                    model.monedaId == "1" ?
                    Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "AdditionalCosts", StringComparison.CurrentCultureIgnoreCase)).fieldValue) :
                    Convert.ToDouble(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "AdditionalCosts", StringComparison.CurrentCultureIgnoreCase)).fieldValue) : 0;
                int graceMonths = entInput.ProgramInput_GraceMonthsUse.Value ?
                    Convert.ToInt32(model.lstInputs.Find(x => UtilitiesGeneral.Contains(x.fieldName, "GraceMonths", StringComparison.CurrentCultureIgnoreCase)).fieldValue) : 0;

                double firstMonth = (-amount) + downPay + secDep + opFee + blindDisc + (-refFee) + (-legalFees) + (-addCosts);
                double lastMonth = 0.0;
                double irr = 0.0;

                Cat_FinancialTerm entPlazo = new Cat_FinancialTerm()
                {
                    FinancialTermId = Convert.ToInt32(model.plazoId)
                };
                entPlazo = entPlazo.GetFirtByData();

                double[] arrayPayment = new double[(entPlazo.FinancialTerm_Value.Value + 1) - graceMonths];
                double guessValue = Convert.ToDouble(ConfigurationManager.AppSettings["Profit_IRRGuess"]);
                arrayPayment[0] = firstMonth;

                double irrAux = 0.0;
                double paymentAux = 0.0;

                while (irr < percTarget)
                {
                    irrAux = irr;
                    paymentAux = paymentInc;

                    paymentInc = amount * percentage;

                    // Balloon payment para producto: Credito, para A.Financiero es Opcion de compra
                    lastMonth = paymentInc + resVal + (-secDep) + (product.FinancialType_Description.Equals("Crédito") ? balloonPay: purchaseOp);

                    for (int i = 1; i < entPlazo.FinancialTerm_Value - graceMonths; i++)
                    {
                        arrayPayment[i] = paymentInc;
                    }

                    arrayPayment[entPlazo.FinancialTerm_Value.Value - graceMonths] = lastMonth;

                    /*
                     * Referencia a libreria Microsoft.VisualBasic se reemplaza por libreria NPOI.
                     * Se mantiene para futuras referencias
                     * 
                    irr = Financial.IRR(ref arrayPayment, guessValue);
                    irr *= 12;
                    irr *= 100;
                    */

                    irr = NPOI.SS.Formula.Functions.Irr.irr(arrayPayment, guessValue);
                    irr *= 12;
                    irr *= 100;

                    if (irr > percTarget)
                    {
                        irr = irrAux;
                        paymentInc = paymentAux;

                        // Balloon payment para producto: Credito, para A.Financiero es Opcion de compra
                        lastMonth = paymentInc + resVal + (-secDep) + (product.FinancialType_Description.Equals("Crédito") ? balloonPay : purchaseOp);

                        for (int i = 1; i < entPlazo.FinancialTerm_Value - graceMonths; i++)
                        {
                            arrayPayment[i] = paymentInc;
                        }

                        arrayPayment[entPlazo.FinancialTerm_Value.Value - graceMonths] = lastMonth;

                        break;
                    }

                    percentage += 0.00001;
                    percentage = Math.Round(percentage, 5);
                }

                model.arrayPagos = arrayPayment;
                model.pagoMensual = Math.Round(paymentInc, 2);
                model.downpayment = downPay;
                model.balloonpayment = balloonPay;
                model.graceMonths = graceMonths;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public static QuotationDataModel CalculateInterestRate(QuotationDataModel model, double amount)
        {
            try
            {
                double guessValue = Convert.ToDouble(ConfigurationManager.AppSettings["Profit_IRRGuess"]);
                double[] arrayPayment = new double[model.arrayPagos.Length];
                int index = 0;

                foreach (var payment in model.arrayPagos)
                {
                    arrayPayment[index] =
                        index == 0 ? (-amount) + model.downpayment :
                        index == model.arrayPagos.Length ? model.pagoMensual + model.balloonpayment :
                        model.productoId.Equals("4") ? (double)payment : model.pagoMensual;
                        // crédito se aplica el valor que incluye balloon, otro se aplica solo el payment
                    index++;
                }

                /*
                 * Referencia a libreria Microsoft.VisualBasic se reemplaza por libreria NPOI.
                 * Se mantiene para futuras referencias
                 * 
                double interestRate = Financial.IRR(ref arrayPayment, guessValue);
                double irr = interestRate * 12;
                */

                double interestRate = NPOI.SS.Formula.Functions.Irr.irr(arrayPayment, guessValue);
                double irr = interestRate * 12;
                irr *= 100;
                model.irr = irr;
                model.arrayPagos = arrayPayment;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public static QuotationIRRModel GetQuotationIRR(QuotationIRRModel data)
        {
            //try
            //{
            //    double multiplicador = Convert.ToInt32(ConfigurationManager.AppSettings["Financial_Multiplier"]);
            //    double divResidual = Convert.ToInt32(ConfigurationManager.AppSettings["Financial_DivisorResidual"]);

            //    double residuo = Math.Floor(data.rate * multiplicador) % divResidual;
            //    double res_cero = Math.Floor(data.rate * multiplicador) / multiplicador;
            //    double res_no = (Math.Floor(data.rate * multiplicador) + (divResidual - (Math.Floor(data.rate * multiplicador) % divResidual))) / multiplicador;

            //    double interes = residuo == 0 ? res_cero : res_no;

            //    switch (data.producto.FinancialType_Description)
            //    {
            //        case "A. Puro":
            //            interes += data.lease;
            //            break;
            //        case "A. Financiero":
            //            interes = data.lease;
            //            break;
            //        case "Crédito":
            //            break;
            //    }

            //    TablaAmortizacion ta = new Models.TablaAmortizacion()
            //    {
            //        FinanciamientoTipo = data.producto.FinancialType_Description,
            //        //Equipos = equipo.Producto,
            //        Monto = data.monto,
            //        Moneda = data.moneda.Currency_Code,
            //        InteresTasa = interes * 100,
            //        PlazoMeses = data.plazo.FinancialTerm_Value.Value,
            //        //PlazoTipo = data.PagoPeriodo.PaymentTerm_Description,
            //        //TODO: verificar su uso, sino quitar
            //        /*
            //        ComisionApertura = entRentabilidad.Profitability_OpenCommission.Value,
            //        RentasDeposito = entRentabilidad.Profitability_SecDepositQuantity.Value,
            //        OpcionCompra = entRentabilidad.Profitability_PurchaseOption.ToString()
            //        */
            //    };

            //    double saldo_insoluto = ta.Monto;

            //    for (int i = 1; i <= ta.PlazoMeses; i++)
            //    {
            //        //mensual=12, trimestral=4, semestral=2
            //        double ir = (interes / data.periodo.PaymentTerm_YearTimes.Value);

            //        //mensual=1, trimestral=3, semestral=6
            //        int np = ta.PlazoMeses / data.periodo.PaymentTerm_Months.Value;

            //        MensualidadAmortiza tablaMens = new Models.MensualidadAmortiza();

            //        switch (data.producto.FinancialType_Description)
            //        {
            //            case "A. Puro":
            //                double renta_mensual = Financial.Pmt(ir, np, -ta.Monto, data.residual_value * ta.Monto, DueDate.EndOfPeriod);
            //                double iva = Convert.ToDouble(ConfigurationManager.AppSettings["Global_IVA"]);

            //                tablaMens.Periodo = i;
            //                tablaMens.RentaMonto = renta_mensual;
            //                tablaMens.IvaMonto = renta_mensual * iva;
            //                tablaMens.PagoTotal = renta_mensual + (renta_mensual * iva);

            //                break;
            //            case "A. Financiero":
            //                double renta_nivelada = Financial.Pmt(ir, np, -ta.Monto, 0, DueDate.EndOfPeriod);
            //                double intereses = (saldo_insoluto * interes) / data.periodo.PaymentTerm_YearTimes.Value;

            //                tablaMens.Periodo = i;
            //                tablaMens.Intereses = intereses;
            //                tablaMens.RentaMonto = renta_nivelada - intereses;
            //                tablaMens.IvaMonto = tablaMens.Intereses + tablaMens.RentaMonto;
            //                tablaMens.PagoTotal = saldo_insoluto - tablaMens.RentaMonto;

            //                saldo_insoluto = tablaMens.PagoTotal;

            //                break;
            //            case "Crédito":
            //                break;
            //        }

            //        ta.TablaRegistros.Add(tablaMens);
            //    }

            //    data.amortizaTable = ta;

            //    //Calculate IRR

            //    MensualidadAmortiza firstMonth = data.amortizaTable.TablaRegistros.First();
            //    MensualidadAmortiza lastMonth = data.amortizaTable.TablaRegistros.Last();

            //    firstMonth.PagoTotal = data.firstMonth;
            //    lastMonth.PagoTotal += data.lastMonth;

            //    data.amortizaTable.TablaRegistros[0] = firstMonth;
            //    data.amortizaTable.TablaRegistros[ta.PlazoMeses - 1] = lastMonth;

            //    double[] arrayPayment = data.amortizaTable.TablaRegistros.Select(x => x.PagoTotal).ToArray();
            //    double guessValue = Convert.ToDouble(ConfigurationManager.AppSettings["Profit_IRRGuess"]);
            //    Int32 irrMultiplier = Convert.ToInt32(ConfigurationManager.AppSettings["Profit_IRRMultiplier"]);

            //    double irr = Financial.IRR(ref arrayPayment, guessValue);
            //    irr *= irrMultiplier;

            //    ProfitInfoModel result = new ProfitInfoModel()
            //    {
            //        profitPercentage = Math.Round(irr * 100, 2),
            //        paymentAmount = Math.Round(data.amortizaTable.TablaRegistros[1].PagoTotal, 2)
            //    };

            //    data.profitInfo = result;
            //    data.profitInfo.CargadoCorrecto = true;

            //}
            //catch(Exception ex)
            //{
            //    throw ex;
            //}

            return data;
        }

        public static QuotationModel GetFinancialValues(QuotationModel data)
        {
            //try
            //{
            //    foreach (Cat_FinancialTerm objPlazo in data.LstFinPlazos)
            //    {
            //        foreach (EquipoModel equipo in data.LstEquipos)
            //        {
            //            Rel_ProgramProduct entProgProd = new Rel_ProgramProduct()
            //            {
            //                ProgramProduct_ProductId = equipo.ProductoId
            //            };
            //            entProgProd = entProgProd.GetFirtByData();

            //            if (entProgProd == null)
            //            {
            //                throw new Exception("No se pudo encontrar la configuración Programa - Producto necesaria.");
            //            }

            //            Rel_ProdCurFinTerm entTerm = new Rel_ProdCurFinTerm()
            //            {
            //                ProdCurFinTerm_CurrencyId = data.FinanciamientoMoneda.CurrencyId,
            //                ProdCurFinTerm_ProductoId = entProgProd.ProgramProduct_ProductId,
            //                ProdCurFinTerm_FinancialTermId = objPlazo.FinancialTermId
            //            };
            //            entTerm = entTerm.GetFirtByData();

            //            if (entTerm == null)
            //            {
            //                throw new Exception("No se pudo encontrar la configuración del plazo selccionado.");
            //            }

            //            Rel_FinTypeCurFinTerm entFinancial = new Rel_FinTypeCurFinTerm()
            //            {
            //                FinTypeCurFinTerm_FinancialTypeId = data.FinanciamientoTipo.FinancialTypeId,
            //                FinTypeCurFinTerm_CurrencyId = data.FinanciamientoMoneda.CurrencyId,
            //                FinTypeCurFinTerm_FinancialTermId = entTerm.ProdCurFinTerm_FinancialTermId.Value
            //            };
            //            entFinancial = entFinancial.GetFirtByData();

            //            if (entFinancial == null)
            //            {
            //                throw new Exception("No se puedieron encontrar los valores de cálculo para los valores seleccionados.");
            //            }

            //            Ope_Profitability entRentabilidad = new Ope_Profitability()
            //            {
            //                Profitability_ProgramId = data.Programa.ProgramId
            //            };
            //            entRentabilidad = entRentabilidad.GetFirtByData();

            //            if (entRentabilidad == null)
            //            {
            //                throw new Exception("No se pudo encontrar la configuración de rentabilidad para el programa: " + data.Programa.Program_Description);
            //            }

            //            double multiplicador = Convert.ToInt32(ConfigurationManager.AppSettings["Financial_Multiplier"]);
            //            double divResidual = Convert.ToInt32(ConfigurationManager.AppSettings["Financial_DivisorResidual"]);

            //            double residuo = Math.Floor(entTerm.ProdCurFinTerm_Rate.Value * multiplicador) % divResidual;
            //            double res_cero = Math.Floor(entTerm.ProdCurFinTerm_Rate.Value * multiplicador) / multiplicador;
            //            double res_no = (Math.Floor(entTerm.ProdCurFinTerm_Rate.Value * multiplicador) + (divResidual - (Math.Floor(entTerm.ProdCurFinTerm_Rate.Value * multiplicador) % divResidual))) / multiplicador;

            //            double interes = residuo == 0 ? res_cero : res_no;
            //            interes += entFinancial.FinTypeCurFinTerm_Lease.Value;

            //            interes = data.FinanciamientoTipo.FinancialType_Group != null && data.FinanciamientoTipo.FinancialType_Group.Equals("A. Financiero") ? entFinancial.FinTypeCurFinTerm_Lease.Value : interes;

            //            TablaAmortizacion ta = new Models.TablaAmortizacion()
            //            {
            //                FinanciamientoTipo = data.FinanciamientoTipo.FinancialType_Description,
            //                Equipos = equipo.Producto,
            //                Monto = equipo.Precio,// * equipo.Cantidad,
            //                Moneda = data.FinanciamientoMoneda.Currency_Code,
            //                InteresTasa = interes * 100,
            //                PlazoMeses = objPlazo.FinancialTerm_Value.Value,
            //                PlazoTipo = data.PagoPeriodo.PaymentTerm_Description,
            //                //TODO: verificar su uso, sino quitar
            //                ComisionApertura = entRentabilidad.Profitability_OpenCommission.Value,
            //                RentasDeposito = entRentabilidad.Profitability_SecDepositQuantity.Value,
            //                OpcionCompra = entRentabilidad.Profitability_PurchaseOption.ToString()
            //            };

            //            double saldo_insoluto = ta.Monto;

            //            for (int i = 1; i <= ta.PlazoMeses; i++)
            //            {
            //                //mensual=12, trimestral=4, semestral=2
            //                double ir = (interes / data.PagoPeriodo.PaymentTerm_YearTimes.Value);

            //                //mensual=1, trimestral=3, semestral=6
            //                int np = ta.PlazoMeses / data.PagoPeriodo.PaymentTerm_Months.Value;

            //                MensualidadAmortiza tablaMens = new Models.MensualidadAmortiza();

            //                switch (data.FinanciamientoTipo.FinancialType_Description)
            //                {
            //                    case "A. Puro":
            //                        double renta_mensual = Financial.Pmt(ir, np, -ta.Monto, entTerm.ProdCurFinTerm_ResidualValue.Value * ta.Monto, DueDate.EndOfPeriod);
            //                        double iva = Convert.ToDouble(ConfigurationManager.AppSettings["Global_IVA"]);

            //                        tablaMens.Periodo = i;
            //                        tablaMens.RentaMonto = renta_mensual;
            //                        tablaMens.IvaMonto = renta_mensual * iva;
            //                        tablaMens.PagoTotal = renta_mensual + (renta_mensual * iva);

            //                        break;
            //                    default:
            //                        double renta_nivelada = Financial.Pmt(ir, np, -ta.Monto, 0, DueDate.EndOfPeriod);
            //                        double intereses = (saldo_insoluto * interes) / data.PagoPeriodo.PaymentTerm_YearTimes.Value;

            //                        tablaMens.Periodo = i;
            //                        tablaMens.Intereses = intereses;
            //                        tablaMens.RentaMonto = renta_nivelada - intereses;
            //                        tablaMens.IvaMonto = tablaMens.Intereses + tablaMens.RentaMonto;
            //                        tablaMens.PagoTotal = saldo_insoluto - tablaMens.RentaMonto;

            //                        saldo_insoluto = tablaMens.PagoTotal;

            //                        break;
            //                }
            //                /*
            //                if (data.FinanciamientoTipo.FinancialType_Description.Equals("A. Financiero"))
            //                {
            //                    double renta_nivelada = Financial.Pmt(ir, np, -ta.Monto, 0, DueDate.EndOfPeriod);
            //                    double intereses = (saldo_insoluto * interes) / data.PagoPeriodo.PaymentTerm_YearTimes.Value;

            //                    tablaMens.Periodo = i;
            //                    tablaMens.Intereses = intereses;
            //                    tablaMens.RentaMonto = renta_nivelada - intereses;
            //                    tablaMens.IvaMonto = tablaMens.Intereses + tablaMens.RentaMonto;
            //                    tablaMens.PagoTotal = saldo_insoluto - tablaMens.RentaMonto;

            //                    saldo_insoluto = tablaMens.PagoTotal;
            //                }
            //                else
            //                {
            //                    double renta_mensual = Financial.Pmt(ir, np, -ta.Monto, entTerm.ProdCurFinTerm_ResidualValue.Value * ta.Monto, DueDate.EndOfPeriod);
            //                    double iva = Convert.ToDouble(ConfigurationManager.AppSettings["Global_Interes"]);

            //                    tablaMens.Periodo = i;
            //                    tablaMens.RentaMonto = renta_mensual;
            //                    tablaMens.IvaMonto = renta_mensual * iva;
            //                    tablaMens.PagoTotal = renta_mensual + (renta_mensual * iva);
            //                }
            //                */
            //                ta.TablaRegistros.Add(tablaMens);
            //            }

            //            data.tablasAmortiza.Add(ta);
            //        }
            //    }

            //    List<TablaAmortizacion> nlstTablaAmortiza = new List<TablaAmortizacion>();

            //    foreach (Cat_FinancialTerm plazo in data.LstFinPlazos)
            //    {
            //        List<TablaAmortizacion> lstTAPlazo = data.tablasAmortiza.Where(x => x.PlazoMeses == plazo.FinancialTerm_Value).ToList();

            //        TablaAmortizacion tamortiza_res = new TablaAmortizacion()
            //        {
            //            Monto = lstTAPlazo.Sum(x => x.Monto),
            //            Equipos = string.Join(", ", lstTAPlazo.Select(x => x.Equipos).ToList()),
            //            FinanciamientoTipo = lstTAPlazo.First().FinanciamientoTipo,
            //            Moneda = lstTAPlazo.First().Moneda,
            //            InteresTasa = lstTAPlazo.First().InteresTasa,
            //            PlazoMeses = lstTAPlazo.First().PlazoMeses,
            //            PlazoTipo = lstTAPlazo.First().PlazoTipo,
            //            ComisionApertura = lstTAPlazo.First().ComisionApertura,
            //            RentasDeposito = lstTAPlazo.First().RentasDeposito,
            //            OpcionCompra = lstTAPlazo.First().OpcionCompra
            //        };

            //        List<MensualidadAmortiza> lstMensAmortiza_res = new List<MensualidadAmortiza>();

            //        for (int i = 1; i <= plazo.FinancialTerm_Value.Value; i++)
            //        {
            //            MensualidadAmortiza mensAmortiza_res = new MensualidadAmortiza();

            //            var rentaMonto = lstTAPlazo.Sum(x => x.TablaRegistros.Where(p => p.Periodo == i).Sum(y => y.RentaMonto));
            //            var ivaMonto = lstTAPlazo.Sum(x => x.TablaRegistros.Where(p => p.Periodo == i).Sum(y => y.IvaMonto));
            //            var pagoTotal = lstTAPlazo.Sum(x => x.TablaRegistros.Where(p => p.Periodo == i).Sum(y => y.PagoTotal));
            //            var intereses = lstTAPlazo.Sum(x => x.TablaRegistros.Where(p => p.Periodo == i).Sum(y => y.Intereses));

            //            mensAmortiza_res.Periodo = i;
            //            mensAmortiza_res.RentaMonto = rentaMonto;
            //            mensAmortiza_res.IvaMonto = ivaMonto;
            //            mensAmortiza_res.PagoTotal = pagoTotal;
            //            mensAmortiza_res.Intereses = intereses;

            //            lstMensAmortiza_res.Add(mensAmortiza_res);
            //        }
                    
            //        tamortiza_res.TablaRegistros = lstMensAmortiza_res;
            //        nlstTablaAmortiza.Add(tamortiza_res);
            //    }
                
            //    data.tablasAmortiza.Clear();
            //    data.tablasAmortiza = nlstTablaAmortiza;
                    
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            return data;
        }

        #region Test Password Encrypt

        private static readonly UTF8Encoding Encoder = new UTF8Encoding();

        public static string Encrypt(string unencrypted)
        {
            if (string.IsNullOrEmpty(unencrypted))
                return string.Empty;

            try
            {
                var encryptedBytes = MachineKey.Protect(Encoder.GetBytes(unencrypted));

                if (encryptedBytes != null && encryptedBytes.Length > 0)
                    return HttpServerUtility.UrlTokenEncode(encryptedBytes);
            }
            catch (Exception)
            {
                return string.Empty;
            }

            return string.Empty;
        }

        public static string Decrypt(string encrypted)
        {
            if (string.IsNullOrEmpty(encrypted))
                return string.Empty;

            try
            {
                var bytes = HttpServerUtility.UrlTokenDecode(encrypted);
                if (bytes != null && bytes.Length > 0)
                {
                    var decryptedBytes = MachineKey.Unprotect(bytes);
                    if (decryptedBytes != null && decryptedBytes.Length > 0)
                        return Encoder.GetString(decryptedBytes);
                }

            }
            catch (Exception ex)
            {
                return string.Empty;
            }

            return string.Empty;
        }
        #endregion

    }
}