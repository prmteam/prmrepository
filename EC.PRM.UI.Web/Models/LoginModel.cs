﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EC.PRM.LogicaNegocio.Entities;

namespace EC.PRM.UI.Web.Models
{
    public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ReturnURL { get; set; }
        public bool isRemember { get; set; }
    }

    public class ChooseProgramModel
    {
        public Ope_ProgramCollection lstProgram { get; set; }
        public Cat_Vendor vendor { get; set; }
        public int programId { get; set; }
    }
}