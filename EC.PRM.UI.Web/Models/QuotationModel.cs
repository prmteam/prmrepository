﻿using EC.PRM.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class QuotationModel : MensajesBaseModel
    {
        public string Evento { get; set; }
        public int CotizacionId { get; set; }
        public List<Cat_Location> LstUbicacion { get; set; }

        public string ClienteNombre { get; set; }
        public string ClienteContacto { get; set; }
        public string ClienteEmail { get; set; }
        public string ClienteUbicacion { get; set; }

        public List<EquipoModel> LstEquipos = new List<EquipoModel>();
        
        public Cat_FinancialType FinanciamientoTipo { get; set; }
        public Cat_FinancialTerm FinanciamientoPlazo { get; set; }
        public Cat_Currency FinanciamientoMoneda { get; set; }

        //public QuotationDataModel Cotizacion { get; set; }

        //TODO: verificar si se requieren las 4 prop anteriores sino, eliminar.
        public Ope_Program Programa { get; set; }

        public bool Delay { get; set; }
        public int DescuentoPorc { get; set; }

        public List<TablaAmortizacion> tablasAmortiza = new List<TablaAmortizacion>();

        public QuotationDataModel calcInfo { get; set; }

        //public List<Cat_FinancialTerm> lstPlazoTarget { get; set; }
        public Ope_QuotInput quotInputInfo { get; set; }
    }

    public class EquipoModel
    {
        public int FamiliaId { get; set; }
        public int ProductoId { get; set; }
        public string Producto { get; set; }
        public double Precio { get; set; }
        public int Cantidad { get; set; }
        public string Moneda { get; set; }
    }

    public class QuotationDataModel : MensajesBaseModel
    {
        public string mostrarCotizacion { get; set; }
        public string productoId { get; set; }
        public string monedaId { get; set; }
        public string plazoId { get; set; }
        public List<FieldData> lstInputs { get; set; }
        public double pagoMensual { get; set; }
        public double irr { get; set; }
        public Array arrayPagos { get; set; }
        public double irrInsurance { get; set; }
        public Array arrayInsurance { get; set; }
        public bool insFinancied { get; set; }
        //public bool purOption { get; set; }

        public double downpayment { get; set; }
        public double balloonpayment { get; set; }
        public int graceMonths { get; set; }
        public string product { get; set; }
    }
}