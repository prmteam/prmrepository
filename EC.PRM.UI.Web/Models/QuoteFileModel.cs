﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class QuoteFileModel : MensajesBaseModel
    {
        public int itemId { get; set; }
        public string rutaPlantillaWord { get; set; }

        public string cotizacionFecha { get; set; }
        public string clienteNombre { get; set; }
        public string clienteOficina { get; set; }
        public string clienteContacto { get; set; }
        public string vendorNombre { get; set; }
        public string producto { get; set; }
        public string activosDesc { get; set; }
        public double activosCosto { get; set; }
        public string activosMoneda { get; set; }
        public string programaNombre { get; set; }
        public string comisionApertura { get; set; }
        public string pagosAnticipados { get; set; }
        public string periodoGracia { get; set; }
        public string depositoSeguridad { get; set; }
        public string opcionCompra { get; set; }
        public string cotizacionVigencia { get; set; }
        public string cotizacionPagos { get; set; }
        public string seguroPoliza { get; set; }
        public string enganche { get; set; }

        public int programaId { get; set; }
        public string vendedorNombre { get; set; }
        public string vendedorCorreo { get; set; }
        public string tipoDownpayment { get; set; }
        public string tasaInteres { get; set; }
    }

    public class ServicioModel
    {
        public int servicioId { get; set; }
        public int cantidad { get; set; }
        public string descripcion { get; set; }
        public double costo { get; set; }
    }
}