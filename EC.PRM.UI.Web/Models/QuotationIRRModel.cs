﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EC.PRM.LogicaNegocio.Entities;

namespace EC.PRM.UI.Web.Models
{
    public class QuotationIRRModel
    {
        public Cat_Currency moneda { get; set; }
        public Cat_FinancialTerm plazo { get; set; }
        public Cat_FinancialType producto { get; set; }
        //public Cat_PaymentTerm periodo { get; set; }
        public double monto { get; set; }
        public double rate { get; set; }
        public double lease { get; set; }
        public double residual_value { get; set; }
        public double firstMonth { get; set; }
        public double lastMonth { get; set; }
        public TablaAmortizacion amortizaTable { get; set; }
        public ProfitInfoModel profitInfo { get; set; }
    }

    public class ProfitInfoModel : MensajesBaseModel
    {
        public double profitPercentage { get; set; }
        public double paymentAmount { get; set; }
    }
}