﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EC.PRM.LogicaNegocio.Entities;

namespace EC.PRM.UI.Web.Models
{
    public class Step2Model : MensajesBaseModel
    {
        public Cat_AssetCollateralCollection lstPFamilia { get; set; }
        public Cat_AssetCollateral familiaSelected { get; set; }
        //public Cat_ProductCollection lstProducto { get; set; }
        public List<Cat_Asset> lstProducto { get; set; }
        public Cat_Asset productoSelected { get; set; }
        public int cantidad { get; set; }
    }

}