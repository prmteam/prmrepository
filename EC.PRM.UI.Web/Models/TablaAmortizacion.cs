﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class TablaAmortizacion
    {
        public string FinanciamientoTipo { get; set; }
        public string Equipos { get; set; }
        public double Monto { get; set; }
        public string Moneda { get; set; }
        public double InteresTasa { get; set; }
        public int PlazoMeses { get; set; }
        public string PlazoTipo { get; set; }
        public double ComisionApertura { get; set; }
        public int RentasDeposito { get; set; }
        public string OpcionCompra { get; set; }
        public List<MensualidadAmortiza> TablaRegistros = new List<MensualidadAmortiza>(); 
    }

    public class MensualidadAmortiza
    {
        public int Periodo { get; set; }

        public double Balance { get; set; }
        public double Capital { get; set; }
        public double Interes { get; set; }
        public double Subtotal { get; set; }
        public double Seguro { get; set; }
        public double Iva { get; set; }
        public double PagoIva { get; set; }

        public double RentaMonto { get; set; }
        public double IvaMonto { get; set; }
        public double PagoTotal { get; set; }
        public double Intereses { get; set; }

    }
}