﻿using EC.PRM.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class DashboardModel : MensajesBaseModel
    {
        public List<DatoGrafico> lstPorcentajeConversion = new List<DatoGrafico>();
        public List<DatoGrafico> lstProcentajeUso = new List<DatoGrafico>();
        public int cotizacionesAbiertas { get; set; }
        public int cotizacionesTotal { get; set; }
        public int cotizacionesConvertidas { get; set; }
        public int cotizacionesGanadas { get; set; }
        public int numVendors { get; set; }
        public int numProgramas { get; set; }
        public int numUsuarios { get; set; }

        public List<Ope_Quotation> lstDetLead = new List<Ope_Quotation>();
        public List<Ope_Quotation> lstDetOps = new List<Ope_Quotation>();
        public List<Ope_Quotation> lstDetClosed = new List<Ope_Quotation>();
        public List<Ejecutivo> lstDetExecutives = new List<Ejecutivo>();
        public List<UsuarioUso> lstDetUso = new List<UsuarioUso>();

        public DataFilterDash datosFiltro = new DataFilterDash();
        public List<Cat_Status> lstQuotStatus { get; set; }
    }

    public class DatoGrafico
    {
        public string Texto { get; set; }
        public double Valor { get; set; }
    }

    public class Ejecutivo
    {
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Perfil { get; set; }
        public string Vendor { get; set; }
    }

    public class UsuarioUso
    {
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Evento { get; set; }
        public DateTime? Fecha { get; set; }
        public string Vendor { get; set; }
    }

    public class DataFilterDash
    {
        public string Vendor { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public string Programa { get; set; }
        public int? IdEstatus { get; set; }
        public string Ejecutivo { get; set; }
        public string Palabra { get; set; }
    }

}