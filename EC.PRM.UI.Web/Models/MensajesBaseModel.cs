﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class MensajesBaseModel
    {
        public bool CargadoCorrecto { get; set; }

        public string Mensajes { get; set; }
    }

    public class MensajesPaginadoModel : MensajesBaseModel
    {
        public int TotalRegistros { get; set; }

        public int PaginaActual { get; set; }
    }
}