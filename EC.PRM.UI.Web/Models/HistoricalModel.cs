﻿using EC.PRM.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class HistoricalModel : MensajesBaseModel
    {
        public List<DataSearch> registrosBusqueda = new List<DataSearch>();
        public DataFilter datosFiltro = new DataFilter();
        public Cat_FinancialTypeCollection productos { get; set; }
        public List<DataList> fechaOpciones = new List<DataList>();
        public Ope_Visual visualInfo { get; set; }
    }

    public class DataSearch
    {
        public int Id { get; set; }
        public string Cliente { get; set; }
        public DateTime Fecha { get; set; }
        public string Producto { get; set; }
        public string Plazos { get; set; }
    }

    public class DataFilter
    {
        public int? Id { get; set; }
        public string Cliente { get; set; }
        public string Fecha { get; set; }
        public int? ProductoId { get; set; }
    }

    /*
    public class FechaOpcion
    {
        public int Value { get; set; }
        public string Text { get; set; }
        public bool Selected { get; set; }
    }
    */
}