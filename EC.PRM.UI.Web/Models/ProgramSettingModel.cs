﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EC.PRM.LogicaNegocio.Entities;

namespace EC.PRM.UI.Web.Models
{
    public class ProgramSettingModel
    {
        public int programaId { get; set; }

        public ProfitSettingModel profitData = new ProfitSettingModel();
        // listas de datos disponibles para captura
        public Cat_VendorCollection lstVendor { get; set; }
        public Cat_FinancialTypeCollection lstProducto { get; set; }
        public Cat_CurrencyCollection lstMoneda { get; set; }
        public Cat_FinancialTermCollection lstPlazo { get; set; }

        //Modal para activos, eliminar al terminar servicios
        public Cat_AssetCollateralCollection lstFamProductos { get; set; }
        public Cat_CoverageCollection lstCobertura { get; set; }
        public Cat_LocationCollection lstLocation { get; set; }

        // Datos de programa actual
        public Ope_Program selPrograma { get; set; }
        public Ope_ProgramInput selProgInput { get; set; }
        public Rel_ProgramFinancialTypeCollection selLstProducto { get; set; }
        public Rel_ProgramCurrencyCollection selLstMoneda { get; set; }
        public Rel_ProgramFinancialTermCollection selLstPlazo { get; set; }
        public Rel_ProgramAssetCollection selLstActivo { get; set; }
        public Ope_Visual selVisual { get; set; }

    }

    public class DataList
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool Selected { get; set; }
    }

    public class ActivoModel : MensajesBaseModel
    {
        public Cat_AssetCollection lstActivos { get; set; }
        public Cat_Asset activoSelected { get; set; }
        public Cat_Coverage cobertura { get; set; }
        public Cat_FinancialTerm plazo { get; set; }
        public Rel_AssetFinancialTerm rvInfo { get; set; }
    }

    public class ProgramSettingData : MensajesBaseModel
    {
        public string programaId { get; set; }
        public string nombre { get; set; }
        public string vendorId { get; set; }
        public string segmentoId { get; set; }
        public string timingId { get; set; }
        public int vigencia { get; set; }
        public List<string> lstProductoId { get; set; }
        public List<string> lstPlazoId { get; set; }
        public List<string> lstMonedaId { get; set; }
        public List<Rel_ProgramAsset> lstActivoId { get; set; }
        public List<FieldData> lstInputs { get; set; }
        public string rgbColor { get; set; }
    }

    public class FieldData
    {
        public string fieldName { get; set; }
        public string fieldValue { get; set; }
    }

    public class PSVendorModel : MensajesBaseModel
    {
        public Cat_VendorCollection lstVendor { get; set; }
    }

    public class AssetProgramModel : MensajesBaseModel
    {
        public Cat_Asset selActivo { get; set; }
        public int selProgramaId { get; set; }

        public Cat_AssetCollateral selGrupo { get; set; }
        public Cat_Coverage selCobertura { get; set; }
        public Rel_AssetFinancialTermCollection selLstPlazoRV { get; set; }

        public Cat_AssetCollateralCollection lstFamProductos { get; set; }
        public Cat_CoverageCollection lstCobertura { get; set; }
        public Cat_AssetServiceCollection lstServicio { get; set; }

        public Cat_AssetServiceCollection lstSelServicio { get; set; }
    }

    public class AssetProgramSaveModel : MensajesBaseModel
    {
        public Cat_Asset selAsset { get; set; }
        public string programId { get; set; }
        public string assetId { get; set; }
        public string coverageId { get; set; }
        public List<string> lstServiceId { get; set; }
    }
}