﻿using EC.PRM.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class Step3Model:MensajesBaseModel
    {
        public Cat_FinancialTypeCollection LstFinTipo { get; set; }
        public Cat_CurrencyCollection LstMoneda { get; set; }
        public Cat_FinancialTermCollection LstFinPlazo { get; set; }
        public Cat_FinancialType FinTipoSelected { get; set; }
        public Cat_Currency FinModenaSelected { get; set; }
        public Cat_FinancialTerm FinPlazoSelected { get; set; }

    }
}