﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EC.PRM.LogicaNegocio.Entities;

namespace EC.PRM.UI.Web.Models
{
    public class VendorCatalogModel
    {
        public string dsearch { get; set; }
        public Cat_LocationCollection lstLocation { get; set; }
        public List<tblVendor> lstVendor { get; set; }
        public Ope_Visual visualInfo { get; set; }
    }

    public class tblVendor : Cat_Vendor
    {
        public string location { get; set; }
    }
}