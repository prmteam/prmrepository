﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class EmailModel
    {
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Recipients { get; set; }
        public string CopyTo { get; set; }
        public string BoldCopy { get; set; }
        public string SMTP { get; set; }
        public int Port { get; set; }
        public string AttachmentFile { get; set; }
        public Dictionary<string, byte[]> Images { get; set; }
    }
}