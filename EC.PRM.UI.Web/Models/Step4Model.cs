﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class Step4Model : MensajesBaseModel
    {
        public bool RetrasoPago { get; set; }
        public bool DescuentoCiego { get; set; }
    }
}