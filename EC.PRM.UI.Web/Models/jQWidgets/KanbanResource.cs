﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jQWidgets.AspNet.Core.Models
{
    public class KanbanResource
    {
        public int Id
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string Image
        {
            get;
            set;
        }
        public bool Common
        {
            get;
            set;
        }
    }
}