﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jQWidgets.AspNet.Core.Models
{
    public class BrowserShare
    {
        public string Browser
        {
            get;
            set;
        }
        public double Share
        {
            get;
            set;
        }
    }
}