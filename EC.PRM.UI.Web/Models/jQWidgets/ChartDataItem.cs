﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jQWidgets.AspNet.Core.Models
{
    public class ChartDataItem
    {
        public int Keith
        {
            get;
            set;
        }
        public int Erica
        {
            get;
            set;
        }
        public int George
        {
            get;
            set;
        }
        public string Day
        {
            get;
            set;
        }
    }
}