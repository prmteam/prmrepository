﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jQWidgets.AspNet.Core.Models
{
    public class TreeItem
    {
        public bool Expanded
        {
            get;
            set;
        }
        public string Label
        {
            get;
            set;
        }

        public string Icon
        {
            get;
            set;
        }

        public List<TreeItem> Items
        {
            get;
            set;
        }
    }
}