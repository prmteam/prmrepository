﻿using EC.PRM.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class AssetCatalogModel
    {
        public string dsearch { get; set; }
        public Cat_AssetCollateralCollection lstCollateral  { get; set; }
        public Cat_CurrencyCollection lstCurrency { get; set; }
        public List<tblAsset> lstAsset { get; set; }
        public Ope_Visual visualInfo { get; set; }
    }

    public class tblAsset : Cat_Asset
    {
        public Cat_AssetCollateral collateral { get; set; }
        public Cat_Currency currency { get; set; }
    }

    public class AssetCollateralModel : MensajesBaseModel
    {
        public Cat_AssetCollateralCollection lstCollateral { get; set; }
    }

    public class CoverageModel : MensajesBaseModel
    {
        public Cat_CoverageCollection lstCoverage { get; set; }
    }
}