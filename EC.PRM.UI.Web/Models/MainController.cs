﻿using EC.PRM.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EC.PRM.UI.Web.Models
{
    public class MainController : Controller
    {
        // GET: Main
        public ActionResult Index()
        {
            Ope_Visual entVisual = new Ope_Visual()
            {
                Visual_ProgramId = 1
            };
            entVisual = entVisual.GetFirtByData();

            return View(entVisual);
        }

        [ChildActionOnly]
        public ActionResult MyActionThatGeneratesAPartial(string parameter1)
        {
            Ope_Visual entVisual = new Ope_Visual()
            {
                Visual_ProgramId = 1
            };
            entVisual = entVisual.GetFirtByData();

            return PartialView(entVisual);
        }
    }
}