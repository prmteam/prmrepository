﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class ProfitSettingModel
    {
        public List<DataList> lstDepositoNumero = new List<DataList>();
        public List<DataList> lstDepositoPorciento = new List<DataList>();
        public List<DataList> lstSubsidioPorciento = new List<DataList>();
        public List<DataList> lstSubsidioMonto = new List<DataList>();
        public List<DataList> lstComAperturaPorciento = new List<DataList>();
        public List<DataList> lstPagoProveedorMeses = new List<DataList>();
        public List<DataList> lstOpCompraPorciento = new List<DataList>();
        public List<DataList> lstComVendedorPorciento = new List<DataList>();
        public List<DataList> lstROI = new List<DataList>();
        public List<DataList> lstEngancePorciento = new List<DataList>();
        public List<DataList> lstRentaAntPorciento = new List<DataList>();

        public List<DataList> lstTiming = new List<DataList>();
        public List<DataList> lstResidualValue = new List<DataList>();
        public List<DataList> lstPurchaseOption = new List<DataList>();
        public List<DataList> lstBalloonPayment = new List<DataList>();
        public List<DataList> lstOpFee = new List<DataList>();
        public List<DataList> lstSecDeposit = new List<DataList>();
        public List<DataList> lstBlindDiscount = new List<DataList>();
        public List<DataList> lstDelay = new List<DataList>();
        public List<DataList> lstReferalFee = new List<DataList>();
        public List<DataList> lstSegment = new List<DataList>();
    }
}