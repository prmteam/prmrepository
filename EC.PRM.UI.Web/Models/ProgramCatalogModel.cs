﻿using EC.PRM.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class ProgramCatalogModel
    {
        public string dsearch { get; set; }
        public Cat_VendorCollection lstVendor { get; set; }
        public List<tblProgram> lstProgram { get; set; }

        public List<DataList> lstSegment = new List<DataList>();
        public List<DataList> lstTiming = new List<DataList>();
        public Ope_Visual visualInfo { get; set; }
    }

    public class tblProgram
    {
        public int programId { get; set; }
        public string program { get; set; }
        public int vendorId { get; set; }
        public string  vendor { get; set; }
        public string segment { get; set; }
        public string timing { get; set; }
    }
}