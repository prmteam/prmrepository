﻿using EC.PRM.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class UserProfileModel : MensajesBaseModel
    {
        public string dsearch { get; set; }
        public Cat_VendorCollection lstVendor { get; set; }
        public Cat_UserProfileCollection lstProfile { get; set; }
        public List<tblUser> lstUser { get; set; }
        public List<Ope_Program> lstProgram { get; set; }
        public Ope_Visual visualInfo { get; set; }
    }

    public class tblUser
    {
        public int userId { get; set; }
        public string user { get; set; }
        public string pass { get; set; }
        public int profileId { get; set; }
        public string profile { get; set; }
        public int vendorId { get; set; }
        public string program { get; set; }
        public int programId { get; set; }
        public string vendor { get; set; }

        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string pathPhoto { get; set; }
    }
}