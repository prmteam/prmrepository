﻿using EC.PRM.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class VisualModel: MensajesBaseModel
    {
        public Ope_Visual visual { get; set; }
    }
}