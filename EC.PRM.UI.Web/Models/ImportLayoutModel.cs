﻿using EC.PRM.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC.PRM.UI.Web.Models
{
    public class ImportLayoutModel : MensajesBaseModel
    {
        public string catalogName { get; set; }
        public Cat_VendorCollection lstVendor { get; set; }
    }
}