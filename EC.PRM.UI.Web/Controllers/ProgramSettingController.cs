﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;

namespace EC.PRM.UI.Web.Controllers
{
    public class ProgramSettingController : Controller
    {
        public ActionResult Index(string programId)
        {
            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                Response.Redirect("/Home/Login");
            }

            ProgramSettingModel model = new Models.ProgramSettingModel();

            model.programaId = String.IsNullOrEmpty(programId) ? 0 : Convert.ToInt32(programId);

            Cat_Vendor entVendor = new Cat_Vendor()
            {
                Vendor_Active = true
            };
            Cat_VendorCollection lstVendor = entVendor.GetCollectionByData();
            model.lstVendor = lstVendor;

            Cat_FinancialType entProducto = new Cat_FinancialType()
            {
                FinancialType_Active = true
            };
            model.lstProducto = entProducto.GetCollectionByData();

            Cat_FinancialTerm entPlazo = new Cat_FinancialTerm()
            {
                FinancialTerm_Active = true
            };
            model.lstPlazo = entPlazo.GetCollectionByData();

            Cat_Currency entMoneda = new Cat_Currency()
            {
                Currency_Active = true
            };
            model.lstMoneda = entMoneda.GetCollectionByData();

            Cat_AssetCollateral entFamActivo = new Cat_AssetCollateral()
            {
                AssetCollateral_Active = true
            };
            model.lstFamProductos = entFamActivo.GetCollectionByData();

            ProfitSettingModel psmodel = new Models.ProfitSettingModel()
            {
                lstTiming = UtilitiesGeneral.GetListValues("Profit_Timing"),
                lstResidualValue = UtilitiesGeneral.GetListValues("Profit_ResidualValue", "%"),
                lstPurchaseOption = UtilitiesGeneral.GetListValues("Profit_PurchaseOption", "%"),
                lstBalloonPayment = UtilitiesGeneral.GetListValues("Profit_BalloonPayment", "%"),
                lstOpFee = UtilitiesGeneral.GetListValues("Profit_OpFee", "%"),
                lstSecDeposit = UtilitiesGeneral.GetListValues("Profit_SecDeposit", "%"),
                lstBlindDiscount = UtilitiesGeneral.GetListValues("Profit_BlindDiscount", "%"),
                lstDelay = UtilitiesGeneral.GetListValues("Profit_Delay", "%"),
                lstReferalFee = UtilitiesGeneral.GetListValues("Profit_ReferalFee", "%"),
                lstSegment = UtilitiesGeneral.GetListValues("Profit_Segment")
            };

            model.profitData = psmodel;

            Cat_Location entLoc = new Cat_Location()
            {
                Location_Active = true
            };
            model.lstLocation = entLoc.GetCollectionByData();

            Cat_Coverage entCover = new Cat_Coverage()
            {
                Coverage_Active = true
            };
            model.lstCobertura = entCover.GetCollectionByData();

            // Datos de programa actual
            if (model.programaId > 0)
            {
                Ope_Program selProgram = new Ope_Program()
                {
                    ProgramId = model.programaId
                };
                selProgram = selProgram.GetFirtByData();

                Ope_ProgramInput selProgInput = new Ope_ProgramInput()
                {
                    ProgramInput_ProgramId = model.programaId
                };
                selProgInput = selProgInput.GetFirtByData();

                Rel_ProgramFinancialType selProducto = new Rel_ProgramFinancialType()
                {
                    ProgramFinancialType_ProgramId = model.programaId
                };
                Rel_ProgramFinancialTypeCollection selLstProducto = selProducto.GetCollectionByData();

                Rel_ProgramCurrency selMoneda = new Rel_ProgramCurrency()
                {
                    ProgramCurrency_ProgramId = model.programaId
                };
                Rel_ProgramCurrencyCollection selLstMoneda = selMoneda.GetCollectionByData();

                Rel_ProgramFinancialTerm selPlazo = new Rel_ProgramFinancialTerm()
                {
                    ProgramFinancialTerm_ProgramId = model.programaId
                };
                Rel_ProgramFinancialTermCollection selLstPlazo = selPlazo.GetCollectionByData();

                Rel_ProgramAsset selActivo = new Rel_ProgramAsset()
                {
                    ProgramAsset_ProgramId = model.programaId
                };
                Rel_ProgramAssetCollection selLstActivo = selActivo.GetCollectionByData();

                Ope_Visual selVisual = new Ope_Visual()
                {
                    Visual_ProgramId = model.programaId
                };
                selVisual = selVisual.GetFirtByData();

                model.selPrograma = selProgram;
                model.selProgInput = selProgInput;
                model.selLstProducto = selLstProducto;
                model.selLstMoneda = selLstMoneda;
                model.selLstPlazo = selLstPlazo;
                model.selLstActivo = selLstActivo;
                model.selVisual = selVisual;
            }

            return View(model);
        }

        public ActionResult ProgramAsset(int programId, int assetId)//, string plazoIds)
        {
            AssetProgramModel modelo = new Models.AssetProgramModel();

            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                Response.Redirect("/Home/Login");
            }

            try
            {
                Cat_AssetCollateral entCollat = new Cat_AssetCollateral()
                {
                    AssetCollateral_Active = true
                };
                Cat_AssetCollateralCollection lstCollat = entCollat.GetCollectionByData();

                Cat_Coverage entCobertura = new Cat_Coverage()
                {
                    Coverage_Active = true
                };
                Cat_CoverageCollection lstCobertura = entCobertura.GetCollectionByData();

                Cat_AssetService entServicio = new Cat_AssetService()
                {
                    AssetService_Active = true
                };
                Cat_AssetServiceCollection lstServicio = entServicio.GetCollectionByData();

                modelo.lstFamProductos = lstCollat;
                //modelo.lstPlazo = lstPlazo;
                modelo.lstCobertura = lstCobertura;
                modelo.lstServicio = lstServicio;
                modelo.selProgramaId = programId;

                // Consulta de servicios de activo existente
                if (assetId > 0)
                {
                    Cat_Asset selActivo = new Cat_Asset()
                    {
                        AssetId = assetId
                    };
                    modelo.selActivo = selActivo.GetFirtByData();

                    Rel_ProgramAssetService relActivoSer = new Rel_ProgramAssetService()
                    {
                        ProgramAssetService_ProgramAssetId = programId,
                        ProgramAssetService_AssetId = assetId
                    };
                    Rel_ProgramAssetServiceCollection lstActivoServ = relActivoSer.GetCollectionByData();

                    Cat_AssetServiceCollection lstSelServ = new Cat_AssetServiceCollection();

                    foreach (Rel_ProgramAssetService objAS in lstActivoServ)
                    {
                        Cat_AssetService selServ = new Cat_AssetService()
                        {
                            AssetServiceId = objAS.ProgramAssetService_AssetServiceId
                        };
                        lstSelServ.Add(selServ.GetFirtByData());
                    }

                    modelo.lstSelServicio = lstSelServ;

                    Cat_AssetCollateral selGrupo = new Cat_AssetCollateral()
                    {
                        AssetCollateralId = selActivo.Asset_AssetCollateralId
                    };
                    selGrupo = selGrupo.GetFirtByData();

                    modelo.selGrupo = selGrupo;

                    Rel_ProgramAsset relProgAsset = new Rel_ProgramAsset()
                    {
                        ProgramAsset_ProgramId = programId,
                        ProgramAsset_AssetId = assetId
                    };
                    relProgAsset = relProgAsset.GetFirtByData();

                    Cat_Coverage selCober = new Cat_Coverage()
                    {
                        CoverageId = relProgAsset.ProgramAsset_CoverageId
                    };
                    selCober = selCober.GetFirtByData();

                    modelo.selCobertura = selCober;

                    Rel_ProgramFinancialTerm relPlazo = new Rel_ProgramFinancialTerm()
                    {
                        ProgramFinancialTerm_ProgramId = programId

                    };
                    Rel_ProgramFinancialTermCollection lstRelPlazo = relPlazo.GetCollectionByData();

                    Rel_AssetFinancialTermCollection relSelPlazo = new Rel_AssetFinancialTermCollection();
                    foreach (Rel_ProgramFinancialTerm objPlazo in lstRelPlazo)
                    {
                        Rel_AssetFinancialTerm relPlazoRV = new Rel_AssetFinancialTerm()
                        {
                            AssetFinancialTerm_AssetId = assetId,
                            AssetFinancialTerm_FinancialTermId = objPlazo.ProgramFinancialTerm_FinancialTerm
                        };
                        relPlazoRV = relPlazoRV.GetFirtByData();

                        if (relPlazoRV != null)
                        {
                            relSelPlazo.Add(relPlazoRV);
                        }
                    }
                    modelo.selLstPlazoRV = relSelPlazo;
                }

                modelo.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                modelo.CargadoCorrecto = false;
                modelo.Mensajes = ex.Message;
            }

            return View(modelo);
        }

        //[HttpPost]
        //public JsonResult SaveProgramInfo(AssetProgramModel modelo)
        //{
        //    //AssetProgramModel programInfo = new AssetProgramModel();
        //    try
        //    {
        //        System.Web.HttpContext.Current.Session["programInfo"] = modelo;

        //        modelo.CargadoCorrecto = true;
        //    }
        //    catch(Exception ex)
        //    {
        //        modelo.CargadoCorrecto = false;
        //        modelo.Mensajes = ex.Message;
        //    }
        //    return this.Json(modelo);
        //}

        [HttpPost]
        public JsonResult GetProductByFamily(int familiaId)
        {
            ActivoModel model = new ActivoModel();
            try
            {
                Cat_AssetCollateral entFamilia = new Cat_AssetCollateral()
                {
                    AssetCollateralId = familiaId
                };
                entFamilia = entFamilia.GetFirtByData();

                Cat_Asset entProducto = new Cat_Asset()
                {
                    Asset_AssetCollateralId = familiaId,
                    Asset_Active = true
                };
                Cat_AssetCollection lstProducto = entProducto.GetCollectionByData();

                model.lstActivos = lstProducto;

                model.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }
            return this.Json(model);
        }

        [HttpPost]
        public JsonResult GetProductDetail(Rel_AssetFinancialTerm relRV, int coverageId)
        {
            ActivoModel model = new ActivoModel();
            try
            {
                Cat_Asset entActivo = new Cat_Asset()
                {
                    AssetId = relRV.AssetFinancialTerm_AssetId
                };
                entActivo = entActivo.GetFirtByData();
                model.activoSelected = entActivo;

                Cat_FinancialTerm entFT = new Cat_FinancialTerm()
                {
                    FinancialTermId = relRV.AssetFinancialTerm_FinancialTermId
                };
                entFT = entFT.GetFirtByData();
                model.plazo = entFT;

                Cat_Coverage entCover = new Cat_Coverage()
                {
                    CoverageId = coverageId
                };
                entCover = entCover.GetFirtByData();
                model.cobertura = entCover;

                relRV = relRV.GetFirtByData();
                if (relRV == null)
                {
                    throw new Exception("No se ha configurado RV para el activo y plazo seleccionado.");
                }
                model.rvInfo = relRV;

                model.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }
            return this.Json(model);
        }

        [HttpPost]
        public JsonResult GetIRR(
            double monto,
            double mesCero, double mesUltimo,
            int monedaId, int plazoId, int productoId,
            double residualValue)
        {

            QuotationIRRModel model = new QuotationIRRModel();
            model.profitInfo = new ProfitInfoModel();

            try
            {
                Cat_Currency moneda = new Cat_Currency()
                {
                    CurrencyId = monedaId
                };
                moneda = moneda.GetFirtByData();

                Cat_FinancialTerm plazo = new Cat_FinancialTerm()
                {
                    FinancialTermId = plazoId
                };
                plazo = plazo.GetFirtByData();

                Cat_FinancialType producto = new Cat_FinancialType()
                {
                    FinancialTypeId = productoId
                };
                producto = producto.GetFirtByData();
                /*
                Cat_PaymentTerm periodo = new Cat_PaymentTerm()
                {
                    PaymentTerm_Description = "Mensual"
                };
                periodo = periodo.GetFirtByData();
                */

                // el lease se puede obtener desde BD ya que, no se requiere el activo para obtenerlo
                // residual value y rate dependen del activo seleccionado por lo que se complica su obtención desde BD
                /*
                Rel_FinTypeCurFinTerm leaseData = new Rel_FinTypeCurFinTerm()
                {
                    FinTypeCurFinTerm_FinancialTypeId = producto.FinancialTypeId,
                    FinTypeCurFinTerm_CurrencyId = moneda.CurrencyId,
                    FinTypeCurFinTerm_FinancialTermId = plazo.FinancialTermId
                };
                leaseData = leaseData.GetFirtByData();
                
                if (leaseData == null || leaseData.FinTypeCurFinTermId == 0)
                {
                    model.profitInfo.Mensajes = "No se pudo encontrar el valor del lease con la configuración solicitada.";
                    model.profitInfo.CargadoCorrecto = false;
                    return this.Json(model.profitInfo);
                }
                */

                model.moneda = moneda;
                model.plazo = plazo;
                model.producto = producto;
                //model.periodo = periodo;
                model.monto = monto;
                //model.rate = leaseData.FinTypeCurFinTerm_Rate.Value;//0.0451;
                //model.lease = leaseData.FinTypeCurFinTerm_Lease.Value;// 0.055;
                model.residual_value = residualValue;// 0.25;
                model.firstMonth = mesCero;
                model.lastMonth = mesUltimo;

                model = UtilitiesGeneral.GetQuotationIRR(model);

            }
            catch (Exception ex)
            {
                model.profitInfo.CargadoCorrecto = false;
                model.profitInfo.Mensajes = ex.Message;
            }

            return this.Json(model.profitInfo);
        }

        [HttpPost]
        public JsonResult SaveVendor(Cat_Vendor entVendor)
        {
            PSVendorModel model = new Models.PSVendorModel();

            try
            {
                entVendor.Vendor_Active = true;
                entVendor.Insert();

                Cat_Vendor objVendor = new Cat_Vendor()
                {
                    Vendor_Active = true
                };

                Cat_VendorCollection lstVendor = objVendor.GetCollectionByData();
                model.lstVendor = lstVendor;
                model.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return this.Json(model);
        }

        [HttpPost]
        public JsonResult SaveProgram(ProgramSettingData programData)
        {
            try
            {
                Ope_Program entProgram = new Ope_Program();

                if (Convert.ToInt32(programData.programaId) == 0)
                {
                    entProgram.Program_Description = programData.nombre;
                    entProgram.Program_VendorId = Convert.ToInt32(programData.vendorId);
                    entProgram.Program_Segment = programData.segmentoId;
                    entProgram.Program_Timing = programData.timingId;
                    entProgram.Program_QuoteValidity = programData.vigencia;
                    entProgram.Program_AlterDate = DateTime.Now;
                    entProgram.Program_AlterDescription = "Nuevo registro";
                    entProgram.Program_Active = true;

                    entProgram.Insert();
                }
                else
                {
                    entProgram.ProgramId = Convert.ToInt32(programData.programaId);
                    entProgram = entProgram.GetFirtByData();

                    entProgram.Program_QuoteValidity = programData.vigencia;
                    entProgram.Program_AlterDescription = "Programa modificado. Fecha:" + DateTime.Now;
                    entProgram.Update();
                }

                Ope_ProgramInput entInput = new Ope_ProgramInput()
                {
                    ProgramInput_ProgramId = entProgram.ProgramId
                };
                entInput = entInput.GetFirtByData();

                if (entInput == null)
                    entInput = new Ope_ProgramInput();

                foreach (var prop in entInput.GetType().GetProperties())
                {
                    FieldData campo = programData.lstInputs.Find(x => x.fieldName.Equals(prop.Name));

                    if (campo != null)
                    {
                        Type tipoDato = prop.PropertyType;
                        Type tipoNul = Nullable.GetUnderlyingType(tipoDato);
                        Console.WriteLine(tipoNul.FullName);

                        if (tipoNul == typeof(double))
                        {
                            prop.SetValue(entInput, Convert.ToDouble(campo.fieldValue));
                        }
                        else if (tipoNul == typeof(int))
                        {
                            prop.SetValue(entInput, Convert.ToInt32(campo.fieldValue));
                        }
                        else if (tipoNul == typeof(bool))
                        {
                            prop.SetValue(entInput, Convert.ToBoolean(campo.fieldValue));
                        }
                    }
                }
                if (Convert.ToInt32(programData.programaId) == 0)
                {
                    entInput.ProgramInput_ProgramId = entProgram.ProgramId;
                    entInput.Insert();
                }
                else
                {
                    entInput.Update();
                }

                // Verificar existencia de algún deseleccionado
                Rel_ProgramFinancialType entProductDel = new Rel_ProgramFinancialType()
                {
                    ProgramFinancialType_ProgramId = entProgram.ProgramId
                };
                Rel_ProgramFinancialTypeCollection lstProductDel = entProductDel.GetCollectionByData();
                foreach (Rel_ProgramFinancialType ele in lstProductDel)
                {
                    if (!programData.lstProductoId.Contains(ele.ProgramFinancialType_FinancialType.ToString()))
                    {
                        ele.Delete();
                    }
                }

                foreach (string productoId in programData.lstProductoId)
                {
                    Rel_ProgramFinancialType entFT = new Rel_ProgramFinancialType()
                    {
                        ProgramFinancialType_ProgramId = entProgram.ProgramId,
                        ProgramFinancialType_FinancialType = Convert.ToInt32(productoId)
                    };
                    entFT = entFT.GetFirtByData();

                    if (entFT == null)
                    {
                        entFT = new Rel_ProgramFinancialType()
                        {
                            ProgramFinancialType_ProgramId = entProgram.ProgramId,
                            ProgramFinancialType_FinancialType = Convert.ToInt32(productoId)
                        };
                        entFT.Insert();
                    }
                }

                // Verificar existencia de algún deseleccionado
                Rel_ProgramFinancialTerm entPlazoDel = new Rel_ProgramFinancialTerm()
                {
                    ProgramFinancialTerm_ProgramId = entProgram.ProgramId
                };
                Rel_ProgramFinancialTermCollection lstPlazoDel = entPlazoDel.GetCollectionByData();
                foreach (Rel_ProgramFinancialTerm ele in lstPlazoDel)
                {
                    if (!programData.lstPlazoId.Contains(ele.ProgramFinancialTerm_FinancialTerm.ToString()))
                    {
                        ele.Delete();
                    }
                }

                foreach (string plazoId in programData.lstPlazoId)
                {
                    Rel_ProgramFinancialTerm entPFTerm = new Rel_ProgramFinancialTerm()
                    {
                        ProgramFinancialTerm_ProgramId = entProgram.ProgramId,
                        ProgramFinancialTerm_FinancialTerm = Convert.ToInt32(plazoId)
                    };
                    entPFTerm = entPFTerm.GetFirtByData();

                    if (entPFTerm == null)
                    {
                        entPFTerm = new Rel_ProgramFinancialTerm()
                        {
                            ProgramFinancialTerm_ProgramId = entProgram.ProgramId,
                            ProgramFinancialTerm_FinancialTerm = Convert.ToInt32(plazoId)
                        };
                        entPFTerm.Insert();
                    }
                }

                // Verificar existencia de algún deseleccionado
                Rel_ProgramCurrency entMonedaDel = new Rel_ProgramCurrency()
                {
                    ProgramCurrency_ProgramId = entProgram.ProgramId
                };
                Rel_ProgramCurrencyCollection lstMonedaDel = entMonedaDel.GetCollectionByData();
                foreach (Rel_ProgramCurrency ele in lstMonedaDel)
                {
                    if (!programData.lstMonedaId.Contains(ele.ProgramCurrency_CurrencyId.ToString()))
                    {
                        ele.Delete();
                    }
                }

                foreach (string monedaId in programData.lstMonedaId)
                {
                    Rel_ProgramCurrency entPC = new Rel_ProgramCurrency()
                    {
                        ProgramCurrency_ProgramId = entProgram.ProgramId,
                        ProgramCurrency_CurrencyId = Convert.ToInt32(monedaId)
                    };
                    entPC = entPC.GetFirtByData();

                    if (entPC == null)
                    {
                        entPC = new Rel_ProgramCurrency()
                        {
                            ProgramCurrency_ProgramId = entProgram.ProgramId,
                            ProgramCurrency_CurrencyId = Convert.ToInt32(monedaId)
                        };
                        entPC.Insert();
                    }
                }

                Ope_Visual entVisual = new Ope_Visual()
                {
                    Visual_ProgramId = entProgram.ProgramId,
                    Visual_BannerColor = programData.rgbColor
                };
                if (Convert.ToInt32(programData.programaId) == 0)
                    entVisual.Insert();
                else
                    entVisual.Update();

                // Registra evento de usuario
                try
                {
                    Cat_User objUser = new Cat_User();
                    if (System.Web.HttpContext.Current.Session["objUser"] != null)
                    {
                        objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];
                    }

                    Cat_Event entEvento = new Cat_Event()
                    {
                        Event_Code = Convert.ToInt32(programData.programaId) == 0 ? "PCREA" : "PEDIT"
                    };
                    entEvento = entEvento.GetFirtByData();

                    Ope_UserEvent entUsrEvento = new Ope_UserEvent()
                    {
                        UserEvent_CatalogId = Convert.ToInt32(programData.programaId),
                        UserEvent_Date = DateTime.Now,
                        UserEvent_EventId = entEvento.EventId,
                        UserEvent_UserId = objUser.UserId != null ? objUser.UserId : 0
                    };
                    entUsrEvento.Insert();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                programData.programaId = entProgram.ProgramId.Value.ToString();
                programData.CargadoCorrecto = true;

            }
            catch (Exception ex)
            {
                programData.CargadoCorrecto = false;
                programData.Mensajes = ex.Message;
            }

            return this.Json(programData);
        }

        [HttpPost]
        public JsonResult SaveAsset(AssetProgramSaveModel data)
        {
            try
            {
                Rel_ProgramAsset relPAVer = new Rel_ProgramAsset()
                {
                    ProgramAsset_ProgramId = Convert.ToInt32(data.programId),
                    ProgramAsset_AssetId = Convert.ToInt32(data.assetId)
                };
                relPAVer = relPAVer.GetFirtByData();
                if (relPAVer != null)
                {
                    throw new Exception("El producto seleccionado ya existe para el programa actual.");
                }

                Rel_ProgramAsset relPA = new Rel_ProgramAsset();

                relPA.ProgramAsset_ProgramId = Convert.ToInt32(data.programId);
                relPA.ProgramAsset_AssetId = Convert.ToInt32(data.assetId);
                relPA.ProgramAsset_CoverageId = Convert.ToInt32(data.coverageId);
                relPA.Insert();

                if (data.lstServiceId != null)
                {
                    foreach (string servId in data.lstServiceId)
                    {
                        Rel_ProgramAssetService relPAS = new Rel_ProgramAssetService()
                        {
                            ProgramAssetService_ProgramAssetId = relPA.ProgramAsset_ProgramId,
                            ProgramAssetService_AssetId = relPA.ProgramAsset_AssetId,
                            ProgramAssetService_AssetServiceId = Convert.ToInt32(servId)
                        };
                        relPAS.Insert();
                    }
                }

                Cat_Asset selAsset = new Cat_Asset()
                {
                    AssetId = relPA.ProgramAsset_AssetId
                };
                data.selAsset = selAsset;
                data.programId = relPA.ProgramAsset_ProgramId.ToString();
                data.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                data.CargadoCorrecto = false;
                data.Mensajes = ex.Message;
            }

            return this.Json(data);
        }

        [HttpPost]
        public JsonResult UpdateAsset(AssetProgramSaveModel data)
        {
            try
            {
                Rel_ProgramAsset relPA = new Rel_ProgramAsset();

                relPA.ProgramAsset_ProgramId = Convert.ToInt32(data.programId);
                relPA.ProgramAsset_AssetId = Convert.ToInt32(data.assetId);

                relPA = relPA.GetFirtByData();

                relPA.ProgramAsset_CoverageId = Convert.ToInt32(data.coverageId);
                relPA.Update();


                // Verificar existencia de algún deseleccionado
                Rel_ProgramAssetService relPASDel = new Rel_ProgramAssetService()
                {
                    ProgramAssetService_ProgramAssetId = relPA.ProgramAsset_ProgramId,
                    ProgramAssetService_AssetId = relPA.ProgramAsset_AssetId
                };
                Rel_ProgramAssetServiceCollection lstPASDel = relPASDel.GetCollectionByData();
                foreach (Rel_ProgramAssetService ele in lstPASDel)
                {
                    if (!data.lstServiceId.Contains(ele.ProgramAssetService_AssetServiceId.ToString()))
                    {
                        ele.Delete();
                    }
                }

                if (data.lstServiceId != null)
                {
                    foreach (string servId in data.lstServiceId)
                    {
                        Rel_ProgramAssetService relPAS = new Rel_ProgramAssetService()
                        {
                            ProgramAssetService_ProgramAssetId = relPA.ProgramAsset_ProgramId,
                            ProgramAssetService_AssetId = relPA.ProgramAsset_AssetId,
                            ProgramAssetService_AssetServiceId = Convert.ToInt32(servId)
                        };
                        relPAS = relPAS.GetFirtByData();

                        if (relPAS == null)
                        {
                            relPAS = new Rel_ProgramAssetService()
                            {
                                ProgramAssetService_ProgramAssetId = relPA.ProgramAsset_ProgramId,
                                ProgramAssetService_AssetId = relPA.ProgramAsset_AssetId,
                                ProgramAssetService_AssetServiceId = Convert.ToInt32(servId)
                            };
                            relPAS.Insert();
                        }
                    }
                }

                Cat_Asset selAsset = new Cat_Asset()
                {
                    AssetId = relPA.ProgramAsset_AssetId
                };
                data.selAsset = selAsset;
                data.programId = relPA.ProgramAsset_ProgramId.ToString();
                data.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                data.CargadoCorrecto = false;
                data.Mensajes = ex.Message;
            }

            return this.Json(data);
        }

        [HttpPost]
        public JsonResult SaveService(Cat_AssetService data)
        {
            MensajesBaseModel result = new Models.MensajesBaseModel();

            try
            {
                Cat_AssetService entServ = new Cat_AssetService()
                {
                    AssetService_Description = data.AssetService_Description
                };
                entServ = entServ.GetFirtByData();

                if (entServ != null)
                {
                    throw new Exception("El servicio ingresado ya existe.");
                }
                else
                {
                    data.AssetService_Active = true;
                    data.Insert();
                }

                result.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                result.CargadoCorrecto = false;
                result.Mensajes = ex.Message;
            }

            return this.Json(result);
        }

        [HttpPost]
        public JsonResult DeleteAsset(int programId, int assetId)
        {
            MensajesBaseModel result = new Models.MensajesBaseModel();

            try
            {
                Rel_ProgramAssetService relPAS = new Rel_ProgramAssetService()
                {
                    ProgramAssetService_ProgramAssetId = programId,
                    ProgramAssetService_AssetId = assetId
                };
                Rel_ProgramAssetServiceCollection lstRelPAS = relPAS.GetCollectionByData();

                foreach(Rel_ProgramAssetService elemento in lstRelPAS)
                {
                    elemento.Delete();
                }

                Rel_ProgramAsset relPA = new Rel_ProgramAsset()
                {
                    ProgramAsset_ProgramId = programId,
                    ProgramAsset_AssetId = assetId
                };
                relPA = relPA.GetFirtByData();
                relPA.Delete();

                result.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                result.CargadoCorrecto = false;
                result.Mensajes = ex.Message;
            }

            return this.Json(result);
        }

        [HttpPost]
        public JsonResult SaveCoverage(Cat_Coverage entCoverage)
        {
            CoverageModel model = new Models.CoverageModel();

            try
            {
                if (System.Web.HttpContext.Current.Session["objUser"] == null)
                {
                    Response.Redirect("/Home/Login");
                }
                Cat_User objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];

                entCoverage.Coverage_Active = true;
                entCoverage.Insert();

                Cat_Coverage objColl = new Cat_Coverage()
                {
                    Coverage_Active = true
                };

                Cat_CoverageCollection lstColl = objColl.GetCollectionByData();
                model.lstCoverage = lstColl;
                model.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return this.Json(model);
        }

    }
}