﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;

namespace EC.PRM.UI.Web.Controllers
{
    public class HistoricalController : Controller
    {
        // GET: Historical
        public ActionResult ShowQuotations(
            int? id,
            string cliente,
            string fechaVal,
            int? productoVal)
        {
            Cat_User objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];
            if (objUser == null)
            {
                Response.Redirect("/Home/Login");
            }
            
            HistoricalModel modelo = new HistoricalModel();

            Ope_Visual entVisual = UtilitiesGeneral.GetVisual();
            modelo.visualInfo = entVisual;

            modelo.datosFiltro.Id = id;
            modelo.datosFiltro.Cliente = cliente;
            modelo.datosFiltro.Fecha = fechaVal;
            modelo.datosFiltro.ProductoId = productoVal;

            modelo.fechaOpciones = UtilitiesGeneral.GetListValues("Historical_Dates");

            Cat_FinancialType entFT = new Cat_FinancialType()
            {
                FinancialType_Active = true
            };
            Cat_FinancialTypeCollection lstFT = entFT.GetCollectionByData();
            modelo.productos = lstFT;

            Ope_Quotation entQuot = new Ope_Quotation();

            Cat_UserProfile entPerfil = new Cat_UserProfile()
            {
                UserProfileId = objUser.User_UserProfileId
            };
            entPerfil = entPerfil.GetFirtByData();

            switch (entPerfil.UserProfile_Profile)
            {
                case "EC Manager":
                    entQuot.Quotation_ProgramId = objUser.User_ProgramId;
                    break;
                case "Program Manager":
                    entQuot.Quotation_ProgramId = objUser.User_ProgramId;
                    break;
                case "Quotation User":
                    entQuot.Quotation_ProgramId = objUser.User_ProgramId;
                    entQuot.Quotation_UserId = objUser.UserId;
                    break;
                default:
                    entQuot.Quotation_ProgramId = objUser.User_ProgramId;
                    break;
            }

            if (id != null && id > 0)
            {
                entQuot.QuotationId = id.Value;
            }
            if (productoVal != null && productoVal > 0)
            {
                Cat_FinancialType entFinType = new Cat_FinancialType()
                {
                    FinancialTypeId = productoVal.Value
                };
                entFinType = entFinType.GetFirtByData();

                entQuot.Quotation_FinancialType = entFinType.FinancialType_Description;
            }

            List<Ope_Quotation> lstQuot = entQuot.GetCollectionByData().OrderByDescending(x=>x.Quotation_Date).ToList();

            foreach (Ope_Quotation objQuot in lstQuot)
            {
                DataSearch objSearch = new DataSearch()
                {
                    Id = objQuot.QuotationId.Value,
                    Cliente = objQuot.Quotation_CustomerName,
                    Fecha = objQuot.Quotation_Date.Value,
                    Plazos = objQuot.Quotation_FinancialTerm,
                    Producto = objQuot.Quotation_FinancialType
                };

                if (!String.IsNullOrEmpty(fechaVal))
                {
                    switch (fechaVal)
                    {
                        case "Este Mes":
                            if (objQuot.Quotation_Date.Value.Year == DateTime.Now.Year &&
                                objQuot.Quotation_Date.Value.Month == DateTime.Now.Month)
                            {
                                modelo.registrosBusqueda.Add(objSearch);
                            }
                            break;
                        case "Mes anterior":
                            if (objQuot.Quotation_Date.Value.Year == DateTime.Now.Year &&
                                objQuot.Quotation_Date.Value.Month == DateTime.Now.Month - 1)
                            {
                                modelo.registrosBusqueda.Add(objSearch);
                            }
                            break;
                        case "Este Semestre":
                            bool primerSem = DateTime.Now.Month >= 6 ? false : true;

                            if (objQuot.Quotation_Date.Value.Year == DateTime.Now.Year &&
                                objQuot.Quotation_Date.Value.Month >= (primerSem ? 1 : 7) &&
                                objQuot.Quotation_Date.Value.Month <= (primerSem ? 6 : 12))
                            {
                                modelo.registrosBusqueda.Add(objSearch);
                            }
                            break;
                        case "Este Año":
                            if (objQuot.Quotation_Date.Value.Year == DateTime.Now.Year)
                            {
                                modelo.registrosBusqueda.Add(objSearch);
                            }
                            break;
                        case "Año Pasado":
                            if (objQuot.Quotation_Date.Value.Year == DateTime.Now.Year - 1)
                            {
                                modelo.registrosBusqueda.Add(objSearch);
                            }
                            break;
                        default:
                            modelo.registrosBusqueda.Add(objSearch);
                            break;
                    }
                }
                else
                {
                    modelo.registrosBusqueda.Add(objSearch);
                }
            }

            if (!String.IsNullOrEmpty(cliente))
            {
                modelo.registrosBusqueda = modelo.registrosBusqueda.Where(x => Utilities.UtilitiesGeneral.Contains(x.Cliente, cliente, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }

            return View(modelo);
        }

    }
}