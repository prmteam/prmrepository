﻿using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;
using System;
using System.Collections.Generic;
//using Microsoft.VisualBasic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace EC.PRM.UI.Web.Controllers
{
    public class QuotationController : Controller
    {

        #region Views

        // GET: Quotation
        public ActionResult Index()
        {

            return View();
        }

        private QuotationModel GetQuotationInfo(QuotationModel modelo, int quoteId, string act)
        {
            try
            {
                Ope_Quotation entQuot = new Ope_Quotation()
                {
                    QuotationId = quoteId
                };
                entQuot = entQuot.GetFirtByData();

                modelo.CotizacionId = quoteId;
                modelo.Evento = act;
                modelo.ClienteNombre = entQuot.Quotation_CustomerName;
                modelo.ClienteContacto = entQuot.Quotation_CustomerContact;
                modelo.ClienteEmail = entQuot.Quotation_CustomerEmail;
                modelo.ClienteUbicacion = entQuot.Quotation_CustomerLocation;

                List<EquipoModel> lstEquipo = new List<EquipoModel>();

                Rel_QuotationAsset relAsset = new Rel_QuotationAsset()
                {
                    QuotationAsset_QuotationId = quoteId
                };
                Rel_QuotationAssetCollection lstAsset = relAsset.GetCollectionByData();

                foreach (Rel_QuotationAsset objAsset in lstAsset)
                {
                    EquipoModel equipo = new EquipoModel()
                    {
                        ProductoId = objAsset.QuotationAsset_AssetId.Value,
                        Precio = objAsset.QuotationAsset_Amount.Value,
                        Cantidad = objAsset.QuotationAsset_Quantity.Value
                    };

                    Cat_Asset entAsset = new Cat_Asset()
                    {
                        AssetId = objAsset.QuotationAsset_AssetId.Value
                    };
                    entAsset = entAsset.GetFirtByData();

                    equipo.Producto = entAsset.Asset_Model;
                    equipo.FamiliaId = entAsset.Asset_AssetCollateralId.Value;

                    lstEquipo.Add(equipo);
                }

                modelo.LstEquipos = lstEquipo;

                Cat_FinancialType entProducto = new Cat_FinancialType()
                {
                    FinancialType_Description = entQuot.Quotation_FinancialType
                };
                entProducto = entProducto.GetFirtByData();
                modelo.FinanciamientoTipo = entProducto;

                Cat_FinancialTerm entPlazo = new Cat_FinancialTerm()
                {
                    FinancialTerm_Description = entQuot.Quotation_FinancialTerm
                };
                entPlazo = entPlazo.GetFirtByData();
                modelo.FinanciamientoPlazo = entPlazo;

                Cat_Currency entMoneda = new Cat_Currency()
                {
                    Currency_Code = entQuot.Quotation_Currency
                };
                entMoneda = entMoneda.GetFirtByData();
                modelo.FinanciamientoMoneda = entMoneda;

                Ope_Program entProgram = new Ope_Program()
                {
                    ProgramId = entQuot.Quotation_ProgramId
                };
                entProgram = entProgram.GetFirtByData();
                modelo.Programa = entProgram;

                System.Web.HttpContext.Current.Session["quotationData"] = modelo;

                modelo.CargadoCorrecto = true;
            }
            catch(Exception ex)
            {
                modelo.CargadoCorrecto = false;
                modelo.Mensajes = ex.Message;
            }

            return modelo;
        }

        public ActionResult Step1(int? quoteId, string act)
        {
            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                Response.Redirect("/Home/Login");
            }

            if (System.Web.HttpContext.Current.Session["quotationData"]!= null)
            {
                System.Web.HttpContext.Current.Session["quotationData"] = null;
            }

            Cat_Location entUbicacion = new Cat_Location()
            {
                Location_Active = true
            };
            Cat_LocationCollection lstUbicacion = entUbicacion.GetCollectionByData();

            //TODO: se agrega modelo para manejar datos entre controlador y vista para realizar funcionalidad de clonación y edición
            QuotationModel modelo = new QuotationModel();
            modelo.LstUbicacion = lstUbicacion;
            modelo.Evento = "new";

            if (quoteId != null && quoteId > 0)
            {
                modelo = this.GetQuotationInfo(modelo, quoteId.Value, act);
            }

            return View(modelo);
        }

        public ActionResult Step2(int? quoteId, string act)
        {
            QuotationModel quotData = new Models.QuotationModel();

            try
            {
                if (System.Web.HttpContext.Current.Session["objUser"] == null)
                {
                    Response.Redirect("/Home/Login");
                }
                Cat_User objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];

                // quotation edit
                if (quoteId != null && quoteId > 0)
                {
                    quotData = this.GetQuotationInfo(quotData, quoteId.Value, act);

                    System.Web.HttpContext.Current.Session["quotationData"] = quotData;
                }
                else
                {
                    quotData = (QuotationModel)System.Web.HttpContext.Current.Session["quotationData"];

                    //TODO: para clonar llevar directo a TablaAmortizacion
                    if (quotData.Evento.Equals("clone"))
                    {
                        QuotationModel quotDB = this.GetQuotationInfo(quotData, quotData.CotizacionId, quotData.Evento);

                        quotData.LstEquipos = quotDB.LstEquipos;
                    }
                }

                if (quotData == null)
                {
                    quotData = new QuotationModel();
                    throw new Exception("Se debe iniciar la cotización desde el primer paso.");
                }

                Ope_Program entPrograma = new Ope_Program()
                {
                    //TODO: el Id del vendor posiblemente se pueda elegir en el paso 1, de un listado que corresponda al usuario logeado
                    // o más bien si es el usuario logeado
                    //Program_VendorId = 1
                    Program_Active = true
                };
                Ope_ProgramCollection lstPrograma = entPrograma.GetCollectionByData();

                List<Rel_AssetCollateralVendor> lstGruposVendor = Rel_AssetCollateralVendor.GetAll().Where(x => x.AssetCollateralVendor_VendorId == objUser.User_VendorId).ToList();
                List<Cat_AssetCollateral> lstGrupo = new List<Cat_AssetCollateral>();

                foreach(Rel_AssetCollateralVendor grupoVendor in lstGruposVendor)
                {
                    Cat_AssetCollateral entGrupo = new Cat_AssetCollateral()
                    {
                        AssetCollateralId = grupoVendor.AssetCollateralVendor_AssetCollateralId,
                        AssetCollateral_Active = true
                    };
                    entGrupo = entGrupo.GetFirtByData();

                    if (entGrupo != null)
                        lstGrupo.Add(entGrupo);
                }

                ViewBag.Programas = lstPrograma;
                ViewBag.ProductoFamilias = lstGrupo;
                ViewBag.ProgramId = objUser.User_ProgramId;

                quotData.CargadoCorrecto = true;
            }
            catch(Exception ex)
            {
                quotData.CargadoCorrecto = false;
                quotData.Mensajes = ex.Message;
            }

            return View(quotData);
        }

        public ActionResult Step3()
        {
            QuotationModel quotData = new Models.QuotationModel();

            try
            {
                quotData = (QuotationModel)System.Web.HttpContext.Current.Session["quotationData"];

                if (quotData == null)
                {
                    quotData = new QuotationModel();
                    throw new Exception("Se debe iniciar la cotización desde el primer paso.");
                }

                Rel_ProgramFinancialType relFT = new Rel_ProgramFinancialType()
                {
                    ProgramFinancialType_ProgramId = quotData.Programa.ProgramId.Value
                };
                Rel_ProgramFinancialTypeCollection lstRelFT = relFT.GetCollectionByData();

                Cat_FinancialType entFT = new Cat_FinancialType()
                {
                    FinancialType_Active = true
                };
                List<Cat_FinancialType> lstFT = entFT.GetCollectionByData().Where(x => lstRelFT.Any(y => y.ProgramFinancialType_FinancialType.Equals(x.FinancialTypeId))).ToList();

                Rel_ProgramCurrency relMoneda = new Rel_ProgramCurrency()
                {
                    ProgramCurrency_ProgramId = quotData.Programa.ProgramId.Value
                };
                Rel_ProgramCurrencyCollection lstRelMoneda = relMoneda.GetCollectionByData();

                Cat_Currency entModena = new Cat_Currency()
                {
                    Currency_Active = true
                };
                List<Cat_Currency> lstMoneda = entModena.GetCollectionByData().Where(x => lstRelMoneda.Any(y => y.ProgramCurrency_CurrencyId.Equals(x.CurrencyId))).ToList();

                Rel_ProgramFinancialTerm relPlazo = new Rel_ProgramFinancialTerm()
                {
                    ProgramFinancialTerm_ProgramId = quotData.Programa.ProgramId.Value
                };
                Rel_ProgramFinancialTermCollection lstRelPlazo = relPlazo.GetCollectionByData();

                Cat_FinancialTerm entFP = new Cat_FinancialTerm()
                {
                    FinancialTerm_Active = true
                };
                List<Cat_FinancialTerm> lstFP = entFP.GetCollectionByData().Where(x => lstRelPlazo.Any(y => y.ProgramFinancialTerm_FinancialTerm.Equals(x.FinancialTermId))).ToList();

                // deshabilitar plazos que no tengan target capturado
                List<Cat_FinancialTerm> lstPlazoTarget = new List<Cat_FinancialTerm>();
                foreach(Cat_FinancialTerm objPlazo in lstFP)
                {
                    Ope_ProfitTarget entTarget = new Ope_ProfitTarget()
                    {
                        ProfitTarget_FinacialTermId = objPlazo.FinancialTermId
                    };
                    entTarget = entTarget.GetFirtByData();

                    if (entTarget != null)
                    {
                        lstPlazoTarget.Add(objPlazo);
                    }
                }
                ViewBag.LstPlazoTarget = lstPlazoTarget;

                ViewBag.LstFinTipo = lstFT;
                ViewBag.LstMoneda = lstMoneda;
                ViewBag.LstFinPlazo = lstFP;

                Ope_ProgramInput entInput = new Ope_ProgramInput()
                {
                    ProgramInput_ProgramId = quotData.Programa.ProgramId
                };
                entInput = entInput.GetFirtByData();
                ViewBag.Input = entInput;

                ViewBag.PermitirSeguro = false;
                foreach (EquipoModel eq in quotData.LstEquipos)
                {
                    Cat_Asset asset = new Cat_Asset()
                    {
                        AssetId = eq.ProductoId
                    };
                    asset = asset.GetFirtByData();

                    Rel_ProgramAsset relProgAsset = new Rel_ProgramAsset()
                    {
                        ProgramAsset_ProgramId = quotData.Programa.ProgramId,
                        ProgramAsset_AssetId =eq.ProductoId
                    };
                    relProgAsset = relProgAsset.GetFirtByData();

                    if (relProgAsset != null)
                    {
                        if (relProgAsset.ProgramAsset_CoverageId > 0)
                        {
                            ViewBag.PermitirSeguro = true;
                            break;
                        }
                    }
                }

                if (quotData.CotizacionId > 0 && quotData.Evento.Equals("edit"))
                {
                    Ope_QuotInput entQInput = new Ope_QuotInput()
                    {
                        QuotInput_QuotationId = quotData.CotizacionId
                    };
                    entQInput = entQInput.GetFirtByData();

                    quotData.quotInputInfo = entQInput;
                }

                quotData.CargadoCorrecto = true;
            }
            catch(Exception ex)
            {
                quotData.CargadoCorrecto = false;
                quotData.Mensajes = ex.Message;
            }

            return View(quotData);
        }

        public ActionResult Step4()
        {
            QuotationModel quotData = new Models.QuotationModel();

            try
            {
                quotData = (QuotationModel)System.Web.HttpContext.Current.Session["quotationData"];

                if (quotData == null)
                {
                    quotData = new QuotationModel();
                    throw new Exception("Se debe iniciar la cotización desde el primer paso.");
                }

                quotData.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                quotData.CargadoCorrecto = false;
                quotData.Mensajes = ex.Message;
            }

            return View(quotData);
        }

        private QuotationDataModel GetQuotationDataModelInfo(QuotationModel quotData)
        {
            QuotationDataModel res = new Models.QuotationDataModel();

            try
            {
                //res.productoId = quotData.FinanciamientoTipo.FinancialTypeId.ToString();
                //res.monedaId = quotData.FinanciamientoMoneda.CurrencyId.ToString();
                //res.plazoId = quotData.FinanciamientoPlazo.FinancialTermId.ToString();

                Ope_QuotInput entQInput = new Ope_QuotInput()
                {
                    QuotInput_QuotationId = quotData.CotizacionId
                };
                entQInput = entQInput.GetFirtByData();

                List<FieldData> lstInput = new List<Models.FieldData>();

                foreach (var prop in entQInput.GetType().GetProperties())
                {
                    Type tipoDato = prop.PropertyType;
                    Type tipoNul = Nullable.GetUnderlyingType(tipoDato);

                    if (prop.GetValue(entQInput) != null)
                        //&& !prop.Name.Equals("QuotInputId")
                        //&& !prop.Name.Equals("QuotInput_QuotationId"))
                    {
                        FieldData colData = new FieldData()
                        {
                            fieldName = prop.Name,
                            fieldValue = prop.GetValue(entQInput).ToString()
                        };
                        lstInput.Add(colData);
                    }
                }
                res.lstInputs = lstInput;

                res.CargadoCorrecto = true;
            }
            catch(Exception ex)
            {
                res.CargadoCorrecto = false;
                res.Mensajes = ex.Message;
            }

            return res;
        }

        public ActionResult TablaAmortizacion(QuotationDataModel model)
        {
            QuotationModel quotData = new Models.QuotationModel();
            try
            {
                quotData = (QuotationModel)System.Web.HttpContext.Current.Session["quotationData"];

                if (quotData == null)
                {
                    quotData = new QuotationModel();
                    throw new Exception("Se debe iniciar la cotización desde el primer paso.");
                }

                if (System.Web.HttpContext.Current.Session["objUser"] == null)
                {
                    Response.Redirect("/Home/Login");
                }

                Cat_User objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];

                if (quotData.Evento.Equals("clone"))
                {
                    QuotationModel quoteDataClone = new QuotationModel();

                    quoteDataClone = GetQuotationInfo(quoteDataClone, quotData.CotizacionId, quotData.Evento);
                    quoteDataClone.ClienteNombre = quotData.ClienteNombre;
                    quoteDataClone.ClienteContacto = quotData.ClienteContacto;
                    quoteDataClone.ClienteEmail = quotData.ClienteEmail;
                    quoteDataClone.ClienteUbicacion = quotData.ClienteUbicacion;

                    quotData = quoteDataClone;

                    model = GetQuotationDataModelInfo(quotData);
                    model.productoId = quotData.FinanciamientoTipo.FinancialTypeId.ToString();
                    model.monedaId = quotData.FinanciamientoMoneda.CurrencyId.ToString();
                    model.plazoId = quotData.FinanciamientoPlazo.FinancialTermId.ToString();
                }

                if (quotData.tablasAmortiza.Count == 0)
                {
                    Cat_FinancialType entProducto = new Cat_FinancialType()
                    {
                        FinancialTypeId = Convert.ToInt32(model.productoId)
                    };
                    entProducto = entProducto.GetFirtByData();

                    Cat_FinancialTerm entPlazo = new Cat_FinancialTerm()
                    {
                        FinancialTermId = Convert.ToInt32(model.plazoId)
                    };
                    entPlazo = entPlazo.GetFirtByData();

                    Cat_Currency entMoneda = new Cat_Currency()
                    {
                        CurrencyId = Convert.ToInt32(model.monedaId)
                    };
                    entMoneda = entMoneda.GetFirtByData();

                    quotData.FinanciamientoTipo = entProducto;
                    quotData.FinanciamientoPlazo = entPlazo;
                    quotData.FinanciamientoMoneda = entMoneda;

                    double resVal = 0.0;
                    double amount = 0.0;
                    
                    foreach (EquipoModel activo in quotData.LstEquipos)
                    {
                        Cat_Asset entActivo = new Cat_Asset()
                        {
                            AssetId = activo.ProductoId
                        };
                        entActivo = entActivo.GetFirtByData();

                        Ope_ExchangeRate entTC = Ope_ExchangeRate.GetAll().OrderByDescending(x => x.ExchangeRate_Date).FirstOrDefault();

                        amount += entActivo.Asset_CurrencyId == 1 && model.monedaId.Equals("2") ?
                            activo.Precio * entTC.ExchangeRate_MXN.Value :
                            entActivo.Asset_CurrencyId == 2 && model.monedaId.Equals("1") ?
                            activo.Precio / entTC.ExchangeRate_MXN.Value :
                            activo.Precio;

                        Rel_ProgramAsset objAct = new Rel_ProgramAsset()
                        {
                            ProgramAsset_ProgramId = quotData.Programa.ProgramId,
                            ProgramAsset_AssetId = activo.ProductoId
                        };
                        objAct = objAct.GetFirtByData();

                        Rel_AssetFinancialTerm relAFT = new Rel_AssetFinancialTerm()
                        {
                            AssetFinancialTerm_AssetId = activo.ProductoId,
                            AssetFinancialTerm_FinancialTermId = entPlazo.FinancialTermId
                        };
                        relAFT = relAFT.GetFirtByData();

                        if (relAFT == null)
                        {
                            throw new Exception("No se ha definido un valor residual (Residual Value) para el activo y plazo seleccionados.");
                        }

                        resVal += relAFT.AssetFinancialTerm_ResidualValue.Value;
                    }

                    //double amount = quotData.LstEquipos.Sum(x => x.Precio);
                    string segment = quotData.Programa.Program_Segment;

                    switch (entProducto.FinancialType_Description)
                    {
                        case "A. Puro":
                            segment = "TL-" + segment;
                            break;
                        case "A. Financiero":
                            segment = "QL-" + segment;
                            break;
                        case "Crédito":
                            segment = "LN-" + segment;
                            break;
                    }

                    Ope_ProfitTarget entTarget = new Ope_ProfitTarget()
                    {
                        ProfitTarget_FinacialTermId = Convert.ToInt32(model.plazoId),
                        ProfitTarger_CurrencyId = Convert.ToInt32(model.monedaId),
                        ProfitTarget_Segment = segment
                    };
                    entTarget = entTarget.GetFirtByData();

                    if (entTarget == null)
                    {
                        throw new Exception("No se ha definido un rendimiento objetivo (Target) para el plazo seleccionado.");
                    }

                    double target = model.insFinancied ? entTarget.ProfitTarget_Percentage.Value * 1.05 : entTarget.ProfitTarget_Percentage.Value;

                    // La primer ejecución obtiene el payment, la segunda obtiene el interest rate.
                    model = UtilitiesGeneral.CalculateQuotation(model, amount, segment, resVal, entTarget.ProfitTarget_Percentage.Value, 
                        quotData.Programa, entProducto);
                    model = UtilitiesGeneral.CalculateInterestRate(model, amount);

                    double secCost = UtilitiesGeneral.CalculateInsuranceAmount(quotData, model);

                    if (model.insFinancied && secCost > 0)
                    {
                        model = UtilitiesGeneral.CalculateInsurance(model, secCost, target, quotData.Programa);
                    }

                    Models.TablaAmortizacion modelTabla = new Models.TablaAmortizacion();

                    foreach (EquipoModel equipo in quotData.LstEquipos)
                    {
                        modelTabla.Equipos += equipo.Producto + ", ";
                    }

                    modelTabla.PlazoMeses = entPlazo.FinancialTerm_Value.Value;
                    modelTabla.Monto = amount;
                    //int periodo = 0;
                    var iva = Convert.ToDouble(ConfigurationManager.AppSettings["Global_IVA"]);

                    for (int periodo = 0; periodo <= modelTabla.PlazoMeses; periodo++)
                    {
                        var insuranceAmount = model.arrayInsurance != null ? model.arrayInsurance.GetValue(1) : 0.0;

                        MensualidadAmortiza objMes = new MensualidadAmortiza()
                        {
                            Periodo = periodo
                        };

                        if (periodo == 0)
                        {
                            objMes.Balance = amount;
                            objMes.Capital = model.downpayment;//downpayment + primer mes (advance:desde arreglo de pagos, arrear:0)
                            objMes.Interes = 0;
                            objMes.Subtotal = model.downpayment;
                            objMes.Seguro = 0;//advance:primer mensualidad, arrear:0
                            objMes.Iva = model.downpayment * iva;
                            objMes.PagoIva = objMes.Subtotal + objMes.Seguro + objMes.Iva;
                        }
                        else if (periodo == model.arrayPagos.Length - 1)
                        {
                            objMes.Balance = modelTabla.TablaRegistros[periodo - 1].Balance - modelTabla.TablaRegistros[periodo - 1].Capital;
                            objMes.Interes = (objMes.Balance * (model.irr / 100)) / 12;
                            objMes.Capital = entProducto.FinancialType_Description.Equals("Crédito") ?
                                model.pagoMensual + model.balloonpayment - objMes.Interes : //pago + balloon - interes, para la última mensualidad
                                model.pagoMensual - objMes.Interes;
                            objMes.Subtotal = objMes.Capital + objMes.Interes;
                            objMes.Seguro = model.insFinancied && secCost > 0 ? Convert.ToDouble(insuranceAmount) : 0.0;
                            objMes.Iva = (objMes.Subtotal + objMes.Seguro) * iva;
                            objMes.PagoIva = objMes.Subtotal + objMes.Seguro + objMes.Iva;
                        }
                        else if (periodo <= model.graceMonths)
                        {
                            objMes.Balance = modelTabla.TablaRegistros[periodo - 1].Balance - modelTabla.TablaRegistros[periodo - 1].Capital;
                            objMes.Capital = 0;
                            objMes.Interes = (objMes.Balance * (model.irr / 100)) / 12;
                            objMes.Subtotal = objMes.Interes + objMes.Capital;
                            objMes.Seguro = model.insFinancied && secCost > 0 ? Convert.ToDouble(insuranceAmount) : 0.0;
                            objMes.Iva = (objMes.Subtotal + objMes.Seguro) * iva;
                            objMes.PagoIva = objMes.Subtotal + objMes.Seguro + objMes.Iva;
                        }
                        else
                        {
                            objMes.Balance = modelTabla.TablaRegistros[periodo - 1].Balance - modelTabla.TablaRegistros[periodo - 1].Capital;
                            objMes.Interes = (objMes.Balance * (model.irr / 100)) / 12;
                            objMes.Capital = model.pagoMensual - objMes.Interes;
                            objMes.Subtotal = objMes.Capital + objMes.Interes;
                            objMes.Seguro = model.insFinancied && secCost > 0 ? Convert.ToDouble(insuranceAmount) : 0.0;
                            objMes.Iva = (objMes.Subtotal + objMes.Seguro) * iva;
                            objMes.PagoIva = objMes.Subtotal + objMes.Seguro + objMes.Iva;
                        }

                        modelTabla.TablaRegistros.Add(objMes);
                        //periodo++;
                    }

                    //foreach (double pago in model.arrayPagos)
                    //{
                    //    int index = Array.IndexOf(model.arrayPagos, pago);
                    //    //double capital=

                    //    MensualidadAmortiza objMes = new MensualidadAmortiza()
                    //    {
                    //        Periodo = periodo,
                    //        RentaMonto = pago,
                    //        IvaMonto = model.insFinancied && secCost > 0 ? Convert.ToDouble(model.arrayInsurance.GetValue(periodo)) : 0.0,
                    //        PagoTotal = model.insFinancied && secCost > 0 ? pago + Convert.ToDouble(model.arrayInsurance.GetValue(periodo)) : pago

                    //        //,Balance=
                    //    };

                    //    if (periodo == 0)
                    //    {
                    //        objMes.Balance = amount;
                    //        objMes.Capital = model.downpayment;//downpayment + primer mes (advance:desde arreglo de pagos, arrear:0)
                    //        objMes.Interes = 0;
                    //        objMes.Subtotal = model.downpayment;
                    //        objMes.Seguro = 0;//advance:primer mensualidad, arrear:0
                    //        objMes.Iva = model.downpayment * iva;
                    //        objMes.PagoIva = objMes.Subtotal + objMes.Seguro + objMes.Iva;
                    //    }
                    //    else if (periodo == model.arrayPagos.Length - 1)
                    //    {
                    //        objMes.Balance = modelTabla.TablaRegistros[periodo - 1].Balance - modelTabla.TablaRegistros[periodo - 1].Capital;
                    //        objMes.Interes = (objMes.Balance * (model.irr / 100)) / 12;
                    //        objMes.Capital = entProducto.FinancialType_Description.Equals("Crédito") ?
                    //            model.pagoMensual + model.balloonpayment - objMes.Interes : //pago + balloon - interes, para la última mensualidad
                    //            model.pagoMensual - objMes.Interes;
                    //        objMes.Subtotal = objMes.Capital + objMes.Interes;
                    //        objMes.Seguro = model.insFinancied && secCost > 0 ? Convert.ToDouble(model.arrayInsurance.GetValue(periodo)) : 0.0;
                    //        objMes.Iva = (objMes.Subtotal + objMes.Seguro) * iva;
                    //        objMes.PagoIva = objMes.Subtotal + objMes.Seguro + objMes.Iva;
                    //    }
                    //    else if (periodo <= model.graceMonths)
                    //    {
                    //        objMes.Balance = amount;
                    //        objMes.Capital = 0;
                    //        objMes.Interes = (objMes.Balance * (model.irr / 100)) / 12;
                    //        objMes.Subtotal = objMes.Interes + objMes.Capital;
                    //        objMes.Seguro = model.insFinancied && secCost > 0 ? Convert.ToDouble(model.arrayInsurance.GetValue(periodo)) : 0.0;
                    //        objMes.Iva = (objMes.Subtotal + objMes.Seguro) * iva;
                    //        objMes.PagoIva = objMes.Subtotal + objMes.Seguro + objMes.Iva;
                    //    }
                    //    else
                    //    {
                    //        objMes.Balance = modelTabla.TablaRegistros[periodo - 1].Balance - modelTabla.TablaRegistros[periodo - 1].Capital;
                    //        objMes.Interes = (objMes.Balance * (model.irr / 100)) / 12;
                    //        objMes.Capital = pago - objMes.Interes;
                    //        objMes.Subtotal = objMes.Capital + objMes.Interes;
                    //        objMes.Seguro = model.insFinancied && secCost > 0 ? Convert.ToDouble(model.arrayInsurance.GetValue(periodo)) : 0.0;
                    //        objMes.Iva = (objMes.Subtotal + objMes.Seguro) * iva;
                    //        objMes.PagoIva = objMes.Subtotal + objMes.Seguro + objMes.Iva;
                    //    }

                    //    modelTabla.TablaRegistros.Add(objMes);
                    //    periodo++;
                    //}

                    quotData.tablasAmortiza.Add(modelTabla);

                    Cat_Status entStatus = new Cat_Status()
                    {
                        Status_Catalog = "Quotation",
                        Status_Status = "Abierto"
                    };
                    entStatus = entStatus.GetFirtByData();

                    // guardar datos de cotización
                    Ope_ExchangeRate entTCQ = Ope_ExchangeRate.GetAll().OrderByDescending(x => x.ExchangeRate_Date).FirstOrDefault();

                    Ope_Quotation entQuot = new Ope_Quotation()
                    {
                        Quotation_CustomerName = quotData.ClienteNombre,
                        Quotation_CustomerContact = quotData.ClienteContacto,
                        Quotation_CustomerEmail = quotData.ClienteEmail,
                        Quotation_CustomerLocation = quotData.ClienteUbicacion,
                        Quotation_Amount = amount,
                        Quotation_FinancialType = entProducto.FinancialType_Description,
                        Quotation_FinancialTerm = entPlazo.FinancialTerm_Description,
                        Quotation_Currency = entMoneda.Currency_Code,
                        Quotation_Payment = model.pagoMensual,
                        Quotation_IRR = model.irr,
                        Quotation_Date = DateTime.Now,
                        Quotation_UserId = objUser.UserId,
                        Quotation_ProgramId = quotData.Programa.ProgramId,
                        Quotation_StatusId = entStatus.StatusId,

                        Quotation_ExchangeRate = entTCQ.ExchangeRate_MXN
                    };

                    // Save / Update Quotation main info
                    if (quotData.CotizacionId > 0 && !quotData.Evento.Equals("clone"))
                    {
                        entQuot.QuotationId = quotData.CotizacionId;
                        entQuot.Update();
                    }
                    else
                    {
                        entQuot.Insert();
                    }

                    quotData.CotizacionId = entQuot.QuotationId.Value;

                    Ope_QuotInput entQInput = new Ope_QuotInput()
                    {
                        QuotInput_QuotationId = entQuot.QuotationId
                    };
                    entQInput = entQInput.GetFirtByData();

                    // Update Quotaition input info requires delete the old record.
                    if (entQInput != null && !quotData.Evento.Equals("clone"))
                    {
                        entQInput.Delete();
                    }
                    else
                    {
                        entQInput = new Ope_QuotInput();
                        entQInput.QuotInput_QuotationId = entQuot.QuotationId;
                    }

                    foreach (FieldData input in model.lstInputs)
                    {
                        string inputName = input.fieldName.Replace("txt", "");
                        switch (inputName)
                        {
                            case "PurchaseOption":
                                entQInput.QuotInput_PurchaseOption = Convert.ToDouble(input.fieldValue);
                                break;
                            case "BalloonPayment":
                                entQInput.QuotInput_BalloonPayment = Convert.ToDouble(input.fieldValue);
                                break;
                            case "OpFee":
                                entQInput.QuotInput_OpFee = Convert.ToDouble(input.fieldValue);
                                break;
                            case "SecurityDeposit":
                                entQInput.QuotInput_SecurityDeposit = Convert.ToDouble(input.fieldValue);
                                break;
                            case "BlindDiscount":
                                entQInput.QuotInput_BlindDiscount = Convert.ToDouble(input.fieldValue);
                                break;
                            case "ReferalFee":
                                entQInput.QuotInput_ReferalFee = Convert.ToDouble(input.fieldValue);
                                break;
                            case "Downpayment":
                                entQInput.QuotInput_Downpayment = Convert.ToDouble(input.fieldValue);
                                break;
                            case "Delay":
                                entQInput.QuotInput_Delay = Convert.ToInt32(input.fieldValue);
                                break;
                            case "GraceMonths":
                                entQInput.QuotInput_GraceMonths = Convert.ToInt32(input.fieldValue);
                                break;
                            case "AdditionalCosts":
                                if (model.monedaId.Equals("1"))
                                {
                                    entQInput.QuotInput_AdditionalCosts = Convert.ToInt32(input.fieldValue);
                                }
                                break;
                            case "AdditionalCostsMXN":
                                if (model.monedaId.Equals("2"))
                                {
                                    entQInput.QuotInput_AdditionalCosts = Convert.ToInt32(input.fieldValue);
                                }
                                break;
                            case "LegalFees":
                                if (model.monedaId.Equals("1"))
                                {
                                    entQInput.QuotInput_LegalFees = Convert.ToDouble(input.fieldValue);
                                }
                                break;
                            case "LegalFeesMXN":
                                if (model.monedaId.Equals("2"))
                                {
                                    entQInput.QuotInput_LegalFees = Convert.ToDouble(input.fieldValue);
                                }
                                break;
                        }
                    }
                    entQInput.Insert();

                    if (quotData.CotizacionId > 0 && !quotData.Evento.Equals("clone"))
                    {
                        // Update quotaition asset info requires delete the old records.
                        Rel_QuotationAsset relOldAsset = new Rel_QuotationAsset()
                        {
                            QuotationAsset_QuotationId = quotData.CotizacionId
                        };
                        Rel_QuotationAssetCollection lstOldAsset = relOldAsset.GetCollectionByData();

                        foreach(Rel_QuotationAsset objOldAsset in lstOldAsset)
                        {
                            objOldAsset.Delete();
                        }
                    }

                    foreach (EquipoModel asset in quotData.LstEquipos)
                    {
                        Rel_QuotationAsset relQA = new Rel_QuotationAsset()
                        {
                            QuotationAsset_QuotationId = entQuot.QuotationId,
                            QuotationAsset_AssetId = asset.ProductoId,
                            QuotationAsset_Amount = asset.Precio,
                            QuotationAsset_Quantity = asset.Cantidad
                        };
                        relQA.Insert();
                    }

                    if (quotData.CotizacionId > 0 && !quotData.Evento.Equals("clone"))
                    {
                        // Update quotaition amortization info requires delete the old records.
                        Det_QuotationDetail detOld = new Det_QuotationDetail()
                        {
                            QuotationDetail_QuotationId = quotData.CotizacionId
                        };
                        Det_QuotationDetailCollection lstOld = detOld.GetCollectionByData();

                        foreach (Det_QuotationDetail objDet in lstOld)
                        {
                            objDet.Delete();
                        }
                    }

                    foreach (MensualidadAmortiza mes in quotData.tablasAmortiza[0].TablaRegistros)
                    {
                        Det_QuotationDetail detQuot = new Det_QuotationDetail()
                        {
                            QuotationDetail_QuotationId = entQuot.QuotationId,
                            QuotationDetail_Period = mes.Periodo,
                            QuotationDetail_Payment = mes.RentaMonto,
                            QuotationDetail_Rate = mes.IvaMonto,
                            QuotationDetail_Total = mes.PagoTotal

                            ,QuotationDetail_Balance=mes.Balance,
                            QuotationDetail_Capital=mes.Capital,
                            QuotationDetail_Interes=mes.Interes,
                            QuotationDetail_Subtotal=mes.Subtotal,
                            QuotationDetail_Insurance=mes.Seguro,
                            QuotationDetail_Iva=mes.Iva,
                            QuotationDetail_PaymentIva=mes.PagoIva
                        };
                        detQuot.Insert();
                    }

                    // Registra evento de usuario
                    try
                    {
                        /*
                        Cat_User objUser = new Cat_User();
                        if (System.Web.HttpContext.Current.Session["objUser"] != null)
                        {
                            objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];
                        }
                        */

                        Cat_Event entEvento = new Cat_Event()
                        {
                            Event_Code = quotData.Evento.Equals("clone") ? "QCLON" : quotData.Evento.Equals("edit") ? "QEDIT" : "QCREA"
                        };
                        entEvento = entEvento.GetFirtByData();

                        Ope_UserEvent entUsrEvento = new Ope_UserEvent()
                        {
                            UserEvent_CatalogId = entQuot.QuotationId,
                            UserEvent_Date = DateTime.Now,
                            UserEvent_EventId = entEvento.EventId,
                            UserEvent_UserId = objUser.UserId != null ? objUser.UserId : 0
                        };
                        entUsrEvento.Insert();
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

                quotData.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                quotData.CargadoCorrecto = false;
                quotData.Mensajes = ex.Message;
            }
            return View(quotData);
        }
        #endregion

        #region Save Step Methods

        [HttpPost]
        public ActionResult GuardarStep1(QuotationModel quotData)
        {
            //QuotationModel quotData = new QuotationModel();

            try
            {
                //Cat_Location entUbicacion = new Cat_Location()
                //{
                //    LocationId = quotData ubicacionId
                //};
                //entUbicacion = entUbicacion.GetFirtByData();

                //if (entUbicacion == null)
                //{
                //    throw new Exception("Ubicación inválida.");
                //}

                //quotData = new Models.QuotationModel()
                //{
                //    Evento = quotData.Evento,
                //    CotizacionId = quotData.CotizacionId,

                //    ClienteNombre = quotData.ClienteNombre,
                //    ClienteContacto = quotData.ClienteContacto,
                //    ClienteEmail = quotData.ClienteEmail,
                //    ClienteUbicacion = quotData.ClienteUbicacion
                //};

                if (quotData.Evento.Equals("clone"))
                {

                }

                System.Web.HttpContext.Current.Session["quotationData"] = quotData;

                quotData.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                quotData.CargadoCorrecto = false;
                quotData.Mensajes = ex.Message;
            }

            return this.Json(quotData);
        }

        [HttpPost]
        public ActionResult ReleaseListAsset()
        {
            QuotationModel quotData = new QuotationModel();

            try
            {
                if (System.Web.HttpContext.Current.Session["quotationData"] != null)
                {
                    quotData = (QuotationModel)System.Web.HttpContext.Current.Session["quotationData"];
                    quotData.LstEquipos = new List<EquipoModel>();
                    System.Web.HttpContext.Current.Session["QuotationData"] = quotData;

                    quotData.CargadoCorrecto = true;
                }
                else
                {
                    quotData.CargadoCorrecto = false;
                    quotData.Mensajes = "Se ha superado el tiempo permitido de inactividad, la cotización de iniciarse desde el inicio.";
                }
            }
            catch(Exception ex)
            {
                quotData.CargadoCorrecto = false;
                quotData.Mensajes = ex.Message;
            }

            return this.Json(quotData);
        }

        [HttpPost]
        public ActionResult GuardarStep2(int cantidad, int productoId, double monto, int programaId, int numReg)
        {
            QuotationModel quotData = new QuotationModel();
            try
            {
                quotData = (QuotationModel)System.Web.HttpContext.Current.Session["quotationData"];
                if (quotData == null)
                {
                    throw new Exception("Los valores temporales han caducado, se debe iniciar nuevamente la cotización.");
                }

                //if (quotData.Evento.Equals("edit") && numReg == 0)
                //{
                //    quotData.LstEquipos.Clear();
                //}
                if (numReg == 1)
                {
                    quotData.LstEquipos = new List<Models.EquipoModel>();
                }

                Cat_Asset objProducto = new Cat_Asset()
                {
                    AssetId = productoId
                };
                objProducto = objProducto.GetFirtByData();

                Cat_Currency objCurrency = new Cat_Currency()
                {
                    CurrencyId = objProducto.Asset_CurrencyId
                };
                objCurrency = objCurrency.GetFirtByData();

                EquipoModel equipo = new EquipoModel()
                {
                    FamiliaId = objProducto.Asset_AssetCollateralId.Value,
                    ProductoId = objProducto.AssetId.Value,
                    Producto = objProducto.Asset_Model,
                    Precio = monto,
                    Cantidad = cantidad,
                    Moneda = objCurrency.Currency_Code
                };

                quotData.LstEquipos.Add(equipo);

                Ope_Program entPrograma = new Ope_Program()
                {
                    ProgramId = programaId
                };
                entPrograma = entPrograma.GetFirtByData();

                quotData.Programa = entPrograma;

                System.Web.HttpContext.Current.Session["QuotationData"] = quotData;

                quotData.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                quotData.CargadoCorrecto = false;
                quotData.Mensajes = ex.Message;
            }

            return this.Json(quotData);
        }

        [HttpPost]
        public ActionResult GuardarStep3(int finTipoId, int monedaId, int periodoId, int plazoId)
        {
            QuotationModel quotData = new Models.QuotationModel();

            try
            {
                quotData = (QuotationModel)System.Web.HttpContext.Current.Session["quotationData"];

                Cat_FinancialType entFinTipo = new Cat_FinancialType()
                {
                    FinancialTypeId = finTipoId
                };
                entFinTipo = entFinTipo.GetFirtByData();

                Cat_FinancialTerm entPlazo = new Cat_FinancialTerm()
                {
                    FinancialTermId = plazoId
                };
                entPlazo = entPlazo.GetFirtByData();

                Cat_Currency entMoneda = new Cat_Currency()
                {
                    CurrencyId = monedaId
                };
                entMoneda = entMoneda.GetFirtByData();

                quotData.FinanciamientoTipo = entFinTipo;
                quotData.FinanciamientoPlazo = entPlazo;
                quotData.FinanciamientoMoneda = entMoneda;

                System.Web.HttpContext.Current.Session["quotationData"] = quotData;
                quotData.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                quotData.CargadoCorrecto = false;
                quotData.Mensajes = ex.Message;
            }

            return this.Json(quotData);
        }

        #endregion

        #region General Methods
        [HttpPost]
        public JsonResult Calculate(QuotationDataModel model)
        {
            try
            {
                QuotationModel quotData = (QuotationModel)System.Web.HttpContext.Current.Session["QuotationData"];
                
                if (quotData == null)
                {
                    quotData = new QuotationModel();
                    throw new Exception("Se debe iniciar la cotización desde el primer paso.");
                }

                double resVal = 0.0;
                double amount = 0.0;

                foreach (EquipoModel activo in quotData.LstEquipos)
                {
                    Cat_Asset entActivo = new Cat_Asset()
                    {
                        AssetId = activo.ProductoId
                    };
                    entActivo = entActivo.GetFirtByData();

                    Ope_ExchangeRate entTC = Ope_ExchangeRate.GetAll().OrderByDescending(x => x.ExchangeRate_Date).FirstOrDefault();

                    amount += entActivo.Asset_CurrencyId == 1 && model.monedaId.Equals("2") ?
                        activo.Precio * entTC.ExchangeRate_MXN.Value :
                        entActivo.Asset_CurrencyId == 2 && model.monedaId.Equals("1") ?
                        activo.Precio / entTC.ExchangeRate_MXN.Value :
                        activo.Precio;

                    Rel_ProgramAsset objAct = new Rel_ProgramAsset()
                    {
                        ProgramAsset_ProgramId = quotData.Programa.ProgramId,
                        ProgramAsset_AssetId = activo.ProductoId
                    };
                    objAct = objAct.GetFirtByData();

                    Rel_AssetFinancialTerm relAFT = new Rel_AssetFinancialTerm()
                    {
                        AssetFinancialTerm_AssetId = activo.ProductoId,
                        AssetFinancialTerm_FinancialTermId = Convert.ToInt32(model.plazoId)
                    };
                    relAFT = relAFT.GetFirtByData();

                    if (relAFT == null)
                    {
                        throw new Exception("No se ha definido un valor residual (Residual Value) para el activo y plazo seleccionados.");
                    }

                    resVal += relAFT.AssetFinancialTerm_ResidualValue.Value;
                }

                string segment = quotData.Programa.Program_Segment;

                Cat_FinancialType entProducto = new Cat_FinancialType()
                {
                    FinancialTypeId = Convert.ToInt32(model.productoId)
                };
                entProducto = entProducto.GetFirtByData();
                model.product = entProducto.FinancialType_Description;

                switch (entProducto.FinancialType_Description)
                {
                    case "A. Puro":
                        segment = "TL-" + segment;
                        break;
                    case "A. Financiero":
                        segment = "QL-" + segment;
                        break;
                    case "Crédito":
                        segment = "LN-" + segment;
                        break;
                }

                Ope_ProfitTarget entTarget = new Ope_ProfitTarget()
                {
                    ProfitTarget_FinacialTermId = Convert.ToInt32(model.plazoId),
                    ProfitTarger_CurrencyId = Convert.ToInt32(model.monedaId),
                    ProfitTarget_Segment = segment,
                    ProfitTarget_Active = true
                };
                entTarget = entTarget.GetFirtByData();

                if (entTarget == null)
                {
                    throw new Exception("No se ha definido un rendimiento objetivo (Target) para el plazo y moneda seleccionados.");
                }

                double target = model.insFinancied ? entTarget.ProfitTarget_Percentage.Value * 1.05 : entTarget.ProfitTarget_Percentage.Value;

                model = UtilitiesGeneral.CalculateQuotation(model, amount, segment, resVal, entTarget.ProfitTarget_Percentage.Value, 
                    quotData.Programa, entProducto);
                model = UtilitiesGeneral.CalculateInterestRate(model, amount);

                double secCost = UtilitiesGeneral.CalculateInsuranceAmount(quotData, model);

                //if (model.insFinancied && secCost > 0)
                //{
                //    model = UtilitiesGeneral.CalculateInsurance(model, secCost, target, quotData.Programa);
                //    model.Mensajes = String.Format("{0:C}\n Segmento: {1}\n Monto Total: {2:C}", secCost, segment, amount);
                //}
                //else
                //{
                //    model.Mensajes = String.Format("{0:C} \n Monto Total: {1:C}", segment, amount);
                //}

                //model.pagoMensual = Math.Round(model.pagoMensual, 2);

                if (model.insFinancied && secCost > 0)
                {
                    model = UtilitiesGeneral.CalculateInsurance(model, secCost, target, quotData.Programa);
                }

                model.Mensajes = String.Format("Monto Total: {0:C}", amount);

                System.Web.HttpContext.Current.Session["QuotationData"] = quotData;

                model.CargadoCorrecto = true;

            }
            catch(Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }
            return this.Json(model);
        }

        [HttpPost]
        public JsonResult SetDiscount(bool delay, int discount)
        {
            Step4Model model = new Models.Step4Model();

            try
            {
                QuotationModel quotData = (QuotationModel)System.Web.HttpContext.Current.Session["QuotationData"];

                quotData.Delay = delay;
                quotData.DescuentoPorc = discount;

                System.Web.HttpContext.Current.Session["QuotationData"] = quotData;

                model.CargadoCorrecto = true;
            }
            catch(Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return this.Json(model);
        }

        [HttpPost]
        public JsonResult GetProductByFamily(int familiaId, int programaId)
        {
            Step2Model model = new Models.Step2Model();
            try
            {
                Rel_ProgramAsset entProgProd = new Rel_ProgramAsset()
                {
                    ProgramAsset_ProgramId = programaId
                };
                Rel_ProgramAssetCollection lstProgProd = entProgProd.GetCollectionByData();

                Cat_AssetCollateral entFamilia = new Cat_AssetCollateral()
                {
                    AssetCollateralId = familiaId
                };
                entFamilia = entFamilia.GetFirtByData();

                Cat_Asset entProducto = new Cat_Asset()
                {
                    Asset_AssetCollateralId = familiaId,
                    Asset_Active = true
                };
                Cat_AssetCollection lstProducto = entProducto.GetCollectionByData();
                List<Cat_Asset> nlstProd = lstProducto.Where(x => lstProgProd.Any(y => y.ProgramAsset_AssetId.Equals(x.AssetId))).ToList();

                model.familiaSelected = entFamilia;
                model.lstProducto = nlstProd;

                model.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }
            return this.Json(model);
        }

        [HttpPost]
        public JsonResult GetProductData(int productoId)
        {
            Step2Model model = new Step2Model();

            try
            {
                Cat_Asset entProducto = new Cat_Asset()
                {
                    AssetId = productoId
                };
                entProducto = entProducto.GetFirtByData();

                model.productoSelected = entProducto;

                model.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return this.Json(model);
        }

        [HttpPost]
        public ActionResult AddProduct(int productoId, int cantidad)
        {
            QuotationModel quotData = (QuotationModel)System.Web.HttpContext.Current.Session["QuotationData"];
            try
            {

                Cat_Asset objProducto = new Cat_Asset()
                {
                    AssetId = productoId
                };
                objProducto = objProducto.GetFirtByData();

                EquipoModel equipo = new EquipoModel()
                {
                    FamiliaId = objProducto.Asset_AssetCollateralId.Value,
                    ProductoId = objProducto.AssetId.Value,
                    Producto = objProducto.Asset_Model,
                    Precio = objProducto.Asset_OEC.Value,
                    Cantidad = cantidad
                };

                quotData.LstEquipos.Add(equipo);
                System.Web.HttpContext.Current.Session["QuotationData"] = quotData;
                quotData.CargadoCorrecto = true;

            }
            catch (Exception ex)
            {
                quotData.CargadoCorrecto = false;
                quotData.Mensajes = ex.Message;
            }

            return this.Json(quotData);
        }

        #endregion

    }
}