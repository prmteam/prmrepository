﻿using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EC.PRM.UI.Web.Controllers
{
    public class AssetCatalogController : Controller
    {
        // GET: AssetCatalog
        public ActionResult Index(string dsearch)
        {
            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                Response.Redirect("/Home/Login");
            }

            AssetCatalogModel model = new Models.AssetCatalogModel();

            Cat_Asset entAsset = new Cat_Asset()
            {
                Asset_Active = true
            };
            Cat_AssetCollection lstAsset = entAsset.GetCollectionByData();

            List<tblAsset> lstTblAsset = new List<Models.tblAsset>();

            foreach(Cat_Asset objAsset in lstAsset)
            {
                if (!String.IsNullOrEmpty(dsearch))
                {
                    if(!UtilitiesGeneral.Contains(objAsset.Asset_Description, dsearch, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                }

                tblAsset objTbl = new tblAsset()
                {
                    AssetId = objAsset.AssetId.Value,
                    Asset_AssetCollateralId = objAsset.Asset_AssetCollateralId,
                    Asset_Brand = objAsset.Asset_Brand,
                    Asset_Model = objAsset.Asset_Model,
                    Asset_Description = objAsset.Asset_Description,
                    Asset_Year = objAsset.Asset_Year,
                    Asset_OEC = objAsset.Asset_OEC,
                    Asset_CurrencyId = objAsset.Asset_CurrencyId
                };

                Cat_AssetCollateral entCollateral = new Cat_AssetCollateral()
                {
                    AssetCollateralId = objAsset.Asset_AssetCollateralId.Value
                };
                entCollateral = entCollateral.GetFirtByData();

                Cat_Currency entCurrency = new Cat_Currency()
                {
                    CurrencyId = objAsset.Asset_CurrencyId.Value
                };
                entCurrency = entCurrency.GetFirtByData();

                objTbl.collateral = entCollateral;
                objTbl.currency = entCurrency;

                lstTblAsset.Add(objTbl);
            }

            Cat_AssetCollateral entColl = new Cat_AssetCollateral()
            {
                AssetCollateral_Active = true
            };
            Cat_AssetCollateralCollection lstColl = entColl.GetCollectionByData();

            Ope_Visual entVisual = UtilitiesGeneral.GetVisual();
            model.visualInfo = entVisual;

            Cat_Currency entCurr = new Cat_Currency()
            {
                Currency_Active = true
            };
            Cat_CurrencyCollection lstCurrency = entCurr.GetCollectionByData();

            model.dsearch = dsearch;
            model.lstCollateral = lstColl;
            model.lstAsset = lstTblAsset;
            model.lstCurrency = lstCurrency;

            return View(model);
        }

        [HttpPost]
        public JsonResult UpdateAsset(Cat_Asset assetInfo, bool activo)
        {
            MensajesBaseModel model = new Models.MensajesBaseModel();
            try
            {
                assetInfo.Asset_Active = activo;
                assetInfo.Update();
                model.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }
            return this.Json(model);
        }

        [HttpPost]
        public JsonResult SaveAsset(Cat_Asset assetInfo)
        {
            MensajesBaseModel model = new Models.MensajesBaseModel();
            try
            {
                if (System.Web.HttpContext.Current.Session["objUser"] == null)
                {
                    Response.Redirect("/Home/Login");
                }
                Cat_User objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];

                assetInfo.Asset_Active = true;
                assetInfo.Insert();

                Rel_AssetCollateralVendor entGrupoVendor = new Rel_AssetCollateralVendor()
                {
                    AssetCollateralVendor_AssetCollateralId = assetInfo.Asset_AssetCollateralId,
                    AssetCollateralVendor_VendorId = objUser.User_VendorId
                };
                entGrupoVendor.Insert();

                model.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }
            return this.Json(model);
        }

        [HttpPost]
        public JsonResult SaveCollateral(Cat_AssetCollateral entCollateral)
        {
            AssetCollateralModel model = new Models.AssetCollateralModel();

            try
            {
                if (System.Web.HttpContext.Current.Session["objUser"] == null)
                {
                    Response.Redirect("/Home/Login");
                }
                Cat_User objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];

                entCollateral.AssetCollateral_Active = true;
                entCollateral.Insert();

                Rel_AssetCollateralVendor entGrupoVendor = new Rel_AssetCollateralVendor()
                {
                    AssetCollateralVendor_AssetCollateralId = entCollateral.AssetCollateralId,
                    AssetCollateralVendor_VendorId = objUser.User_VendorId
                };
                entGrupoVendor.Insert();

                Cat_AssetCollateral objColl = new Cat_AssetCollateral()
                {
                    AssetCollateral_Active = true
                };

                Cat_AssetCollateralCollection lstColl = objColl.GetCollectionByData();
                model.lstCollateral = lstColl;
                model.CargadoCorrecto = true;
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return this.Json(model);
        }
    }
}