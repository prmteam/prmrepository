﻿using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

namespace EC.PRM.UI.Web.Controllers
{
    public class QuotationApiController : ApiController
    {
        [HttpGet]
        [Route("api/QuotationApi/SendQuotationEmail")]
        public MensajesBaseModel SendQuotationEmail(int? itemId, string address)
        {
            MensajesBaseModel resp = new MensajesBaseModel();
            QuoteFileModel modelo = new Models.QuoteFileModel();

            try
            {
                if (itemId != null)
                {
                    string fileExists = HttpContext.Current.Server.MapPath("~/FilesTemp") + "\\Quot" + itemId + ".pdf";

                    if (!File.Exists(fileExists))
                    {
                        //File.Delete(fileExists);

                        modelo.itemId = itemId.Value;
                        modelo.rutaPlantillaWord = "~/DocTemplate/PRM_Quotation.docx";
                        resp = UtilitiesGeneral.CreateQuoteFile(modelo);
                    }
                    else
                    {
                        resp.CargadoCorrecto = true;
                    }
                    //resp.CargadoCorrecto = true;
                    //resp.Mensajes = HttpContext.Current.Server.MapPath("~/") + "FilesTemp\\Quot3.pdf";

                    if (resp.CargadoCorrecto)
                    {
                        string textEmail = "Buen día,<br/><br/>Por medio del presente se pone a su disposición la cotización de financiamiento solicitada.<br/><br/>Saludos cordiales.<br/><br/>";

                        EmailModel objEmail = new EmailModel()
                        {
                            Subject = "EC PRM Cotización",
                            Recipients = address,
                            Message = textEmail,
                            SMTP = ConfigurationManager.AppSettings["Email_SMTP"],
                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Email_Port"]),
                            User = ConfigurationManager.AppSettings["Email_User"],
                            Password = ConfigurationManager.AppSettings["Email_Pass"],
                            Sender = ConfigurationManager.AppSettings["Email_Sender"],
                            AttachmentFile = HttpContext.Current.Server.MapPath("~/") + "FilesTemp\\Quot" + itemId + ".pdf"
                        };

                        if (!Email.Send(objEmail))
                        {
                            throw new Exception("No se pudo enviar el correo.\nVerifique la configuración del servicio SMTP.");
                        }

                        resp.CargadoCorrecto = true;
                    }

                    // Registra evento de usuario
                    // Por seguridad api controller no puede acceder al objeto HttpContext.Current.Session.
                    // De requerirse registrar este evento, desarrollar funcionalida para recibir el id del usuario desde la vista
                    //try
                    //{
                    //    Cat_User objUser = new Cat_User();
                    //    if (System.Web.HttpContext.Current.Session["objUser"] != null)
                    //    {
                    //        objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];
                    //    }

                    //    Cat_Event entEvento = new Cat_Event()
                    //    {
                    //        Event_Code = "QSEND"
                    //    };
                    //    entEvento = entEvento.GetFirtByData();

                    //    Ope_UserEvent entUsrEvento = new Ope_UserEvent()
                    //    {
                    //        UserEvent_CatalogId = itemId,
                    //        UserEvent_Date = DateTime.Now,
                    //        UserEvent_EventId = entEvento.EventId,
                    //        UserEvent_UserId = objUser.UserId != null ? objUser.UserId : 0
                    //    };
                    //    entUsrEvento.Insert();
                    //}
                    //catch(Exception ex)
                    //{
                    //    Console.WriteLine(ex.Message);
                    //}
                }
                else
                {
                    throw new Exception("Se requiere el identificador de la cotización.");
                }

            }
            catch (Exception ex)
            {
                resp.CargadoCorrecto = false;
                resp.Mensajes = ex.Message;
            }

            return resp;
        }

        [HttpGet]
        [Route("api/QuotationApi/CreateQuoteFile")]
        public MensajesBaseModel CreateQuoteFile(int? itemId)
        {
            MensajesBaseModel resp = new MensajesBaseModel();
            QuoteFileModel modelo = new Models.QuoteFileModel();
            
            try
            {
                if (itemId != null)
                {
                    modelo.itemId = itemId.Value;
                    modelo.rutaPlantillaWord = "~/DocTemplate/PRM_Quotation.docx";

                    /////// Test NPOI
                    //resp = UtilitiesGeneral.CreateQuoteFile(modelo);

                    resp = UtilitiesGeneral.CreateQuoteFileNPOI(modelo);

                    // Registra evento de usuario
                    // Por seguridad api controller no puede acceder al objeto HttpContext.Current.Session.
                    // De requerirse registrar este evento, desarrollar funcionalida para recibir el id del usuario desde la vista
                    //try
                    //{
                    //    Cat_User objUser = new Cat_User();

                    //    if(HttpContext.Current!= null)
                    //    {
                    //        if (System.Web.HttpContext.Current.Session["objUser"] != null)
                    //        {
                    //            objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];
                    //        }
                    //    }



                    //    Cat_Event entEvento = new Cat_Event()
                    //    {
                    //        Event_Code = "QDOWN"
                    //    };
                    //    entEvento = entEvento.GetFirtByData();

                    //    Ope_UserEvent entUsrEvento = new Ope_UserEvent()
                    //    {
                    //        UserEvent_CatalogId = itemId,
                    //        UserEvent_Date = DateTime.Now,
                    //        UserEvent_EventId = entEvento.EventId,
                    //        UserEvent_UserId = objUser.UserId != null ? objUser.UserId : 0
                    //    };
                    //    entUsrEvento.Insert();
                    //}
                    //catch(Exception ex)
                    //{
                    //    Console.WriteLine(ex.Message);
                    //}
                }
                else
                {
                    throw new Exception("Se requiere el identificador de la cotización.");
                }

            }
            catch(Exception ex)
            {
                resp.CargadoCorrecto = false;
                resp.Mensajes = ex.Message;
            }

            return resp;
        }

        private static FileAttributes RemoveAttribute(FileAttributes attributes, FileAttributes attributesToRemove)
        {
            return attributes & ~attributesToRemove;
        }
    }
}
