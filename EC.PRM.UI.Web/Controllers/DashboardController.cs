﻿using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EC.PRM.UI.Web.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index(
            string vendor,
            string fechaIni,
            string fechaFin,
            string programa,
            int? idEstatus,
            string ejecutivo,
            string palabra)
        {
            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                return View("../Home/Login");
            }

            DashboardModel modelo = new Models.DashboardModel();

            try
            {
                modelo.datosFiltro.Vendor = vendor;
                modelo.datosFiltro.FechaInicio = fechaIni;
                modelo.datosFiltro.FechaFin = fechaFin;
                modelo.datosFiltro.Programa = programa;
                modelo.datosFiltro.Ejecutivo = ejecutivo;
                modelo.datosFiltro.IdEstatus = idEstatus;
                modelo.datosFiltro.Palabra = palabra;

                Cat_Status cotAbierta = new Cat_Status()
                {
                    Status_Catalog = "Quotation",
                    Status_Status = "Abierto"
                };
                cotAbierta = cotAbierta.GetFirtByData();

                int numCotAbierta = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotAbierta.StatusId).Count();
                modelo.lstDetLead = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotAbierta.StatusId).OrderByDescending(x=>x.Quotation_Date).ToList();

                Cat_Status cotProceso = new Cat_Status()
                {
                    Status_Catalog = "Quotation",
                    Status_Status = "En proceso"
                };
                cotProceso = cotProceso.GetFirtByData();
                modelo.lstDetOps = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotProceso.StatusId).OrderByDescending(x=>x.Quotation_Date).ToList();

                int numCotConvertida = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotProceso.StatusId).Count();
                int numCotTotal = numCotAbierta + numCotConvertida;
                double porcConvertida = (numCotConvertida * 100) / numCotTotal;

                int numVendor = Cat_Vendor.GetAll().Where(x => x.Vendor_Active == true).Count();
                int numProgama = Ope_Program.GetAll().Where(x => x.Program_Active == true).Count();
                int numUsuario = Cat_User.GetAll().Where(x => x.User_Active == true).Count();
                int numUsuarioEvento = Ope_UserEvent.GetAll().Select(x => x.UserEvent_UserId).Distinct().Count();
                double porcUso = (numUsuarioEvento * 100) / numUsuario;

                modelo.lstDetClosed = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == 3 || x.Quotation_StatusId == 4 || x.Quotation_StatusId == 5).OrderByDescending(x=>x.Quotation_Date).ToList();

                var lstPerfil = Cat_UserProfile.GetAll().Where(x => x.UserProfile_Active == true);
                var lstUser = Cat_User.GetAll().Where(x => x.User_Active == true);
                var lstEvento = Cat_Event.GetAll().Where(x => x.Event_Active == true);
                var lstUso = Ope_UserEvent.GetAll().OrderByDescending(x => x.UserEvent_Date).Take(20);
                var lstVendor = Cat_Vendor.GetAll().Where(x => x.Vendor_Active == true);

                var detUso = (from e in lstUso
                              join u in lstUser
                              on e.UserEvent_UserId equals u.UserId
                              join v in lstEvento
                              on e.UserEvent_EventId equals v.EventId
                              join r in lstVendor
                              on u.User_VendorId equals r.VendorId
                              select new UsuarioUso
                              {
                                  Usuario = u.User_User,
                                  Nombre = u.User_FirstName,
                                  Apellido = u.User_LastName,
                                  Evento = v.Event_Descritipon,
                                  Fecha = e.UserEvent_Date,
                                  Vendor = r.Vendor_Name
                              }).ToList();

                modelo.lstDetUso = detUso;

                var detEjecutivo = (from u in lstUser
                                    join p in lstPerfil
                                    on u.User_UserProfileId equals p.UserProfileId
                                    join r in lstVendor
                                    on u.User_VendorId equals r.VendorId
                                    select new Ejecutivo
                                    {
                                        Usuario = u.User_User,
                                        Nombre = u.User_FirstName,
                                        Apellido = u.User_LastName,
                                        Perfil = p.UserProfile_Profile,
                                        Vendor = r.Vendor_Name
                                    }).ToList();

                modelo.lstDetExecutives = detEjecutivo;

                modelo.cotizacionesAbiertas = numCotAbierta;
                modelo.cotizacionesConvertidas = numCotConvertida;
                modelo.cotizacionesTotal = numCotTotal;
                modelo.numVendors = numVendor;
                modelo.numProgramas = numProgama;
                modelo.numUsuarios = numUsuario;

                modelo.lstQuotStatus = Cat_Status.GetAll().Where(x => x.Status_Active == true).ToList();

                List<Ope_Quotation> lstDetLeadAux = modelo.lstDetLead;
                List<Ope_Quotation> lstDetOpsAux = modelo.lstDetOps;
                List<Ope_Quotation> lstDetClosedAux = modelo.lstDetClosed;
                List<Ejecutivo> lstDetExecAux = modelo.lstDetExecutives;
                List<UsuarioUso> lstDetUsoAux = modelo.lstDetUso;

                ////// filtro Vendor

                if (!String.IsNullOrEmpty(modelo.datosFiltro.Vendor))
                {
                    Cat_Vendor entVendor = Cat_Vendor.GetAll().Where(x => UtilitiesGeneral.Contains(x.Vendor_Name, modelo.datosFiltro.Vendor, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                    if (entVendor == null)
                    {
                        modelo.lstDetLead = new List<Ope_Quotation>();
                        modelo.lstDetOps = new List<Ope_Quotation>();
                        modelo.lstDetClosed = new List<Ope_Quotation>();
                        modelo.lstDetExecutives = new List<Ejecutivo>();
                        modelo.lstDetUso = new List<UsuarioUso>();
                        modelo.CargadoCorrecto = true;

                        return View(modelo);
                    }
                    else
                    {
                        Ope_Program entProgram = new Ope_Program()
                        {
                            Program_VendorId = entVendor.VendorId
                        };
                        Ope_ProgramCollection lstProgramV = entProgram.GetCollectionByData();

                        modelo.lstDetLead = new List<Ope_Quotation>();
                        modelo.lstDetOps = new List<Ope_Quotation>();
                        modelo.lstDetClosed = new List<Ope_Quotation>();
                        modelo.lstDetExecutives = new List<Ejecutivo>();
                        modelo.lstDetUso = new List<UsuarioUso>();

                        foreach (Ope_Program objProgram in lstProgramV)
                        {
                            modelo.lstDetLead.AddRange(lstDetLeadAux.Where(x => x.Quotation_ProgramId == objProgram.ProgramId).ToList());
                            modelo.lstDetOps.AddRange(lstDetOpsAux.Where(x => x.Quotation_ProgramId == objProgram.ProgramId).ToList());
                            modelo.lstDetClosed = modelo.lstDetClosed.Where(x => x.Quotation_ProgramId == entProgram.ProgramId).ToList();
                            modelo.lstDetExecutives = modelo.lstDetExecutives.Where(x => x.Vendor == modelo.datosFiltro.Vendor).ToList();
                            modelo.lstDetUso = modelo.lstDetUso.Where(x => x.Vendor == modelo.datosFiltro.Vendor).ToList();
                        }

                        modelo.lstDetLead = modelo.lstDetLead.OrderByDescending(x => x.Quotation_Date).ToList();
                    }
                }

                ///// filtro Programa
                if (!String.IsNullOrEmpty(modelo.datosFiltro.Programa))
                {
                    Ope_Program entProgramF = Ope_Program.GetAll().Where(x => UtilitiesGeneral.Contains(x.Program_Description, modelo.datosFiltro.Programa, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                    if (entProgramF == null)
                    {
                        modelo.lstDetLead = new List<Ope_Quotation>();
                        modelo.lstDetOps = new List<Ope_Quotation>();
                        modelo.lstDetClosed = new List<Ope_Quotation>();
                        modelo.lstDetExecutives = new List<Ejecutivo>();
                        modelo.lstDetUso = new List<UsuarioUso>();
                        modelo.CargadoCorrecto = true;

                        return View(modelo);
                    }
                    else
                    {
                        modelo.lstDetLead = modelo.lstDetLead.Where(x => x.Quotation_ProgramId == entProgramF.ProgramId).ToList();
                        modelo.lstDetOps = modelo.lstDetOps.Where(x => x.Quotation_ProgramId == entProgramF.ProgramId).ToList();
                        modelo.lstDetClosed = modelo.lstDetClosed.Where(x => x.Quotation_ProgramId == entProgramF.ProgramId).ToList();
                    }
                }

                ////// filtro Fechas

                if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaInicio) || !String.IsNullOrEmpty(modelo.datosFiltro.FechaFin))
                {
                    modelo.lstDetLead = new List<Ope_Quotation>();
                    modelo.lstDetOps = new List<Ope_Quotation>();
                    modelo.lstDetClosed = new List<Ope_Quotation>();
                    //modelo.lstDetExecutives = new List<Ejecutivo>();
                    modelo.lstDetUso = new List<UsuarioUso>();

                    foreach(Ope_Quotation q in lstDetLeadAux)
                    {
                        if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaInicio) && !String.IsNullOrEmpty(modelo.datosFiltro.FechaFin))
                        {
                            if (Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) <=
                                Convert.ToDateTime(modelo.datosFiltro.FechaFin) &&
                                Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) >=
                                Convert.ToDateTime(modelo.datosFiltro.FechaInicio))
                            {
                                modelo.lstDetLead.Add(q);
                            }
                        }
                        else if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaFin))
                        {
                            if (Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) <=
                                Convert.ToDateTime(modelo.datosFiltro.FechaFin))
                            {
                                modelo.lstDetLead.Add(q);
                            }
                        }
                        else if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaInicio))
                        {
                            if (Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) >=
                                Convert.ToDateTime(modelo.datosFiltro.FechaInicio))
                            {
                                modelo.lstDetLead.Add(q);
                            }
                        }
                    }

                    foreach(Ope_Quotation q in lstDetOpsAux)
                    {
                        if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaInicio) && !String.IsNullOrEmpty(modelo.datosFiltro.FechaFin))
                        {
                            if (Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) <=
                                Convert.ToDateTime(modelo.datosFiltro.FechaFin) &&
                                Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) >=
                                Convert.ToDateTime(modelo.datosFiltro.FechaInicio))
                            {
                                modelo.lstDetOps.Add(q);
                            }
                        }
                        else if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaFin))
                        {
                            if (Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) <=
                                Convert.ToDateTime(modelo.datosFiltro.FechaFin))
                            {
                                modelo.lstDetOps.Add(q);
                            }
                        }
                        else if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaInicio))
                        {
                            if (Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) >=
                                Convert.ToDateTime(modelo.datosFiltro.FechaInicio))
                            {
                                modelo.lstDetOps.Add(q);
                            }
                        }
                    }

                    foreach(Ope_Quotation q in lstDetClosedAux)
                    {
                        if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaInicio) && !String.IsNullOrEmpty(modelo.datosFiltro.FechaFin))
                        {
                            if (Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) <=
                                Convert.ToDateTime(modelo.datosFiltro.FechaFin) &&
                                Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) >=
                                Convert.ToDateTime(modelo.datosFiltro.FechaInicio))
                            {
                                modelo.lstDetClosed.Add(q);
                            }
                        }
                        else if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaFin))
                        {
                            if (Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) <=
                                Convert.ToDateTime(modelo.datosFiltro.FechaFin))
                            {
                                modelo.lstDetClosed.Add(q);
                            }
                        }
                        else if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaInicio))
                        {
                            if (Convert.ToDateTime(q.Quotation_Date.Value.ToShortDateString()) >=
                                Convert.ToDateTime(modelo.datosFiltro.FechaInicio))
                            {
                                modelo.lstDetClosed.Add(q);
                            }
                        }
                    }

                    foreach(UsuarioUso q in lstDetUsoAux)
                    {
                        if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaInicio) && !String.IsNullOrEmpty(modelo.datosFiltro.FechaFin))
                        {
                            if (Convert.ToDateTime(q.Fecha.Value.ToShortDateString()) <=
                                Convert.ToDateTime(modelo.datosFiltro.FechaFin) &&
                                Convert.ToDateTime(q.Fecha.Value.ToShortDateString()) >=
                                Convert.ToDateTime(modelo.datosFiltro.FechaInicio))
                            {
                                modelo.lstDetUso.Add(q);
                            }
                        }
                        else if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaFin))
                        {
                            if (Convert.ToDateTime(q.Fecha.Value.ToShortDateString()) <=
                                Convert.ToDateTime(modelo.datosFiltro.FechaFin))
                            {
                                modelo.lstDetUso.Add(q);
                            }
                        }
                        else if (!String.IsNullOrEmpty(modelo.datosFiltro.FechaInicio))
                        {
                            if (Convert.ToDateTime(q.Fecha.Value.ToShortDateString()) >=
                                Convert.ToDateTime(modelo.datosFiltro.FechaInicio))
                            {
                                modelo.lstDetUso.Add(q);
                            }
                        }
                    }

                }

                //////

                // Filtro ejecutivo.

                if (!String.IsNullOrEmpty(modelo.datosFiltro.Ejecutivo))
                {
                    Cat_User entEjecutivo = Cat_User.GetAll().Where(x => UtilitiesGeneral.Contains(x.User_FirstName, modelo.datosFiltro.Ejecutivo, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                    if (entEjecutivo == null)
                    {
                        modelo.lstDetLead = new List<Ope_Quotation>();
                        modelo.lstDetOps = new List<Ope_Quotation>();
                        modelo.lstDetClosed = new List<Ope_Quotation>();
                        modelo.lstDetExecutives = new List<Ejecutivo>();
                        modelo.lstDetUso = new List<UsuarioUso>();
                        modelo.CargadoCorrecto = true;

                        return View(modelo);
                    }
                    else if (!String.IsNullOrEmpty(modelo.datosFiltro.Ejecutivo))
                    {
                        modelo.lstDetLead = modelo.lstDetLead.Where(x => x.Quotation_UserId == entEjecutivo.UserId.Value).ToList();
                        modelo.lstDetOps = modelo.lstDetOps.Where(x => x.Quotation_UserId == entEjecutivo.UserId.Value).ToList();
                        modelo.lstDetClosed = modelo.lstDetClosed.Where(x => x.Quotation_UserId == entEjecutivo.UserId.Value).ToList();
                        modelo.lstDetExecutives = modelo.lstDetExecutives.Where(x => UtilitiesGeneral.Contains(x.Nombre, modelo.datosFiltro.Ejecutivo, StringComparison.CurrentCultureIgnoreCase)).ToList();
                        modelo.lstDetUso = modelo.lstDetUso.Where(x => UtilitiesGeneral.Contains(x.Nombre, modelo.datosFiltro.Ejecutivo, StringComparison.CurrentCultureIgnoreCase)).ToList();
                    }
                }

                ///////

                ////// Filtro Estatus.

                if (modelo.datosFiltro.IdEstatus.HasValue && modelo.datosFiltro.IdEstatus > 0)
                {
                    Cat_Status entEstatus = Cat_Status.GetOne(modelo.datosFiltro.IdEstatus.Value);

                    modelo.lstDetLead = modelo.lstDetLead.Where(x => x.Quotation_StatusId == entEstatus.StatusId.Value).ToList();
                    modelo.lstDetOps = modelo.lstDetOps.Where(x => x.Quotation_StatusId == entEstatus.StatusId.Value).ToList();
                    modelo.lstDetClosed = modelo.lstDetClosed.Where(x => x.Quotation_StatusId == entEstatus.StatusId.Value).ToList();
                }

                ///////

                modelo.CargadoCorrecto = true;
            }
            catch(Exception ex)
            {
                modelo.CargadoCorrecto = false;
                modelo.Mensajes = ex.Message;
            }

            return View(modelo);
        }

        public JsonResult GetEstausData()
        {
            try
            {
                List<DatoGrafico> lstEstatus = new List<Models.DatoGrafico>();

                Cat_Status cotAbierta = new Cat_Status()
                {
                    Status_Catalog = "Quotation",
                    Status_Status = "Abierto"
                };
                cotAbierta = cotAbierta.GetFirtByData();
                int numCotAbierta = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotAbierta.StatusId).Count();
                DatoGrafico dgAbierto = new DatoGrafico()
                {
                    Texto = "Abierto",
                    Valor = numCotAbierta
                };
                lstEstatus.Add(dgAbierto);

                Cat_Status cotProceso = new Cat_Status()
                {
                    Status_Catalog = "Quotation",
                    Status_Status = "En proceso"
                };
                cotProceso = cotProceso.GetFirtByData();
                int numCotConvertida = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotProceso.StatusId).Count();
                DatoGrafico dgProceso = new Models.DatoGrafico()
                {
                    Texto = "Proceso",
                    Valor = numCotConvertida
                };
                lstEstatus.Add(dgProceso);

                Cat_Status cotCerrado = new Cat_Status()
                {
                    Status_Catalog = "Quotation",
                    Status_Status = "Cerrado"
                };
                cotCerrado = cotCerrado.GetFirtByData();
                int numCotCerrado = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotCerrado.StatusId).Count();
                DatoGrafico dgCerrado = new Models.DatoGrafico()
                {
                    Texto = "Cerrado",
                    Valor = numCotCerrado
                };
                lstEstatus.Add(dgCerrado);

                Cat_Status cotPerdido = new Cat_Status()
                {
                    Status_Catalog = "Quotation",
                    Status_Status = "Perdido"
                };
                cotPerdido = cotPerdido.GetFirtByData();
                int numCotPerdido = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotPerdido.StatusId).Count();
                DatoGrafico dgPerdido = new Models.DatoGrafico()
                {
                    Texto = "Perdido",
                    Valor = numCotPerdido
                };
                lstEstatus.Add(dgPerdido);

                Cat_Status cotGanado = new Cat_Status()
                {
                    Status_Catalog = "Quotation",
                    Status_Status = "Ganado"
                };
                cotGanado = cotGanado.GetFirtByData();
                int numCotGanado = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotGanado.StatusId).Count();
                DatoGrafico dgGanado = new DatoGrafico()
                {
                    Texto = "Ganado",
                    Valor = 0
                };
                lstEstatus.Add(dgGanado);

                var result = (from d in lstEstatus
                              select new { d.Texto, d.Valor });

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetConvertionData()
        {
            try
            {
                Cat_Status cotAbierta = new Cat_Status()
                {
                    Status_Catalog = "Quotation",
                    Status_Status = "Abierto"
                };
                cotAbierta = cotAbierta.GetFirtByData();

                int numCotAbierta = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotAbierta.StatusId).Count();

                Cat_Status cotProceso = new Cat_Status()
                {
                    Status_Catalog = "Quotation",
                    Status_Status = "En proceso"
                };
                cotProceso = cotProceso.GetFirtByData();

                int numCotConvertida = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotProceso.StatusId).Count();
                int numCotTotal = numCotAbierta + numCotConvertida;
                double porcConvertida = (numCotConvertida * 100) / numCotTotal;

                List<DatoGrafico> lstPorcentajeConversion = new List<DatoGrafico>();
                DatoGrafico dpConvertida = new DatoGrafico()
                {
                    Texto = "Cotizaciones convertidas ",
                    Valor = porcConvertida
                };
                DatoGrafico dpConvertidaResto = new DatoGrafico()
                {
                    Texto = "Sin convertir",
                    Valor = 100 - porcConvertida
                };
                lstPorcentajeConversion.Add(dpConvertida);
                lstPorcentajeConversion.Add(dpConvertidaResto);

                var result = (from d in lstPorcentajeConversion
                              select new { d.Texto, d.Valor });

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetSystemUserData()
        {
            try
            {
                int numUsuario = Cat_User.GetAll().Where(x => x.User_Active == true).Count();
                int numUsuarioEvento = Ope_UserEvent.GetAll().Select(x => x.UserEvent_UserId).Distinct().Count();
                double porcUso = (numUsuarioEvento * 100) / numUsuario;

                DatoGrafico dpUso = new DatoGrafico()
                {
                    Texto = "Uso del sistema",
                    Valor = porcUso
                };
                DatoGrafico dpUsoResto = new Models.DatoGrafico()
                {
                    Texto = "Otro ",
                    Valor = 100 - porcUso
                };

                List<DatoGrafico> lstPorcentajeUso = new List<DatoGrafico>();
                lstPorcentajeUso.Add(dpUso);
                lstPorcentajeUso.Add(dpUsoResto);

                var result = (from d in lstPorcentajeUso
                              select new { d.Texto, d.Valor });

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetDashboardData()
        {
            DashboardModel modelo = new Models.DashboardModel();

            Cat_Status cotAbierta = new Cat_Status()
            {
                Status_Catalog = "Quotation",
                Status_Status = "Abierto"
            };
            cotAbierta = cotAbierta.GetFirtByData();

            int numCotAbierta = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotAbierta.StatusId).Count();

            Cat_Status cotProceso = new Cat_Status()
            {
                Status_Catalog = "Quotation",
                Status_Status = "En proceso"
            };
            cotProceso = cotProceso.GetFirtByData();

            int numCotConvertida = Ope_Quotation.GetAll().Where(x => x.Quotation_StatusId == cotProceso.StatusId).Count();
            int numCotTotal = numCotAbierta + numCotConvertida;
            double porcConvertida = (numCotConvertida * 100) / numCotTotal;

            int numVendor = Cat_Vendor.GetAll().Where(x => x.Vendor_Active == true).Count();
            int numProgama = Ope_Program.GetAll().Where(x => x.Program_Active == true).Count();
            int numUsuario = Cat_User.GetAll().Where(x => x.User_Active == true).Count();
            int numUsuarioEvento = Ope_UserEvent.GetAll().Select(x => x.UserEvent_UserId).Distinct().Count();
            double porcUso = (numUsuarioEvento * 100) / numUsuario;

            modelo.cotizacionesAbiertas = numCotAbierta;
            modelo.cotizacionesConvertidas = numCotConvertida;
            modelo.cotizacionesTotal = numCotTotal;
            modelo.numVendors = numVendor;
            modelo.numProgramas = numProgama;
            modelo.numUsuarios = numUsuario;

            DatoGrafico dpConvertida = new DatoGrafico()
            {
                Texto = "Cotizaciones convertidas ",
                Valor = porcConvertida
            };
            DatoGrafico dpConvertidaResto = new DatoGrafico()
            {
                Texto = "Sin convertir",
                Valor = 100 - porcConvertida
            };

            DatoGrafico dpUso = new DatoGrafico()
            {
                Texto = "Uso del sistema",
                Valor = porcUso
            };
            DatoGrafico dpUsoResto = new Models.DatoGrafico()
            {
                Texto = "Otro ",
                Valor = 100 - porcUso
            };

            modelo.lstPorcentajeConversion.Add(dpConvertida);
            modelo.lstPorcentajeConversion.Add(dpConvertidaResto);
            modelo.lstProcentajeUso.Add(dpUso);
            modelo.lstProcentajeUso.Add(dpUsoResto);

            modelo.CargadoCorrecto = true;

            var result = (from d in modelo.lstPorcentajeConversion
                          select new { d.Texto, d.Valor }).Take(50);
            return Json(result, JsonRequestBehavior.AllowGet);

        }
    }
}