﻿using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EC.PRM.UI.Web.Controllers
{
    public class VendorCatalogController : Controller
    {
        // GET: VendorCatalog
        public ActionResult Index(string dsearch)
        {
            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                Response.Redirect("/Home/Login");
            }

            VendorCatalogModel model = new Models.VendorCatalogModel();

            Cat_Vendor entVendor = new Cat_Vendor()
            {
                Vendor_Active = true
            };
            Cat_VendorCollection lstVendor = entVendor.GetCollectionByData();

            List<tblVendor> lstTblVendor = new List<tblVendor>();

            foreach(Cat_Vendor objVendor in lstVendor)
            {
                if (!String.IsNullOrEmpty(dsearch))
                {
                    if(!UtilitiesGeneral.Contains(objVendor.Vendor_Name, dsearch, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                }

                tblVendor objTbl = new tblVendor()
                {
                    VendorId=objVendor.VendorId.Value,
                    Vendor_Name=objVendor.Vendor_Name,
                    Vendor_SICCode=objVendor.Vendor_SICCode,
                    Vendor_ChannelManager=objVendor.Vendor_ChannelManager,
                    Vendor_PhoneNumber=objVendor.Vendor_PhoneNumber,
                    Vendor_Email=objVendor.Vendor_Email,
                    Vendor_LocationId=objVendor.Vendor_LocationId
                };

                Cat_Location entLocation = new Cat_Location()
                {
                    LocationId = objVendor.Vendor_LocationId.Value
                };
                entLocation = entLocation.GetFirtByData();

                objTbl.location = entLocation.Location_Name;

                lstTblVendor.Add(objTbl);
            }

            Cat_Location entLoc = new Cat_Location()
            {
                Location_Active = true
            };
            Cat_LocationCollection lstLocation = entLoc.GetCollectionByData();

            Ope_Visual entVisual = UtilitiesGeneral.GetVisual();
            model.visualInfo = entVisual;

            model.dsearch = dsearch;
            model.lstLocation = lstLocation;
            model.lstVendor = lstTblVendor;

            return View(model);
        }

        [HttpPost]
        public JsonResult UpdateVendor(Cat_Vendor vendorInfo, bool activo)
        {
            MensajesBaseModel model = new Models.MensajesBaseModel();
            try
            {
                vendorInfo.Vendor_Active = activo;
                vendorInfo.Update();
                model.CargadoCorrecto = true;

                // Registra evento de usuario
                try
                {
                    Cat_User objUser = new Cat_User();
                    if (System.Web.HttpContext.Current.Session["objUser"] != null)
                    {
                        objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];
                    }

                    Cat_Event entEvento = new Cat_Event()
                    {
                        Event_Code = activo ? "VEDIT" : "VDEL"
                    };
                    entEvento = entEvento.GetFirtByData();

                    Ope_UserEvent entUsrEvento = new Ope_UserEvent()
                    {
                        UserEvent_CatalogId = vendorInfo.VendorId,
                        UserEvent_Date = DateTime.Now,
                        UserEvent_EventId = entEvento.EventId,
                        UserEvent_UserId = objUser.UserId != null ? objUser.UserId : 0
                    };
                    entUsrEvento.Insert();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            catch(Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }
            return this.Json(model);
        }

        [HttpPost]
        public JsonResult SaveVendor(Cat_Vendor vendorInfo)
        {
            MensajesBaseModel model = new Models.MensajesBaseModel();
            try
            {
                vendorInfo.Vendor_Active = true;
                vendorInfo.Insert();
                model.CargadoCorrecto = true;

                // Registra evento de usuario
                try
                {
                    Cat_User objUser = new Cat_User();
                    if (System.Web.HttpContext.Current.Session["objUser"] != null)
                    {
                        objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];
                    }

                    Cat_Event entEvento = new Cat_Event()
                    {
                        Event_Code = "VCREA"
                    };
                    entEvento = entEvento.GetFirtByData();

                    Ope_UserEvent entUsrEvento = new Ope_UserEvent()
                    {
                        UserEvent_CatalogId = vendorInfo.VendorId,
                        UserEvent_Date = DateTime.Now,
                        UserEvent_EventId = entEvento.EventId,
                        UserEvent_UserId = objUser.UserId != null ? objUser.UserId : 0
                    };
                    entUsrEvento.Insert();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }
            return this.Json(model);
        }
    }
}