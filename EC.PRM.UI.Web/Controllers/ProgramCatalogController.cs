﻿using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EC.PRM.UI.Web.Controllers
{
    public class ProgramCatalogController : Controller
    {
        // GET: ProgramCatalog
        public ActionResult Index(string dsearch)
        {
            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                Response.Redirect("/Home/Login");
            }

            ProgramCatalogModel model = new Models.ProgramCatalogModel();

            Ope_Program entProgram = new Ope_Program()
            {
                Program_Active = true
            };
            Ope_ProgramCollection lstProgram = entProgram.GetCollectionByData();

            List<tblProgram> lstTblProgram = new List<tblProgram>();
            foreach(Ope_Program objProgram in lstProgram)
            {
                if (!String.IsNullOrEmpty(dsearch))
                {
                    if (!UtilitiesGeneral.Contains(objProgram.Program_Description, dsearch, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                }

                tblProgram objTbl = new tblProgram()
                {
                    programId = objProgram.ProgramId.Value,
                    program = objProgram.Program_Description,
                    timing = objProgram.Program_Timing,
                    segment = objProgram.Program_Segment
                };

                Cat_Vendor entProgVendor = new Cat_Vendor()
                {
                    VendorId = objProgram.Program_VendorId
                };
                entProgVendor = entProgVendor.GetFirtByData();

                objTbl.vendorId = entProgVendor.VendorId.Value;
                objTbl.vendor = entProgVendor.Vendor_Name;

                lstTblProgram.Add(objTbl);
            }

            Cat_Vendor entVendor = new Cat_Vendor()
            {
                Vendor_Active = true
            };
            Cat_VendorCollection lstVendor = entVendor.GetCollectionByData();

            Ope_Visual entVisual = UtilitiesGeneral.GetVisual();
            model.visualInfo = entVisual;

            model.dsearch = dsearch;
            model.lstProgram = lstTblProgram;
            model.lstVendor = lstVendor;
            model.lstTiming = UtilitiesGeneral.GetListValues("Profit_Timing");
            model.lstSegment = UtilitiesGeneral.GetListValues("Profit_Segment");

            return View(model);
        }

        [HttpPost]
        public JsonResult SaveProgram(int programId, string program, int vendorId, string segment, string timing, bool active)
        {
            MensajesBaseModel model = new Models.MensajesBaseModel();
            try
            {
                Ope_Program entProgram = new Ope_Program()
                {
                    ProgramId = programId,
                    Program_Description = program,
                    Program_VendorId = vendorId,
                    Program_Segment = segment,
                    Program_Timing = timing,
                    Program_Active = active
                };
                entProgram.Update();

                model.CargadoCorrecto = true;

                // Registra evento de usuario
                try
                {
                    Cat_User objUser = new Cat_User();
                    if (System.Web.HttpContext.Current.Session["objUser"] != null)
                    {
                        objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];
                    }

                    Cat_Event entEvento = new Cat_Event()
                    {
                        Event_Code = active ? "PEDIT" : "PDEL"
                    };
                    entEvento = entEvento.GetFirtByData();

                    Ope_UserEvent entUsrEvento = new Ope_UserEvent()
                    {
                        UserEvent_CatalogId = programId,
                        UserEvent_Date = DateTime.Now,
                        UserEvent_EventId = entEvento.EventId,
                        UserEvent_UserId = objUser.UserId != null ? objUser.UserId : 0
                    };
                    entUsrEvento.Insert();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            catch(Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }
            return this.Json(model);
        }
    }
}