﻿using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace EC.PRM.UI.Web.Controllers
{
    public class HomeController : Controller
    {

        #region Home View

        public ActionResult Index()
        {
            //ViewBag.LookFeel = UtilitiesGeneral.GetVisual();

            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                Response.Redirect("Login");
            }

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetVisual()
        {
            VisualModel model = new VisualModel();

            try
            {
                Ope_Visual entVisual = UtilitiesGeneral.GetVisual();
                if (entVisual != null && entVisual.VisualId != null && entVisual.VisualId > 0)
                {
                    model.visual = entVisual;
                    model.CargadoCorrecto = true;
                }
                else
                {
                    throw new Exception("No se pudo obtener la configuración visual.");
                }
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return this.Json(model);
        }

        #endregion

        #region Login View

        public ActionResult Logout()
        {
            try
            {
                if (System.Web.HttpContext.Current.Session["objUser"] != null)
                {
                    FormsAuthentication.SignOut();

                    Session.Clear();
                    System.Web.HttpContext.Current.Session.RemoveAll();
                }

                return RedirectToAction("Login");
            }
            catch
            {
                throw;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                model.Username = String.IsNullOrEmpty(model.Username) ? "hhh" : model.Username;

                Cat_User entUser = new Cat_User()
                {
                    User_Active = true,
                    User_User = model.Username
                };
                entUser = entUser.GetFirtByData();

                if (entUser != null)
                {
                    if (entUser.UserId == 2 && String.IsNullOrEmpty(entUser.User_Password))
                    {
                        System.Web.HttpContext.Current.Session["objUser"] = entUser;
                        //return RedirectToAction("Index", "Home");
                        return RedirectToAction("ChooseProgram", "Home");
                    }
                    
                    var passdec = UtilitiesGeneral.Decrypt(entUser.User_Password);

                    if (passdec.Equals(model.Password))
                    {
                        System.Web.HttpContext.Current.Session["objUser"] = entUser;

                        //return RedirectToAction("Index", "Home");
                        return RedirectToAction("ChooseProgram", "Home");
                    }
                    else
                    {
                        TempData["ErrorMSG"] = "Acceso denegado, favor de verificar los datos.";
                        return View(model);
                    }
                }

            }
            return View();
        }

        #endregion

        #region Choose Program View

        public ActionResult ChooseProgram()
        {
            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                return RedirectToAction("Login", "Home");
            }

            ChooseProgramModel model = new Models.ChooseProgramModel();

            Cat_User objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];

            Cat_Vendor entVendor = new Cat_Vendor()
            {
                VendorId = objUser.User_VendorId
            };
            entVendor = entVendor.GetFirtByData();

            Ope_Program entProgram = new Ope_Program()
            {
                Program_Active = true,
                Program_VendorId = entVendor.VendorId
            };
            Ope_ProgramCollection lstProgram = entProgram.GetCollectionByData();

            model.lstProgram = lstProgram;
            model.vendor = entVendor;

            return View(model);
        }

        [HttpPost]
        public ActionResult ContinueHome(ChooseProgramModel model)
        {
            try
            {
                if (System.Web.HttpContext.Current.Session["objUser"] == null)
                {
                    return RedirectToAction("Login", "Home");
                }

                if (model.programId == 0)
                {
                    return RedirectToAction("ChooseProgram", "Home");
                }

                Cat_User objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];

                objUser.User_ProgramId = model.programId;

                System.Web.HttpContext.Current.Session["objUser"] = objUser;

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return RedirectToAction("ChooseProgram", "Home");
            }
        }

        #endregion

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}