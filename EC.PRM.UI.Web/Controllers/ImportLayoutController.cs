﻿using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace EC.PRM.UI.Web.Controllers
{
    public class ImportLayoutController : Controller
    {
        // GET: ImportLayout
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ImportDataFile(string catalog)
        {
            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                Response.Redirect("/Home/Login");
            }

            ImportLayoutModel model = new Models.ImportLayoutModel();

            model.catalogName = catalog;

            Cat_Vendor entVendor = new Cat_Vendor()
            {
                Vendor_Active = true
            };
            model.lstVendor = entVendor.GetCollectionByData();

            return View(model);
        }

        private ImportLayoutModel ImportTarget(
            string path, ImportLayoutModel model, Cat_User objUser)
        {
            try
            {
                List<DataTable> lstData = ExcelReader.GetDataFromExcel(path);
                int numCorrect = 0, numFail = 0;

                foreach (DataTable dtTarget in lstData)
                {
                    Cat_Currency entCurrency = new Cat_Currency()
                    {
                        Currency_Code = dtTarget.TableName
                    };
                    entCurrency = entCurrency.GetFirtByData();

                    if (entCurrency == null)
                    {
                        model.CargadoCorrecto = false;
                        model.Mensajes = "Layout para carga de target incorrecto, favor de verificar.";
                    }

                    var lstTargetExcel = dtTarget.AsEnumerable()
                        .Select(row => new
                        {
                            segment = row.Field<string>("F1"),
                            term12 = row.Field<double?>("12"),
                            term24 = row.Field<double?>("24"),
                            term36 = row.Field<double?>("36"),
                            term48 = row.Field<double?>("48"),
                            term60 = row.Field<double?>("60"),
                            term72 = row.Field<double?>("72"),
                            term80 = row.Field<double?>("80")
                        })
                        .Distinct().ToList();

                    Cat_FinancialTermCollection lstTerm = Cat_FinancialTerm.GetAll();

                    foreach (var targetExcel in lstTargetExcel)
                    {
                        try
                        {
                            Type t = targetExcel.GetType();
                            PropertyInfo[] lp = t.GetProperties();

                            foreach (PropertyInfo p in lp)
                            {
                                Ope_ProfitTarget entTarget = new Ope_ProfitTarget()
                                {
                                    ProfitTarget_Segment = targetExcel.segment.Trim(),
                                    ProfitTarger_CurrencyId = entCurrency.CurrencyId
                                };

                                var v = p.GetValue(targetExcel, null);

                                if (v != null)
                                {
                                    if (!p.Name.Equals("segment"))
                                    {
                                        string monthNum = p.Name.Substring(p.Name.Length - 2, 2);
                                        int month = Convert.ToInt32(monthNum);
                                        int termId = lstTerm.Find(x => x.FinancialTerm_Value == month).FinancialTermId.Value;

                                        entTarget.ProfitTarget_FinacialTermId = termId;
                                        entTarget.ProfitTarget_Active = true;

                                        entTarget = entTarget.GetFirtByData();

                                        if (entTarget != null)
                                        {
                                            entTarget.ProfitTarget_Active = false;
                                            entTarget.Update();
                                        }

                                        entTarget = new Ope_ProfitTarget();
                                        entTarget.ProfitTarget_Segment = targetExcel.segment;
                                        entTarget.ProfitTarger_CurrencyId = entCurrency.CurrencyId;
                                        entTarget.ProfitTarget_FinacialTermId = termId;

                                        entTarget.ProfitTarget_Percentage = Convert.ToDouble(v) * 100;
                                        entTarget.ProfitTarget_UserIdUpload = objUser.UserId;
                                        entTarget.ProfitTarget_DateUpload = DateTime.Now;
                                        entTarget.ProfitTarget_Active = true;
                                        entTarget.Insert();

                                        numCorrect++;
                                    }
                                }
                            }
                        }
                        catch
                        {
                            numFail++;
                        }
                    }
                }

                model.CargadoCorrecto = true;
                model.Mensajes = numCorrect + " registros importados correctamente, " + numFail + " registros no se pudieron importar.";
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return model;
        }

        private ImportLayoutModel ImportAsset(
            string path, ImportLayoutModel model, string cboVendor, Cat_User objUser)
        {
            try
            {
                List<DataTable> lstData = ExcelReader.GetDataFromExcel(path);
                DataTable dtAsset = lstData.Find(x => x.TableName.Equals("LayoutAsset"));
                int numCorrect = 0, numFail = 0;

                var lstAssetExcel = dtAsset.AsEnumerable()
                        .Select(row => new
                        {
                            collateral = row.Field<string>("CollateralDescription"),
                            brand = row.Field<string>("Brand"),
                            model = row.Field<string>("Model"),
                            description = row.Field<string>("Description"),
                            year = row.Field<double>("Year"),
                            currencyCode = row.Field<string>("Currency")
                        })
                        .Distinct().ToList();

                foreach (var assetExcel in lstAssetExcel)
                {
                    Cat_Asset entAsset = new Cat_Asset()
                    {
                        Asset_Brand = assetExcel.brand,
                        Asset_Model = assetExcel.model,
                        Asset_Description = assetExcel.description,
                        Asset_Year = Convert.ToInt32(assetExcel.year),
                        Asset_Active = true
                    };
                    entAsset = entAsset.GetFirtByData();

                    if (entAsset != null)
                    {
                        numFail++;
                        continue;
                    }

                    Cat_AssetCollateral entColat = new Cat_AssetCollateral()
                    {
                        AssetCollateral_Description = assetExcel.collateral
                    };
                    entColat = entColat.GetFirtByData();

                    if (entColat == null)
                    {
                        entColat = new Cat_AssetCollateral()
                        {
                            AssetCollateral_Description = assetExcel.collateral,
                            AssetCollateral_Active = true
                        };

                        entColat.Insert();

                        Rel_AssetCollateralVendor entGrupoVendor = new Rel_AssetCollateralVendor()
                        {
                            AssetCollateralVendor_AssetCollateralId = entColat.AssetCollateralId,
                            AssetCollateralVendor_VendorId = objUser.User_VendorId
                        };
                        entGrupoVendor.Insert();
                    }

                    Cat_Currency entMoneda = new Cat_Currency()
                    {
                        Currency_Code = assetExcel.currencyCode
                    };

                    if (entMoneda == null)
                    {
                        numFail++;
                        continue;
                    }

                    var lstRV = (from row in dtAsset.AsEnumerable()
                                 where row.Field<string>("CollateralDescription") == assetExcel.collateral &&
                                     row.Field<string>("Brand") == assetExcel.brand &&
                                     row.Field<string>("Model") == assetExcel.model &&
                                     row.Field<string>("Description") == assetExcel.description &&
                                     row.Field<double>("Year") == assetExcel.year
                                 select row).ToList();

                    entAsset = new Cat_Asset()
                    {
                        Asset_AssetCollateralId = entColat.AssetCollateralId,
                        Asset_Brand = assetExcel.brand,
                        Asset_Model = assetExcel.model,
                        Asset_Description = assetExcel.description,
                        Asset_Year = Convert.ToInt32(assetExcel.year),
                        Asset_OEC = Convert.ToDouble(lstRV[0]["Price"].ToString()),
                        Asset_Active = true,
                        Asset_VendorId = Convert.ToInt32(cboVendor),
                        Asset_CurrencyId = entMoneda.CurrencyId
                    };
                    entAsset.Insert();

                    foreach (var objRV in lstRV)
                    {
                        Cat_FinancialTerm entFT = new Cat_FinancialTerm()
                        {
                            FinancialTerm_Value = Convert.ToInt32(objRV["TermMonth"])
                        };
                        entFT = entFT.GetFirtByData();

                        if (entFT == null)
                        {
                            numFail++;
                            continue;
                        }

                        Rel_AssetFinancialTerm relAssetFT = new Rel_AssetFinancialTerm()
                        {
                            AssetFinancialTerm_AssetId = entAsset.AssetId,
                            AssetFinancialTerm_FinancialTermId = entFT.FinancialTermId,
                            AssetFinancialTerm_ResidualValue = Convert.ToDouble(objRV["ResidualValue"].ToString()) * 100
                        };
                        relAssetFT.Insert();
                        numCorrect++;
                    }
                }

                model.CargadoCorrecto = true;
                model.Mensajes = numCorrect + " registros importados correctamente, " + numFail + " registros no se pudieron importar.";
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return model;
        }

        [HttpPost]
        public ActionResult ImportDataFile(HttpPostedFileBase postedFile, string catalog, string cboVendor)
        {

            ImportLayoutModel model = new Models.ImportLayoutModel();

            try
            {
                if (System.Web.HttpContext.Current.Session["objUser"] == null)
                {
                    Response.Redirect("/Home/Login");
                }
                Cat_User objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];

                model.catalogName = catalog;

                if (catalog.Equals("Asset"))
                {
                    if (cboVendor == "0")
                    {
                        model.CargadoCorrecto = false;
                        model.Mensajes = "Selecciona un Vendor y elige un archivo Excel.";

                        return View(model);
                    }

                    Cat_Vendor entVendor = new Cat_Vendor()
                    {
                        Vendor_Active = true
                    };
                    model.lstVendor = entVendor.GetCollectionByData();
                }

                string fileExt = string.Empty;

                if (postedFile != null)
                {
                    fileExt = Path.GetExtension(postedFile.FileName);
                }

                if (string.IsNullOrEmpty(fileExt) || (!fileExt.Equals(".xlsx") && !fileExt.Equals(".xls")))
                {
                    model.CargadoCorrecto = false;
                    model.Mensajes = "Selecciona un archivo Excel y elige un Vendor.";

                    return View(model);
                }

                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                path += Path.GetFileName(postedFile.FileName);
                postedFile.SaveAs(path);

                switch (model.catalogName)
                {
                    case "Asset":
                        model = ImportAsset(path, model, cboVendor, objUser);
                        break;
                    case "Target":
                        model = ImportTarget(path, model, objUser);
                        break;
                }
            }
            catch(Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return View(model);
        }
    }
}