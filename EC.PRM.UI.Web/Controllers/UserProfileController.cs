﻿using EC.PRM.LogicaNegocio.Entities;
using EC.PRM.UI.Web.Models;
using EC.PRM.UI.Web.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EC.PRM.UI.Web.Controllers
{
    public class UserProfileController : Controller
    {
        // GET: UserProfile
        public ActionResult Index(string dsearch)
        {
            if (System.Web.HttpContext.Current.Session["objUser"] == null)
            {
                Response.Redirect("/Home/Login");
            }

            UserProfileModel model = new Models.UserProfileModel();

            try
            {
                Cat_User entUser = new Cat_User()
                {
                    User_Active = true
                };
                Cat_UserCollection lstUser = entUser.GetCollectionByData();

                List<tblUser> lstTblUser = new List<Models.tblUser>();
                foreach (Cat_User objUser in lstUser)
                {
                    if (!String.IsNullOrEmpty(dsearch))
                    {
                        if (!UtilitiesGeneral.Contains(objUser.User_User, dsearch, StringComparison.CurrentCultureIgnoreCase))
                        {
                            continue;
                        }
                    }

                    tblUser objTbl = new Models.tblUser()
                    {
                        userId = objUser.UserId.Value,
                        user = objUser.User_User,
                        pass = UtilitiesGeneral.Decrypt(objUser.User_Password),
                        firstName = objUser.User_FirstName,
                        lastName = objUser.User_LastName,
                        email = objUser.User_Email,
                        pathPhoto = objUser.User_ImageUrl
                    };

                    Cat_UserProfile entProf = new Cat_UserProfile()
                    {
                        UserProfileId = objUser.User_UserProfileId
                    };
                    entProf = entProf.GetFirtByData();

                    Cat_Vendor entUsrVendor = new Cat_Vendor()
                    {
                        VendorId = objUser.User_VendorId
                    };
                    entUsrVendor = entUsrVendor.GetFirtByData();

                    objTbl.profileId = entProf.UserProfileId.Value;
                    objTbl.profile = entProf.UserProfile_Profile;
                    objTbl.vendorId = entUsrVendor.VendorId.Value;
                    objTbl.vendor = entUsrVendor.Vendor_Name;

                    if (objUser.User_ProgramId != null && objUser.User_ProgramId != 0)
                    {
                        Ope_Program objProgram = new Ope_Program()
                        {
                            ProgramId = objUser.User_ProgramId
                        };
                        objProgram = objProgram.GetFirtByData();

                        objTbl.programId = objProgram.ProgramId.Value;
                        objTbl.program = objProgram.Program_Description;
                    }

                    lstTblUser.Add(objTbl);
                }

                Cat_UserProfile entUserProfile = new Cat_UserProfile()
                {
                    UserProfile_Active = true
                };
                Cat_UserProfileCollection lstProfile = entUserProfile.GetCollectionByData();

                Cat_Vendor entVendor = new Cat_Vendor()
                {
                    Vendor_Active = true
                };
                Cat_VendorCollection lstVendor = entVendor.GetCollectionByData();

                Ope_Program entProgram = new Ope_Program()
                {
                    Program_Active = true
                };
                Ope_ProgramCollection lstProgram = entProgram.GetCollectionByData();

                Ope_Visual entVisual = UtilitiesGeneral.GetVisual();

                model.dsearch = dsearch;
                model.lstUser = lstTblUser;
                model.lstProfile = lstProfile;
                model.lstVendor = lstVendor;
                model.lstProgram = lstProgram;
                model.visualInfo = entVisual;

                model.CargadoCorrecto = true;
            }
            catch(Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return View(model);
        }

        [HttpPost]
        public JsonResult UpdateUser(Cat_User userInfo, bool activo)
        {
            MensajesBaseModel model = new Models.MensajesBaseModel();
            try
            {
                Cat_User entUser = new Cat_User()
                {
                    UserId = userInfo.UserId,
                    User_User = userInfo.User_User,
                    User_Password = UtilitiesGeneral.Encrypt(userInfo.User_Password),
                    User_UserProfileId = userInfo.User_UserProfileId,
                    User_VendorId = userInfo.User_VendorId,
                    User_Active = activo
                };
                entUser.Update();
                model.CargadoCorrecto = true;

                // Registra evento de usuario
                try
                {
                    Cat_User objUser = new Cat_User();
                    if (System.Web.HttpContext.Current.Session["objUser"] != null)
                    {
                        objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];
                    }

                    Cat_Event entEvento = new Cat_Event()
                    {
                        Event_Code = activo ? "UEDIT" : "UDEL"
                    };
                    entEvento = entEvento.GetFirtByData();

                    Ope_UserEvent entUsrEvento = new Ope_UserEvent()
                    {
                        UserEvent_CatalogId = userInfo.UserId,
                        UserEvent_Date = DateTime.Now,
                        UserEvent_EventId = entEvento.EventId,
                        UserEvent_UserId = objUser.UserId != null ? objUser.UserId : 0
                    };
                    entUsrEvento.Insert();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            catch (Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return this.Json(model);
        }

        [HttpPost]
        public JsonResult SaveUser(Cat_User userInfo)
        {
            MensajesBaseModel model = new Models.MensajesBaseModel();
            try
            {
                Cat_User entUser = new Cat_User()
                {
                    User_User = userInfo.User_User,
                    User_Password = UtilitiesGeneral.Encrypt(userInfo.User_Password),
                    User_UserProfileId = userInfo.User_UserProfileId,
                    User_VendorId = userInfo.User_VendorId,
                    User_Active = true
                };

                Cat_User valUser = new Cat_User()
                {
                    User_User = userInfo.User_User
                };
                valUser = valUser.GetFirtByData();

                if (valUser == null)
                {
                    entUser.Insert();
                }
                else
                {
                    throw new Exception(String.Format("El usuario {0} ya existe.", entUser.User_User));
                }

                model.CargadoCorrecto = true;

                // Registra evento de usuario
                try
                {
                    Cat_User objUser = new Cat_User();
                    if (System.Web.HttpContext.Current.Session["objUser"] != null)
                    {
                        objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];
                    }

                    Cat_Event entEvento = new Cat_Event()
                    {
                        Event_Code = "UCREA"
                    };
                    entEvento = entEvento.GetFirtByData();

                    Ope_UserEvent entUsrEvento = new Ope_UserEvent()
                    {
                        UserEvent_CatalogId = entUser.UserId,
                        UserEvent_Date = DateTime.Now,
                        UserEvent_EventId = entEvento.EventId,
                        UserEvent_UserId = objUser.UserId != null ? objUser.UserId : 0
                    };
                    entUsrEvento.Insert();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            catch(Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return this.Json(model);
        }

        [HttpPost]
        public JsonResult UploadPhoto(HttpPostedFileBase postedFile, int idUser)
        {
            MensajesBaseModel model = new Models.MensajesBaseModel();

            try
            {
                if (System.Web.HttpContext.Current.Session["objUser"] == null)
                {
                    Response.Redirect("/Home/Login");
                }
                Cat_User objUser = (Cat_User)System.Web.HttpContext.Current.Session["objUser"];

                string fileExt = string.Empty;

                if (postedFile != null)
                {
                    fileExt = Path.GetExtension(postedFile.FileName);
                }

                if (string.IsNullOrEmpty(fileExt) || (!fileExt.Equals(".jpg") && !fileExt.Equals(".png")))
                {
                    model.CargadoCorrecto = false;
                    model.Mensajes = "Selecciona un archivo de imagen válido (formato jpg o png).";

                    return this.Json(model);
                }

                string path = Server.MapPath("~/Uploads/UserPhotos/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                //path += Path.GetFileName(postedFile.FileName);
                path += idUser + ".png";
                postedFile.SaveAs(path);

                model.CargadoCorrecto = true;
                model.Mensajes = "../Uploads/UserPhotos/" + idUser + ".png";
            }
            catch(Exception ex)
            {
                model.CargadoCorrecto = false;
                model.Mensajes = ex.Message;
            }

            return this.Json(model);
        }
    }
}