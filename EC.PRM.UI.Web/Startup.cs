﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EC.PRM.UI.Web.Startup))]
namespace EC.PRM.UI.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
