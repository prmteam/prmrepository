﻿function keepDataStep3(url) {
    var strFinTipoId = $("input[name='cbxFinTipo']:checked").val();
    var strMonedaId = $("input[name='cbxMoneda']:checked").val();
    var plazosIds = $("input[name='cbxFinPlazo']:checked").map(function () {
        return $(this).val();
    });

    if (!(strFinTipoId) || !(strMonedaId) || plazosIds.length < 1) {
        bootstrap_alert.warning('Favor de seleccionar todos los valores para el financiamiento.');
        $('#cbxFinTipo').focus();
        return;
    }

    var strPlazos = "";
    $.each(plazosIds, function (key, value) {
        strPlazos += "," + value;
    });

    $.ajax({
        url: url,
        type: 'POST',
        data: { finTipoId: strFinTipoId, monedaId: strMonedaId, plazosId: strPlazos }
    }).success(function (msg) {

        if (msg.CargadoCorrecto) {
            window.location.href = "/Quotation/Step4";
        }
        else {
            alert(msg.Mensajes);
        }
    });
}