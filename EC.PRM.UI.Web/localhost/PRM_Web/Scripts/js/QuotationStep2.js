﻿$(document).ready(function () {
    $('#cboFamilia').change(function () {
 
        $("#cboFamilia option:selected").each(function () {
            $('#cboProducto').empty();
            $('#cboProducto').append('<option>--- Elige un producto ---');
            $('#txtPrecio').val('$' + Intl.NumberFormat('es-MX').format('0'));
            $('#txtCantidad').val('');

            var familiaSel = $(this).val();

            $.ajax({
                url: "/Quotation/GetProductByFamily?familiaId=" + familiaSel,
                type: 'POST',
                data: { familiaId: familiaSel }
            }).success(function (msg) {
                if (msg.CargadoCorrecto) {

                    $.each(msg.lstProducto, function (id, value) {
                        $('#cboProducto').append('<option value="' + msg.lstProducto[id].ProductoId + '">' + msg.lstProducto[id].Producto_Modelo + '</option>'); 
                    });
                }
                else {
                    alert(msg.Mensajes);
                }

            });
        })
    });

    $('#cboProducto').change(function () {
        var strProductoId = $(this).val();
        $('#txtPrecio').val('$' + Intl.NumberFormat('es-MX').format('0'));

        $.ajax({
            url: "/Quotation/GetProductData?productoId=" + strProductoId,
            type: 'POST',
            data: { productoId: strProductoId }
        }).success(function (msg) {
            if (msg.CargadoCorrecto) {
                var precio = msg.productoSelected.Producto_Precio;
                           
                $('#txtPrecio').val('$' + Intl.NumberFormat('es-MX').format(precio));
                $('#txtCantidad').val('');
            }
            else {
                alert(msg.Mensajes);
            }

        });

    });
    
    $('#tblEquipos').on('click', '.delete-row', function () {
        $(this).parent().remove();
    });

    $('#txtCantidad').focusout(function () {
        $(".alert").alert('close');
    })
});

function addRow() {
    var sf = document.getElementById("cboFamilia");
    var strFamiliaId = sf.options[sf.selectedIndex].value;
    var sp = document.getElementById("cboProducto");
    var strProductoId = sp.options[sp.selectedIndex].value;
    var strProducto = sp.options[sp.selectedIndex].text;
    var strCantidad = $('#txtCantidad').val();

    if (strCantidad.length < 1 || $('#txtPrecio').val().length < 1 || sp.selectedIndex < 1) {
        bootstrap_alert.warning('Favor de elegir un producto e ingresar la cantidad.');
        $('#txtCantidad').focus();
        return;
    }

    var markup = "<tr><td width='20'>(" + strCantidad + ")</td><td width='100'>" +
        strProducto + "<td class='delete-row' style='cursor: pointer; color: #7ad98f'>(Eliminar)</td><td class='td_id' style='display:none;'>"+
        strProductoId + "</td></tr>";
    $("#tblEquipos tbody").append(markup);

    $('#td_id').hide();
}

function keepDataStep2(url) {
    event.preventDefault();
    var numequipos = $("#tblEquipos tbody tr").length - 1;

    if (numequipos < 1) {
        bootstrap_alert.warning('Favor de agregar productos para cotizar.');
        $('#txtCantidad').focus();
        return;
    }

    $("#tblEquipos tbody tr").each(function (index) {
        var campo1, campo2, campo3, campo4;

        $(this).children("td").each(function (index2) {
            switch (index2) {
                case 0:
                    campo1 = $(this).text();
                    break;
                case 1:
                    campo2 = $(this).text();
                    break;
                case 2:
                    campo3 = $(this).text();
                    break;
                case 3:
                    campo4 = $(this).text();
            }
            
            $(this).css("background-color", "#ECF8E0");
            
        });

        if (campo1 != 'Equipos') {   
            var strcantidad = campo1.replace('(', '').replace(')', '');

            $.ajax({
                url: url,
                type: 'POST',
                data: { cantidad: strcantidad, productoId: campo4 }
            }).success(function (msg) {
                if (msg.CargadoCorrecto) {
                    if (numequipos == index) {
                        window.location.href = "/Quotation/Step3";
                    }
                }
                else {
                    alert(msg.Mensajes);
                }
            });
        }
    });
}