﻿$(document).ready(function () {
    bootstrap_alert = function () { }
    bootstrap_alert.warning = function (message) {
        $('#alert_placeholder').html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')
    }

    $('#txtCliente').focusout(function () {
        $(".alert").alert('close');
    });
    $('#txtContacto').focusout(function () {
        $(".alert").alert('close');
    });
    $('#txtEmail').focusout(function () {
        $(".alert").alert('close');
    });
    $('#cboUbicaion').focusout(function () {
        $(".alert").alert('close');
    });
});

function KeepDataStep1(url) {
    /*
    AbrirModal(
    {
        Mensaje: "<p><h3>Llena los campos</h3></p>",
        AlineacionMensaje: "center"
    });
    */
    
    var e = document.getElementById("cboUbicacion");
    var strUbicacionId = e.options[e.selectedIndex].value;
    var strCliente = document.getElementById("txtCliente").value;
    var strContacto = document.getElementById("txtContacto").value;
    var strEmail = document.getElementById("txtEmail").value;

    if (strCliente.length < 1) {
        bootstrap_alert.warning('Favor de capturar el nombre del cliente.');
        $('#txtCliente').focus();
        return;
    }
    if (strContacto.length < 1) {
        bootstrap_alert.warning('Favor de capturar el nombre del contacto.');
        $('#txtContacto').focus();
        return;
    }
    emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (!emailRegex.test(strEmail)) {
        bootstrap_alert.warning('Favor de capturar una cuenta de correo válida.');
        $('#txtEmail').focus();
        return;
    }
    if (strUbicacionId < 1) {
        bootstrap_alert.warning('Favor de elegir la ubicación del cliente.');
        $('#cboUbicacion').focus();
        return;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: {ubicacionId: strUbicacionId, cliente: strCliente, contacto: strContacto, email: strEmail}
    }).success(function (msg) {
        if (msg.CargadoCorrecto) {
            window.location.href = "/Quotation/Step2";
        }
        else {
            alert(msg.Mensajes);
        }
    });
    
}