﻿using EC.PRM.WinService.srvBanxico;
using EC.PRM.WinService.Utilities;
using EC.PRM.LogicaNegocio.Entities;
using NGM.Framework.ToolLog;
using NGM.Framework.WindowsService.Temporizadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EC.PRM.WinService.Processes
{
    class ExchangeRate : IRecurrente
    {
        public TextFileLog InstanciaLog { get; set; }

        public CancellationToken TokenCancelacion { get; set; }

        public int? numAttempt { get; set; }

        public void Ejecutar()
        {
            try
            {
                DgieWSPortClient tcclient = new DgieWSPortClient();
                string strTDCambio = tcclient.tiposDeCambioBanxico();
                List<string> tdc = General.GetExchangeRate(strTDCambio);

                string tdc_fecha = tdc[0].Split('\t')[0];
                string tdc_factor = tdc[0].Split('\t')[1];

                Ope_ExchangeRate entTDC = new Ope_ExchangeRate()
                {
                    ExchangeRate_Date = Convert.ToDateTime(tdc_fecha),
                    ExchangeRate_MXN = Convert.ToDouble(tdc_factor)
                };
                entTDC = entTDC.GetFirtByData();

                if (entTDC == null)
                {
                    if (numAttempt.HasValue)
                    {
                        numAttempt++;
                    }
                    else
                    {
                        numAttempt = 1;
                    }

                    entTDC = new Ope_ExchangeRate()
                    {
                        ExchangeRate_Date = Convert.ToDateTime(tdc_fecha),
                        ExchangeRate_MXN = Convert.ToDouble(tdc_factor),
                        ExchangeRate_CurrencyId = 1,
                        ExchangeRate_Source = "http://www.banxico.org.mx",
                        ExchangeRate_Attemps = numAttempt
                    };
                    entTDC.Insert();

                    this.InstanciaLog.EscribirEnLog("Tipo de cambio guardado correctamente: " +
                        entTDC.ExchangeRate_Date + ", " + entTDC.ExchangeRate_MXN, TipoMensajeLog.Informacion);
                }

            }
            catch (Exception ex)
            {
                if (numAttempt.HasValue)
                {
                    numAttempt++;
                }
                else
                {
                    numAttempt = 1;
                }

                this.InstanciaLog.EscribirEnLog(ex.Message, TipoMensajeLog.Error, ex);
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
