﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EC.PRM.WinService.Utilities
{
    public static class General
    {
        public static List<string> GetExchangeRate(string strTDCambio)
        {
            List<string> resp = new List<string>();

            XElement root = XElement.Parse(@strTDCambio);
            XNamespace bm = "http://www.banxico.org.mx/structure/key_families/dgie/sie/series/compact";
            IEnumerable<XElement> address =
                from el in root.Descendants(bm + "DataSet").Elements(bm + "Series")
                where (string)el.Attribute("IDSERIE") == "SF43718"//"SF60653"
                select el;

            foreach (XElement el in address)
            {
                resp.Add(el.Element(bm + "Obs").Attribute("TIME_PERIOD").Value + "\t" + el.Element(bm + "Obs").Attribute("OBS_VALUE").Value);
            }

            return resp;
        }
    }
}
