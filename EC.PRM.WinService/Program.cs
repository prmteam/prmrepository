﻿using EC.PRM.WinService.Processes;
using NGM.Framework.WindowsService;
using NGM.Framework.WindowsService.Temporizadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.PRM.WinService
{
    class Program
    {
        static void Main(string[] args)
        {
            Inicio.IniciarServicio(args, new DefinicionServicio()
            {
                Nombre = "EC.PRM.Quotation.ExchangeRate",
                NombreMostrar = "Engenium Capital PRM Service.",
                Descripcion = "Servicio que obtiene el tipo de cambio desde banxico.",
                CrearProcesos = () => new List<ITemporizable>()
                {
                    new Temporizable<ExchangeRate>()
                }
            });
        }
    }
}
