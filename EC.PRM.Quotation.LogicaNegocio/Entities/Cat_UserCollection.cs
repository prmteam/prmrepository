﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_User.
    /// </summary>
    [Serializable]
    public class Cat_UserCollection : BusinessCollection<Cat_User>
    {
    }
}