﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Ope_Program.
    /// </summary>
    [Serializable]
    public class Ope_ProgramCollection : BusinessCollection<Ope_Program>
    {
    }
}