﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Rel_QuotationAsset.
    /// </summary>
    [Serializable]
    public class Rel_QuotationAssetCollection : BusinessCollection<Rel_QuotationAsset>
    {
    }
}