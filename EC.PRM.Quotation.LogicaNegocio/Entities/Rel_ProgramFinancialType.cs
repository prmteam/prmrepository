﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Rel_ProgramFinancialType.
    /// </summary>
    [DataContract, Serializable]
    public partial class Rel_ProgramFinancialType : BusinessObject<Rel_ProgramFinancialType, Rel_ProgramFinancialTypeCollection>
    {
        #region Constantes Columnas
protected const string ColumnaProgramFinancialTypeId = "ProgramFinancialTypeId";
protected const string ColumnaProgramFinancialType_ProgramId = "ProgramFinancialType_ProgramId";
protected const string ColumnaProgramFinancialType_FinancialType = "ProgramFinancialType_FinancialType";
protected const string ColumnaProgramFinancialType_AlterDate = "ProgramFinancialType_AlterDate";
protected const string ColumnaProgramFinancialType_AlterUser = "ProgramFinancialType_AlterUser";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Rel_ProgramFinancialType.
        /// </summary>
        public Rel_ProgramFinancialType()
            : base()
        {
            this.TableName = "tbl_rel_ProgramFinancialType";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaProgramFinancialTypeId, DbType.Int32, true)]
        public int? ProgramFinancialTypeId { get; set; }

[DataMember, BdAction(ColumnaProgramFinancialType_ProgramId, DbType.Int32, true, true)]
        public int? ProgramFinancialType_ProgramId { get; set; }

[DataMember, BdAction(ColumnaProgramFinancialType_FinancialType, DbType.Int32, true, true)]
        public int? ProgramFinancialType_FinancialType { get; set; }

[DataMember, BdAction(ColumnaProgramFinancialType_AlterDate, DbType.DateTime, true, true)]
        public DateTime? ProgramFinancialType_AlterDate { get; set; }

[DataMember, BdAction(ColumnaProgramFinancialType_AlterUser, DbType.Int32, true, true)]
        public int? ProgramFinancialType_AlterUser { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.ProgramFinancialTypeId = this.ObtenerValorColumna<int?>(ColumnaProgramFinancialTypeId);
this.ProgramFinancialType_ProgramId = this.ObtenerValorColumna<int?>(ColumnaProgramFinancialType_ProgramId);
this.ProgramFinancialType_FinancialType = this.ObtenerValorColumna<int?>(ColumnaProgramFinancialType_FinancialType);
this.ProgramFinancialType_AlterDate = this.ObtenerValorColumna<DateTime?>(ColumnaProgramFinancialType_AlterDate);
this.ProgramFinancialType_AlterUser = this.ObtenerValorColumna<int?>(ColumnaProgramFinancialType_AlterUser);
        }
        #endregion
    }
}