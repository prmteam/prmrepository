﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_Asset.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_Asset : BusinessObject<Cat_Asset, Cat_AssetCollection>
    {
        #region Constantes Columnas
protected const string ColumnaAssetId = "AssetId";
protected const string ColumnaAsset_AssetCollateralId = "Asset_AssetCollateralId";
protected const string ColumnaAsset_VendorId = "Asset_VendorId";
protected const string ColumnaAsset_Brand = "Asset_Brand";
protected const string ColumnaAsset_Model = "Asset_Model";
protected const string ColumnaAsset_Description = "Asset_Description";
protected const string ColumnaAsset_Year = "Asset_Year";
protected const string ColumnaAsset_OEC = "Asset_OEC";
protected const string ColumnaAsset_Active = "Asset_Active";
protected const string ColumnaAsset_CurrencyId = "Asset_CurrencyId";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_Asset.
        /// </summary>
        public Cat_Asset()
            : base()
        {
            this.TableName = "tbl_cat_Asset";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaAssetId, DbType.Int32, true)]
        public int? AssetId { get; set; }

[DataMember, BdAction(ColumnaAsset_AssetCollateralId, DbType.Int32, true, true)]
        public int? Asset_AssetCollateralId { get; set; }

[DataMember, BdAction(ColumnaAsset_VendorId, DbType.Int32, true, true)]
        public int? Asset_VendorId { get; set; }

[DataMember, BdAction(ColumnaAsset_Brand, DbType.AnsiString, true, true)]
        public string Asset_Brand { get; set; }

[DataMember, BdAction(ColumnaAsset_Model, DbType.AnsiString, true, true)]
        public string Asset_Model { get; set; }

[DataMember, BdAction(ColumnaAsset_Description, DbType.AnsiString, true, true)]
        public string Asset_Description { get; set; }

[DataMember, BdAction(ColumnaAsset_Year, DbType.Int32, true, true)]
        public int? Asset_Year { get; set; }

[DataMember, BdAction(ColumnaAsset_OEC, DbType.Double, true, true)]
        public double? Asset_OEC { get; set; }

[DataMember, BdAction(ColumnaAsset_Active, DbType.Boolean, true, true)]
        public bool? Asset_Active { get; set; }

[DataMember, BdAction(ColumnaAsset_CurrencyId, DbType.Int32, true, true)]
        public int? Asset_CurrencyId { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.AssetId = this.ObtenerValorColumna<int?>(ColumnaAssetId);
this.Asset_AssetCollateralId = this.ObtenerValorColumna<int?>(ColumnaAsset_AssetCollateralId);
this.Asset_VendorId = this.ObtenerValorColumna<int?>(ColumnaAsset_VendorId);
this.Asset_Brand = this.ObtenerValorColumna<string>(ColumnaAsset_Brand);
this.Asset_Model = this.ObtenerValorColumna<string>(ColumnaAsset_Model);
this.Asset_Description = this.ObtenerValorColumna<string>(ColumnaAsset_Description);
this.Asset_Year = this.ObtenerValorColumna<int?>(ColumnaAsset_Year);
this.Asset_OEC = this.ObtenerValorColumna<double?>(ColumnaAsset_OEC);
this.Asset_Active = this.ObtenerValorColumna<bool?>(ColumnaAsset_Active);
this.Asset_CurrencyId = this.ObtenerValorColumna<int?>(ColumnaAsset_CurrencyId);
        }
        #endregion
    }
}