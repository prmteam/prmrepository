﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Ope_ProfitTarget.
    /// </summary>
    [Serializable]
    public class Ope_ProfitTargetCollection : BusinessCollection<Ope_ProfitTarget>
    {
    }
}