﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_FinancialType.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_FinancialType : BusinessObject<Cat_FinancialType, Cat_FinancialTypeCollection>
    {
        #region Constantes Columnas
protected const string ColumnaFinancialTypeId = "FinancialTypeId";
protected const string ColumnaFinancialType_Description = "FinancialType_Description";
protected const string ColumnaFinancialType_Text = "FinancialType_Text";
protected const string ColumnaFinancialType_Group = "FinancialType_Group";
protected const string ColumnaFinancialType_Active = "FinancialType_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_FinancialType.
        /// </summary>
        public Cat_FinancialType()
            : base()
        {
            this.TableName = "tbl_cat_FinancialType";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaFinancialTypeId, DbType.Int32, true)]
        public int? FinancialTypeId { get; set; }

[DataMember, BdAction(ColumnaFinancialType_Description, DbType.AnsiString, true, true)]
        public string FinancialType_Description { get; set; }

[DataMember, BdAction(ColumnaFinancialType_Text, DbType.AnsiString, true, true)]
        public string FinancialType_Text { get; set; }

[DataMember, BdAction(ColumnaFinancialType_Group, DbType.AnsiString, true, true)]
        public string FinancialType_Group { get; set; }

[DataMember, BdAction(ColumnaFinancialType_Active, DbType.Boolean, true, true)]
        public bool? FinancialType_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.FinancialTypeId = this.ObtenerValorColumna<int?>(ColumnaFinancialTypeId);
this.FinancialType_Description = this.ObtenerValorColumna<string>(ColumnaFinancialType_Description);
this.FinancialType_Text = this.ObtenerValorColumna<string>(ColumnaFinancialType_Text);
this.FinancialType_Group = this.ObtenerValorColumna<string>(ColumnaFinancialType_Group);
this.FinancialType_Active = this.ObtenerValorColumna<bool?>(ColumnaFinancialType_Active);
        }
        #endregion
    }
}