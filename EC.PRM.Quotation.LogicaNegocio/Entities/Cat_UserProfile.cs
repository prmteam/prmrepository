﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_UserProfile.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_UserProfile : BusinessObject<Cat_UserProfile, Cat_UserProfileCollection>
    {
        #region Constantes Columnas
protected const string ColumnaUserProfileId = "UserProfileId";
protected const string ColumnaUserProfile_Profile = "UserProfile_Profile";
protected const string ColumnaUserProfile_Active = "UserProfile_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_UserProfile.
        /// </summary>
        public Cat_UserProfile()
            : base()
        {
            this.TableName = "tbl_cat_UserProfile";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaUserProfileId, DbType.Int32, true)]
        public int? UserProfileId { get; set; }

[DataMember, BdAction(ColumnaUserProfile_Profile, DbType.AnsiString, true, true)]
        public string UserProfile_Profile { get; set; }

[DataMember, BdAction(ColumnaUserProfile_Active, DbType.Boolean, true, true)]
        public bool? UserProfile_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.UserProfileId = this.ObtenerValorColumna<int?>(ColumnaUserProfileId);
this.UserProfile_Profile = this.ObtenerValorColumna<string>(ColumnaUserProfile_Profile);
this.UserProfile_Active = this.ObtenerValorColumna<bool?>(ColumnaUserProfile_Active);
        }
        #endregion
    }
}