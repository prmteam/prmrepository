﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_Status.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_Status : BusinessObject<Cat_Status, Cat_StatusCollection>
    {
        #region Constantes Columnas
protected const string ColumnaStatusId = "StatusId";
protected const string ColumnaStatus_Catalog = "Status_Catalog";
protected const string ColumnaStatus_Status = "Status_Status";
protected const string ColumnaStatus_Active = "Status_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_Status.
        /// </summary>
        public Cat_Status()
            : base()
        {
            this.TableName = "tbl_cat_Status";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaStatusId, DbType.Int32, true)]
        public int? StatusId { get; set; }

[DataMember, BdAction(ColumnaStatus_Catalog, DbType.AnsiString, true, true)]
        public string Status_Catalog { get; set; }

[DataMember, BdAction(ColumnaStatus_Status, DbType.AnsiString, true, true)]
        public string Status_Status { get; set; }

[DataMember, BdAction(ColumnaStatus_Active, DbType.Boolean, true, true)]
        public bool? Status_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.StatusId = this.ObtenerValorColumna<int?>(ColumnaStatusId);
this.Status_Catalog = this.ObtenerValorColumna<string>(ColumnaStatus_Catalog);
this.Status_Status = this.ObtenerValorColumna<string>(ColumnaStatus_Status);
this.Status_Active = this.ObtenerValorColumna<bool?>(ColumnaStatus_Active);
        }
        #endregion
    }
}