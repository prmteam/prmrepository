﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Ope_QuotInput.
    /// </summary>
    [Serializable]
    public class Ope_QuotInputCollection : BusinessCollection<Ope_QuotInput>
    {
    }
}