﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_Vendor.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_Vendor : BusinessObject<Cat_Vendor, Cat_VendorCollection>
    {
        #region Constantes Columnas
protected const string ColumnaVendorId = "VendorId";
protected const string ColumnaVendor_Name = "Vendor_Name";
protected const string ColumnaVendor_SICCode = "Vendor_SICCode";
protected const string ColumnaVendor_ChannelManager = "Vendor_ChannelManager";
protected const string ColumnaVendor_PhoneNumber = "Vendor_PhoneNumber";
protected const string ColumnaVendor_Email = "Vendor_Email";
protected const string ColumnaVendor_LocationId = "Vendor_LocationId";
protected const string ColumnaVendor_Active = "Vendor_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_Vendor.
        /// </summary>
        public Cat_Vendor()
            : base()
        {
            this.TableName = "tbl_cat_Vendor";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaVendorId, DbType.Int32, true)]
        public int? VendorId { get; set; }

[DataMember, BdAction(ColumnaVendor_Name, DbType.AnsiString, true, true)]
        public string Vendor_Name { get; set; }

[DataMember, BdAction(ColumnaVendor_SICCode, DbType.AnsiString, true, true)]
        public string Vendor_SICCode { get; set; }

[DataMember, BdAction(ColumnaVendor_ChannelManager, DbType.AnsiString, true, true)]
        public string Vendor_ChannelManager { get; set; }

[DataMember, BdAction(ColumnaVendor_PhoneNumber, DbType.AnsiString, true, true)]
        public string Vendor_PhoneNumber { get; set; }

[DataMember, BdAction(ColumnaVendor_Email, DbType.AnsiString, true, true)]
        public string Vendor_Email { get; set; }

[DataMember, BdAction(ColumnaVendor_LocationId, DbType.Int32, true, true)]
        public int? Vendor_LocationId { get; set; }

[DataMember, BdAction(ColumnaVendor_Active, DbType.Boolean, true, true)]
        public bool? Vendor_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.VendorId = this.ObtenerValorColumna<int?>(ColumnaVendorId);
this.Vendor_Name = this.ObtenerValorColumna<string>(ColumnaVendor_Name);
this.Vendor_SICCode = this.ObtenerValorColumna<string>(ColumnaVendor_SICCode);
this.Vendor_ChannelManager = this.ObtenerValorColumna<string>(ColumnaVendor_ChannelManager);
this.Vendor_PhoneNumber = this.ObtenerValorColumna<string>(ColumnaVendor_PhoneNumber);
this.Vendor_Email = this.ObtenerValorColumna<string>(ColumnaVendor_Email);
this.Vendor_LocationId = this.ObtenerValorColumna<int?>(ColumnaVendor_LocationId);
this.Vendor_Active = this.ObtenerValorColumna<bool?>(ColumnaVendor_Active);
        }
        #endregion
    }
}