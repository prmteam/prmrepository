﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_Event.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_Event : BusinessObject<Cat_Event, Cat_EventCollection>
    {
        #region Constantes Columnas
protected const string ColumnaEventId = "EventId";
protected const string ColumnaEvent_Code = "Event_Code";
protected const string ColumnaEvent_Descritipon = "Event_Descritipon";
protected const string ColumnaEvent_CatalogName = "Event_CatalogName";
protected const string ColumnaEvent_Image = "Event_Image";
protected const string ColumnaEvent_Active = "Event_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_Event.
        /// </summary>
        public Cat_Event()
            : base()
        {
            this.TableName = "tbl_cat_Event";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaEventId, DbType.Int32, true)]
        public int? EventId { get; set; }

[DataMember, BdAction(ColumnaEvent_Code, DbType.AnsiString, true, true)]
        public string Event_Code { get; set; }

[DataMember, BdAction(ColumnaEvent_Descritipon, DbType.AnsiString, true, true)]
        public string Event_Descritipon { get; set; }

[DataMember, BdAction(ColumnaEvent_CatalogName, DbType.AnsiString, true, true)]
        public string Event_CatalogName { get; set; }

[DataMember, BdAction(ColumnaEvent_Image, DbType.AnsiString, true, true)]
        public string Event_Image { get; set; }

[DataMember, BdAction(ColumnaEvent_Active, DbType.Boolean, true, true)]
        public bool? Event_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.EventId = this.ObtenerValorColumna<int?>(ColumnaEventId);
this.Event_Code = this.ObtenerValorColumna<string>(ColumnaEvent_Code);
this.Event_Descritipon = this.ObtenerValorColumna<string>(ColumnaEvent_Descritipon);
this.Event_CatalogName = this.ObtenerValorColumna<string>(ColumnaEvent_CatalogName);
this.Event_Image = this.ObtenerValorColumna<string>(ColumnaEvent_Image);
this.Event_Active = this.ObtenerValorColumna<bool?>(ColumnaEvent_Active);
        }
        #endregion
    }
}