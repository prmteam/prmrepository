﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Det_QuotationDetail.
    /// </summary>
    [Serializable]
    public class Det_QuotationDetailCollection : BusinessCollection<Det_QuotationDetail>
    {
    }
}