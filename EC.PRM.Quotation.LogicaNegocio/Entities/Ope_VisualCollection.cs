﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Ope_Visual.
    /// </summary>
    [Serializable]
    public class Ope_VisualCollection : BusinessCollection<Ope_Visual>
    {
    }
}