﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Rel_AssetFinancialTerm.
    /// </summary>
    [Serializable]
    public class Rel_AssetFinancialTermCollection : BusinessCollection<Rel_AssetFinancialTerm>
    {
    }
}