﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_AssetService.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_AssetService : BusinessObject<Cat_AssetService, Cat_AssetServiceCollection>
    {
        #region Constantes Columnas
protected const string ColumnaAssetServiceId = "AssetServiceId";
protected const string ColumnaAssetService_Description = "AssetService_Description";
protected const string ColumnaAssetService_Cost = "AssetService_Cost";
protected const string ColumnaAssetService_Active = "AssetService_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_AssetService.
        /// </summary>
        public Cat_AssetService()
            : base()
        {
            this.TableName = "tbl_cat_AssetService";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaAssetServiceId, DbType.Int32, true)]
        public int? AssetServiceId { get; set; }

[DataMember, BdAction(ColumnaAssetService_Description, DbType.AnsiString, true, true)]
        public string AssetService_Description { get; set; }

[DataMember, BdAction(ColumnaAssetService_Cost, DbType.Double, true, true)]
        public double? AssetService_Cost { get; set; }

[DataMember, BdAction(ColumnaAssetService_Active, DbType.Boolean, true, true)]
        public bool? AssetService_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.AssetServiceId = this.ObtenerValorColumna<int?>(ColumnaAssetServiceId);
this.AssetService_Description = this.ObtenerValorColumna<string>(ColumnaAssetService_Description);
this.AssetService_Cost = this.ObtenerValorColumna<double?>(ColumnaAssetService_Cost);
this.AssetService_Active = this.ObtenerValorColumna<bool?>(ColumnaAssetService_Active);
        }
        #endregion
    }
}