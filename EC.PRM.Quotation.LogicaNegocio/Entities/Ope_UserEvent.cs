﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Ope_UserEvent.
    /// </summary>
    [DataContract, Serializable]
    public partial class Ope_UserEvent : BusinessObject<Ope_UserEvent, Ope_UserEventCollection>
    {
        #region Constantes Columnas
protected const string ColumnaUserEventId = "UserEventId";
protected const string ColumnaUserEvent_EventId = "UserEvent_EventId";
protected const string ColumnaUserEvent_UserId = "UserEvent_UserId";
protected const string ColumnaUserEvent_CatalogId = "UserEvent_CatalogId";
protected const string ColumnaUserEvent_Date = "UserEvent_Date";
protected const string ColumnaUserEvent_ExtraInfo = "UserEvent_ExtraInfo";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Ope_UserEvent.
        /// </summary>
        public Ope_UserEvent()
            : base()
        {
            this.TableName = "tbl_ope_UserEvent";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaUserEventId, DbType.Int32, true)]
        public int? UserEventId { get; set; }

[DataMember, BdAction(ColumnaUserEvent_EventId, DbType.Int32, true, true)]
        public int? UserEvent_EventId { get; set; }

[DataMember, BdAction(ColumnaUserEvent_UserId, DbType.Int32, true, true)]
        public int? UserEvent_UserId { get; set; }

[DataMember, BdAction(ColumnaUserEvent_CatalogId, DbType.Int32, true, true)]
        public int? UserEvent_CatalogId { get; set; }

[DataMember, BdAction(ColumnaUserEvent_Date, DbType.DateTime, true, true)]
        public DateTime? UserEvent_Date { get; set; }

[DataMember, BdAction(ColumnaUserEvent_ExtraInfo, DbType.AnsiString, true, true)]
        public string UserEvent_ExtraInfo { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.UserEventId = this.ObtenerValorColumna<int?>(ColumnaUserEventId);
this.UserEvent_EventId = this.ObtenerValorColumna<int?>(ColumnaUserEvent_EventId);
this.UserEvent_UserId = this.ObtenerValorColumna<int?>(ColumnaUserEvent_UserId);
this.UserEvent_CatalogId = this.ObtenerValorColumna<int?>(ColumnaUserEvent_CatalogId);
this.UserEvent_Date = this.ObtenerValorColumna<DateTime?>(ColumnaUserEvent_Date);
this.UserEvent_ExtraInfo = this.ObtenerValorColumna<string>(ColumnaUserEvent_ExtraInfo);
        }
        #endregion
    }
}