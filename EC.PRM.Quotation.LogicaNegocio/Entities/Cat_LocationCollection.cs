﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_Location.
    /// </summary>
    [Serializable]
    public class Cat_LocationCollection : BusinessCollection<Cat_Location>
    {
    }
}