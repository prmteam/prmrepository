﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Rel_CoverageFinancialTerm.
    /// </summary>
    [Serializable]
    public class Rel_CoverageFinancialTermCollection : BusinessCollection<Rel_CoverageFinancialTerm>
    {
    }
}