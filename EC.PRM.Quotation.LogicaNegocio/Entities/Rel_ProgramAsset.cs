﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Rel_ProgramAsset.
    /// </summary>
    [DataContract, Serializable]
    public partial class Rel_ProgramAsset : BusinessObject<Rel_ProgramAsset, Rel_ProgramAssetCollection>
    {
        #region Constantes Columnas
protected const string ColumnaProgramAssetId = "ProgramAssetId";
protected const string ColumnaProgramAsset_ProgramId = "ProgramAsset_ProgramId";
protected const string ColumnaProgramAsset_AssetId = "ProgramAsset_AssetId";
protected const string ColumnaProgramAsset_CoverageId = "ProgramAsset_CoverageId";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Rel_ProgramAsset.
        /// </summary>
        public Rel_ProgramAsset()
            : base()
        {
            this.TableName = "tbl_rel_ProgramAsset";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaProgramAssetId, DbType.Int32, true)]
        public int? ProgramAssetId { get; set; }

[DataMember, BdAction(ColumnaProgramAsset_ProgramId, DbType.Int32, true, true)]
        public int? ProgramAsset_ProgramId { get; set; }

[DataMember, BdAction(ColumnaProgramAsset_AssetId, DbType.Int32, true, true)]
        public int? ProgramAsset_AssetId { get; set; }

[DataMember, BdAction(ColumnaProgramAsset_CoverageId, DbType.Int32, true, true)]
        public int? ProgramAsset_CoverageId { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.ProgramAssetId = this.ObtenerValorColumna<int?>(ColumnaProgramAssetId);
this.ProgramAsset_ProgramId = this.ObtenerValorColumna<int?>(ColumnaProgramAsset_ProgramId);
this.ProgramAsset_AssetId = this.ObtenerValorColumna<int?>(ColumnaProgramAsset_AssetId);
this.ProgramAsset_CoverageId = this.ObtenerValorColumna<int?>(ColumnaProgramAsset_CoverageId);
        }
        #endregion
    }
}