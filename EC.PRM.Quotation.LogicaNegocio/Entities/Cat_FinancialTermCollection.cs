﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_FinancialTerm.
    /// </summary>
    [Serializable]
    public class Cat_FinancialTermCollection : BusinessCollection<Cat_FinancialTerm>
    {
    }
}