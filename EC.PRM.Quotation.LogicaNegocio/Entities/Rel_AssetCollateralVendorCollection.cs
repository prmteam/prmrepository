﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Rel_AssetCollateralVendor.
    /// </summary>
    [Serializable]
    public class Rel_AssetCollateralVendorCollection : BusinessCollection<Rel_AssetCollateralVendor>
    {
    }
}