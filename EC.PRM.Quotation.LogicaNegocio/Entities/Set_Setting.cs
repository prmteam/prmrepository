﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Set_Setting.
    /// </summary>
    [DataContract, Serializable]
    public partial class Set_Setting : BusinessObject<Set_Setting, Set_SettingCollection>
    {
        #region Constantes Columnas
protected const string ColumnaSettingId = "SettingId";
protected const string ColumnaSetting_Key = "Setting_Key";
protected const string ColumnaSetting_Value = "Setting_Value";
protected const string ColumnaSetting_Description = "Setting_Description";
protected const string ColumnaSetting_Active = "Setting_Active";
protected const string ColumnaSetting_DateCreate = "Setting_DateCreate";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Set_Setting.
        /// </summary>
        public Set_Setting()
            : base()
        {
            this.TableName = "tbl_set_Setting";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaSettingId, DbType.Int32, true)]
        public int? SettingId { get; set; }

[DataMember, BdAction(ColumnaSetting_Key, DbType.AnsiString, true, true)]
        public string Setting_Key { get; set; }

[DataMember, BdAction(ColumnaSetting_Value, DbType.AnsiString, true, true)]
        public string Setting_Value { get; set; }

[DataMember, BdAction(ColumnaSetting_Description, DbType.AnsiString, true, true)]
        public string Setting_Description { get; set; }

[DataMember, BdAction(ColumnaSetting_Active, DbType.Boolean, true, true)]
        public bool? Setting_Active { get; set; }

[DataMember, BdAction(ColumnaSetting_DateCreate, DbType.DateTime, true, true)]
        public DateTime? Setting_DateCreate { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.SettingId = this.ObtenerValorColumna<int?>(ColumnaSettingId);
this.Setting_Key = this.ObtenerValorColumna<string>(ColumnaSetting_Key);
this.Setting_Value = this.ObtenerValorColumna<string>(ColumnaSetting_Value);
this.Setting_Description = this.ObtenerValorColumna<string>(ColumnaSetting_Description);
this.Setting_Active = this.ObtenerValorColumna<bool?>(ColumnaSetting_Active);
this.Setting_DateCreate = this.ObtenerValorColumna<DateTime?>(ColumnaSetting_DateCreate);
        }
        #endregion
    }
}