﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Rel_ProgramFinancialTerm.
    /// </summary>
    [DataContract, Serializable]
    public partial class Rel_ProgramFinancialTerm : BusinessObject<Rel_ProgramFinancialTerm, Rel_ProgramFinancialTermCollection>
    {
        #region Constantes Columnas
protected const string ColumnaProgramFinancialTermId = "ProgramFinancialTermId";
protected const string ColumnaProgramFinancialTerm_ProgramId = "ProgramFinancialTerm_ProgramId";
protected const string ColumnaProgramFinancialTerm_FinancialTerm = "ProgramFinancialTerm_FinancialTerm";
protected const string ColumnaProgramFinancialTerm_AlterDate = "ProgramFinancialTerm_AlterDate";
protected const string ColumnaProgramFinancialTerm_AlterUserId = "ProgramFinancialTerm_AlterUserId";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Rel_ProgramFinancialTerm.
        /// </summary>
        public Rel_ProgramFinancialTerm()
            : base()
        {
            this.TableName = "tbl_rel_ProgramFinancialTerm";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaProgramFinancialTermId, DbType.Int32, true)]
        public int? ProgramFinancialTermId { get; set; }

[DataMember, BdAction(ColumnaProgramFinancialTerm_ProgramId, DbType.Int32, true, true)]
        public int? ProgramFinancialTerm_ProgramId { get; set; }

[DataMember, BdAction(ColumnaProgramFinancialTerm_FinancialTerm, DbType.Int32, true, true)]
        public int? ProgramFinancialTerm_FinancialTerm { get; set; }

[DataMember, BdAction(ColumnaProgramFinancialTerm_AlterDate, DbType.DateTime, true, true)]
        public DateTime? ProgramFinancialTerm_AlterDate { get; set; }

[DataMember, BdAction(ColumnaProgramFinancialTerm_AlterUserId, DbType.Int32, true, true)]
        public int? ProgramFinancialTerm_AlterUserId { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.ProgramFinancialTermId = this.ObtenerValorColumna<int?>(ColumnaProgramFinancialTermId);
this.ProgramFinancialTerm_ProgramId = this.ObtenerValorColumna<int?>(ColumnaProgramFinancialTerm_ProgramId);
this.ProgramFinancialTerm_FinancialTerm = this.ObtenerValorColumna<int?>(ColumnaProgramFinancialTerm_FinancialTerm);
this.ProgramFinancialTerm_AlterDate = this.ObtenerValorColumna<DateTime?>(ColumnaProgramFinancialTerm_AlterDate);
this.ProgramFinancialTerm_AlterUserId = this.ObtenerValorColumna<int?>(ColumnaProgramFinancialTerm_AlterUserId);
        }
        #endregion
    }
}