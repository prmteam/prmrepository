﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_UserProfile.
    /// </summary>
    [Serializable]
    public class Cat_UserProfileCollection : BusinessCollection<Cat_UserProfile>
    {
    }
}