﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Ope_UserEvent.
    /// </summary>
    [Serializable]
    public class Ope_UserEventCollection : BusinessCollection<Ope_UserEvent>
    {
    }
}