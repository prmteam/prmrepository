﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Ope_QuotInput.
    /// </summary>
    [DataContract, Serializable]
    public partial class Ope_QuotInput : BusinessObject<Ope_QuotInput, Ope_QuotInputCollection>
    {
        #region Constantes Columnas
protected const string ColumnaQuotInputId = "QuotInputId";
protected const string ColumnaQuotInput_QuotationId = "QuotInput_QuotationId";
protected const string ColumnaQuotInput_ResidualValue = "QuotInput_ResidualValue";
protected const string ColumnaQuotInput_PurchaseOption = "QuotInput_PurchaseOption";
protected const string ColumnaQuotInput_BalloonPayment = "QuotInput_BalloonPayment";
protected const string ColumnaQuotInput_OpFee = "QuotInput_OpFee";
protected const string ColumnaQuotInput_SecurityDeposit = "QuotInput_SecurityDeposit";
protected const string ColumnaQuotInput_BlindDiscount = "QuotInput_BlindDiscount";
protected const string ColumnaQuotInput_ReferalFee = "QuotInput_ReferalFee";
protected const string ColumnaQuotInput_Delay = "QuotInput_Delay";
protected const string ColumnaQuotInput_GraceMonths = "QuotInput_GraceMonths";
protected const string ColumnaQuotInput_AdditionalCosts = "QuotInput_AdditionalCosts";
protected const string ColumnaQuotInput_LegalFees = "QuotInput_LegalFees";
protected const string ColumnaQuotInput_Downpayment = "QuotInput_Downpayment";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Ope_QuotInput.
        /// </summary>
        public Ope_QuotInput()
            : base()
        {
            this.TableName = "tbl_ope_QuotInput";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaQuotInputId, DbType.Int32, true)]
        public int? QuotInputId { get; set; }

[DataMember, BdAction(ColumnaQuotInput_QuotationId, DbType.Int32, true, true)]
        public int? QuotInput_QuotationId { get; set; }

[DataMember, BdAction(ColumnaQuotInput_ResidualValue, DbType.Double, true, true)]
        public double? QuotInput_ResidualValue { get; set; }

[DataMember, BdAction(ColumnaQuotInput_PurchaseOption, DbType.Double, true, true)]
        public double? QuotInput_PurchaseOption { get; set; }

[DataMember, BdAction(ColumnaQuotInput_BalloonPayment, DbType.Double, true, true)]
        public double? QuotInput_BalloonPayment { get; set; }

[DataMember, BdAction(ColumnaQuotInput_OpFee, DbType.Double, true, true)]
        public double? QuotInput_OpFee { get; set; }

[DataMember, BdAction(ColumnaQuotInput_SecurityDeposit, DbType.Double, true, true)]
        public double? QuotInput_SecurityDeposit { get; set; }

[DataMember, BdAction(ColumnaQuotInput_BlindDiscount, DbType.Double, true, true)]
        public double? QuotInput_BlindDiscount { get; set; }

[DataMember, BdAction(ColumnaQuotInput_ReferalFee, DbType.Double, true, true)]
        public double? QuotInput_ReferalFee { get; set; }

[DataMember, BdAction(ColumnaQuotInput_Delay, DbType.Int32, true, true)]
        public int? QuotInput_Delay { get; set; }

[DataMember, BdAction(ColumnaQuotInput_GraceMonths, DbType.Int32, true, true)]
        public int? QuotInput_GraceMonths { get; set; }

[DataMember, BdAction(ColumnaQuotInput_AdditionalCosts, DbType.Double, true, true)]
        public double? QuotInput_AdditionalCosts { get; set; }

[DataMember, BdAction(ColumnaQuotInput_LegalFees, DbType.Double, true, true)]
        public double? QuotInput_LegalFees { get; set; }

[DataMember, BdAction(ColumnaQuotInput_Downpayment, DbType.Double, true, true)]
        public double? QuotInput_Downpayment { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.QuotInputId = this.ObtenerValorColumna<int?>(ColumnaQuotInputId);
this.QuotInput_QuotationId = this.ObtenerValorColumna<int?>(ColumnaQuotInput_QuotationId);
this.QuotInput_ResidualValue = this.ObtenerValorColumna<double?>(ColumnaQuotInput_ResidualValue);
this.QuotInput_PurchaseOption = this.ObtenerValorColumna<double?>(ColumnaQuotInput_PurchaseOption);
this.QuotInput_BalloonPayment = this.ObtenerValorColumna<double?>(ColumnaQuotInput_BalloonPayment);
this.QuotInput_OpFee = this.ObtenerValorColumna<double?>(ColumnaQuotInput_OpFee);
this.QuotInput_SecurityDeposit = this.ObtenerValorColumna<double?>(ColumnaQuotInput_SecurityDeposit);
this.QuotInput_BlindDiscount = this.ObtenerValorColumna<double?>(ColumnaQuotInput_BlindDiscount);
this.QuotInput_ReferalFee = this.ObtenerValorColumna<double?>(ColumnaQuotInput_ReferalFee);
this.QuotInput_Delay = this.ObtenerValorColumna<int?>(ColumnaQuotInput_Delay);
this.QuotInput_GraceMonths = this.ObtenerValorColumna<int?>(ColumnaQuotInput_GraceMonths);
this.QuotInput_AdditionalCosts = this.ObtenerValorColumna<double?>(ColumnaQuotInput_AdditionalCosts);
this.QuotInput_LegalFees = this.ObtenerValorColumna<double?>(ColumnaQuotInput_LegalFees);
this.QuotInput_Downpayment = this.ObtenerValorColumna<double?>(ColumnaQuotInput_Downpayment);
        }
        #endregion
    }
}