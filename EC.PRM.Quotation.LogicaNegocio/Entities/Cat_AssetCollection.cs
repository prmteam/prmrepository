﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_Asset.
    /// </summary>
    [Serializable]
    public class Cat_AssetCollection : BusinessCollection<Cat_Asset>
    {
    }
}