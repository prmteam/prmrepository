﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Ope_Program.
    /// </summary>
    [DataContract, Serializable]
    public partial class Ope_Program : BusinessObject<Ope_Program, Ope_ProgramCollection>
    {
        #region Constantes Columnas
protected const string ColumnaProgramId = "ProgramId";
protected const string ColumnaProgram_VendorId = "Program_VendorId";
protected const string ColumnaProgram_Description = "Program_Description";
protected const string ColumnaProgram_Segment = "Program_Segment";
protected const string ColumnaProgram_Timing = "Program_Timing";
protected const string ColumnaProgram_QuoteValidity = "Program_QuoteValidity";
protected const string ColumnaProgram_Active = "Program_Active";
protected const string ColumnaProgram_AlterDate = "Program_AlterDate";
protected const string ColumnaProgram_AlterUserId = "Program_AlterUserId";
protected const string ColumnaProgram_AlterDescription = "Program_AlterDescription";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Ope_Program.
        /// </summary>
        public Ope_Program()
            : base()
        {
            this.TableName = "tbl_ope_Program";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaProgramId, DbType.Int32, true)]
        public int? ProgramId { get; set; }

[DataMember, BdAction(ColumnaProgram_VendorId, DbType.Int32, true, true)]
        public int? Program_VendorId { get; set; }

[DataMember, BdAction(ColumnaProgram_Description, DbType.AnsiString, true, true)]
        public string Program_Description { get; set; }

[DataMember, BdAction(ColumnaProgram_Segment, DbType.AnsiString, true, true)]
        public string Program_Segment { get; set; }

[DataMember, BdAction(ColumnaProgram_Timing, DbType.AnsiString, true, true)]
        public string Program_Timing { get; set; }

[DataMember, BdAction(ColumnaProgram_QuoteValidity, DbType.Int32, true, true)]
        public int? Program_QuoteValidity { get; set; }

[DataMember, BdAction(ColumnaProgram_Active, DbType.Boolean, true, true)]
        public bool? Program_Active { get; set; }

[DataMember, BdAction(ColumnaProgram_AlterDate, DbType.DateTime, true, true)]
        public DateTime? Program_AlterDate { get; set; }

[DataMember, BdAction(ColumnaProgram_AlterUserId, DbType.Int32, true, true)]
        public int? Program_AlterUserId { get; set; }

[DataMember, BdAction(ColumnaProgram_AlterDescription, DbType.AnsiString, true, true)]
        public string Program_AlterDescription { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.ProgramId = this.ObtenerValorColumna<int?>(ColumnaProgramId);
this.Program_VendorId = this.ObtenerValorColumna<int?>(ColumnaProgram_VendorId);
this.Program_Description = this.ObtenerValorColumna<string>(ColumnaProgram_Description);
this.Program_Segment = this.ObtenerValorColumna<string>(ColumnaProgram_Segment);
this.Program_Timing = this.ObtenerValorColumna<string>(ColumnaProgram_Timing);
this.Program_QuoteValidity = this.ObtenerValorColumna<int?>(ColumnaProgram_QuoteValidity);
this.Program_Active = this.ObtenerValorColumna<bool?>(ColumnaProgram_Active);
this.Program_AlterDate = this.ObtenerValorColumna<DateTime?>(ColumnaProgram_AlterDate);
this.Program_AlterUserId = this.ObtenerValorColumna<int?>(ColumnaProgram_AlterUserId);
this.Program_AlterDescription = this.ObtenerValorColumna<string>(ColumnaProgram_AlterDescription);
        }
        #endregion
    }
}