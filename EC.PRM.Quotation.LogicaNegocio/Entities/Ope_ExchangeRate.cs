﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Ope_ExchangeRate.
    /// </summary>
    [DataContract, Serializable]
    public partial class Ope_ExchangeRate : BusinessObject<Ope_ExchangeRate, Ope_ExchangeRateCollection>
    {
        #region Constantes Columnas
protected const string ColumnaExchangeRateId = "ExchangeRateId";
protected const string ColumnaExchangeRate_Date = "ExchangeRate_Date";
protected const string ColumnaExchangeRate_MXN = "ExchangeRate_MXN";
protected const string ColumnaExchangeRate_CurrencyId = "ExchangeRate_CurrencyId";
protected const string ColumnaExchangeRate_Source = "ExchangeRate_Source";
protected const string ColumnaExchangeRate_Attemps = "ExchangeRate_Attemps";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Ope_ExchangeRate.
        /// </summary>
        public Ope_ExchangeRate()
            : base()
        {
            this.TableName = "tbl_ope_ExchangeRate";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaExchangeRateId, DbType.Int32, true)]
        public int? ExchangeRateId { get; set; }

[DataMember, BdAction(ColumnaExchangeRate_Date, DbType.Date, true, true)]
        public DateTime? ExchangeRate_Date { get; set; }

[DataMember, BdAction(ColumnaExchangeRate_MXN, DbType.Double, true, true)]
        public double? ExchangeRate_MXN { get; set; }

[DataMember, BdAction(ColumnaExchangeRate_CurrencyId, DbType.Int32, true, true)]
        public int? ExchangeRate_CurrencyId { get; set; }

[DataMember, BdAction(ColumnaExchangeRate_Source, DbType.AnsiString, true, true)]
        public string ExchangeRate_Source { get; set; }

[DataMember, BdAction(ColumnaExchangeRate_Attemps, DbType.Int32, true, true)]
        public int? ExchangeRate_Attemps { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.ExchangeRateId = this.ObtenerValorColumna<int?>(ColumnaExchangeRateId);
this.ExchangeRate_Date = this.ObtenerValorColumna<DateTime?>(ColumnaExchangeRate_Date);
this.ExchangeRate_MXN = this.ObtenerValorColumna<double?>(ColumnaExchangeRate_MXN);
this.ExchangeRate_CurrencyId = this.ObtenerValorColumna<int?>(ColumnaExchangeRate_CurrencyId);
this.ExchangeRate_Source = this.ObtenerValorColumna<string>(ColumnaExchangeRate_Source);
this.ExchangeRate_Attemps = this.ObtenerValorColumna<int?>(ColumnaExchangeRate_Attemps);
        }
        #endregion
    }
}