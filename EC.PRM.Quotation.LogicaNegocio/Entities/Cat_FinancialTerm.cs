﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_FinancialTerm.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_FinancialTerm : BusinessObject<Cat_FinancialTerm, Cat_FinancialTermCollection>
    {
        #region Constantes Columnas
protected const string ColumnaFinancialTermId = "FinancialTermId";
protected const string ColumnaFinancialTerm_Value = "FinancialTerm_Value";
protected const string ColumnaFinancialTerm_Description = "FinancialTerm_Description";
protected const string ColumnaFinancialTerm_Active = "FinancialTerm_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_FinancialTerm.
        /// </summary>
        public Cat_FinancialTerm()
            : base()
        {
            this.TableName = "tbl_cat_FinancialTerm";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaFinancialTermId, DbType.Int32, true)]
        public int? FinancialTermId { get; set; }

[DataMember, BdAction(ColumnaFinancialTerm_Value, DbType.Int32, true, true)]
        public int? FinancialTerm_Value { get; set; }

[DataMember, BdAction(ColumnaFinancialTerm_Description, DbType.AnsiString, true, true)]
        public string FinancialTerm_Description { get; set; }

[DataMember, BdAction(ColumnaFinancialTerm_Active, DbType.Boolean, true, true)]
        public bool? FinancialTerm_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.FinancialTermId = this.ObtenerValorColumna<int?>(ColumnaFinancialTermId);
this.FinancialTerm_Value = this.ObtenerValorColumna<int?>(ColumnaFinancialTerm_Value);
this.FinancialTerm_Description = this.ObtenerValorColumna<string>(ColumnaFinancialTerm_Description);
this.FinancialTerm_Active = this.ObtenerValorColumna<bool?>(ColumnaFinancialTerm_Active);
        }
        #endregion
    }
}