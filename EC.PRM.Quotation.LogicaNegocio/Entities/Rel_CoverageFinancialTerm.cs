﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Rel_CoverageFinancialTerm.
    /// </summary>
    [DataContract, Serializable]
    public partial class Rel_CoverageFinancialTerm : BusinessObject<Rel_CoverageFinancialTerm, Rel_CoverageFinancialTermCollection>
    {
        #region Constantes Columnas
protected const string ColumnaCoverageFinancialTermId = "CoverageFinancialTermId";
protected const string ColumnaCoverageFinancialTerm_CoverageId = "CoverageFinancialTerm_CoverageId";
protected const string ColumnaCoverageFinancialTerm_FinancialTermId = "CoverageFinancialTerm_FinancialTermId";
protected const string ColumnaCoverageFinancialTerm_Factor = "CoverageFinancialTerm_Factor";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Rel_CoverageFinancialTerm.
        /// </summary>
        public Rel_CoverageFinancialTerm()
            : base()
        {
            this.TableName = "tbl_rel_CoverageFinancialTerm";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaCoverageFinancialTermId, DbType.Int32, true)]
        public int? CoverageFinancialTermId { get; set; }

[DataMember, BdAction(ColumnaCoverageFinancialTerm_CoverageId, DbType.Int32, true, true)]
        public int? CoverageFinancialTerm_CoverageId { get; set; }

[DataMember, BdAction(ColumnaCoverageFinancialTerm_FinancialTermId, DbType.Int32, true, true)]
        public int? CoverageFinancialTerm_FinancialTermId { get; set; }

[DataMember, BdAction(ColumnaCoverageFinancialTerm_Factor, DbType.Double, true, true)]
        public double? CoverageFinancialTerm_Factor { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.CoverageFinancialTermId = this.ObtenerValorColumna<int?>(ColumnaCoverageFinancialTermId);
this.CoverageFinancialTerm_CoverageId = this.ObtenerValorColumna<int?>(ColumnaCoverageFinancialTerm_CoverageId);
this.CoverageFinancialTerm_FinancialTermId = this.ObtenerValorColumna<int?>(ColumnaCoverageFinancialTerm_FinancialTermId);
this.CoverageFinancialTerm_Factor = this.ObtenerValorColumna<double?>(ColumnaCoverageFinancialTerm_Factor);
        }
        #endregion
    }
}