﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_AssetCollateral.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_AssetCollateral : BusinessObject<Cat_AssetCollateral, Cat_AssetCollateralCollection>
    {
        #region Constantes Columnas
protected const string ColumnaAssetCollateralId = "AssetCollateralId";
protected const string ColumnaAssetCollateral_Description = "AssetCollateral_Description";
protected const string ColumnaAssetCollateral_Active = "AssetCollateral_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_AssetCollateral.
        /// </summary>
        public Cat_AssetCollateral()
            : base()
        {
            this.TableName = "tbl_cat_AssetCollateral";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaAssetCollateralId, DbType.Int32, true)]
        public int? AssetCollateralId { get; set; }

[DataMember, BdAction(ColumnaAssetCollateral_Description, DbType.AnsiString, true, true)]
        public string AssetCollateral_Description { get; set; }

[DataMember, BdAction(ColumnaAssetCollateral_Active, DbType.Boolean, true, true)]
        public bool? AssetCollateral_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.AssetCollateralId = this.ObtenerValorColumna<int?>(ColumnaAssetCollateralId);
this.AssetCollateral_Description = this.ObtenerValorColumna<string>(ColumnaAssetCollateral_Description);
this.AssetCollateral_Active = this.ObtenerValorColumna<bool?>(ColumnaAssetCollateral_Active);
        }
        #endregion
    }
}