﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Rel_ProgramAsset.
    /// </summary>
    [Serializable]
    public class Rel_ProgramAssetCollection : BusinessCollection<Rel_ProgramAsset>
    {
    }
}