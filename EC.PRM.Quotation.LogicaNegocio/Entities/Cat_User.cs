﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_User.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_User : BusinessObject<Cat_User, Cat_UserCollection>
    {
        #region Constantes Columnas
protected const string ColumnaUserId = "UserId";
protected const string ColumnaUser_FirstName = "User_FirstName";
protected const string ColumnaUser_LastName = "User_LastName";
protected const string ColumnaUser_User = "User_User";
protected const string ColumnaUser_Password = "User_Password";
protected const string ColumnaUser_ImageUrl = "User_ImageUrl";
protected const string ColumnaUser_Email = "User_Email";
protected const string ColumnaUser_VendorId = "User_VendorId";
protected const string ColumnaUser_UserProfileId = "User_UserProfileId";
protected const string ColumnaUser_ProgramId = "User_ProgramId";
protected const string ColumnaUser_Active = "User_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_User.
        /// </summary>
        public Cat_User()
            : base()
        {
            this.TableName = "tbl_cat_User";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaUserId, DbType.Int32, true)]
        public int? UserId { get; set; }

[DataMember, BdAction(ColumnaUser_FirstName, DbType.AnsiString, true, true)]
        public string User_FirstName { get; set; }

[DataMember, BdAction(ColumnaUser_LastName, DbType.AnsiString, true, true)]
        public string User_LastName { get; set; }

[DataMember, BdAction(ColumnaUser_User, DbType.AnsiString, true, true)]
        public string User_User { get; set; }

[DataMember, BdAction(ColumnaUser_Password, DbType.AnsiString, true, true)]
        public string User_Password { get; set; }

[DataMember, BdAction(ColumnaUser_ImageUrl, DbType.AnsiString, true, true)]
        public string User_ImageUrl { get; set; }

[DataMember, BdAction(ColumnaUser_Email, DbType.AnsiString, true, true)]
        public string User_Email { get; set; }

[DataMember, BdAction(ColumnaUser_VendorId, DbType.Int32, true, true)]
        public int? User_VendorId { get; set; }

[DataMember, BdAction(ColumnaUser_UserProfileId, DbType.Int32, true, true)]
        public int? User_UserProfileId { get; set; }

[DataMember, BdAction(ColumnaUser_ProgramId, DbType.Int32, true, true)]
        public int? User_ProgramId { get; set; }

[DataMember, BdAction(ColumnaUser_Active, DbType.Boolean, true, true)]
        public bool? User_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.UserId = this.ObtenerValorColumna<int?>(ColumnaUserId);
this.User_FirstName = this.ObtenerValorColumna<string>(ColumnaUser_FirstName);
this.User_LastName = this.ObtenerValorColumna<string>(ColumnaUser_LastName);
this.User_User = this.ObtenerValorColumna<string>(ColumnaUser_User);
this.User_Password = this.ObtenerValorColumna<string>(ColumnaUser_Password);
this.User_ImageUrl = this.ObtenerValorColumna<string>(ColumnaUser_ImageUrl);
this.User_Email = this.ObtenerValorColumna<string>(ColumnaUser_Email);
this.User_VendorId = this.ObtenerValorColumna<int?>(ColumnaUser_VendorId);
this.User_UserProfileId = this.ObtenerValorColumna<int?>(ColumnaUser_UserProfileId);
this.User_ProgramId = this.ObtenerValorColumna<int?>(ColumnaUser_ProgramId);
this.User_Active = this.ObtenerValorColumna<bool?>(ColumnaUser_Active);
        }
        #endregion
    }
}