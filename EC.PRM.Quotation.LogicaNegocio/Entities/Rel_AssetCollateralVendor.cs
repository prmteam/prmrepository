﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Rel_AssetCollateralVendor.
    /// </summary>
    [DataContract, Serializable]
    public partial class Rel_AssetCollateralVendor : BusinessObject<Rel_AssetCollateralVendor, Rel_AssetCollateralVendorCollection>
    {
        #region Constantes Columnas
protected const string ColumnaAssetCollateralVendorId = "AssetCollateralVendorId";
protected const string ColumnaAssetCollateralVendor_VendorId = "AssetCollateralVendor_VendorId";
protected const string ColumnaAssetCollateralVendor_AssetCollateralId = "AssetCollateralVendor_AssetCollateralId";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Rel_AssetCollateralVendor.
        /// </summary>
        public Rel_AssetCollateralVendor()
            : base()
        {
            this.TableName = "tbl_rel_AssetCollateralVendor";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaAssetCollateralVendorId, DbType.Int32, true)]
        public int? AssetCollateralVendorId { get; set; }

[DataMember, BdAction(ColumnaAssetCollateralVendor_VendorId, DbType.Int32, true, true)]
        public int? AssetCollateralVendor_VendorId { get; set; }

[DataMember, BdAction(ColumnaAssetCollateralVendor_AssetCollateralId, DbType.Int32, true, true)]
        public int? AssetCollateralVendor_AssetCollateralId { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.AssetCollateralVendorId = this.ObtenerValorColumna<int?>(ColumnaAssetCollateralVendorId);
this.AssetCollateralVendor_VendorId = this.ObtenerValorColumna<int?>(ColumnaAssetCollateralVendor_VendorId);
this.AssetCollateralVendor_AssetCollateralId = this.ObtenerValorColumna<int?>(ColumnaAssetCollateralVendor_AssetCollateralId);
        }
        #endregion
    }
}