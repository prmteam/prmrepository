﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_Coverage.
    /// </summary>
    [Serializable]
    public class Cat_CoverageCollection : BusinessCollection<Cat_Coverage>
    {
    }
}