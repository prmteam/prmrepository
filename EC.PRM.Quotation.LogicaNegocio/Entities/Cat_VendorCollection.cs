﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_Vendor.
    /// </summary>
    [Serializable]
    public class Cat_VendorCollection : BusinessCollection<Cat_Vendor>
    {
    }
}