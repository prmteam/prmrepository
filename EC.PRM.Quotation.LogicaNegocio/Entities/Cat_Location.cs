﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_Location.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_Location : BusinessObject<Cat_Location, Cat_LocationCollection>
    {
        #region Constantes Columnas
protected const string ColumnaLocationId = "LocationId";
protected const string ColumnaLocation_Name = "Location_Name";
protected const string ColumnaLocation_Active = "Location_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_Location.
        /// </summary>
        public Cat_Location()
            : base()
        {
            this.TableName = "tbl_cat_Location";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaLocationId, DbType.Int32, true)]
        public int? LocationId { get; set; }

[DataMember, BdAction(ColumnaLocation_Name, DbType.AnsiString, true, true)]
        public string Location_Name { get; set; }

[DataMember, BdAction(ColumnaLocation_Active, DbType.Boolean, true, true)]
        public bool? Location_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.LocationId = this.ObtenerValorColumna<int?>(ColumnaLocationId);
this.Location_Name = this.ObtenerValorColumna<string>(ColumnaLocation_Name);
this.Location_Active = this.ObtenerValorColumna<bool?>(ColumnaLocation_Active);
        }
        #endregion
    }
}