﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Rel_QuotationAsset.
    /// </summary>
    [DataContract, Serializable]
    public partial class Rel_QuotationAsset : BusinessObject<Rel_QuotationAsset, Rel_QuotationAssetCollection>
    {
        #region Constantes Columnas
protected const string ColumnaQuotationAssetId = "QuotationAssetId";
protected const string ColumnaQuotationAsset_QuotationId = "QuotationAsset_QuotationId";
protected const string ColumnaQuotationAsset_AssetId = "QuotationAsset_AssetId";
protected const string ColumnaQuotationAsset_Quantity = "QuotationAsset_Quantity";
protected const string ColumnaQuotationAsset_Amount = "QuotationAsset_Amount";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Rel_QuotationAsset.
        /// </summary>
        public Rel_QuotationAsset()
            : base()
        {
            this.TableName = "tbl_rel_QuotationAsset";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaQuotationAssetId, DbType.Int32, true)]
        public int? QuotationAssetId { get; set; }

[DataMember, BdAction(ColumnaQuotationAsset_QuotationId, DbType.Int32, true, true)]
        public int? QuotationAsset_QuotationId { get; set; }

[DataMember, BdAction(ColumnaQuotationAsset_AssetId, DbType.Int32, true, true)]
        public int? QuotationAsset_AssetId { get; set; }

[DataMember, BdAction(ColumnaQuotationAsset_Quantity, DbType.Int32, true, true)]
        public int? QuotationAsset_Quantity { get; set; }

[DataMember, BdAction(ColumnaQuotationAsset_Amount, DbType.Double, true, true)]
        public double? QuotationAsset_Amount { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.QuotationAssetId = this.ObtenerValorColumna<int?>(ColumnaQuotationAssetId);
this.QuotationAsset_QuotationId = this.ObtenerValorColumna<int?>(ColumnaQuotationAsset_QuotationId);
this.QuotationAsset_AssetId = this.ObtenerValorColumna<int?>(ColumnaQuotationAsset_AssetId);
this.QuotationAsset_Quantity = this.ObtenerValorColumna<int?>(ColumnaQuotationAsset_Quantity);
this.QuotationAsset_Amount = this.ObtenerValorColumna<double?>(ColumnaQuotationAsset_Amount);
        }
        #endregion
    }
}