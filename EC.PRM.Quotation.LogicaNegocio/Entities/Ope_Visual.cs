﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Ope_Visual.
    /// </summary>
    [DataContract, Serializable]
    public partial class Ope_Visual : BusinessObject<Ope_Visual, Ope_VisualCollection>
    {
        #region Constantes Columnas
protected const string ColumnaVisualId = "VisualId";
protected const string ColumnaVisual_ProgramId = "Visual_ProgramId";
protected const string ColumnaVisual_BGColor = "Visual_BGColor";
protected const string ColumnaVisual_BannerColor = "Visual_BannerColor";
protected const string ColumnaVisual_HeadColor = "Visual_HeadColor";
protected const string ColumnaVisual_SecondColor = "Visual_SecondColor";
protected const string ColumnaVisual_Logo = "Visual_Logo";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Ope_Visual.
        /// </summary>
        public Ope_Visual()
            : base()
        {
            this.TableName = "tbl_ope_Visual";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaVisualId, DbType.Int32, true)]
        public int? VisualId { get; set; }

[DataMember, BdAction(ColumnaVisual_ProgramId, DbType.Int32, true, true)]
        public int? Visual_ProgramId { get; set; }

[DataMember, BdAction(ColumnaVisual_BGColor, DbType.AnsiString, true, true)]
        public string Visual_BGColor { get; set; }

[DataMember, BdAction(ColumnaVisual_BannerColor, DbType.AnsiString, true, true)]
        public string Visual_BannerColor { get; set; }

[DataMember, BdAction(ColumnaVisual_HeadColor, DbType.AnsiString, true, true)]
        public string Visual_HeadColor { get; set; }

[DataMember, BdAction(ColumnaVisual_SecondColor, DbType.AnsiString, true, true)]
        public string Visual_SecondColor { get; set; }

[DataMember, BdAction(ColumnaVisual_Logo, DbType.AnsiString, true, true)]
        public string Visual_Logo { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.VisualId = this.ObtenerValorColumna<int?>(ColumnaVisualId);
this.Visual_ProgramId = this.ObtenerValorColumna<int?>(ColumnaVisual_ProgramId);
this.Visual_BGColor = this.ObtenerValorColumna<string>(ColumnaVisual_BGColor);
this.Visual_BannerColor = this.ObtenerValorColumna<string>(ColumnaVisual_BannerColor);
this.Visual_HeadColor = this.ObtenerValorColumna<string>(ColumnaVisual_HeadColor);
this.Visual_SecondColor = this.ObtenerValorColumna<string>(ColumnaVisual_SecondColor);
this.Visual_Logo = this.ObtenerValorColumna<string>(ColumnaVisual_Logo);
        }
        #endregion
    }
}