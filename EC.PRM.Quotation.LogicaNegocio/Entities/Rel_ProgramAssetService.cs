﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Rel_ProgramAssetService.
    /// </summary>
    [DataContract, Serializable]
    public partial class Rel_ProgramAssetService : BusinessObject<Rel_ProgramAssetService, Rel_ProgramAssetServiceCollection>
    {
        #region Constantes Columnas
protected const string ColumnaProgramAssetServiceId = "ProgramAssetServiceId";
protected const string ColumnaProgramAssetService_ProgramAssetId = "ProgramAssetService_ProgramAssetId";
protected const string ColumnaProgramAssetService_AssetId = "ProgramAssetService_AssetId";
protected const string ColumnaProgramAssetService_AssetServiceId = "ProgramAssetService_AssetServiceId";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Rel_ProgramAssetService.
        /// </summary>
        public Rel_ProgramAssetService()
            : base()
        {
            this.TableName = "tbl_rel_ProgramAssetService";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaProgramAssetServiceId, DbType.Int32, true)]
        public int? ProgramAssetServiceId { get; set; }

[DataMember, BdAction(ColumnaProgramAssetService_ProgramAssetId, DbType.Int32, true, true)]
        public int? ProgramAssetService_ProgramAssetId { get; set; }

[DataMember, BdAction(ColumnaProgramAssetService_AssetId, DbType.Int32, true, true)]
        public int? ProgramAssetService_AssetId { get; set; }

[DataMember, BdAction(ColumnaProgramAssetService_AssetServiceId, DbType.Int32, true, true)]
        public int? ProgramAssetService_AssetServiceId { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.ProgramAssetServiceId = this.ObtenerValorColumna<int?>(ColumnaProgramAssetServiceId);
this.ProgramAssetService_ProgramAssetId = this.ObtenerValorColumna<int?>(ColumnaProgramAssetService_ProgramAssetId);
this.ProgramAssetService_AssetId = this.ObtenerValorColumna<int?>(ColumnaProgramAssetService_AssetId);
this.ProgramAssetService_AssetServiceId = this.ObtenerValorColumna<int?>(ColumnaProgramAssetService_AssetServiceId);
        }
        #endregion
    }
}