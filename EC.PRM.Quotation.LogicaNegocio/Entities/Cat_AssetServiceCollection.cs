﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_AssetService.
    /// </summary>
    [Serializable]
    public class Cat_AssetServiceCollection : BusinessCollection<Cat_AssetService>
    {
    }
}