﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Ope_Quotation.
    /// </summary>
    [Serializable]
    public class Ope_QuotationCollection : BusinessCollection<Ope_Quotation>
    {
    }
}