﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_AssetCollateral.
    /// </summary>
    [Serializable]
    public class Cat_AssetCollateralCollection : BusinessCollection<Cat_AssetCollateral>
    {
    }
}