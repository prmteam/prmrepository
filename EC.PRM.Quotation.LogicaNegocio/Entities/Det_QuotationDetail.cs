﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Det_QuotationDetail.
    /// </summary>
    [DataContract, Serializable]
    public partial class Det_QuotationDetail : BusinessObject<Det_QuotationDetail, Det_QuotationDetailCollection>
    {
        #region Constantes Columnas
protected const string ColumnaQuotationDetailId = "QuotationDetailId";
protected const string ColumnaQuotationDetail_QuotationId = "QuotationDetail_QuotationId";
protected const string ColumnaQuotationDetail_Period = "QuotationDetail_Period";
protected const string ColumnaQuotationDetail_Payment = "QuotationDetail_Payment";
protected const string ColumnaQuotationDetail_Rate = "QuotationDetail_Rate";
protected const string ColumnaQuotationDetail_Total = "QuotationDetail_Total";
protected const string ColumnaQuotationDetail_Balance = "QuotationDetail_Balance";
protected const string ColumnaQuotationDetail_Capital = "QuotationDetail_Capital";
protected const string ColumnaQuotationDetail_Interes = "QuotationDetail_Interes";
protected const string ColumnaQuotationDetail_Subtotal = "QuotationDetail_Subtotal";
protected const string ColumnaQuotationDetail_Insurance = "QuotationDetail_Insurance";
protected const string ColumnaQuotationDetail_Iva = "QuotationDetail_Iva";
protected const string ColumnaQuotationDetail_PaymentIva = "QuotationDetail_PaymentIva";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Det_QuotationDetail.
        /// </summary>
        public Det_QuotationDetail()
            : base()
        {
            this.TableName = "tbl_det_QuotationDetail";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaQuotationDetailId, DbType.Int32, true)]
        public int? QuotationDetailId { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_QuotationId, DbType.Int32, true, true)]
        public int? QuotationDetail_QuotationId { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_Period, DbType.Int32, true, true)]
        public int? QuotationDetail_Period { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_Payment, DbType.Double, true, true)]
        public double? QuotationDetail_Payment { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_Rate, DbType.Double, true, true)]
        public double? QuotationDetail_Rate { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_Total, DbType.Double, true, true)]
        public double? QuotationDetail_Total { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_Balance, DbType.Double, true, true)]
        public double? QuotationDetail_Balance { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_Capital, DbType.Double, true, true)]
        public double? QuotationDetail_Capital { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_Interes, DbType.Double, true, true)]
        public double? QuotationDetail_Interes { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_Subtotal, DbType.Double, true, true)]
        public double? QuotationDetail_Subtotal { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_Insurance, DbType.Double, true, true)]
        public double? QuotationDetail_Insurance { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_Iva, DbType.Double, true, true)]
        public double? QuotationDetail_Iva { get; set; }

[DataMember, BdAction(ColumnaQuotationDetail_PaymentIva, DbType.Double, true, true)]
        public double? QuotationDetail_PaymentIva { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.QuotationDetailId = this.ObtenerValorColumna<int?>(ColumnaQuotationDetailId);
this.QuotationDetail_QuotationId = this.ObtenerValorColumna<int?>(ColumnaQuotationDetail_QuotationId);
this.QuotationDetail_Period = this.ObtenerValorColumna<int?>(ColumnaQuotationDetail_Period);
this.QuotationDetail_Payment = this.ObtenerValorColumna<double?>(ColumnaQuotationDetail_Payment);
this.QuotationDetail_Rate = this.ObtenerValorColumna<double?>(ColumnaQuotationDetail_Rate);
this.QuotationDetail_Total = this.ObtenerValorColumna<double?>(ColumnaQuotationDetail_Total);
this.QuotationDetail_Balance = this.ObtenerValorColumna<double?>(ColumnaQuotationDetail_Balance);
this.QuotationDetail_Capital = this.ObtenerValorColumna<double?>(ColumnaQuotationDetail_Capital);
this.QuotationDetail_Interes = this.ObtenerValorColumna<double?>(ColumnaQuotationDetail_Interes);
this.QuotationDetail_Subtotal = this.ObtenerValorColumna<double?>(ColumnaQuotationDetail_Subtotal);
this.QuotationDetail_Insurance = this.ObtenerValorColumna<double?>(ColumnaQuotationDetail_Insurance);
this.QuotationDetail_Iva = this.ObtenerValorColumna<double?>(ColumnaQuotationDetail_Iva);
this.QuotationDetail_PaymentIva = this.ObtenerValorColumna<double?>(ColumnaQuotationDetail_PaymentIva);
        }
        #endregion
    }
}