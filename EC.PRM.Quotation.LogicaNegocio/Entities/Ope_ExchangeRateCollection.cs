﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Ope_ExchangeRate.
    /// </summary>
    [Serializable]
    public class Ope_ExchangeRateCollection : BusinessCollection<Ope_ExchangeRate>
    {
    }
}