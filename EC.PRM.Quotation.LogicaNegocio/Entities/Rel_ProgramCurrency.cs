﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Rel_ProgramCurrency.
    /// </summary>
    [DataContract, Serializable]
    public partial class Rel_ProgramCurrency : BusinessObject<Rel_ProgramCurrency, Rel_ProgramCurrencyCollection>
    {
        #region Constantes Columnas
protected const string ColumnaProgramCurrencyId = "ProgramCurrencyId";
protected const string ColumnaProgramCurrency_ProgramId = "ProgramCurrency_ProgramId";
protected const string ColumnaProgramCurrency_CurrencyId = "ProgramCurrency_CurrencyId";
protected const string ColumnaProgramCurrency_AlterDate = "ProgramCurrency_AlterDate";
protected const string ColumnaProgramCurrency_AlterUserId = "ProgramCurrency_AlterUserId";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Rel_ProgramCurrency.
        /// </summary>
        public Rel_ProgramCurrency()
            : base()
        {
            this.TableName = "tbl_rel_ProgramCurrency";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaProgramCurrencyId, DbType.Int32, true)]
        public int? ProgramCurrencyId { get; set; }

[DataMember, BdAction(ColumnaProgramCurrency_ProgramId, DbType.Int32, true, true)]
        public int? ProgramCurrency_ProgramId { get; set; }

[DataMember, BdAction(ColumnaProgramCurrency_CurrencyId, DbType.Int32, true, true)]
        public int? ProgramCurrency_CurrencyId { get; set; }

[DataMember, BdAction(ColumnaProgramCurrency_AlterDate, DbType.DateTime, true, true)]
        public DateTime? ProgramCurrency_AlterDate { get; set; }

[DataMember, BdAction(ColumnaProgramCurrency_AlterUserId, DbType.Int32, true, true)]
        public int? ProgramCurrency_AlterUserId { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.ProgramCurrencyId = this.ObtenerValorColumna<int?>(ColumnaProgramCurrencyId);
this.ProgramCurrency_ProgramId = this.ObtenerValorColumna<int?>(ColumnaProgramCurrency_ProgramId);
this.ProgramCurrency_CurrencyId = this.ObtenerValorColumna<int?>(ColumnaProgramCurrency_CurrencyId);
this.ProgramCurrency_AlterDate = this.ObtenerValorColumna<DateTime?>(ColumnaProgramCurrency_AlterDate);
this.ProgramCurrency_AlterUserId = this.ObtenerValorColumna<int?>(ColumnaProgramCurrency_AlterUserId);
        }
        #endregion
    }
}