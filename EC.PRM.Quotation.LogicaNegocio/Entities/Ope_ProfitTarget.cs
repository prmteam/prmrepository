﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Ope_ProfitTarget.
    /// </summary>
    [DataContract, Serializable]
    public partial class Ope_ProfitTarget : BusinessObject<Ope_ProfitTarget, Ope_ProfitTargetCollection>
    {
        #region Constantes Columnas
protected const string ColumnaProfitTargetId = "ProfitTargetId";
protected const string ColumnaProfitTarget_FinancialType = "ProfitTarget_FinancialType";
protected const string ColumnaProfitTarget_FinacialTermId = "ProfitTarget_FinacialTermId";
protected const string ColumnaProfitTarger_CurrencyId = "ProfitTarger_CurrencyId";
protected const string ColumnaProfitTarget_ROI = "ProfitTarget_ROI";
protected const string ColumnaProfitTarget_Segment = "ProfitTarget_Segment";
protected const string ColumnaProfitTarget_Percentage = "ProfitTarget_Percentage";
protected const string ColumnaProfitTarget_DateUpload = "ProfitTarget_DateUpload";
protected const string ColumnaProfitTarget_UserIdUpload = "ProfitTarget_UserIdUpload";
protected const string ColumnaProfitTarget_Active = "ProfitTarget_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Ope_ProfitTarget.
        /// </summary>
        public Ope_ProfitTarget()
            : base()
        {
            this.TableName = "tbl_ope_ProfitTarget";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaProfitTargetId, DbType.Int32, true)]
        public int? ProfitTargetId { get; set; }

[DataMember, BdAction(ColumnaProfitTarget_FinancialType, DbType.Int32, true, true)]
        public int? ProfitTarget_FinancialType { get; set; }

[DataMember, BdAction(ColumnaProfitTarget_FinacialTermId, DbType.Int32, true, true)]
        public int? ProfitTarget_FinacialTermId { get; set; }

[DataMember, BdAction(ColumnaProfitTarger_CurrencyId, DbType.Int32, true, true)]
        public int? ProfitTarger_CurrencyId { get; set; }

[DataMember, BdAction(ColumnaProfitTarget_ROI, DbType.Double, true, true)]
        public double? ProfitTarget_ROI { get; set; }

[DataMember, BdAction(ColumnaProfitTarget_Segment, DbType.AnsiString, true, true)]
        public string ProfitTarget_Segment { get; set; }

[DataMember, BdAction(ColumnaProfitTarget_Percentage, DbType.Double, true, true)]
        public double? ProfitTarget_Percentage { get; set; }

[DataMember, BdAction(ColumnaProfitTarget_DateUpload, DbType.DateTime, true, true)]
        public DateTime? ProfitTarget_DateUpload { get; set; }

[DataMember, BdAction(ColumnaProfitTarget_UserIdUpload, DbType.Int32, true, true)]
        public int? ProfitTarget_UserIdUpload { get; set; }

[DataMember, BdAction(ColumnaProfitTarget_Active, DbType.Boolean, true, true)]
        public bool? ProfitTarget_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.ProfitTargetId = this.ObtenerValorColumna<int?>(ColumnaProfitTargetId);
this.ProfitTarget_FinancialType = this.ObtenerValorColumna<int?>(ColumnaProfitTarget_FinancialType);
this.ProfitTarget_FinacialTermId = this.ObtenerValorColumna<int?>(ColumnaProfitTarget_FinacialTermId);
this.ProfitTarger_CurrencyId = this.ObtenerValorColumna<int?>(ColumnaProfitTarger_CurrencyId);
this.ProfitTarget_ROI = this.ObtenerValorColumna<double?>(ColumnaProfitTarget_ROI);
this.ProfitTarget_Segment = this.ObtenerValorColumna<string>(ColumnaProfitTarget_Segment);
this.ProfitTarget_Percentage = this.ObtenerValorColumna<double?>(ColumnaProfitTarget_Percentage);
this.ProfitTarget_DateUpload = this.ObtenerValorColumna<DateTime?>(ColumnaProfitTarget_DateUpload);
this.ProfitTarget_UserIdUpload = this.ObtenerValorColumna<int?>(ColumnaProfitTarget_UserIdUpload);
this.ProfitTarget_Active = this.ObtenerValorColumna<bool?>(ColumnaProfitTarget_Active);
        }
        #endregion
    }
}