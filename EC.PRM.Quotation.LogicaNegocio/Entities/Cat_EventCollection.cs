﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_Event.
    /// </summary>
    [Serializable]
    public class Cat_EventCollection : BusinessCollection<Cat_Event>
    {
    }
}