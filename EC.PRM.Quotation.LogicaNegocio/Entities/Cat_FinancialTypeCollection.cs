﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_FinancialType.
    /// </summary>
    [Serializable]
    public class Cat_FinancialTypeCollection : BusinessCollection<Cat_FinancialType>
    {
    }
}