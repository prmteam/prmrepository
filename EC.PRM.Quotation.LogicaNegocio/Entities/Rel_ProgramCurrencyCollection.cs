﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Rel_ProgramCurrency.
    /// </summary>
    [Serializable]
    public class Rel_ProgramCurrencyCollection : BusinessCollection<Rel_ProgramCurrency>
    {
    }
}