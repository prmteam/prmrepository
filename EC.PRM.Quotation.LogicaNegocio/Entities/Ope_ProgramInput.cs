﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Ope_ProgramInput.
    /// </summary>
    [DataContract, Serializable]
    public partial class Ope_ProgramInput : BusinessObject<Ope_ProgramInput, Ope_ProgramInputCollection>
    {
        #region Constantes Columnas
protected const string ColumnaProgramInputId = "ProgramInputId";
protected const string ColumnaProgramInput_ProgramId = "ProgramInput_ProgramId";
protected const string ColumnaProgramInput_BalloonPaymentMin = "ProgramInput_BalloonPaymentMin";
protected const string ColumnaProgramInput_BalloonPaymentMax = "ProgramInput_BalloonPaymentMax";
protected const string ColumnaProgramInput_BalloonPaymentInc = "ProgramInput_BalloonPaymentInc";
protected const string ColumnaProgramInput_BalloonPaymentVer = "ProgramInput_BalloonPaymentVer";
protected const string ColumnaProgramInput_PurchaseOptionMin = "ProgramInput_PurchaseOptionMin";
protected const string ColumnaProgramInput_PurchaseOptionMax = "ProgramInput_PurchaseOptionMax";
protected const string ColumnaProgramInput_PurchaseOptionInc = "ProgramInput_PurchaseOptionInc";
protected const string ColumnaProgramInput_PurchaseOptionVer = "ProgramInput_PurchaseOptionVer";
protected const string ColumnaProgramInput_OpFeeMin = "ProgramInput_OpFeeMin";
protected const string ColumnaProgramInput_OpFeeMax = "ProgramInput_OpFeeMax";
protected const string ColumnaProgramInput_OpFeeInc = "ProgramInput_OpFeeInc";
protected const string ColumnaProgramInput_OpFeeVer = "ProgramInput_OpFeeVer";
protected const string ColumnaProgramInput_SecurityDepositMin = "ProgramInput_SecurityDepositMin";
protected const string ColumnaProgramInput_SecurityDepositMax = "ProgramInput_SecurityDepositMax";
protected const string ColumnaProgramInput_SecurityDepositInc = "ProgramInput_SecurityDepositInc";
protected const string ColumnaProgramInput_SecurityDepositVer = "ProgramInput_SecurityDepositVer";
protected const string ColumnaProgramInput_BlindDiscountMin = "ProgramInput_BlindDiscountMin";
protected const string ColumnaProgramInput_BlindDiscountMax = "ProgramInput_BlindDiscountMax";
protected const string ColumnaProgramInput_BlindDiscountInc = "ProgramInput_BlindDiscountInc";
protected const string ColumnaProgramInput_BlindDiscountVer = "ProgramInput_BlindDiscountVer";
protected const string ColumnaProgramInput_ReferalFeeMin = "ProgramInput_ReferalFeeMin";
protected const string ColumnaProgramInput_ReferalFeeMax = "ProgramInput_ReferalFeeMax";
protected const string ColumnaProgramInput_ReferalFeeInc = "ProgramInput_ReferalFeeInc";
protected const string ColumnaProgramInput_ReferalFeeVer = "ProgramInput_ReferalFeeVer";
protected const string ColumnaProgramInput_DelayMin = "ProgramInput_DelayMin";
protected const string ColumnaProgramInput_DelayMax = "ProgramInput_DelayMax";
protected const string ColumnaProgramInput_DelayInc = "ProgramInput_DelayInc";
protected const string ColumnaProgramInput_DelayVer = "ProgramInput_DelayVer";
protected const string ColumnaProgramInput_GraceMonthsMin = "ProgramInput_GraceMonthsMin";
protected const string ColumnaProgramInput_GraceMonthsMax = "ProgramInput_GraceMonthsMax";
protected const string ColumnaProgramInput_GraceMonthsInc = "ProgramInput_GraceMonthsInc";
protected const string ColumnaProgramInput_GraceMonthsVer = "ProgramInput_GraceMonthsVer";
protected const string ColumnaProgramInput_AdditionalCostsMin = "ProgramInput_AdditionalCostsMin";
protected const string ColumnaProgramInput_AdditionalCostsMax = "ProgramInput_AdditionalCostsMax";
protected const string ColumnaProgramInput_AdditionalCostsInc = "ProgramInput_AdditionalCostsInc";
protected const string ColumnaProgramInput_AdditionalCostsVer = "ProgramInput_AdditionalCostsVer";
protected const string ColumnaProgramInput_LegalFeesMin = "ProgramInput_LegalFeesMin";
protected const string ColumnaProgramInput_LegalFeesMax = "ProgramInput_LegalFeesMax";
protected const string ColumnaProgramInput_LegalFeesInc = "ProgramInput_LegalFeesInc";
protected const string ColumnaProgramInput_LegalFeesVer = "ProgramInput_LegalFeesVer";
protected const string ColumnaProgramInput_BalloonPaymentUse = "ProgramInput_BalloonPaymentUse";
protected const string ColumnaProgramInput_PurchaseOptionUse = "ProgramInput_PurchaseOptionUse";
protected const string ColumnaProgramInput_OpFeeUse = "ProgramInput_OpFeeUse";
protected const string ColumnaProgramInput_SecurityDepositUse = "ProgramInput_SecurityDepositUse";
protected const string ColumnaProgramInput_BlindDiscountUse = "ProgramInput_BlindDiscountUse";
protected const string ColumnaProgramInput_ReferalFeeUse = "ProgramInput_ReferalFeeUse";
protected const string ColumnaProgramInput_DelayUse = "ProgramInput_DelayUse";
protected const string ColumnaProgramInput_GraceMonthsUse = "ProgramInput_GraceMonthsUse";
protected const string ColumnaProgramInput_AdditionalCostsUse = "ProgramInput_AdditionalCostsUse";
protected const string ColumnaProgramInput_LegalFeesUse = "ProgramInput_LegalFeesUse";
protected const string ColumnaProgramInput_DownpaymentMin = "ProgramInput_DownpaymentMin";
protected const string ColumnaProgramInput_DownpaymentMax = "ProgramInput_DownpaymentMax";
protected const string ColumnaProgramInput_DownpaymentInc = "ProgramInput_DownpaymentInc";
protected const string ColumnaProgramInput_DownpaymentVer = "ProgramInput_DownpaymentVer";
protected const string ColumnaProgramInput_DownpaymentUse = "ProgramInput_DownpaymentUse";
protected const string ColumnaProgramInput_AdditionalCostsMXNMin = "ProgramInput_AdditionalCostsMXNMin";
protected const string ColumnaProgramInput_AdditionalCostsMXNMax = "ProgramInput_AdditionalCostsMXNMax";
protected const string ColumnaProgramInput_AdditionalCostsMXNInc = "ProgramInput_AdditionalCostsMXNInc";
protected const string ColumnaProgramInput_LegalFeesMXNMin = "ProgramInput_LegalFeesMXNMin";
protected const string ColumnaProgramInput_LegalFeesMXNMax = "ProgramInput_LegalFeesMXNMax";
protected const string ColumnaProgramInput_LegalFeesMXNInc = "ProgramInput_LegalFeesMXNInc";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Ope_ProgramInput.
        /// </summary>
        public Ope_ProgramInput()
            : base()
        {
            this.TableName = "tbl_ope_ProgramInput";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaProgramInputId, DbType.Int32, true)]
        public int? ProgramInputId { get; set; }

[DataMember, BdAction(ColumnaProgramInput_ProgramId, DbType.Int32, true, true)]
        public int? ProgramInput_ProgramId { get; set; }

[DataMember, BdAction(ColumnaProgramInput_BalloonPaymentMin, DbType.Double, true, true)]
        public double? ProgramInput_BalloonPaymentMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_BalloonPaymentMax, DbType.Double, true, true)]
        public double? ProgramInput_BalloonPaymentMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_BalloonPaymentInc, DbType.Double, true, true)]
        public double? ProgramInput_BalloonPaymentInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_BalloonPaymentVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_BalloonPaymentVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_PurchaseOptionMin, DbType.Double, true, true)]
        public double? ProgramInput_PurchaseOptionMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_PurchaseOptionMax, DbType.Double, true, true)]
        public double? ProgramInput_PurchaseOptionMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_PurchaseOptionInc, DbType.Double, true, true)]
        public double? ProgramInput_PurchaseOptionInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_PurchaseOptionVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_PurchaseOptionVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_OpFeeMin, DbType.Double, true, true)]
        public double? ProgramInput_OpFeeMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_OpFeeMax, DbType.Double, true, true)]
        public double? ProgramInput_OpFeeMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_OpFeeInc, DbType.Double, true, true)]
        public double? ProgramInput_OpFeeInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_OpFeeVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_OpFeeVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_SecurityDepositMin, DbType.Double, true, true)]
        public double? ProgramInput_SecurityDepositMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_SecurityDepositMax, DbType.Double, true, true)]
        public double? ProgramInput_SecurityDepositMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_SecurityDepositInc, DbType.Double, true, true)]
        public double? ProgramInput_SecurityDepositInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_SecurityDepositVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_SecurityDepositVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_BlindDiscountMin, DbType.Double, true, true)]
        public double? ProgramInput_BlindDiscountMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_BlindDiscountMax, DbType.Double, true, true)]
        public double? ProgramInput_BlindDiscountMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_BlindDiscountInc, DbType.Double, true, true)]
        public double? ProgramInput_BlindDiscountInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_BlindDiscountVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_BlindDiscountVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_ReferalFeeMin, DbType.Double, true, true)]
        public double? ProgramInput_ReferalFeeMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_ReferalFeeMax, DbType.Double, true, true)]
        public double? ProgramInput_ReferalFeeMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_ReferalFeeInc, DbType.Double, true, true)]
        public double? ProgramInput_ReferalFeeInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_ReferalFeeVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_ReferalFeeVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_DelayMin, DbType.Int32, true, true)]
        public int? ProgramInput_DelayMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_DelayMax, DbType.Int32, true, true)]
        public int? ProgramInput_DelayMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_DelayInc, DbType.Int32, true, true)]
        public int? ProgramInput_DelayInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_DelayVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_DelayVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_GraceMonthsMin, DbType.Int32, true, true)]
        public int? ProgramInput_GraceMonthsMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_GraceMonthsMax, DbType.Int32, true, true)]
        public int? ProgramInput_GraceMonthsMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_GraceMonthsInc, DbType.Int32, true, true)]
        public int? ProgramInput_GraceMonthsInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_GraceMonthsVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_GraceMonthsVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_AdditionalCostsMin, DbType.Double, true, true)]
        public double? ProgramInput_AdditionalCostsMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_AdditionalCostsMax, DbType.Double, true, true)]
        public double? ProgramInput_AdditionalCostsMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_AdditionalCostsInc, DbType.Double, true, true)]
        public double? ProgramInput_AdditionalCostsInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_AdditionalCostsVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_AdditionalCostsVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_LegalFeesMin, DbType.Double, true, true)]
        public double? ProgramInput_LegalFeesMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_LegalFeesMax, DbType.Double, true, true)]
        public double? ProgramInput_LegalFeesMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_LegalFeesInc, DbType.Double, true, true)]
        public double? ProgramInput_LegalFeesInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_LegalFeesVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_LegalFeesVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_BalloonPaymentUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_BalloonPaymentUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_PurchaseOptionUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_PurchaseOptionUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_OpFeeUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_OpFeeUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_SecurityDepositUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_SecurityDepositUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_BlindDiscountUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_BlindDiscountUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_ReferalFeeUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_ReferalFeeUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_DelayUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_DelayUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_GraceMonthsUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_GraceMonthsUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_AdditionalCostsUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_AdditionalCostsUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_LegalFeesUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_LegalFeesUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_DownpaymentMin, DbType.Double, true, true)]
        public double? ProgramInput_DownpaymentMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_DownpaymentMax, DbType.Double, true, true)]
        public double? ProgramInput_DownpaymentMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_DownpaymentInc, DbType.Double, true, true)]
        public double? ProgramInput_DownpaymentInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_DownpaymentVer, DbType.Boolean, true, true)]
        public bool? ProgramInput_DownpaymentVer { get; set; }

[DataMember, BdAction(ColumnaProgramInput_DownpaymentUse, DbType.Boolean, true, true)]
        public bool? ProgramInput_DownpaymentUse { get; set; }

[DataMember, BdAction(ColumnaProgramInput_AdditionalCostsMXNMin, DbType.Double, true, true)]
        public double? ProgramInput_AdditionalCostsMXNMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_AdditionalCostsMXNMax, DbType.Double, true, true)]
        public double? ProgramInput_AdditionalCostsMXNMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_AdditionalCostsMXNInc, DbType.Double, true, true)]
        public double? ProgramInput_AdditionalCostsMXNInc { get; set; }

[DataMember, BdAction(ColumnaProgramInput_LegalFeesMXNMin, DbType.Double, true, true)]
        public double? ProgramInput_LegalFeesMXNMin { get; set; }

[DataMember, BdAction(ColumnaProgramInput_LegalFeesMXNMax, DbType.Double, true, true)]
        public double? ProgramInput_LegalFeesMXNMax { get; set; }

[DataMember, BdAction(ColumnaProgramInput_LegalFeesMXNInc, DbType.Double, true, true)]
        public double? ProgramInput_LegalFeesMXNInc { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.ProgramInputId = this.ObtenerValorColumna<int?>(ColumnaProgramInputId);
this.ProgramInput_ProgramId = this.ObtenerValorColumna<int?>(ColumnaProgramInput_ProgramId);
this.ProgramInput_BalloonPaymentMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_BalloonPaymentMin);
this.ProgramInput_BalloonPaymentMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_BalloonPaymentMax);
this.ProgramInput_BalloonPaymentInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_BalloonPaymentInc);
this.ProgramInput_BalloonPaymentVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_BalloonPaymentVer);
this.ProgramInput_PurchaseOptionMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_PurchaseOptionMin);
this.ProgramInput_PurchaseOptionMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_PurchaseOptionMax);
this.ProgramInput_PurchaseOptionInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_PurchaseOptionInc);
this.ProgramInput_PurchaseOptionVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_PurchaseOptionVer);
this.ProgramInput_OpFeeMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_OpFeeMin);
this.ProgramInput_OpFeeMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_OpFeeMax);
this.ProgramInput_OpFeeInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_OpFeeInc);
this.ProgramInput_OpFeeVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_OpFeeVer);
this.ProgramInput_SecurityDepositMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_SecurityDepositMin);
this.ProgramInput_SecurityDepositMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_SecurityDepositMax);
this.ProgramInput_SecurityDepositInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_SecurityDepositInc);
this.ProgramInput_SecurityDepositVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_SecurityDepositVer);
this.ProgramInput_BlindDiscountMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_BlindDiscountMin);
this.ProgramInput_BlindDiscountMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_BlindDiscountMax);
this.ProgramInput_BlindDiscountInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_BlindDiscountInc);
this.ProgramInput_BlindDiscountVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_BlindDiscountVer);
this.ProgramInput_ReferalFeeMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_ReferalFeeMin);
this.ProgramInput_ReferalFeeMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_ReferalFeeMax);
this.ProgramInput_ReferalFeeInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_ReferalFeeInc);
this.ProgramInput_ReferalFeeVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_ReferalFeeVer);
this.ProgramInput_DelayMin = this.ObtenerValorColumna<int?>(ColumnaProgramInput_DelayMin);
this.ProgramInput_DelayMax = this.ObtenerValorColumna<int?>(ColumnaProgramInput_DelayMax);
this.ProgramInput_DelayInc = this.ObtenerValorColumna<int?>(ColumnaProgramInput_DelayInc);
this.ProgramInput_DelayVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_DelayVer);
this.ProgramInput_GraceMonthsMin = this.ObtenerValorColumna<int?>(ColumnaProgramInput_GraceMonthsMin);
this.ProgramInput_GraceMonthsMax = this.ObtenerValorColumna<int?>(ColumnaProgramInput_GraceMonthsMax);
this.ProgramInput_GraceMonthsInc = this.ObtenerValorColumna<int?>(ColumnaProgramInput_GraceMonthsInc);
this.ProgramInput_GraceMonthsVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_GraceMonthsVer);
this.ProgramInput_AdditionalCostsMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_AdditionalCostsMin);
this.ProgramInput_AdditionalCostsMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_AdditionalCostsMax);
this.ProgramInput_AdditionalCostsInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_AdditionalCostsInc);
this.ProgramInput_AdditionalCostsVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_AdditionalCostsVer);
this.ProgramInput_LegalFeesMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_LegalFeesMin);
this.ProgramInput_LegalFeesMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_LegalFeesMax);
this.ProgramInput_LegalFeesInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_LegalFeesInc);
this.ProgramInput_LegalFeesVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_LegalFeesVer);
this.ProgramInput_BalloonPaymentUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_BalloonPaymentUse);
this.ProgramInput_PurchaseOptionUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_PurchaseOptionUse);
this.ProgramInput_OpFeeUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_OpFeeUse);
this.ProgramInput_SecurityDepositUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_SecurityDepositUse);
this.ProgramInput_BlindDiscountUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_BlindDiscountUse);
this.ProgramInput_ReferalFeeUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_ReferalFeeUse);
this.ProgramInput_DelayUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_DelayUse);
this.ProgramInput_GraceMonthsUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_GraceMonthsUse);
this.ProgramInput_AdditionalCostsUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_AdditionalCostsUse);
this.ProgramInput_LegalFeesUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_LegalFeesUse);
this.ProgramInput_DownpaymentMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_DownpaymentMin);
this.ProgramInput_DownpaymentMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_DownpaymentMax);
this.ProgramInput_DownpaymentInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_DownpaymentInc);
this.ProgramInput_DownpaymentVer = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_DownpaymentVer);
this.ProgramInput_DownpaymentUse = this.ObtenerValorColumna<bool?>(ColumnaProgramInput_DownpaymentUse);
this.ProgramInput_AdditionalCostsMXNMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_AdditionalCostsMXNMin);
this.ProgramInput_AdditionalCostsMXNMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_AdditionalCostsMXNMax);
this.ProgramInput_AdditionalCostsMXNInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_AdditionalCostsMXNInc);
this.ProgramInput_LegalFeesMXNMin = this.ObtenerValorColumna<double?>(ColumnaProgramInput_LegalFeesMXNMin);
this.ProgramInput_LegalFeesMXNMax = this.ObtenerValorColumna<double?>(ColumnaProgramInput_LegalFeesMXNMax);
this.ProgramInput_LegalFeesMXNInc = this.ObtenerValorColumna<double?>(ColumnaProgramInput_LegalFeesMXNInc);
        }
        #endregion
    }
}