﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Ope_ProgramInput.
    /// </summary>
    [Serializable]
    public class Ope_ProgramInputCollection : BusinessCollection<Ope_ProgramInput>
    {
    }
}