﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Rel_ProgramFinancialType.
    /// </summary>
    [Serializable]
    public class Rel_ProgramFinancialTypeCollection : BusinessCollection<Rel_ProgramFinancialType>
    {
    }
}