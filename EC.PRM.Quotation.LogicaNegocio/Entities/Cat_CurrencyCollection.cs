﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_Currency.
    /// </summary>
    [Serializable]
    public class Cat_CurrencyCollection : BusinessCollection<Cat_Currency>
    {
    }
}