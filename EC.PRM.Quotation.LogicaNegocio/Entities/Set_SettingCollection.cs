﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Set_Setting.
    /// </summary>
    [Serializable]
    public class Set_SettingCollection : BusinessCollection<Set_Setting>
    {
    }
}