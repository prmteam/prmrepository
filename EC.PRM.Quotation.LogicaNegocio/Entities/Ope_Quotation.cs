﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Ope_Quotation.
    /// </summary>
    [DataContract, Serializable]
    public partial class Ope_Quotation : BusinessObject<Ope_Quotation, Ope_QuotationCollection>
    {
        #region Constantes Columnas
protected const string ColumnaQuotationId = "QuotationId";
protected const string ColumnaQuotation_ProgramId = "Quotation_ProgramId";
protected const string ColumnaQuotation_CustomerName = "Quotation_CustomerName";
protected const string ColumnaQuotation_CustomerContact = "Quotation_CustomerContact";
protected const string ColumnaQuotation_CustomerEmail = "Quotation_CustomerEmail";
protected const string ColumnaQuotation_CustomerLocation = "Quotation_CustomerLocation";
protected const string ColumnaQuotation_Amount = "Quotation_Amount";
protected const string ColumnaQuotation_FinancialType = "Quotation_FinancialType";
protected const string ColumnaQuotation_FinancialTerm = "Quotation_FinancialTerm";
protected const string ColumnaQuotation_Currency = "Quotation_Currency";
protected const string ColumnaQuotation_Payment = "Quotation_Payment";
protected const string ColumnaQuotation_IRR = "Quotation_IRR";
protected const string ColumnaQuotation_Date = "Quotation_Date";
protected const string ColumnaQuotation_UserId = "Quotation_UserId";
protected const string ColumnaQuotation_StatusId = "Quotation_StatusId";
protected const string ColumnaQuotation_ExchangeRate = "Quotation_ExchangeRate";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Ope_Quotation.
        /// </summary>
        public Ope_Quotation()
            : base()
        {
            this.TableName = "tbl_ope_Quotation";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaQuotationId, DbType.Int32, true)]
        public int? QuotationId { get; set; }

[DataMember, BdAction(ColumnaQuotation_ProgramId, DbType.Int32, true, true)]
        public int? Quotation_ProgramId { get; set; }

[DataMember, BdAction(ColumnaQuotation_CustomerName, DbType.AnsiString, true, true)]
        public string Quotation_CustomerName { get; set; }

[DataMember, BdAction(ColumnaQuotation_CustomerContact, DbType.AnsiString, true, true)]
        public string Quotation_CustomerContact { get; set; }

[DataMember, BdAction(ColumnaQuotation_CustomerEmail, DbType.AnsiString, true, true)]
        public string Quotation_CustomerEmail { get; set; }

[DataMember, BdAction(ColumnaQuotation_CustomerLocation, DbType.AnsiString, true, true)]
        public string Quotation_CustomerLocation { get; set; }

[DataMember, BdAction(ColumnaQuotation_Amount, DbType.Double, true, true)]
        public double? Quotation_Amount { get; set; }

[DataMember, BdAction(ColumnaQuotation_FinancialType, DbType.AnsiString, true, true)]
        public string Quotation_FinancialType { get; set; }

[DataMember, BdAction(ColumnaQuotation_FinancialTerm, DbType.AnsiString, true, true)]
        public string Quotation_FinancialTerm { get; set; }

[DataMember, BdAction(ColumnaQuotation_Currency, DbType.AnsiString, true, true)]
        public string Quotation_Currency { get; set; }

[DataMember, BdAction(ColumnaQuotation_Payment, DbType.Double, true, true)]
        public double? Quotation_Payment { get; set; }

[DataMember, BdAction(ColumnaQuotation_IRR, DbType.Double, true, true)]
        public double? Quotation_IRR { get; set; }

[DataMember, BdAction(ColumnaQuotation_Date, DbType.DateTime, true, true)]
        public DateTime? Quotation_Date { get; set; }

[DataMember, BdAction(ColumnaQuotation_UserId, DbType.Int32, true, true)]
        public int? Quotation_UserId { get; set; }

[DataMember, BdAction(ColumnaQuotation_StatusId, DbType.Int32, true, true)]
        public int? Quotation_StatusId { get; set; }

[DataMember, BdAction(ColumnaQuotation_ExchangeRate, DbType.Double, true, true)]
        public double? Quotation_ExchangeRate { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.QuotationId = this.ObtenerValorColumna<int?>(ColumnaQuotationId);
this.Quotation_ProgramId = this.ObtenerValorColumna<int?>(ColumnaQuotation_ProgramId);
this.Quotation_CustomerName = this.ObtenerValorColumna<string>(ColumnaQuotation_CustomerName);
this.Quotation_CustomerContact = this.ObtenerValorColumna<string>(ColumnaQuotation_CustomerContact);
this.Quotation_CustomerEmail = this.ObtenerValorColumna<string>(ColumnaQuotation_CustomerEmail);
this.Quotation_CustomerLocation = this.ObtenerValorColumna<string>(ColumnaQuotation_CustomerLocation);
this.Quotation_Amount = this.ObtenerValorColumna<double?>(ColumnaQuotation_Amount);
this.Quotation_FinancialType = this.ObtenerValorColumna<string>(ColumnaQuotation_FinancialType);
this.Quotation_FinancialTerm = this.ObtenerValorColumna<string>(ColumnaQuotation_FinancialTerm);
this.Quotation_Currency = this.ObtenerValorColumna<string>(ColumnaQuotation_Currency);
this.Quotation_Payment = this.ObtenerValorColumna<double?>(ColumnaQuotation_Payment);
this.Quotation_IRR = this.ObtenerValorColumna<double?>(ColumnaQuotation_IRR);
this.Quotation_Date = this.ObtenerValorColumna<DateTime?>(ColumnaQuotation_Date);
this.Quotation_UserId = this.ObtenerValorColumna<int?>(ColumnaQuotation_UserId);
this.Quotation_StatusId = this.ObtenerValorColumna<int?>(ColumnaQuotation_StatusId);
this.Quotation_ExchangeRate = this.ObtenerValorColumna<double?>(ColumnaQuotation_ExchangeRate);
        }
        #endregion
    }
}