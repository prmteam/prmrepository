﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_Currency.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_Currency : BusinessObject<Cat_Currency, Cat_CurrencyCollection>
    {
        #region Constantes Columnas
protected const string ColumnaCurrencyId = "CurrencyId";
protected const string ColumnaCurrency_Code = "Currency_Code";
protected const string ColumnaCurrency_Symbol = "Currency_Symbol";
protected const string ColumnaCurrency_Description = "Currency_Description";
protected const string ColumnaCurrency_Active = "Currency_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_Currency.
        /// </summary>
        public Cat_Currency()
            : base()
        {
            this.TableName = "tbl_cat_Currency";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaCurrencyId, DbType.Int32, true)]
        public int? CurrencyId { get; set; }

[DataMember, BdAction(ColumnaCurrency_Code, DbType.AnsiString, true, true)]
        public string Currency_Code { get; set; }

[DataMember, BdAction(ColumnaCurrency_Symbol, DbType.AnsiStringFixedLength, true, true)]
        public string Currency_Symbol { get; set; }

[DataMember, BdAction(ColumnaCurrency_Description, DbType.AnsiString, true, true)]
        public string Currency_Description { get; set; }

[DataMember, BdAction(ColumnaCurrency_Active, DbType.Boolean, true, true)]
        public bool? Currency_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.CurrencyId = this.ObtenerValorColumna<int?>(ColumnaCurrencyId);
this.Currency_Code = this.ObtenerValorColumna<string>(ColumnaCurrency_Code);
this.Currency_Symbol = this.ObtenerValorColumna<string>(ColumnaCurrency_Symbol);
this.Currency_Description = this.ObtenerValorColumna<string>(ColumnaCurrency_Description);
this.Currency_Active = this.ObtenerValorColumna<bool?>(ColumnaCurrency_Active);
        }
        #endregion
    }
}