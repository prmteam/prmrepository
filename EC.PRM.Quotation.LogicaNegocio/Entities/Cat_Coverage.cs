﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_Coverage.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_Coverage : BusinessObject<Cat_Coverage, Cat_CoverageCollection>
    {
        #region Constantes Columnas
protected const string ColumnaCoverageId = "CoverageId";
protected const string ColumnaCoverage_Description = "Coverage_Description";
protected const string ColumnaCoverage_Cost = "Coverage_Cost";
protected const string ColumnaCoverage_Factor = "Coverage_Factor";
protected const string ColumnaCoverage_Active = "Coverage_Active";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_Coverage.
        /// </summary>
        public Cat_Coverage()
            : base()
        {
            this.TableName = "tbl_cat_Coverage";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaCoverageId, DbType.Int32, true)]
        public int? CoverageId { get; set; }

[DataMember, BdAction(ColumnaCoverage_Description, DbType.AnsiString, true, true)]
        public string Coverage_Description { get; set; }

[DataMember, BdAction(ColumnaCoverage_Cost, DbType.Double, true, true)]
        public double? Coverage_Cost { get; set; }

[DataMember, BdAction(ColumnaCoverage_Factor, DbType.Double, true, true)]
        public double? Coverage_Factor { get; set; }

[DataMember, BdAction(ColumnaCoverage_Active, DbType.Boolean, true, true)]
        public bool? Coverage_Active { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.CoverageId = this.ObtenerValorColumna<int?>(ColumnaCoverageId);
this.Coverage_Description = this.ObtenerValorColumna<string>(ColumnaCoverage_Description);
this.Coverage_Cost = this.ObtenerValorColumna<double?>(ColumnaCoverage_Cost);
this.Coverage_Factor = this.ObtenerValorColumna<double?>(ColumnaCoverage_Factor);
this.Coverage_Active = this.ObtenerValorColumna<bool?>(ColumnaCoverage_Active);
        }
        #endregion
    }
}