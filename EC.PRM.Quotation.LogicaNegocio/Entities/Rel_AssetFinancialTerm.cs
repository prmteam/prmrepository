﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Rel_AssetFinancialTerm.
    /// </summary>
    [DataContract, Serializable]
    public partial class Rel_AssetFinancialTerm : BusinessObject<Rel_AssetFinancialTerm, Rel_AssetFinancialTermCollection>
    {
        #region Constantes Columnas
protected const string ColumnaAssetFinancialTermId = "AssetFinancialTermId";
protected const string ColumnaAssetFinancialTerm_AssetId = "AssetFinancialTerm_AssetId";
protected const string ColumnaAssetFinancialTerm_FinancialTermId = "AssetFinancialTerm_FinancialTermId";
protected const string ColumnaAssetFinancialTerm_ResidualValue = "AssetFinancialTerm_ResidualValue";
protected const string ColumnaAssetFinancialTerm_FMV = "AssetFinancialTerm_FMV";
protected const string ColumnaAssetFinancialTerm_TermType = "AssetFinancialTerm_TermType";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Rel_AssetFinancialTerm.
        /// </summary>
        public Rel_AssetFinancialTerm()
            : base()
        {
            this.TableName = "tbl_rel_AssetFinancialTerm";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaAssetFinancialTermId, DbType.Int32, true)]
        public int? AssetFinancialTermId { get; set; }

[DataMember, BdAction(ColumnaAssetFinancialTerm_AssetId, DbType.Int32, true, true)]
        public int? AssetFinancialTerm_AssetId { get; set; }

[DataMember, BdAction(ColumnaAssetFinancialTerm_FinancialTermId, DbType.Int32, true, true)]
        public int? AssetFinancialTerm_FinancialTermId { get; set; }

[DataMember, BdAction(ColumnaAssetFinancialTerm_ResidualValue, DbType.Double, true, true)]
        public double? AssetFinancialTerm_ResidualValue { get; set; }

[DataMember, BdAction(ColumnaAssetFinancialTerm_FMV, DbType.Double, true, true)]
        public double? AssetFinancialTerm_FMV { get; set; }

[DataMember, BdAction(ColumnaAssetFinancialTerm_TermType, DbType.AnsiString, true, true)]
        public string AssetFinancialTerm_TermType { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.AssetFinancialTermId = this.ObtenerValorColumna<int?>(ColumnaAssetFinancialTermId);
this.AssetFinancialTerm_AssetId = this.ObtenerValorColumna<int?>(ColumnaAssetFinancialTerm_AssetId);
this.AssetFinancialTerm_FinancialTermId = this.ObtenerValorColumna<int?>(ColumnaAssetFinancialTerm_FinancialTermId);
this.AssetFinancialTerm_ResidualValue = this.ObtenerValorColumna<double?>(ColumnaAssetFinancialTerm_ResidualValue);
this.AssetFinancialTerm_FMV = this.ObtenerValorColumna<double?>(ColumnaAssetFinancialTerm_FMV);
this.AssetFinancialTerm_TermType = this.ObtenerValorColumna<string>(ColumnaAssetFinancialTerm_TermType);
        }
        #endregion
    }
}