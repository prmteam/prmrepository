﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Rel_ProgramAssetService.
    /// </summary>
    [Serializable]
    public class Rel_ProgramAssetServiceCollection : BusinessCollection<Rel_ProgramAssetService>
    {
    }
}