﻿namespace EC.PRM.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Rel_ProgramFinancialTerm.
    /// </summary>
    [Serializable]
    public class Rel_ProgramFinancialTermCollection : BusinessCollection<Rel_ProgramFinancialTerm>
    {
    }
}